import { Request, RestBindings, get, ResponseObject, Response } from '@loopback/rest';
import { inject } from '@loopback/context';
import { DBVersion } from '../models/DBVersion';
const apm = require('elastic-apm-node');

/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          greeting: { type: 'string' },
          date: { type: 'string' },
          url: { type: 'string' },
          headers: {
            type: 'object',
            properties: {
              'Content-Type': { type: 'string' },
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

const DB_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          result: { type: 'boolean' },
          status_code: { type: 'number' },
          status_message: { type: 'string' },
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class PingController {
  dbVersionModel: DBVersion;

  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @inject(RestBindings.Http.RESPONSE) private response: Response,
  ) {
    this.dbVersionModel = new DBVersion();
  }

  // Map to `GET /ping`
  @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  ping(): object {
    // Reply with a greeting, the current time, the url, and request headers
    apm.setTransactionName('GET /ping');
    return {
      greeting: 'Hello from LoopBack',
      date: new Date(),
      url: this.req.url,
      headers: Object.assign({}, this.req.headers),
    };
  }

  @get('/checkAppStatus', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  async checkAppStatus() {
    apm.setTransactionName('GET /checkAppStatus');

    let response: any = {
      result: 'S',
      statusCode: 200,
      statusMessage: 'Connection success.',
      data: {}
    }
    try {
      let res: any = await this.dbVersionModel.healthCheck();
      console.log('getDbVersion response : ', res)

      this.response.status(response.statusCode).send(response);

    } catch (error) {
      console.log('error : ', error);
      let err: any = error;
      apm.captureError(new Error(err));

      response.result = 'E';
      response.statusCode = 503;
      response.statusMessage = 'Connection unsuccess.';
      response.data = error;

      this.response.status(response.statusCode).send(response);

    }
  }

  // Connect to DB
  @get('/getDBVersion', {
    responses: {
      '200': DB_RESPONSE,
    },
  })
  async getDBVersion() {

    let res: any = await this.dbVersionModel.getVersion();

    let result = false;
    let status_code = 501;
    let status_message = 'Error to get DBVersion in DB';
    let db_vers = '';

    if (res.hasOwnProperty('db_version')) {
      result = true;
      status_code = 200;
      status_message = 'Success';
      db_vers = res.db_version;
    }

    return {
      result: result,
      statusCode: status_code,
      statusMessage: status_message,
      data: db_vers
    }
  }

}
