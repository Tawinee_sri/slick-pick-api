import {
  requestBody,
  post,
} from '@loopback/rest';
import { LogUtil } from '../models/LogUtil';
import { PickShipmentRequest } from '../models/picking/pick-shipment/pick-shipment-request.model';
var Constants = require('../models/picking/picking.schema');
const apm = require('elastic-apm-node');

const RES_SUCCESS = '200,Success';

export class PickShipmentRequestController {

  constructor() { }

  @post('/checkPickShipmentUpdateRequest', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Check Data what server receive and prepare to write into DB",
    'x-controller-name': 'Picking Shipment Service',
  })
  async checkPickShipmentUpdateRequest(
    @requestBody({
      content: { 'application/json': { schema: Constants.SHIPMENT_UPDATE_REQUEST } }
    }) _reqBody: any,
  ) {

    try {
      apm.setTransactionName('POST /checkPickShipmentUpdateRequest');

      let pickShipmentModel = new PickShipmentRequest();
      pickShipmentModel.setData(_reqBody);


      let msg = RES_SUCCESS.split(',');
      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: pickShipmentModel.getCurrentData()
      };

    } catch (e) {
      let error: any = e;
      let err = error.message.split(',');

      apm.captureError(new Error(error));
      return {
        result: 'E',
        statusCode: parseInt(err[0]),
        statusMessage: err[1],
        data: {}
      };

    }
  }

  @post('/setPickShipmentUpdateRequest', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Check Data what server receive and prepare to write into DB",
    'x-controller-name': 'Picking Shipment Service',
  })
  async setPickShipmentUpdateRequest(
    @requestBody({
      content: { 'application/json': { schema: Constants.SHIPMENT_UPDATE_REQUEST } }
    }) _reqBody: any,
  ) {

    try {
      apm.setTransactionName('POST /setPickShipmentUpdateRequest');

      let pickShipmentModel = new PickShipmentRequest();
      pickShipmentModel.setData(_reqBody);

      LogUtil.logError('post', '/setPickShipmentUpdateRequest', _reqBody, 'resuestBody');

      return await pickShipmentModel.save().then(res => {

        let msg = RES_SUCCESS.split(',');
        return {
          result: 'S',
          statusCode: parseInt(msg[0]),
          statusMessage: msg[1],
          data: {
            success_set_request: res,
            fail_set_request: 0,
          }
        };

      }).catch((err) => {
        apm.captureError(new Error(err));

        return {
          result: 'E',
          statusCode: 500,
          statusMessage: err.message,
          data: {
            success_set_request: 0,
            fail_set_request: 1,
          }
        };
      });


    } catch (e) {
      let error: any = e;
      console.error('error : ', error);
      let err = error.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(err[0]),
        statusMessage: err[1],
        data: {}
      };

    }
  }

}
