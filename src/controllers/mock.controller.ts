import {
  get,
  param,
  post,
  requestBody
} from '@loopback/rest';
import { LogUtil } from '../models/LogUtil';
import { Mock } from '../models/mock/mock.model';
var Constants = require('../models/picking/picking.schema');
const RES_SUCCESS = '200,Success';

export class MockController {
  constructor() {
  }

  @get('/getuser/{bu}/{loc}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: { type: 'number', example: 999 },
                bu: { type: 'string', example: 'CDS' },
                loc: { type: 'string', example: '10116' },
                status: { type: 'string', example: 'A' },
                userList: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: {
                      userId: { type: 'string', example: '00001' },
                      cat_01: { type: 'string', example: 'A' },
                      cat_02: { type: 'string', example: 'A' },
                      cat_03: { type: 'string', example: 'A' },
                      cat_04: { type: 'string', example: 'A' },
                      cat_05: { type: 'string', example: 'A' },
                      zone: { type: 'string', example: '' }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "description": "Retrieve mock user management",
    'x-controller-name': 'Picking Service',
  })
  async getMockUser(
    @param.path.string('bu') _bu: string,
    @param.path.string('loc') _loc: string
  ) {
    let mock = new Mock();
    return mock.getMockUser(_bu, _loc);
  }
}
