import {
  get,
  post,
  param,
  requestBody,
  put,
  del
} from '@loopback/rest';
import { User } from '../models/user/user.model';
import { LogUtil } from '../models/LogUtil';

var Constants = require('../models/picking/picking.schema');
const apm = require('elastic-apm-node');

export class UserController {
  constructor() {
  }

  @get('/users/{bu}/{loc}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: { type: 'number', example: 999 },
                bu: { type: 'string', example: 'CDS' },
                loc: { type: 'string', example: '10116' },
                status: { type: 'string', example: 'A' },
                userList: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: {
                      userId: { type: 'string', example: '00001' },
                      cat_01: { type: 'string', example: 'A' },
                      cat_02: { type: 'string', example: 'A' },
                      cat_03: { type: 'string', example: 'A' },
                      cat_04: { type: 'string', example: 'A' },
                      cat_05: { type: 'string', example: 'A' },
                      zone: { type: 'string', example: '' }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "description": "Retrieve user management",
    'x-controller-name': 'User Service',
  })
  async getUser(
    @param.path.string('bu') _bu: string,
    @param.path.string('loc') _loc: string,
    @param.query.string('ignoreStatus') _statusIgnore: string
  ) {
    apm.setTransactionName('GET /users');

    let user = new User();
    try {
      if (_statusIgnore == null || _statusIgnore == undefined || _statusIgnore.length == 0) {
        return user.getUser(_bu, _loc);
      } else {
        return user.getUserByStatus(_bu, _loc, _statusIgnore);
      }
    } catch (err) {
      let error: any = err;
      apm.captureError(new Error(error));
      console.log('/users error : ', error);
    }
  }

  @post('/users', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Insert user",
    'x-controller-name': 'User Service',
  }) async postUser(
    @requestBody({
      content: { 'application/json': { schema: Constants.POST_USER_SCHEMA } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /users');

      let user = new User();
      let res = await user.postUser(_reqBody);
      return {
        result: 'S',
        statusCode: 200,
        statusMessage: 'Success',
        data: {
          success_post_user: res,
          fail_post_user: 0
        }
      };
    } catch (error) {
      let err: any = error;
      LogUtil.logError('post', '/user', _reqBody, err.message);
      apm.captureError(new Error(err));
      let msg = err.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          success_post_user: 0,
          fail_post_user: 1
        }
      };
    }
  }

  @put('/users', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update user",
    'x-controller-name': 'User Service',
  }) async putUser(
    @requestBody({
      content: { 'application/json': { schema: Constants.PUT_USER_SCHEMA } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('PUT /users');

      let user = new User();
      let res = await user.putUser(_reqBody);
      return {
        result: 'S',
        statusCode: 200,
        statusMessage: 'Success',
        data: {
          success_put_user: res,
          fail_put_user: 0
        }
      };
    } catch (error) {
      let err: any = error;
      apm.captureError(new Error(err));
      LogUtil.logError('post', '/user', _reqBody, err.message);
      let msg = err.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          success_put_user: 0,
          fail_put_user: 1
        }
      };
    }
  }

  @del('/users', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Delete user",
    'x-controller-name': 'User Service',
  }) async delUser(
    @requestBody({
      content: { 'application/json': { schema: Constants.DEL_USER_SCHEMA } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('DELETE /users');

      let user = new User();
      let res = await user.delUser(_reqBody);
      return {
        result: 'S',
        statusCode: 200,
        statusMessage: 'Success',
        data: {
          success_del_user: res,
          fail_del_user: 0
        }
      };
    } catch (error) {
      let err: any = error;
      apm.captureError(new Error(err));
      LogUtil.logError('post', '/user', _reqBody, err.message);
      let msg = err.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          success_del_user: 0,
          fail_del_user: 1
        }
      };
    }
  }
}
