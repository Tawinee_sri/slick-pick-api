import {
  requestBody,
  post,
} from '@loopback/rest';
import { LogUtil } from '../models/LogUtil';
import { PickingRequest } from '../models/picking/picking.model';
const apm = require('elastic-apm-node');

var Constants = require('../models/picking/picking.schema');

const RES_SUCCESS = '200,Success';
const ERR_IMPORT_PICKREQUEST = '30501,Error Import Pick Request!';

export class PickRequestController {
  constructor() {
  }

  /**
   *
   * API Code : API.PACK.01
   * API Name : setPickingRequest
   * - return row affect when save all model relate
   */
  @post('/setPickingRequest', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'], example: 'S' },
                statusCode: { type: 'number', example: 200 },
                statusMessage: { type: 'string', example: 'Success' },
                data: {
                  type: 'object',
                  properties: {
                    success_set_request: { type: 'number' },
                    fail_set_request: { type: 'number' },
                  }
                },
              },
            }
          }
        }
      },
    },
    "description": "Receive Order Data and set into Picking DB",
    'x-controller-name': "Picking Service",
  }) async setPickingRequest(
    @requestBody({
      content: { 'application/json': { schema: Constants.PICKING_SCHEMA } }
    }) _reqBody: any,
  ) {
    apm.setTransactionName('POST /setPickingRequest');
    try {

      let dataPicking = new PickingRequest();
      dataPicking.setData(_reqBody);
      LogUtil.logError('post', '/setPickingRequest', _reqBody, "");

      let res = await dataPicking.save();
      let msg = RES_SUCCESS.split(',');
      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          success_set_request: res,
          fail_set_request: 0
        }
      };
    } catch (error) {
      let err: any = error;
      apm.captureError(new Error(err));
      LogUtil.logError('post error', '/setPickingRequest', _reqBody, err.message);
      let msg = err.message.indexOf(',') > -1 ? err.message.split(',') : err.message;
      return {
        result: 'E',
        statusCode: err.message.indexOf(',') > -1 ? parseInt(msg[0]) : 400,
        statusMessage: err.message.indexOf(',') > -1 ? msg[1] : msg,
        data: {
          success_set_request: 0,
          fail_set_request: 1,
        }
      };
    }
  }

  @post('/checkPickRequest', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Check Data what server receive and prepare to write into DB",
    'x-controller-name': 'Picking Service',
  })
  async checkPickRequest(
    @requestBody({
      content: { 'application/json': { schema: Constants.PICKING_SCHEMA } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /checkPickRequest');

      let dataPicking = new PickingRequest();
      dataPicking.setData(_reqBody);

      let msg = RES_SUCCESS.split(',');
      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: dataPicking.getPickingItem()
      };
    } catch (error) {
      let err: any = error;
      apm.captureError(new Error(err));
      LogUtil.logError('post', '/checkPickRequest', _reqBody, err.message);
      let msg = err.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {

        }
      };
    }
  }

}
