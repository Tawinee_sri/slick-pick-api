import {
  requestBody,
  post,
  get,
  param,
} from '@loopback/rest';
import { ITEM_STATUS, TASK_STATUS, PICK_REQUEST_STATUS, PICK_ALLOCATION_TYPE, FM_WS, FM_AUTHORITIES, PICK_MODE, ASSIGN_TYPE, SUB_ASSIGN_TYPE, FM_UM_MAPPING, PICK_PRIORITY_TYPE, PICK_PRIORITY_LEVEL } from '../models/picking/constant';
import { LogUtil } from '../models/LogUtil';
import { PickTask } from '../models/picking/pick-task/pick-task.model';
import { Barcode } from '../models/picking/pick-task/barcode.model';
import { DBVersion } from '../models/DBVersion';


import axios from 'axios';
import { PickListModel } from './../models/picking/pick-list/picklist.model';
const apm = require('elastic-apm-node');

var Constants = require('../models/picking/picking.schema');

const RES_SUCCESS = '200,Success';
const ERR_IMPORT_PICKREQUEST = '30501,Error Import Pick Request!';
const ERR_PICK_ALLOCATE = '500,Error Allocate Pick Request';
const ERR_ASSIGN_TASK = '500,Error Assign Task';
const ERR_GET_TASK_LIST = '500,Error Get Task List';
const ERR_GET_TASK_DATA_RECEIVE = '500,Error Get Task';
const ERR_PACK_RECEIVE_STATUS = '500,Error Set Pack Receive';
const ERR_BAD_REQUEST = '400,Bad request';

export class PickTaskController {
  constructor() {
  }
  /**
   * API Name /setPickTaskAllocation
   */
  @post('/setPickTaskAllocation', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Insert data to pick_task and pick_task_detail",
    'x-controller-name': 'Picking Service',
  })
  async setPickTaskAllocation(
    @requestBody({
      content: { 'application/json': { schema: Constants.PICK_TASK_ALLOCATION } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /setPickTaskAllocation');

      let pickTask = new PickTask();

      pickTask.setAllocateData(_reqBody);
      return await pickTask.allocateTask()
        .then((res: any) => {
          let msg = RES_SUCCESS.split(',');
          return {
            result: 'S',
            statusCode: parseInt(msg[0]),
            statusMessage: msg[1],
            data: {
              pick_request_rows: res.countRequest,
              pick_task_rows: res.countPickTask,
              wave_id: res.wave_id,
              fail_set_request: 0
            }
          };
        }).catch((err: any) => {
          apm.captureError(new Error(err));
          console.log('ERROR 1 : ', err);
          throw err;
        });
    } catch (err) {
      let errormsg: any = err;
      apm.captureError(new Error(errormsg));
      LogUtil.logError('post', '/setPickTaskAllocation', _reqBody, errormsg.message);
      console.log('ERROR 2 : ', err);

      let msg = ERR_PICK_ALLOCATE.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          pick_request_rows: 0,
          pick_task_rows: 0,
          fail_set_request: 1
        }
      };
    }
  }

  /**
   * API Name /setAssignPicktask
   */
  @post('/setAssignPicktask', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Insert data to pick_task and pick_task_detail",
    'x-controller-name': 'Picking Service',
  })
  async setAssignPicktask(
    @requestBody({
      content: { 'application/json': { schema: Constants.PICK_ASSIGN_PICK_TASK } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /setAssignPicktask');

      let pickTask = new PickTask();
      pickTask.setAssignData(_reqBody);

      if (_reqBody && _reqBody.checkAppVersion == 'Y') {
        return await pickTask.checkAppVersion(_reqBody.appVersion).then(res => {
          if (res == '') {
            let msg = RES_SUCCESS.split(',');
            return {
              result: 'S',
              statusCode: parseInt(msg[0]),
              statusMessage: msg[1],
              data: {}
            };

          } else {
            let err = res;
            return {
              result: 'E',
              statusCode: parseInt(err.split(',')[0]),
              statusMessage: err.split(',')[1],
              data: {}
            };

          }
        });

      } else {
        return await pickTask.assignTask()
          .then((res: any) => {
            if (res.rowCount > 0) {
              let msg = RES_SUCCESS.split(',');
              return {
                result: 'S',
                statusCode: parseInt(msg[0]),
                statusMessage: msg[1],
                data: {
                  affectedRows: res.rowCount
                }
              };
            } else {
              return {
                result: 'E',
                statusCode: 201,
                statusMessage: 'Order is already cancelled.',
                data: {
                  affectedRows: res.rowCount
                }
              };
            }
          });
      }
    } catch (err) {
      let errormsg: any = err;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/setAssignPicktask', _reqBody, errormsg.message);
      let msg = ERR_ASSIGN_TASK.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {
          affectedRows: 0
        }
      };
    }
  }

  /**
    * API name updateItemStatus
    */
  @post('/updateItemStatus', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update item status and task",
    'x-controller-name': 'Picking Service',
  })
  async updateItemStatus(
    @requestBody({
      content: { 'application/json': { schema: Constants.UPDATE_ITEM_STATUS } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /updateItemStatus');
      let task = new PickTask();
      await task.findTaskById(_reqBody.task_id);

      if (_reqBody.isSuppApprove && _reqBody.isSuppApprove == 'Y') {
        return await task.setSuppervisorApprove(_reqBody).then((res: any) => {
          LogUtil.logError('post', '/setSuppervisorApprove', _reqBody, res);
          let msg = RES_SUCCESS.split(',');
          return {
            result: 'S',
            statusCode: parseInt(msg[0]),
            statusMessage: msg[1],
            data: res
          };
        });
      } else {
        return await task.updateItemStatus(_reqBody)
          .then((res: any) => {
            LogUtil.logError('post', '/updateItemStatus', _reqBody, res);
            let msg = RES_SUCCESS.split(',');
            return {
              result: 'S',
              statusCode: parseInt(msg[0]),
              statusMessage: msg[1],
              data: res
            };
          });
      }
    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/updateItemStatus', _reqBody, errormsg.message);
      let msg = errormsg.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  /**
        * API name sendPickResponse
        */
  @post('/sendPickResponse', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update sendPickResponse directly [URL: " + FM_WS.POST_READY_TO_PACK + "] [client_id: " + FM_AUTHORITIES.CLIENT_ID + "] [client_secret: " + FM_AUTHORITIES.CLIENT_SECRET + "]",
    'x-controller-name': 'Picking Service',
  })
  async sendPickResponse(
    @requestBody({
      content: { 'application/json': { schema: {} } }
    }) _reqBody: any,
  ) {
    apm.setTransactionName('POST /sendPickResponse');

    var config = {
      headers: {
        client_id: '743f8aacc60e4c87aa304e46bd004627',
        client_secret: '235847B0051149e2b80C8c54BC07A001',
        'content-type': 'application/json'
      }
    };

    var url = 'http://internal.uat.cgapi.central.co.th:8286/bs/slickfm/pickresp/1_0';

    var data = JSON.stringify(_reqBody);

    let result = await axios.post(url, data, config).then((res: any) => {
      return {
        send_text: 'in then res',
        data: JSON.stringify(res.data)
      };
    });
    return {
      send_url: url,
      send_data: _reqBody,
      send_header: config,
      response: JSON.stringify(result)
    };

  }


  /**
      * API name updateTaskStatus
      */
  @post('/updateTaskStatus', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update task status directly",
    'x-controller-name': 'Picking Service',
  })
  async updateTaskStatus(
    @requestBody({
      content: { 'application/json': { schema: {} } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /updateTaskStatus');
      let task = new PickTask();
      await task.findTaskById(_reqBody.task_id);

      return await task.updateTaskStatus(_reqBody)
        .then((res: any) => {
          let msg = RES_SUCCESS.split(',');
          return {
            result: 'S',
            statusCode: parseInt(msg[0]),
            statusMessage: msg[1],
            data: res
          };
        });
    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/updateTaskStatus', _reqBody, errormsg.message);
      let msg = errormsg.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  /**
    * API name getTaskDataToReceive
    */
  @post('/getTaskDataToReceive', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Get task data with items",
    'x-controller-name': 'Picking Service',
  })
  async getTaskDataToReceive(
    @requestBody({
      content: { 'application/json': { schema: Constants.GET_TASK_DATA } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /getTaskDataToReceive');
      let task = new PickTask();
      let response = await task.getTaskDataToReceive(_reqBody.task_id);

      let msg = RES_SUCCESS.split(',');
      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: response
      };
    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/getTaskDataToReceive', _reqBody, errormsg.message);
      let msg = ERR_GET_TASK_DATA_RECEIVE.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  /**
   * API name setPackReceiveItemStatus
   */
  @post('/setPackReceiveItemStatus', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update item status and task",
    'x-controller-name': 'Picking Service',
  })
  async setPackReceiveItemStatus(
    @requestBody({
      content: { 'application/json': { schema: Constants.PACK_RECEIVE_ITEM_STATUS } }
    }) _reqBody: any
  ) {
    try {
      apm.setTransactionName('POST /setPackReceiveItemStatus');

      let task = new PickTask();
      return await task.setPackReceiveItemStatus(_reqBody).then((res: any) => {
        let msg = RES_SUCCESS.split(',');
        return {
          result: 'S',
          statusCode: parseInt(msg[0]),
          statusMessage: msg[1],
          data: res
        };
      });

    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/setPackReceiveItemStatus', _reqBody, errormsg.message);
      console.log('/setPackReceiveItemStatus error : ', error);
      let msg = ERR_PACK_RECEIVE_STATUS.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  /**
  * API Name /getPickTaskList
  */
  @get('/getPickTaskList/{user_id}/{scope}/{roleid}/{bu}/{loc_id}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: {},
              }
            }
          }
        }
      },
    },
    "description": "Retrieve pick_task data",
    'x-controller-name': 'Picking Service',
  })
  async getPickTaskList(
    @param.path.string('user_id') _user_id: string,
    @param.path.string('scope') _scope: string,
    @param.path.string('roleid') _role_id: string,
    @param.path.string('bu') _bu: string,
    @param.path.string('loc_id') _loc_id: string,
    @param.query.number('task_id') task_id: number
  ) {
    apm.setTransactionName('GET /getPickTaskList');
    try {
      console.log('TIME 1 : ', new Date().toUTCString());

      let scope = (_scope != '') ? _scope : 'A';
      let pickTask = new PickTask();
      let results: any = await pickTask.getPickTaskListByScope(_user_id, scope, _role_id, _bu, _loc_id, task_id);

      // Create Response Data
      let msg = RES_SUCCESS.split(',');
      let result = {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: results
      };
      console.log('<RESULT> :::', result.data.length);

      if (scope = 'M') {
        let decline_reason: any = await pickTask.getDeclineReason(_bu);
        result['declineReason'] = decline_reason;
      }

      if (!task_id) {
        let dynamic_field_list: any = await pickTask.getDynamicFieldList(_bu, _loc_id, _role_id);
        result['field_list'] = dynamic_field_list;
      }

      console.log('TIME 2 : ', new Date().toUTCString());
      return result;
    }
    catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      console.log('TIME 2 : ', new Date().toUTCString());
      console.log('[getPickTaskList] error : ', error)
      LogUtil.logError('get', '/getPickTaskList', `{ user_id: ${_user_id}, scope: ${_scope}, roleid: ${_role_id}, bu: ${_bu}, loc_id: ${_loc_id}}`, errormsg.message);
      let msg = ERR_GET_TASK_LIST.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {

        }
      };
    }
  }

  @get('/getSKU/{user_id}/{bu}/{barcode}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: {},
              }
            }
          }
        }
      },
    },
    "description": "Get SKU by user_id, bu and barcode",
    'x-controller-name': 'Picking Service',
  })
  async getSKU(
    @param.path.string('user_id') _user_id: string,
    @param.path.string('bu') _bu: string,
    @param.path.string('barcode') _barcode: string,
  ) {
    try {
      apm.setTransactionName('GET /getSKU');

      let barcode = new Barcode();
      let res = await barcode.getSKU(_user_id, _bu, _barcode);
      // //console.log(res);
      return {
        result: 'S',
        statusCode: 200,
        statusMessage: 'Success',
        data: res
      };
    }
    catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));
      LogUtil.logError('get', '/getSKU', `{ user_id: ${_user_id}, bu: ${_bu}, barcode: ${_barcode}}`, errormsg.message);
      let msg = errormsg.message.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: {}
      };
    }
  }

  /**
   * API name setCancelItemStatus
   */
  @post('/setCancelItemStatus', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update item status to cancel",
    'x-controller-name': 'Picking Service',
  })
  async setCancelItemStatus(
    @requestBody({
      content: { 'application/json': { schema: Constants.CANCEL_ORDER_ITEM } }
    }) _reqBody: any,
  ) {
    try {
      apm.setTransactionName('POST /setCancelItemStatus');
      let task = new PickTask();
      LogUtil.logError('post', '/setCancelItemStatus', _reqBody, '');

      return await task.setCancelPickItem(_reqBody)
        .then((res: any) => {
          let msg = RES_SUCCESS.split(',');
          return {
            result: 'S',
            statusCode: parseInt(msg[0]),
            statusMessage: msg[1],
            data: res
          };
        });
    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      LogUtil.logError('post', '/setCancelItemStatus', _reqBody, errormsg.message);
      let msg = ERR_PACK_RECEIVE_STATUS.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  /**
   * API name updatePickingQueue
   * : use for trigger reponse queue
   */
  @post('/updatePickingQueue', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Update item status to cancel",
    'x-controller-name': 'Picking Service',
  })
  async updatePickingQueue() {
    apm.setTransactionName('POST /updatePickingQueue');

    try {
      console.log(new Date().toISOString() + ',' + '/updatePickingQueue,start function');
      LogUtil.logError('start run q', '/updatePickingQueue', '', '');

      let task = new PickTask();
      let limit = 1000;

      return task.batchPickResponse(limit)
        .then((res: any) => {

          let msg = RES_SUCCESS.split(',');
          return {
            result: 'S',
            statusCode: parseInt(msg[0]),
            statusMessage: msg[1],
            data: res
          };
        });

    } catch (error) {
      let errormsg: any = error;
      apm.captureError(new Error(errormsg));

      console.log(new Date().toISOString() + ',' + '/updatePickingQueue,error in function :', error);
      LogUtil.logError('error at run q', '/updatePickingQueue', '', errormsg.message);
      let msg = ERR_PACK_RECEIVE_STATUS.split(',');
      return {
        result: 'E',
        statusCode: parseInt(msg[0]),
        statusMessage: errormsg.message,
        data: {}
      }
    }
  }

  @post("/excelPicklist", {
    responses: {
      "200": {
        description: "",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                result: { type: "string", enum: ["S", "E"] },
                statusCode: { type: "number" },
                statusMessage: { type: "string" },
                data: { type: "object" },
              },
            },
          },
        },
      },
    },
    description: "",
    "x-controller-name": "Picking Service",
  })
  async excelPicklist(
    @requestBody({ content: { "application/json": { schema: Constants.PICK_LIST } }, }) _reqBody: any
  ) {
    try {
      apm.setTransactionName('POST /excelPicklist');

      let excel = new PickListModel();
      const checkMode = _reqBody.fulfillmentType.toUpperCase();
      if (checkMode === "PICKLIST" || checkMode === 'PICKLIST_WAVE') {
        return new Promise(async (resolve, reject) => {
          await excel.picklist(_reqBody).then(async (res: any) => {
            resolve(res);
          }).catch((err) => {
            apm.captureError(new Error(err));
            console.log(err);
            reject(err)
          });
        });
      }
    } catch (err) {
      let errormsg: any = err;
      console.error('Error /excelPicklist : ', err);
      apm.captureError(new Error(errormsg));
      return new Error(errormsg);
    }
  }

  /**
   * API name getMergeOrder   */

  @get('/getMergeOrder/{user_id}/{scope}/{roleid}/{bu}/{loc_id}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Get Merge Order",
    'x-controller-name': 'Picking Service',
  })
  async getMergeOrder(
    @param.path.string('user_id') _user_id: string,
    @param.path.string('scope') _scope: string,
    @param.path.string('roleid') _role_id: string,
    @param.path.string('bu') _bu: string,
    @param.path.string('loc_id') _loc_id: string,
  ) {
    apm.setTransactionName('GET /getMergeOrder');

    try {
      let msg = RES_SUCCESS.split(',');
      let pickMergeOrder = new PickTask();

      let results: any
      let result

      if (_scope === "M") {
        results = await pickMergeOrder.getMergeOrder(_bu);

        result = {
          result: 'S',
          statusCode: parseInt(msg[0]),
          statusMessage: msg[1],
          data: results
        };

        let dynamic_field_list: any = await pickMergeOrder.getDynamicFieldList(_bu, _loc_id, _role_id);
        result['field_list'] = dynamic_field_list;

        console.log('dynamic_field_list', dynamic_field_list)
      }

      console.log('TIME 2 : ', new Date().toUTCString());
      return result;



      return result

    } catch (error) {
      console.log('ERROR=======================================================', error)
      return error;

    }
  }

  /**
  * API name getMergeOrderDetail   */

  @get('/getMergeOrderDetail/{user_id}/{scope}/{roleid}/{bu}/{loc_id}/{task_id}', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "Get Merge Order Detail",
    'x-controller-name': 'Picking Service',
  })
  async getMergeOrderDetail(
    @param.path.string('user_id') _user_id: string,
    @param.path.string('scope') _scope: string,
    @param.path.string('roleid') _role_id: string,
    @param.path.string('bu') _bu: string,
    @param.path.string('loc_id') _loc_id: string,
    @param.path.string('task_id') task_id: string
  ) {
    apm.setTransactionName('GET /getMergeOrderDetail');
    try {
      let msg = RES_SUCCESS.split(',');
      let pickMergeOrderDetail = new PickTask();
      let results: any = await pickMergeOrderDetail.getMergeOrderDetail(task_id);

      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: results
      };
    } catch (error) {
      return error;

    }
  }

  /**
   * API name setMeageOrder   */

  @post('/setMeageOrder', {
    responses: {
      '200': {
        description: "",
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                result: { type: 'string', enum: ['S', 'E'] },
                statusCode: { type: 'number' },
                statusMessage: { type: 'string' },
                data: { type: 'object' },
              }
            }
          }
        }
      }
    },
    "description": "set Meage Order",
    'x-controller-name': 'Picking Service',
  })
  async setMeageOrder(
    @requestBody({
      content: { 'application/json': { schema: Constants.PICK_TASK_MERGEORDER } }
    }) _reqBody: any,
  ) {
    try {
      let pickMergeOrder = new PickTask();
      pickMergeOrder.setAllocateMeageOrderData(_reqBody);
      let msg = RES_SUCCESS.split(',');

      await pickMergeOrder.setMergeOrder(_reqBody);
      let res: any = await pickMergeOrder.getTaskIdAfterMerge(_reqBody)
      console.log('resresresres', res)
      return {
        result: 'S',
        statusCode: parseInt(msg[0]),
        statusMessage: msg[1],
        data: res
      };

    } catch (error) {
      return error
    }
  }

}
