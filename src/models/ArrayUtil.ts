export class ArrayUtil {
  static groupBy<T extends any, K extends keyof T>(array: T[], key: K): Record<T[K], T[]> {
    return array.reduce(
      (objectsByKeyValue, obj) => {
        const value = obj[key]
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj)
        return objectsByKeyValue
      },
      {} as Record<T[K], T[]>
    )
  }

  static shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }
}
