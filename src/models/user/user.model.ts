import { UtilModel } from "../UtilModel";
import { ParseParamsProvider } from "@loopback/rest";
import { LogUtil } from "../LogUtil";

export class User {
  constructor() { }

  async getUserByStatus(bu: string, loc: string, statusIgnore: string) {
    try {
      if ("Y" != statusIgnore) {
        return this.getUser(bu, loc);
      }

      let sql = `select * from ${UtilModel.SCHEMA}.users where bu = ? and loc = ?`;

      let users = await UtilModel.getInstance().sqlQuerySync(sql, [bu, loc]);
      users = users.map((x: any) => ({
        userId: x.userid,
        cat_01: x.cat_01,
        cat_02: x.cat_02,
        cat_03: x.cat_03,
        cat_04: x.cat_04,
        cat_05: x.cat_05,
        zone: x.zone,
        brand: x.brand
      }));

      return {
        id: 1,
        bu: bu,
        loc: loc,
        status: 'A',
        userList: users
      }
    } catch (err) {
      let error: any = err;
      LogUtil.logError('get', '/getUserByStatus', `Error`, error.message);
      return {};
    }
  }

  async getUser(bu: string, loc: string) {
    try {
      let sql = `select * from ${UtilModel.SCHEMA}.users where bu = ? and loc = ? and status = ?`;
      let users = await UtilModel.getInstance().sqlQuerySync(sql, [bu, loc, 'A']);
      users = users.map((x: any) => ({
        userId: x.userid,
        cat_01: x.cat_01,
        cat_02: x.cat_02,
        cat_03: x.cat_03,
        cat_04: x.cat_04,
        cat_05: x.cat_05,
        zone: x.zone,
        brand: x.brand
      }));

      return {
        id: 1,
        bu: bu,
        loc: loc,
        status: 'A',
        userList: users
      }
    } catch (err) {
      let error: any = err;
      LogUtil.logError('get', '/getUser', `Error`, error.message);
      return {};
    }
  }

  async postUser(body: any) {
    console.log('<<<<<<<<<<< postUser >>>>>>>>>>>');
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errconn, conn, done) => {
        if (errconn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errconn);
            }
            done();
          })
        }

        if (!body.hasOwnProperty('cat_06')) {
          body.cat_06 = '';
        }

        if (!body.hasOwnProperty('remark')) {
          body.remark = '';
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let sql = `select count(0) as cnt from ${UtilModel.SCHEMA}.users where userId = ?`;
            console.log('sql ===>> ', sql);
            let res: any = await UtilModel.getInstance().sqlConnQuery(conn, sql, [body.userId]);
            console.log('response ===>> ', res);
            if (res != undefined && res.length > 0 && res[0].cnt > 0) {
              throw new Error('500,Duplicated');
            }

            res = await this.insertData(conn, body);
            await UtilModel.getInstance().sqlConnCommit(conn);
            done();
            resolve(res.rowCount);
          } catch (err) {
            conn.query('ROLLBACK');
            console.error(err);
            reject(err);
          } finally {
            // conn.release();
            done();
          }
        });
      });
    });
  }

  async insertData(conn: any, body: any) {
    let cols = [
      'userId',
      'cat_01',
      'cat_02',
      'cat_03',
      'cat_04',
      'cat_05',
      'cat_06',
      'zone',
      'brand',
      'remark',
      'bu',
      'loc',
      'status',
      'created_time'
    ];
    let arr_sqldata = [
      [
        body.userId,
        body.cat_01,
        body.cat_02,
        body.cat_03,
        body.cat_04,
        body.cat_05,
        body.cat_06,
        body.zone,
        body.brand,
        body.remark,
        body.bu,
        body.loc,
        body.status,
        new Date()
      ]
    ];

    return await UtilModel.getInstance().sqlConnInsert(conn, 'users', cols, arr_sqldata);
  }

  async putUser(body: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().getConnection((errconn: any, conn: any) => {
        if (errconn) {
          reject(errconn);
        }

        conn.beginTransaction(async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let sql = `select count(0) as cnt from users where userId = ?`;
            let res: any = await UtilModel.getInstance().sqlConnQuery(conn, sql, [body.userId]);
            if (res == undefined || res.length == 0 || res[0].cnt == 0) {
              throw new Error('500,Not found');
            }

            sql = `update users set `;
            let params: any = [];

            if (body.hasOwnProperty('cat_01')) {
              sql += `cat_01 = ?, `;
              params.push(body.cat_01);
            }

            if (body.hasOwnProperty('cat_02')) {
              sql += `cat_02 = ?, `;
              params.push(body.cat_02);
            }

            if (body.hasOwnProperty('cat_03')) {
              sql += `cat_03 = ?, `;
              params.push(body.cat_03);
            }

            if (body.hasOwnProperty('cat_04')) {
              sql += `cat_04 = ?, `;
              params.push(body.cat_04);
            }

            if (body.hasOwnProperty('cat_05')) {
              sql += `cat_05 = ?, `;
              params.push(body.cat_05);
            }

            if (body.hasOwnProperty('cat_06')) {
              sql += `cat_06 = ?, `;
              params.push(body.cat_06);
            }

            if (body.hasOwnProperty('zone')) {
              sql += `zone = ?, `;
              params.push(body.zone);
            }

            if (body.hasOwnProperty('brand')) {
              sql += `brand = ?, `;
              params.push(body.brand);
            }

            if (body.hasOwnProperty('remark')) {
              sql += `remark = ?, `;
              params.push(body.remark);
            }

            if (body.hasOwnProperty('bu')) {
              sql += `bu = ?, `;
              params.push(body.bu);
            }

            if (body.hasOwnProperty('loc')) {
              sql += `loc = ?, `;
              params.push(body.loc);
            }

            if (body.hasOwnProperty('status')) {
              sql += `status = ?, `;
              params.push(body.status);
            }

            if (params.length == 0) {
              throw new Error('500,Require one update field');
            }

            sql = sql.substring(0, sql.length - 2);

            sql += ` where userId = ?`;
            params.push(body.userId);

            res = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            await UtilModel.getInstance().sqlConnCommit(conn);
            resolve(res.rowCount);
          } catch (err) {
            conn.rollback();
            console.error(err);
            reject(err);
          } finally {
            conn.release();
          }
        });
      });
    });
  }

  async delUser(body: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().getConnection((errconn: any, conn: any) => {
        if (errconn) {
          reject(errconn);
        }

        conn.beginTransaction(async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let sql = `select count(0) as cnt from users where userId = ?`;
            let res: any = await UtilModel.getInstance().sqlConnQuery(conn, sql, [body.userId]);
            if (res == undefined || res.length == 0 || res[0].cnt == 0) {
              throw new Error('500,Not found');
            }

            sql = `delete from users where userId = ?`;
            res = await UtilModel.getInstance().sqlConnQuery(conn, sql, [body.userId]);
            await UtilModel.getInstance().sqlConnCommit(conn);
            resolve(res.rowCount);
          } catch (err) {
            conn.rollback();
            console.error(err);
            reject(err);
          } finally {
            conn.release();
          }
        });
      });
    });
  }

}
