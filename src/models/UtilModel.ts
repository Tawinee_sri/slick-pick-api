import { param } from '@loopback/rest';
import { resolvePtr } from 'dns';
import fs = require('fs');

var healthCheckMysql = require('health-check-mysql');

//const express = require('express');
// const mysql = require('mysql');
const { Pool, Client } = require('pg');

export class UtilModel {
  private static instance: UtilModel;

  SCHEMA: string = 'slick_picking';

  pool: any = new Pool();
  conn_env: string;
  static SCHEMA: string = "slick_picking";

  constructor() { }

  static getInstance(): UtilModel {
    if (!UtilModel.instance) {
      UtilModel.instance = new UtilModel();
      UtilModel.instance.dbConnect();
    }

    return UtilModel.instance;
  }

  public static getPool(): any {
    return UtilModel.getInstance().pool;
  }

  dbConnect() {
    let vm = this;
    vm.conn_env = "local";

    let dataConnection = JSON.parse(
      fs.readFileSync('./src/models/mysql.connection.' + this.conn_env + '.json', 'utf-8'),
    );

    this.pool = new Pool(dataConnection);
    this.pool.on('error', (err, client) => {
      this.dbConnect();
      return;
    });

  }

  setQueryString(sql: string, params: any) {
    let queryStr = '';
    if (sql.indexOf('?') == -1) {
      return queryStr = sql;
    }

    let sqlArr = sql.split('?');
    for (let i = 0; i < params.length; i++) {
      queryStr += sqlArr[i] + '$' + (i + 1);
    }
    queryStr += sqlArr[sqlArr.length - 1];
    return queryStr;
  }

  sqlQueryInsertString(arr_data: Array<any>) {
    let strArr: any = [];

    let indx = 0;
    for (let i = 0; i < arr_data.length; i++) {
      let queryStr: any = [];
      let data = arr_data[i];
      for (let j = 0; j < data.length; j++) {
        queryStr.push('$' + (indx + 1));
        indx++;
      }
      strArr.push(`(${queryStr.join(',')})`);
    }
    console.log('[sqlQueryInsertString] strArr : ', strArr);
    return `values ${strArr.join(',')}`;
  }

  sqlInsert(table: String, arr_col: Array<String>, arr_data: Array<any>) {
    let insertSQL = `insert into slick_picking.${table} ( ${arr_col.join(',')} ) ` + this.sqlQueryInsertString(arr_data);
    console.log(' ---------- sqlInsert insertSQL => ', insertSQL);

    let data: any = [];
    for (let arr of arr_data) {
      for (let i of arr) {
        data.push(i);
      }
    }

    return new Promise((resolve, reject) => {
      this.pool.connect((errConn: any, connection: any, done: any) => {
        if (errConn) reject(errConn);

        connection.query(insertSQL, data, async (err: any, results: any) => {
          done();
          if (err) {
            console.log(' ---------- sqlInsert err => ', err);
            connection.end();
            reject(err);
          }
          // connection.end();
          resolve(results);
        });
      });
    });
  }

  async sqlQuerySync(sql: string, params: Array<any>): Promise<any> {
    return await new Promise((resolve, reject) => {
      this.pool.connect((err, conn, done) => {
        if (err) {
          console.error('[function sqlQuerySync] error : ', err);
          reject(err);
        }

        conn.query(this.setQueryString(sql, params), params, (err, res) => {
          console.log('res : ', res);
          console.log('err : ', err);
          done();
          if (err) {
            conn.end();
            reject(err);
          }
          // conn.end();
          resolve(res.rows);

        });
      });
    });
  }

  sqlQuery(sql: string, params: Array<any>): Promise<any> {
    return new Promise((resolve, reject) => {
      this.pool.connect((err, conn, done) => {
        if (err) reject(err);

        conn.query(this.setQueryString(sql, params), params, (err, res) => {
          done();
          if (err) {
            console.log(' ---------- sqlQuery err => ', err);
            conn.end();
            reject(err);
          }

          if (sql.toUpperCase().indexOf('UPDATE ') == -1) {
            resolve(res.rows);
          } else {
            resolve(res);
          }

        });
      });

    });
  }

  async sqlQuerySingleRow(sql: string, params: Array<any>): Promise<any> {
    return new Promise((resolve, reject) => {
      this.pool.connect((errConn: any, connection: any, done: any) => {
        if (errConn) reject(errConn);

        connection.query(`${this.setQueryString(sql, params)} limit 1`, params, async (err: any, results: any) => {
          done();
          if (err) {
            console.log(' ---------- sqlQuery err => ', err);
            connection.end();
            reject(err);
          }
          if (results.length == 0) reject("No data found");

          resolve(results[0]);
        });
      });
    });
  }

  sqlCount(sql: string, params: Array<any>) {
    return new Promise((resolve, reject) => {
      this.pool.connect((errConn: any, connection: any, done: any) => {
        if (errConn) reject(errConn);

        connection.query(`select count(0) as cnt from(${this.setQueryString(sql, params)}) as A`, params, async (err: any, results: any) => {
          done();
          if (err) reject(err);

          resolve(results[0]);
        });
      });
    });
  }

  async sqlConnQuery(conn: any, sql: string, params: Array<any>): Promise<any> {
    return await new Promise((resolve, reject) => {
      console.log('query : ', this.setQueryString(sql, params));
      console.log('params : ', params);
      conn.query(this.setQueryString(sql, params), params, async (err: any, results: any) => {
        if (err) {
          console.log(' ---------- sqlQuery err => ', err);
          conn.end();
          reject(err);
        }

        if (sql.toUpperCase().indexOf('UPDATE ') == -1) {
          resolve(results.rows);
        } else {
          resolve(results);
        }

      });
    });
  }

  async sqlConnInsert(conn: any, table: String, arr_col: Array<String>, arr_data: Array<any>) {
    let insertSQL = `insert into slick_picking.${table} ( ${arr_col.join(',')} ) ` + this.sqlQueryInsertString(arr_data);
    if (table !== 'alloc_process' && table !== 'error_log') insertSQL += ` RETURNING id; `;

    console.log(' ---------- sqlConnInsert insertSQL => ', insertSQL);
    let data: any = [];
    for (let arr of arr_data) {
      for (let i of arr) {
        data.push(i);
      }
    }
    //  = arr_data[0];
    return await new Promise((resolve, reject) => {
      conn.query(insertSQL, data, async (err: any, results: any) => {
        if (err) reject(err);

        resolve(results);
      });
    });
  }

  async sqlConnCommit(conn: any, done?: any) {
    return await new Promise((resolve, reject) => {

      conn.query('COMMIT', err => {
        if (err) {
          conn.query('ROLLBACK');
          reject(err);
        }
        // done();
        resolve(0);
      });

    });
  }

  async sqlCountSync(sql: string, params: Array<any>) {
    return await new Promise((resolve, reject) => {
      this.pool.getConnection((errConn: any, connection: any) => {
        if (errConn) reject(errConn);

        connection.query(`select count(0) as cnt from(${sql}) as A`, params, async (err: any, results: any) => {
          if (err) reject(err);

          connection.release();

          resolve(results[0].cnt);
        });
      });
    });
  }

  async dbHealthCheck() {
    let status = false;
    let name = this.SCHEMA;

    try {
      const result = await this.pool.query('select * from pg_stat_statements');
      if (!result) return {
        status,
        name,
        condition: {
          health: "unhealthy",
          cause: "no stats"
        }
      };

      status = true;
      return {
        status,
        name,
        condition: {
          health: "healthy"
        }
      }

    } catch (error) {
      return {
        status,
        name,
        condition: {
          health: "down",
          cause: "unable to execute queries"
        }
      }
    }
  }

}
