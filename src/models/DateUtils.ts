/**
 * Convert string to date
 * support pattern
 *  - yyyy-mm-dd hh:mm:ss (format 1)
 *  - dd/mm/yyyy hh:mm:ss (format 2)
 * param
 *  - strDate - string (format dd-mm-yyyy hh:mm:ss)
 */
let format1 = /([\d]{4}-[\d]{1,2}-[\d]{1,2}) ([\d]{1,2}:[\d]{1,2}:[\d]{1,2})/;
let format2 = /([\d]{1,2})\/([\d]{1,2})\/([\d]{4}) ([\d]{1,2}:[\d]{1,2}:[\d]{1,2})/;

const pad = (val: number, size: number) => {
  let s = String(val);
  while (s.length < (size || 2)) { s = "0" + s; }
  return s;
}

export function strToDdate(strDate: string): Date {
  let formatDate;

  if (strDate.match(format1)) {
    formatDate = strDate.replace(format1, '$1T$2');
  } else if (strDate.match(format2)) {
    formatDate = strDate.replace(format2, '$3-$2-$1T$4');
  }

  if (!formatDate) {
    throw new Error('500, Data invalid');
  }

  let dateNum = Date.parse(formatDate);
  let date = new Date(dateNum);
  if (!(date instanceof Date) || isNaN(date.getTime())) {
    throw new Error('500, Data invalid');
  }
  return date;
}

/**
 * Convert Date To String (format dd/MM/yyyy hh:mm:ss)
 * param
 *  - strDate - Date
 */
export function dateToStrDate2(date: Date): string {
  return !date ? '' : `${pad(date.getDate(), 2)}/${pad((date.getMonth() + 1), 2)}/${date.getFullYear()} ${pad(date.getHours(), 2)}:${pad(date.getMinutes(), 2)}:${pad(date.getSeconds(), 2)}`;
}

/**
 * Convert Date To String (format yyyy-MM-dd hh:mm:ss)
 * param
 *  - strDate - Date
 */
export function dateToStrDate(date: Date): string {
  if (!date || !(date instanceof Date) || isNaN(Number(date))) {
    return '';
  }
  return `${date.getFullYear()}-${pad((date.getMonth() + 1), 2)}-${pad(date.getDate(), 2)} ${pad(date.getHours(), 2)}:${pad(date.getMinutes(), 2)}:${pad(date.getSeconds(), 2)}`;
}

/**
 * Convert Date To String (format yy-MM-dd hh:mm)
 * param
 *  - strDate - Date
 */
export function dateToStrDate3(date: Date): string {
  if (!date || !(date instanceof Date) || isNaN(Number(date))) {
    return '';
  }
  return `${pad(date.getDate(), 2)}-${pad((date.getMonth() + 1), 2)}-${date.getFullYear().toString().substr(-2)} ${pad(date.getHours(), 2)}:${pad(date.getMinutes(), 2)}`;
}

