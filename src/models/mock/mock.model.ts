import { UtilModel } from "../UtilModel";
import axios from 'axios';
import { FM_AUTHORITIES, FM_WS } from "../picking/constant";
import { LogUtil } from "../LogUtil";

const ERR_USERNAME = "400,Error Require Username.";
const ERR_PASSWORD = "400,Error Require Password.";

export class Mock {
  username: string = "";
  password: string = "";

  constructor() { }

  async getMockUser(bu: string, loc: string) {
    try {
      let sql = `select * from pick_users where bu = ? and loc = ? and status = ?`;
      let users = await UtilModel.getInstance().sqlQuerySync(sql, [bu, loc, 'A']);
      users = users.map((x: any) => ({
        userId: x.userId,
        cat_01: x.cat_01,
        cat_02: x.cat_02,
        cat_03: x.cat_03,
        cat_04: x.cat_04,
        cat_05: x.cat_05,
        zone: x.zone,
        brand: x.brand
      }));

      return {
        id: 1,
        bu: bu,
        loc: loc,
        status: 'A',
        userList: users
      }
    } catch (error) {
      return {};
    }
  }

  setData(_reqBody: any) {
    if (!_reqBody.hasOwnProperty('username')) {
      throw new Error(ERR_USERNAME);
    }
    this.username = _reqBody.username;

    if (!_reqBody.hasOwnProperty('password')) {
      throw new Error(ERR_PASSWORD);
    }
    this.password = _reqBody.password;
  }

  async getUserLogin() {
    let result = '';
    let url = FM_WS.LOGIN_URL;
    let headers: any = {};
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
    headers["Accept"] = "application/json";
    headers["Content-Type"] = "application/json";
    headers["client_id"] = FM_AUTHORITIES.LOGIN_CLIENT_ID;
    headers["client_secret"] = FM_AUTHORITIES.LOGIN_CLIENT_SECRET;

    let config = {
      headers: headers
    }

    let _req = {
      username: this.username,
      password: this.password,
      bu: 'GOH'
    }

    await axios.post(url, _req, config).then(res => {
      console.log('login response : ', res);
      result = res.data;
      LogUtil.logError('post', '/loginAuthentication', JSON.stringify(result), '');
    }).catch(error => {
      console.log('Error login : ', error);
      LogUtil.logError('post', '/loginAuthentication', error, 'Login Error');
      throw new Error(error.message);
    });

    return result;
  }

  async getMockUpUserLogin() {
    if (this.password != 'Slick1234') {
      throw new Error('401,Error Wrong Username or Password.');
    }

    let users = await UtilModel.getInstance().sqlQuery('select * from slick_picking.users where LOWER(userId) = ? and status = ?', [this.username.toLowerCase(), 'A']);
    console.log('users from db : ', users);

    if (users.length == 0) {
      throw new Error('401,Error Wrong Username or Password.');
    }

    // let mockUser = JSON.parse(fs.readFileSync('../mock/user001.json', 'utf-8'));
    console.log('mockUser : ', mockUser);

    mockUser.user.firstname = users[0].userid;
    mockUser.user.bu_code = users[0].bu;
    mockUser.user.store_loc = users[0].loc;

    mockUser.policy[0].detail.push({
      topic: 'custom',
      selected: [{
        um_type: 'ID',
        um_val: '1'
      }, {
        um_type: 'userId',
        um_val: this.username
      }, {
        um_type: 'cat_01',
        um_val: users[0].cat_01
      }, {
        um_type: 'cat_02',
        um_val: users[0].cat_02
      }, {
        um_type: 'cat_03',
        um_val: users[0].cat_03
      }, {
        um_type: 'cat_04',
        um_val: users[0].cat_04
      }, {
        um_type: 'cat_05',
        um_val: users[0].cat_05
      }, {
        um_type: 'cat_06',
        um_val: users[0].cat_06
      }, {
        um_type: 'zone',
        um_val: users[0].zone
      }, {
        um_type: 'brand',
        um_val: users[0].brand
      }, {
        um_type: 'remark',
        um_val: users[0].remark
      }, {
        um_type: 'bu',
        um_val: users[0].bu
      }, {
        um_type: 'loc',
        um_val: users[0].loc
      }, {
        um_type: 'status',
        um_val: users[0].status
      }]
    })

    return mockUser;
  }

  async mappingUserLogin(mockUser: any) {

    let profile = {
      lastName: mockUser.user.lastname,
      country: "",
      city: "",
      created: null,
      mobile: "",
      language: mockUser.user.language,
      userId: this.username,
      enabled: true,
      imgUrl: "https://s3-ap-southeast-1.amazonaws.com/slick-admin-portal/20/member/profile/1541157520939ariel-lin-e1471577146851.jpg",
      zipcode: null,
      firstName: mockUser.user.firstname,
      lastNameTh: mockUser.user.lastname,
      file: null,
      imgRemoved: false,
      firstNameTh: mockUser.user.firstname,
      addressLine1: "",
      addressLine2: "",
      state: "",
      updated: null,
      email: mockUser.user.email
    }

    let modules: any = [];
    for (let m of mockUser.permission[0].role_attribute[0].module) {
      modules.push({
        clientId: "13d96c1d70dd4f3e8f77bee053d3f792",
        name: m.module_code,
        clientSecret: "972F119c91A64Ceb801deB234716bbBB",
        id: 1,
        position: m.module_position,
        subModules: []
      });
    }

    modules = this.username.toLowerCase() == 'fulfilment009' ? modules.filter(m => m.name.toLowerCase() == 'picking' || m.name.toLowerCase() == 'packing') : modules;

    let operateBU: any = [];
    operateBU.push({
      loc: [{
        formatCode: null,
        default: true,
        loc_name_th: "เอ็กซ์เทน",
        loc_code: mockUser.user.store_loc,
        loc_name_eng: "X10",
        bu_code: mockUser.user.bu_code,
        loc_status: "S"
      }],
      bu: {
        theme_color: {
          color: "Red",
          colorCode: "#ed2528"
        },
        default: true,
        bu_status: "A",
        bu_code: mockUser.user.bu_code,
        bu_name_eng: "Central Department Store",
        bu_name_th: "เซ็นทรัลดีพาร์ทเมนท์สโตร์"
      },
      roles: [{
        default: true,
        name: mockUser.permission[0].role_name,
        id: 11,
        position: this.username.toLowerCase() == 'fulfilment009' ? 60 : 10
      }],
      modules: modules
    });

    let userReturn: any;
    userReturn = {
      valid: new Date().toISOString(),
      success: true,
      member: {
        profile: profile,
        operateBU: operateBU
      },
      token: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkZ1bGZpbG1lbnQwMDIiLCJuYW1lIjoiRnVsZmlsbWVudDAwMiIsIlByaW5jaXBhbE5hbWUiOiJGdWxmaWxtZW50MDAyQGNlbnRyYWwuY28udGgiLCJpYXQiOjE1Njc1NzA5MDEsImV4cCI6MTU2NzY1NzMwMX0.w3OIcgp1DrUskBB9pLeSIrBUVi5CASZMV0JsxoMKqGo'
    };

    return userReturn;
  }
}

const mockUser = {
  "user": {
    "firstname": "Slick001",
    "lastname": "X10Test",
    "email": "slick001@gmail.com",
    "language": "TH",
    "bu_code": "CDS",
    "store_loc": "10116",
    "user_type": "employee"
  },
  "permission": [
    {
      "role_name": "Picker",
      "role_code": "11",
      "role_priority": 1,
      "role_attribute": [
        {
          "bu_code": "CDS",
          "bu_priority": 1,
          "store_loc": [
            {
              "loc_code": "10116",
              "loc_priority": 1,
              "policy_code": []
            }
          ],
          "module": [
            {
              "parent_module_code": "Slick",
              "module_name": "Dashboard",
              "module_code": "Dashboard",
              "module_position": 1,
              "is_editable": true,
              "custom_keyval": {
                "custome_data1": ""
              }
            },
            {
              "parent_module_code": "Slick",
              "module_name": "Picking",
              "module_code": "Picking",
              "module_position": 2,
              "is_editable": true,
              "custom_keyval": {
                "custome_data1": ""
              }
            },
            {
              "parent_module_code": "Slick",
              "module_name": "Packing",
              "module_code": "Packing",
              "module_position": 3,
              "is_editable": true,
              "custom_keyval": {
                "custome_data1": ""
              }
            },
            {
              "parent_module_code": "Slick",
              "module_name": "ClickAndCollect",
              "module_code": "ClickAndCollect",
              "module_position": 4,
              "is_editable": true,
              "custom_keyval": {
                "custome_data1": ""
              }
            },
            {
              "parent_module_code": "Slick",
              "module_name": "Return",
              "module_code": "Return",
              "module_position": 5,
              "is_editable": true,
              "custom_keyval": {
                "custome_data1": ""
              }
            }
          ],
          "custom_keyval": {
            "custome_key1": ""
          }
        }
      ]
    }
  ],
  "policy": [
    {
      "policy_group_code": "Slick",
      "policy_bu": "CDS",
      "policy_name": "User Management",
      "policy_code": "UM",
      "detail": [
        {
          "topic": "store_loc",
          "selected": [
            {
              "loc_code": "",
              "loc_name_th": "",
              "loc_name_en": ""
            }
          ]
        },
        {
          "topic": "category",
          "selected": [
            {
              "category_level": 1,
              "category_001": "",
              "category_type": "",
              "category_name_th": "",
              "category_name_en": ""
            }
          ]
        },
        {
          "topic": "brand",
          "selected": [
            {
              "brand_code": "",
              "brand_name_th": "",
              "brand_name_en": ""
            }
          ]
        },
        {
          "topic": "zone",
          "selected": [
            {
              "zone_code": "",
              "zone_name": ""
            }
          ]
        },
        {
          "topic": "channel",
          "selected": [
            {
              "channel_code": "",
              "channel_name": ""
            }
          ]
        },
        {
          "topic": "custom",
          "selected": [
            {
              "um_type": "ID",
              "um_val": "1"
            },
            {
              "um_type": "userId",
              "um_val": "BNOLPA001"
            },
            {
              "um_type": "cat_01",
              "um_val": "1000"
            },
            {
              "um_type": "cat_02",
              "um_val": "1000"
            },
            {
              "um_type": "cat_03",
              "um_val": "A"
            },
            {
              "um_type": "cat_04",
              "um_val": "A"
            },
            {
              "um_type": "cat_05",
              "um_val": "A"
            },
            {
              "um_type": "cat_06",
              "um_val": "A"
            },
            {
              "um_type": "zone",
              "um_val": "A"
            },
            {
              "um_type": "brand",
              "um_val": "A"
            },
            {
              "um_type": "remark",
              "um_val": ""
            },
            {
              "um_type": "bu",
              "um_val": "CDS"
            },
            {
              "um_type": "loc",
              "um_val": "10116"
            },
            {
              "um_type": "status",
              "um_val": "I"
            }
          ]
        }
      ]
    }
  ]
}
