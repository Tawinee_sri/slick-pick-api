import { utils } from "mocha";
import { UtilModel } from "../../UtilModel";
import { FM_WS, ITEM_STATUS, TASK_STATUS, FM_AUTHORITIES } from '../constant';
import axios from 'axios';

/**
 * Declare message error
 */
const ERR_barcode = '30901,Error Required Field';
const ERR_task = '30902,Error Required Field';
const ERR_user = '30903,Error Required Field';
const ERR_bu = '30904,Error Required Field';
const ERR_barcodeNotFound = '30905,Error Not Found';
const ERR_connectionFM = '30906,Network Error';
const ERR_forbiddenFM = '30907,Forbidden';
const ERR_orderKey = '30908,Error Required Field';
const ERR_FMTimeout = '30909,Timeout';
const ERR_debug_sku1 = '50001,Debug get SKU at line 129';
const ERR_debug_sku2 = '50002,Debug get SKU at line 159';

export class Barcode {

  // field
  barcode: string;
  task_id: number;
  user_id: string;
  bu: string;
  orderKey: string;

  setData(data: any) {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Cuurent Data is not an object');
    }

    // barcode
    if (!data.hasOwnProperty('barcode')) {
      throw new Error(ERR_barcode);
    }
    this.barcode = data.barcode;

    // task_id
    if (!data.hasOwnProperty('task_id')) {
      throw new Error(ERR_task);
    }
    this.task_id = data.task_id;

    // user_id
    if (!data.hasOwnProperty('user_id')) {
      throw new Error(ERR_user);
    }
    this.user_id = data.user_id;

    // bu
    if (!data.hasOwnProperty('bu')) {
      throw new Error(ERR_bu);
    }
    this.bu = data.bu;

    // orderKey
    if (!data.hasOwnProperty('orderKey')) {
      throw new Error(ERR_orderKey)
    }
    this.orderKey = data.orderKey;
  }

  async getSku(user_id: string, bu: string, barcode: string) {
    return await axios.get(
      `${FM_WS.GET_SKU}?userid=${user_id}&bu=${bu}&barcode=${barcode}`,
      {
        timeout: 1000,
        headers: {
          client_id: FM_AUTHORITIES.CLIENT_ID,
          client_secret: FM_AUTHORITIES.CLIENT_SECRET
        }
      }
    );
  }

  async scanBarcode() {
    return new Promise(async (resolve: any, reject: any) => {
      UtilModel.getPool().getConnection((errConn: any, conn: any) => {
        if (errConn) reject(errConn);

        conn.beginTransaction(async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let sqlQuery = `
              select pt.id, i.pickRequestId, i.itemKey, i.barcode, i.sku from pick_task pt
              inner join pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join pick_request pr
              on pd.pickRequestId = pr.pickRequestId
              inner join pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              where pt.id = ?
              and i.barcode = ?
              and i.status not in (?)
              and pr.orderKey = ?
              limit 1
            `;

            let dataQuery = [
              this.task_id,
              this.barcode,
              [ITEM_STATUS.DECLINED, ITEM_STATUS.PICKED],
              this.orderKey
            ];

            const items = await UtilModel.getInstance().sqlConnQuery(conn, sqlQuery, dataQuery);
            let data = items;

            /************ check sku when barcode not found ************/
            if (items.length == 0) {
              let resp: any;
              try {
                resp = await this.getSku(this.user_id, this.bu, this.barcode);
              } catch (err) {
                if (err.code && err.code == 'ECONNABORTED') {
                  throw new Error(ERR_FMTimeout);
                } else if (err.response && err.response.status == 504) {
                  throw new Error(`${err.response.status}, ${err.response.data}`);
                } else {
                  throw new Error(ERR_forbiddenFM);
                }
              }

              // TODO: need to change to correct response
              if (resp == null || resp == undefined) {
                throw new Error(ERR_connectionFM);
              }

              if (resp.data.products.length == 0) {
                throw new Error(ERR_barcodeNotFound);
              }

              let skus = resp.data.products.map((x: { sku: string; }) => x.sku);

              let skuQuery = `
                select pt.id, i.pickRequestId, i.itemKey, i.barcode, i.sku from pick_task pt
                inner join pick_task_detail pd
                on pt.id = pd.pickTaskId
                inner join pick_request pr
                on pd.pickRequestId = pr.pickRequestId
                inner join pr_items i
                on pd.pickRequestId = i.pickRequestId
                and pd.itemKey = i.itemKey
                where pt.id = ?
                and i.sku in (?)
                and i.status <> ?
                and pr.orderKey = ?
                limit 1
              `;

              let dataSku = [
                this.task_id,
                skus,
                ITEM_STATUS.PICKED,
                this.orderKey
              ];

              let debug_sql = JSON.stringify(dataSku);
              const skuItems = await UtilModel.getInstance().sqlConnQuery(conn, skuQuery, dataSku);
              if (skuItems.length == 0) {
                throw new Error(ERR_barcodeNotFound);
                //reject(new Error('50002,['+debug_sql.replace(/,/g,'-')+']'));
              }

              data = skuItems;
            }

            /************ update pick barcode to pick task detail item ************/

            sqlQuery = `
              select * from pick_task_detail pt
              inner join pick_task_detail_item pi
              on pt.ID = pi.pickTaskDetailId
              where pt.pickTaskId = ?
              and pt.pickRequestId = ?
              and pt.itemKey = ?
              and pi.itemStatus not in (?)
            `;

            dataQuery = [
              this.task_id,
              data[0].pickRequestId,
              data[0].itemKey,
              [ITEM_STATUS.DECLINED, ITEM_STATUS.PICKED]
            ];

            // assume response length >= 1 from previous query
            let dataResp = await UtilModel.getInstance().sqlConnQuery(conn, sqlQuery, dataQuery);

            sqlQuery = `
              update pick_task_detail_item
              set pickBarcode = ?,
                  itemStatus = ?
              where id = ?
            `;

            dataQuery = [
              this.barcode,
              ITEM_STATUS.PICKED,
              dataResp[0].id
            ];

            await UtilModel.getInstance().sqlConnQuery(conn, sqlQuery, dataQuery);

            /************ update item status block ************/

            let sqlUpdate = `
              update pr_items
              set status = ?
              where pickRequestId = ?
              and itemKey = ?
            `;

            let dataUpdate = [
              dataResp.length == 1 ? ITEM_STATUS.PICKED : ITEM_STATUS.PARTIAL,
              data[0].pickRequestId,
              data[0].itemKey
            ];

            let res = await UtilModel.getInstance().sqlConnQuery(conn, sqlUpdate, dataUpdate);

            /************ check and update pick task block ************/

            let sql = `
              select distinct i.status from pick_task pt
              inner join pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join pick_request pr
              on pt.pickRequestId = pr.pickRequestId
              inner join pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              where pr.pickRequestId = ?
              and pt.id = ?
              and i.status <> ?
            `;

            let params = [data[0].pickRequestId, this.task_id, ITEM_STATUS.DECLINED];

            let results = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            let taskStatus = TASK_STATUS.PENDING;

            if (results.length == 1 && results[0].status == ITEM_STATUS.PICKED) {
              taskStatus = TASK_STATUS.PICKED;
              params = [taskStatus, this.task_id];
            } else {
              taskStatus = TASK_STATUS.PARTIAL;
              params = [taskStatus, this.task_id];
            }

            sql = `
              update pick_task set status = ? where id = ?
            `;

            await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            await UtilModel.getInstance().sqlConnCommit(conn);

            resolve(res);
          } catch (err) {
            conn.rollback();
            reject(err);
          }
        });
      });
    });
  }

  async getSKU(user_id: string, bu: string, barcode: string) {
    return new Promise(async (resolve: any, reject: any) => {
      try {
        let resp = await this.getSku(user_id, bu, barcode);
        resolve(resp.data.products);
      } catch (err) {
        reject(new Error(ERR_forbiddenFM));
      }
    })
  }

}
