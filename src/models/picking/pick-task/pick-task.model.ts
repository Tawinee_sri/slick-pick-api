import { UtilModel } from "../../UtilModel";
import { LogUtil } from '../../LogUtil';
import { ArrayUtil } from "../../ArrayUtil";
import { ITEM_STATUS, TASK_STATUS, PICK_REQUEST_STATUS, PICK_ALLOCATION_TYPE, FM_WS, FM_AUTHORITIES, PICK_MODE, ASSIGN_TYPE, SUB_ASSIGN_TYPE, FM_UM_MAPPING, PICK_PRIORITY_TYPE, PICK_PRIORITY_LEVEL, ITEM_STATUS_FM_MAPPING, PICK_REQUEST_STATUS_FM_MAPPING, PROJECT_VERSION, CHANNEL } from '../../picking/constant';
import { PickingItem } from '../picking-item/picking-item.model';
import { PickingResponse, PickResponse } from '../picking.model';
import { PickingItemResponse } from '../picking-item/picking-item-response.model';
import { dateToStrDate, dateToStrDate3 } from '../../DateUtils';
import axios from 'axios';
import { reject } from "bluebird";
import { DBVersion } from "../../DBVersion";
import { User } from "../../../models/user/user.model";
import { constants } from "os";

/**
 * Declare message error
 */
const ERR_user = '30701,Error Required Field';
const ERR_bu = '30702,Error Required Field';
const ERR_role = '30703,Error Required Field';
const ERR_task = '30704,Error Required Field';
const ERR_itemNotFound = '30705,Item Not Found';
const ERR_wrongUpdateCondition = '30706,Error Condition';
const ERR_cancelRequire = '30707,Error Cancel Require Reason';
const ERR_loc = '30708,Error Required Field';
const ERR_forbiddenUM = '30709,Error Forbidden';
const ERR_canItemNotFound = '30710,Item Not Found';
const ERR_qtyLE0 = '30711,Qty must more than 0';
const ERR_IdSkuAndItemKey = '30712,Error Require Field';
const ERR_actualWeightIs0 = '30713, actualWeight must not be 0';
const ERR_mergeOrder = '30714,Error Required Field';
const ERR_user_id = '30715,Error Required Field'

const MAX_REQ_PER_TASK = 15;

export class PickTask {

  // config
  USE_TABLE: string = 'pick_task';
  SCHEMA: string = 'slick_picking';

  // pk
  id: number;

  // link
  pickRequestId: string;
  pickAllocationType: string;

  // field
  user_id: string;
  bu: string
  role: string;
  status: string;
  loc: string;
  orderkey: string;
  receivingScan: string;
  locHandling: string;
  sourceSubOrderId: string;
  mergeOrder: String;

  // child
  pickingItems: Array<PickingItem>;

  // axios config
  config = {
    headers: {
      // client_id: FM_AUTHORITIES.CLIENT_ID,
      // client_secret: FM_AUTHORITIES.CLIENT_SECRET
      'ocp-apim-subscription-key': FM_AUTHORITIES.AZURE_KEY
    }
  }

  async postReadyToPack(body: any) {
    let response: any;
    LogUtil.logError('post', FM_WS.POST_READY_TO_PACK, JSON.stringify(body), 'request');
    await axios.post(FM_WS.POST_READY_TO_PACK, body, this.config)
      .then((res: any) => {
        // LogUtil.logError('post', FM_WS.POST_READY_TO_PACK, JSON.stringify(body), JSON.stringify(res.data));
        response = JSON.parse(res.data);
      }).catch((err: any) => {
        throw err;
      });

    if (response.statusCode != '200') {
      throw new Error(response.result);
    }

    return {
      result: true,
      data: response
    };
  }

  async batchPickResponse(limit?: number) {

    console.log(new Date().toISOString() + ',' + '/batchPickResponse,in function step,start function');
    UtilModel.getPool().connect(async (errConn: any, conn: any, done: any) => {
      if (errConn) {
        conn.query('ROLLBACK', err => {
          if (err) {
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,getPool() step,connection error reject from function' + errConn);
            reject(errConn);
          }
          done();
        })
      }

      conn.query('BEGIN', async (errTrans: any) => {
        if (errTrans) {
          console.log(new Date().toISOString() + ',' + '/batchPickResponse,conn.beginTransaction(),beginTransaction error reject from function' + errTrans);
          reject(errTrans);
        }

        try {

          console.log(new Date().toISOString() + ',' + '/batchPickResponse,start in try,start method');

          let readyToPost: any = {};

          let sql = `select * from ${this.SCHEMA + '.'}fm_response_q where 1=1 order by updated_time `;
          if (limit) {
            sql += `limit ` + limit;
          }

          let params: any = [];
          let req = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

          console.log(new Date().toISOString() + ',' + '/batchPickResponse,check length in q,length = ' + req.length);
          if (req.length < 1) {
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,check length in q,length < 1 return from function conn release after this');
            LogUtil.logError('end run q', '/updatePickingQueue', 'req.length < 1', '');
            reject('/updatePickingQueue => queue.length < 1');
          }

          for (let r of req) {
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], requestID : ' + r.pickrequestid);
            sql = `
              select
                pr.sourceOrderId, pr.sourceSubOrderId,
                pr.pickRequestId, pr.subOrderKey, pr.orderKey, pr.bu, pr.status as requestStatus, pr.statusReason as requestStatusReason, pr.created_time pickRequestDate,
                pt.created_time pickAllocationDate, pt.updated_time pickAssignDate, pt.user_id pickedBy, pt.status pickTaskStatus,
                pdi.pickBarcode, pd.created_time pickedDate, pd.pickTaskId, pdi.statusReason declineReason, pdi.itemstatus ,
                i.*
              from ${this.SCHEMA + '.'}pick_request pr
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pr.pickRequestId = pd.pickRequestId
              inner join ${this.SCHEMA + '.'}pick_task_detail_item pdi
              on pdi.pickTaskDetailId = pd.ID
              inner join ${this.SCHEMA + '.'}pick_task pt
              on pt.id = pd.pickTaskId
              inner join ${this.SCHEMA + '.'}pr_items i
              on pr.pickRequestId = i.pickRequestId
              and pd.pickItemId = i.id
              and pd.itemKey = i.itemKey
              where pr.pickRequestId = ?
            `;

            params = [r.pickrequestid];
            let respItems = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            if (respItems.length == 0) {
              // not found any item, delete from queue
              sql = `delete from ${this.SCHEMA + '.'}fm_response_q where id = ?`;
              params = [r.id];
              await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              console.log(new Date().toISOString() + ',' + '/batchPickResponse,check respItems.length delete from q');
              LogUtil.logError('delete from q : ' + r.id, '/batchPickResponse', 'delete from q', '');
              UtilModel.getInstance().sqlConnCommit(conn);
              continue;
            }

            let itemStatusReduce = [...respItems.reduce((x: any, y: any) => {
              const key = y.status;
              const item = x.get(key) || Object.assign({}, y);
              return x.set(key, item);
            }, new Map).values()];

            // break when processing status
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], check complete status (not in [' + ITEM_STATUS.DECLINED + '] and not in [' + ITEM_STATUS.READY_TO_PACK + '] ');
            let doneStatus = itemStatusReduce.filter((x: any) => x.status != ITEM_STATUS.DECLINED && x.status != ITEM_STATUS.READY_TO_PACK);
            if (doneStatus.length > 0) {
              console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], found status not complete delete it from queue and go next loop');
              sql = `delete from ${this.SCHEMA + '.'}fm_response_q where id = ?`;
              params = [r.id];
              await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              UtilModel.getInstance().sqlConnCommit(conn);
              continue;
            }

            // indentify pick request status
            let pickRequestStatus = PICK_REQUEST_STATUS.CANCELED;
            sql = `
              select count(0) as cnt from ${this.SCHEMA + '.'}pick_task_detail pd
              inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
              on pd.ID = pi.pickTaskDetailId
              where pd.pickRequestId = ?
              and pi.statusReason <> 'CANCEL_BY_FM'
            `;

            params = [r.pickrequestid];
            let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            if (cnt != undefined && cnt.length > 0 && cnt[0].cnt > 0) {
              pickRequestStatus = PICK_REQUEST_STATUS.FINISHED;
            }


            sql = `update ${this.SCHEMA + '.'}pick_request set status = ? where pickRequestId = ?`;
            params = [pickRequestStatus, r.pickrequestid];
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], update pick_request status to [' + pickRequestStatus + ']');
            LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'update pick_request to ' + pickRequestStatus);

            //console.log(`--> update task status for request: ${pickRequestStatus} <--`);
            sql = `
              update ${this.SCHEMA + '.'}pick_task pt
              set status = ?
              where exists (
                select 1 from ${this.SCHEMA + '.'}pick_task_detail pd
                where pt.ID = pd.pickTaskId
                and pd.pickRequestId = ?
              ) and pt.status = ?
              `;

            params = [TASK_STATUS.FINISHED, r.pickrequestid, TASK_STATUS.READY_TO_PACK];
            let res_pick_task = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], update pick_task status to [' + TASK_STATUS.FINISHED + '] result ', res_pick_task);
            LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'update pick_task to ' + TASK_STATUS.FINISHED);

            //console.log(`--> update item status for request: ${pickRequestStatus} <--`);
            sql = `update ${this.SCHEMA + '.'}pr_items set status = ? where pickRequestId = ?`;
            params = [ITEM_STATUS.FINISHED, r.pickrequestid];
            let res_pr_items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], update pr_items status to [' + ITEM_STATUS.FINISHED + '] result ', res_pr_items);
            LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'update pr_items to ' + ITEM_STATUS.FINISHED);

            //console.log(`--> update task item for request: ${pickRequestStatus} <--`);
            sql = `
              update ${this.SCHEMA + '.'}pick_task_detail_item
              set itemStatus = ?
              where exists (
                select 1 from ${this.SCHEMA + '.'}pick_task_detail pd
                inner join ${this.SCHEMA + '.'}pr_items p
                on pd.pickRequestId = p.pickRequestId
                and pd.itemKey = p.itemKey
                and pd.pickItemId = p.id
                where pickTaskDetailId = pd.ID
                and pd.pickRequestId = ?
                ) and itemStatus = ?
                `;

            params = [ITEM_STATUS.FINISHED, r.pickrequestid, ITEM_STATUS.READY_TO_PACK];

            let res_pick_task_detail_item = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '], update pick_task_detail_item status to [' + ITEM_STATUS.FINISHED + '] result ', res_pick_task_detail_item);
            LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'update pick_task_detail_item to ' + ITEM_STATUS.FINISHED);

            let response = new PickResponse();
            let resp = new PickingResponse();
            let now = new Date();
            // assign anything
            resp.pickRequestId = respItems[0].pickrequestid;
            resp.pickResponseDate = dateToStrDate(now);
            resp.subOrderKey = respItems[0].suborderkey;
            resp.orderKey = respItems[0].orderkey;
            resp.bu = respItems[0].bu;
            resp.status = PICK_REQUEST_STATUS_FM_MAPPING[pickRequestStatus]; // send status CANCELED when all task CANCELED
            resp.statusReason = null; // (respItems[0].status != ITEM_STATUS.DECLINED) ? null : respItems[0].statusReason;

            resp.items = new Array();
            for (let r of respItems) {
              let itemStatus = r.itemstatus;
              if (r.itemstatus == ITEM_STATUS.DECLINED) {
                sql = `
                  select pi.itemStatus, pi.statusReason
                  from ${this.SCHEMA + '.'}pick_task_detail pd
                  inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
                  on pd.ID = pi.pickTaskDetailId
                  where pd.pickRequestId = ?
                  and pd.itemKey = ?
                  group by pi.itemStatus, pi.statusReason
                `;

                params = [r.pickrequestid, r.itemkey];
                let chk = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                if (chk.length == 1 && chk[0].itemstatus == ITEM_STATUS.DECLINED && chk[0].statusreason == 'CANCEL_BY_FM') {
                  sql = `update ${this.SCHEMA + '.'}pr_items set statusReason = 'CANCEL_BY_FM' where pickRequestId = ? and itemKey = ?`;
                  params = [r.pickrequestid, r.itemkey];
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                  console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '] and item loop[' + r.itemkey + '], update pr_items for reason');
                  LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'in check update pr_items again with -CANCEL_BY_FM- reason at itemKey:' + r.itemKey);

                  itemStatus = ITEM_STATUS.CANCELED;
                }
              }

              let i = new PickingItemResponse();
              i.itemKey = r.itemkey;
              i.barcode = r.barcode;
              i.qty = 1;
              i.qtyUnit = r.qtyunit;
              i.weight = (r.weighteditem.toUpperCase() === 'Y') ? parseFloat(r.actualweight) : parseFloat(r.weight);
              i.weightUnit = r.weightunit;
              i.status = ITEM_STATUS_FM_MAPPING[itemStatus];
              i.statusReason = (r.itemstatus != ITEM_STATUS.DECLINED) ? null : r.declinereason;
              i.statusDate = dateToStrDate(now);//dateToStrDate(r.statusdate);
              i.pickRequestDate = dateToStrDate(respItems[0].pickrequestdate);
              i.pickAllocationDate = dateToStrDate(respItems[0].pickallocationdate);
              i.pickAssignDate = dateToStrDate(respItems[0].pickassigndate);
              i.pickedBy = respItems[0].pickedby;
              i.pickBarcode = r.pickbarcode;
              i.pickedDate = dateToStrDate(r.pickeddate);
              i.packReceivedBy = this.user_id;
              i.packedReceivedDate = dateToStrDate(now);
              i.locId = r.locbarcode || '';

              resp.items.push(i);
            }

            // group itemkey & status
            let pickRespItems: any = [];
            for (let item of resp.items) {
              let indx = pickRespItems.findIndex((i: any) => i.status == item.status && i.itemKey == item.itemKey && i.statusReason == item.statusReason);
              if (indx > -1) {
                pickRespItems[indx].qty += 1;
              } else {
                pickRespItems.push(item);
              }
            }
            resp.items = pickRespItems;

            response.pickResponse = resp;
            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '] , send to fm with /postReadyToPack data :' + JSON.stringify(response));

            LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', response, 'pickResponse request');
            // readyToPost = await this.postReadyToPack(response);
            readyToPost = await this.checkExtendOrder(response, respItems[0]);

            console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '] , receive from fm response data :' + JSON.stringify(readyToPost));

            if (readyToPost.result) {
              console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickRequestId + '] , fm response success');
              //console.log('--> post fm success, remove from queue, continue next request <--');
              LogUtil.logError('run q:' + r.pickrequestid, '/updatePickingQueue', 'pickRequestId = ' + r.pickrequestid, 'send pickResponse OK');
              LogUtil.logSnowFlake(conn, respItems[0].pickTaskId, TASK_STATUS.FINISHED, 'function batchPickResponse');

              sql = `delete from ${this.SCHEMA + '.'}fm_response_q where id = ?`;
              params = [r.id];
              await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '] , delete row [' + r.id + '] from queue');

              UtilModel.getInstance().sqlConnCommit(conn);
            } else {
              console.log(new Date().toISOString() + ',' + '/batchPickResponse,loop[' + r.pickrequestid + '] , fm response failed - rollback all transaction to not update and go next loop');
              //console.log('--> post fm fail, continue next request, let queue here for next batch <--');
              conn.query('ROLLBACK');
              continue;
            }
          }
          //console.log('--> commit <--');
          await UtilModel.getInstance().sqlConnCommit(conn);
        } catch (err) {
          console.log(new Date().toISOString() + ',' + '/batchPickResponse,in catch, got error in catch will be rollback and reject :', err);
          conn.query('ROLLBACK');
          reject(err);
        } finally {
          console.log(new Date().toISOString() + ',' + '/batchPickResponse,in finally, try to release connection :');
          // conn.release();
          done();
        }
      });

    });

  }

  async checkExtendOrder(response: any, respItems: any) {
    console.log('[checkExtendOrder] respItems : ', respItems);
    let sourceOrderId: string = respItems.sourceorderid;
    let sourceSubOrderId: string = respItems.sourcesuborderid;
    if (sourceOrderId.indexOf('X10') > -1 && sourceSubOrderId.indexOf('X10') > -1) {
      LogUtil.logError('post', FM_WS.POST_READY_TO_PACK, JSON.stringify(response), 'request');
      return { result: true }
    } else {
      return await this.postReadyToPack(response);
    }
  }

  async addResponseQueue(conn: any, pickRequestId: any, pickTaskId: any) {
    let sql = `insert into ${this.SCHEMA + '.'}fm_response_q(pickRequestId, pickTaskId) values(?, ?)`;
    await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickRequestId, pickTaskId]);
  }

  setAllocateData(data: any) {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Cuurent Data is not an object');
    }

    // user_id
    if (!data.hasOwnProperty('user_id')) {
      throw new Error(ERR_user);
    }
    this.user_id = data.user_id;

    // role
    if (!data.hasOwnProperty('role')) {
      throw new Error(ERR_role);
    }
    this.role = data.role;

    // bu
    if (!data.hasOwnProperty('bu')) {
      throw new Error(ERR_bu);
    }
    this.bu = data.bu;

    if (!data.hasOwnProperty('loc')) {
      throw new Error(ERR_loc);
    }
    this.loc = data.loc;
  }

  setAllocateMeageOrderData(data: any) {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Cuurent Data is not an object');
    }

    // user_id
    if (!data.hasOwnProperty('user_id')) {
      throw new Error(ERR_user);
    }
    this.user_id = data.user_id;

    // role
    if (!data.hasOwnProperty('role')) {
      throw new Error(ERR_role);
    }
    this.role = data.role;

    // bu
    if (!data.hasOwnProperty('bu')) {
      throw new Error(ERR_bu);
    }
    this.bu = data.bu;

    //loc
    if (!data.hasOwnProperty('loc')) {
      throw new Error(ERR_loc);
    }
    this.loc = data.loc;

    //mergeOrder
    if (!data.hasOwnProperty('mergeOrder')) {
      throw new Error(ERR_mergeOrder);
    }
    this.mergeOrder = data.mergeOrder;
  }

  setAssignData(data: any) {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Cuurent Data is not an object');
    }

    // user_id
    if (!data.hasOwnProperty('user_id')) {
      throw new Error(ERR_user);
    }
    this.user_id = data.user_id;

    // role
    if (!data.hasOwnProperty('role')) {
      throw new Error(ERR_role);
    }
    this.role = data.role;

    // task_id
    if (!data.hasOwnProperty('task_id')) {
      throw new Error(ERR_task);
    }
    this.id = data.task_id;
  }
  async getMockUser(bu: string, loc: string) {
    let sql = `select * from pick_users where bu = ? and loc = ? and status = ?`;
    let users = await UtilModel.getInstance().sqlQuerySync(sql, [bu, loc, 'A']);
    users = users.map((x: any) => ({
      userId: x.userId,
      cat_01: x.cat_01,
      cat_02: x.cat_02,
      cat_03: x.cat_03,
      cat_04: x.cat_04,
      cat_05: x.cat_05,
      zone: x.zone,
      brand: x.brand
    }));

    return {
      id: 1,
      bu: bu,
      loc: loc,
      status: 'A',
      userList: users
    }
  }
  async allocatePickByMode(conn: any, pickMode: string) {
    return new Promise((resolve: any, reject: any) => {
      conn.query('BEGIN', async (errTrans: any) => {
        if (errTrans) {
          reject(errTrans);
        }

        this.user_id = '';
        const sleep = (milliseconds: number) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
        //console.log(`allocation task by pick mode: ${pickMode}`);

        try {
          /********** allocate multi order task block **********/

          let sql = `select count(0) as cnt from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
          let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [this.bu, this.loc]);
          if (cnt != undefined && cnt.length > 0 && cnt[0].cnt == 0) {
            await UtilModel.getInstance().sqlConnInsert(conn, "alloc_process", ['sourceBU', 'sourceLoc', 'byOrder', 'multiOrder', 'wave'], [[this.bu, this.loc, 'N', 'N', 'N']]);
          }

          if (pickMode == PICK_MODE.WAVE) {
            let state = 'Y';
            while (state == 'Y') {
              let jobchk = `select wave from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
              let rest = await UtilModel.getInstance().sqlConnQuery(conn, jobchk, [this.bu, this.loc]);
              console.log('rest : ', rest);
              state = rest[0].wave;
              if (state == 'Y') {
                await sleep(2000);
              }
            }

            await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set wave = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);
            await UtilModel.getInstance().sqlConnCommit(conn);
          } else if (pickMode == PICK_MODE.MULTI_ORDER) {
            let state = 'Y';
            while (state == 'Y') {
              let jobchk = `select multiOrder from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
              let rest = await UtilModel.getInstance().sqlConnQuery(conn, jobchk, [this.bu, this.loc]);
              state = rest[0].wave;
              if (state == 'Y') {
                await sleep(2000);
              }
            }

            await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set multiOrder = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);
            await UtilModel.getInstance().sqlConnCommit(conn);
          }


          let countRequest = 0;
          let countPickTasks = 0;
          let pickAssignType: string;
          let pickSubAssignType: string;
          let waveId = 0;
          let multiorderId = 0;

          let wave_id = 0;

          sql = `
            select p.*, concat(p.sourceBU, '-', p.sourceLoc, '-', p.pickAllocationType, '-' , p.pickCatLvl) allocationKey from (
              select
                  pr.pickRequestId, pr.sourceBU, pr.sourceLoc,
                  pp.pickMode, pp.pickAssignType, pp.pickSubAssignType,
                  string_agg(pa.pickAllocationType::char, '|' order by pa.pickAllocationType) pickAllocationType,
                  string_agg(pa.pickAllocationTypeSeq::char, '|' order by pa.pickAllocationTypeSeq) pickAllocationTypeSeq,
                  string_agg(pa.pickCatLvl::char, '|' order by pa.pickCatLvl) pickCatLvl,
                  count(0) countAllocation
              from ${this.SCHEMA + '.'}pick_request pr
              join ${this.SCHEMA + '.'}pr_pick_params pp
              on pr.pickRequestId = pp.pickRequestId
              join ${this.SCHEMA + '.'}pr_pickparams_allocation pa
              on pr.pickRequestId = pa.pickRequestId
              where pr.status = ?
              and pp.pickMode = ?
          `;

          let params = [PICK_REQUEST_STATUS.NEW, pickMode];

          if (this.bu != null && this.bu != undefined && this.bu.length > 0) {
            sql += ' and pr.sourceBU = ? '
            params.push(this.bu);

            // update_sql += `and sourceBU = ? `;
            // update_param.push(this.bu);
          }

          if (this.loc != null && this.loc != undefined && this.loc.length > 0) {
            sql += ' and pr.sourceLoc = ? '
            params.push(this.loc);

            // update_sql += ' and sourceLoc = ? '
            // update_param.push(this.loc);
          }

          sql += `
              group by pr.pickRequestId, pr.sourceBU, pr.sourceLoc, pp.pickMode, pp.pickAssignType, pp.pickSubAssignType
            ) p
          `;
          console.log('sql : ', sql);
          console.log('params : ', params);
          let items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
          console.log('items : ', items);

          let allocateItem = ArrayUtil.groupBy(items, 'allocationkey');

          if (pickMode == PICK_MODE.WAVE) {
            sql = `select id from ${this.SCHEMA + '.'}wave_seq limit 1`;
            items = await UtilModel.getInstance().sqlConnQuery(conn, sql, []);
            waveId = items[0].id;
          } else if (pickMode == PICK_MODE.MULTI_ORDER) {
            sql = `select id from ${this.SCHEMA + '.'}multiorder_seq limit 1`;
            items = await UtilModel.getInstance().sqlConnQuery(conn, sql, []);
            multiorderId = items[0].id;
          }

          // console.log('before sleep 5 sec');
          // await sleep(5000);
          // console.log('after sleep 5 sec');

          // await UtilModel.getInstance().sqlConnQuery(conn, update_sql, update_param);

          // loop each allocation grouping
          for (let k in allocateItem) {
            // define load check array (equal load, round robin) for allocate level
            let userAssgins = new Array();
            let currentBu = allocateItem[k][0].sourcebu;
            let currentLoc = allocateItem[k][0].sourceloc;
            pickAssignType = allocateItem[k][0].pickassigntype;
            pickSubAssignType = allocateItem[k][0].picksubassigntype;

            let pickRequestIds = new Array<string>();
            for (let r of allocateItem[k]) {
              pickRequestIds.push(r.pickrequestid);
              countRequest += 1;
            }

            let queryIn: any = [];
            pickRequestIds.forEach(i => queryIn.push('?'));
            let update_sql = `
              update ${this.SCHEMA + '.'}pick_request
              set status = ?
              where status = ?
              and pickRequestId in (${queryIn.join(',')})
            `;
            params = [PICK_REQUEST_STATUS.PICK_RESERVE, PICK_REQUEST_STATUS.NEW];
            for (let p of pickRequestIds) {
              params.push(p);
            }
            await UtilModel.getInstance().sqlConnQuery(conn, update_sql, params).then(res => console.log('update pick_request result : ', res));

            //console.log('-- pickRequestIds --');
            //console.log(pickRequestIds);

            sql = `
              select pa.pickAllocationType, pa.pickCatLvl
              from ${this.SCHEMA + '.'}pr_pickparams_allocation pa
              where pa.pickRequestId = ?
              order by pa.pickAllocationTypeSeq
            `;

            params = [allocateItem[k][0].pickrequestid];
            items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            // allocation column name list
            let groupBy = new Array<string>();
            for (let i of items) {
              if (i.pickallocationtype == PICK_ALLOCATION_TYPE.NONE) {
                // handle with do nothing
              } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_CATEGORY) {
                let catCol = `cat${i.pickcatlvl}`;
                groupBy.push(catCol);
              } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_ZONE) {
                groupBy.push("zone");
              } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_BRAND) {
                groupBy.push("brand");
              } else {
                throw new Error('500,Error data not support');
              }
            }

            //console.log('-- groupBy --');
            //console.log(groupBy);

            // create only 1 task when pick allocation type == '0001'

            queryIn = [];
            pickRequestIds.forEach(i => queryIn.push('?'));

            let groupSql;
            if (groupBy.length == 0) {
              groupSql = `
                select 'g' as g
                from ${this.SCHEMA + '.'}pr_items
                where pickRequestId in (${queryIn.join(',')})
                group by g
              `
            } else {
              groupSql = `
                select ${groupBy.join(', ')}, count(distinct pickRequestId) as cntReq
                from ${this.SCHEMA + '.'}pr_items
                where pickRequestId in (${queryIn.join(',')})
                group by ${groupBy.join(', ')}
              `;
            }

            //console.log(sql);

            /********** get active user block **********/
            let activeUsers: any;
            try {
              // activeUsers = await this.getActiveUser(currentBu, currentLoc);
              let user = new User();
              // let user_list = await user.getUserByStatus(currentBu, currentLoc, 'Y');
              let user_list = await user.getUserByStatus(currentBu, currentLoc, 'N');
              activeUsers = {
                data: user_list
              }
              console.log('activeUsers ======>>>>>> ', activeUsers);
            } catch (err) {
              throw new Error(ERR_forbiddenUM);
            }

            // max request per task = 15
            // for (let i = 0; i < pickRequestIds.length; i += 15) {


            // let slicePickRequestIds = pickRequestIds.slice(i, i + 15);

            //console.log('-- slice pickRequestId --');
            //console.log(slicePickRequestIds);

            // get items group by column name of allocation
            // let groups = await UtilModel.getInstance().sqlConnQuery(conn, groupSql, [slicePickRequestIds]);
            let groups = await UtilModel.getInstance().sqlConnQuery(conn, groupSql, pickRequestIds);
            console.log('groups1 : ', groups);
            // initialize filter active user as all active user
            let filterActiveUser = activeUsers.data.userList;
            //console.log('-- active user --');
            //console.log(filterActiveUser);

            // insert task group by pick allocation (eg. cat1)
            for await (let group of groups) {

              // if req of each group more than max req per task
              // then split items of that group to multi task
              // by select distinct req of that group with value of group to get exact pickRequestId and split them
              // eg where cat1, cat2 dynamic by pick allocation
              // need to refactor if/else to call single method later to reduct duplicate of code
              console.log('group[cntReq] > MAX_REQ_PER_TASK');
              if (group['cntReq'] > MAX_REQ_PER_TASK) {
                let splitParams = pickRequestIds;
                let splitSql = `
                  select distinct pickRequestId
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (?)
                `;

                for await (let col of groupBy) {
                  splitSql += `
                    and ${col} = ?
                  `;
                  splitParams.push(group[col]);
                }

                let groupRequest = await UtilModel.getInstance().sqlConnQuery(conn, splitSql, splitParams);
                console.log('groupRequest : ', groupRequest);
                let groupPickRequestIds = new Array<string>();
                for (let r of groupRequest) {
                  groupPickRequestIds.push(r.pickrequestid);
                }

                for (let i = 0; i < groupPickRequestIds.length; i += MAX_REQ_PER_TASK) {
                  let slicePickRequestIds = groupPickRequestIds.slice(i, i + MAX_REQ_PER_TASK);

                  let [countPickTask, pickTaskId] = await this.insertPickTask(conn, '', '', currentBu, waveId, multiorderId).then((res: any) => {
                    return [res.countPickTask, res.pick_task_id];
                  });
                  await UtilModel.getInstance().sqlConnInsert(conn, "error_log", ['method', 'api_path', 'req_body', 'message'], [['post', '/setPickTaskAllocation', `pickTaskId: ${pickTaskId}`, '']]);

                  countPickTasks += countPickTask;
                  filterActiveUser = activeUsers.data.userList;

                  let insertDetail = `
                  insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
                  select ?,
                    id,
                    pickRequestId,
                    itemKey,
                    current_timestamp
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (?)
                `;

                  // use slice pick request
                  let insertDetailParam = [pickTaskId];
                  for (let p of slicePickRequestIds) {
                    insertDetailParam.push(p);
                  }

                  // append where condition when group items by ???
                  if (groupBy.length > 0) {
                    for await (let col of groupBy) {
                      insertDetail += `
                      and ${col} = ?
                    `;

                      // insert value of group by column to filter data each task
                      insertDetailParam.push(group[col]);

                      // filter user to match responsibility (allocate column)
                      filterActiveUser = filterActiveUser.filter((x: any) => "A" == x[FM_UM_MAPPING[col]] || group[col] == x[FM_UM_MAPPING[col]]);
                    }
                    //console.log(`found active user match with allocation: ${filterActiveUser.map((x: any) => x.userId)}`);
                  }

                  // reduce duplicate userId after match
                  filterActiveUser = [...filterActiveUser.reduce((x: any, y: any) => {
                    const key = y.userId;
                    const item = x.get(key) || Object.assign({}, y);
                    return x.set(key, item);
                  }, new Map).values()];
                  //console.log(`-- reduce userId --`);
                  //console.log(filterActiveUser.map((x: any) => x.userId));


                  //console.log('-- insert pick_task_detail --');
                  //console.log(insertDetail);
                  console.log("HERE insertDetail ---------- ");
                  await UtilModel.getInstance().sqlConnQuery(conn, insertDetail, insertDetailParam);

                  sql = `
                  select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
                  inner join ${this.SCHEMA + '.'}pr_items i
                  on pt.pickRequestId = i.pickRequestId
                  and pt.itemKey = i.itemKey
                  and pt.pickItemId = i.id
                  where pt.pickTaskId = ?
                `;

                  // expand qty of items into pick_task_detail_item
                  const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);

                  let arr_sqldata = new Array();
                  for (let r of results) {
                    for (let i = 0; i < r.qty; i++) {
                      arr_sqldata.push([r.id, r.status, '', new Date()]);
                    }
                  }

                  let pickTaskDetailItemCols = [
                    'pickTaskDetailId',
                    'itemStatus',
                    'statusReason',
                    'created_time'
                  ];

                  await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', pickTaskDetailItemCols, arr_sqldata);

                  /********** push assignment block **********/
                  if (pickAssignType == ASSIGN_TYPE.PULL) {
                    // multi orde is push only but need to check for skip if found pull mode
                    continue;
                  }

                  if (filterActiveUser.length == 0) {
                    // not assign when all active user not reponsibility with task
                    continue;
                  }

                  let shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);

                  if (pickSubAssignType == SUB_ASSIGN_TYPE.LEAST_LOAD) {
                    sql = `
                    select pt.user_id, count(0) as item_count from ${this.SCHEMA + '.'}pick_task pt
                    join ${this.SCHEMA + '.'}pick_task_detail pd
                    on pt.id = pd.pickTaskId
                    join ${this.SCHEMA + '.'}pick_task_detail_item pi
                    on pd.id = pi.pickTaskDetailId
                    where pt.user_id in (?)
                    and pi.itemStatus = ?
                    group by pt.user_id
                  `;

                    let users = filterActiveUser.map((x: any) => ({ userId: x.userid, count: 0 }));

                    let itemCountPerUser = await UtilModel.getInstance().sqlConnQuery(conn, sql, [users.map((x: any) => x.userId), ITEM_STATUS.NEW]);
                    for await (let u of itemCountPerUser) {
                      let i = users.findIndex((x: any) => x.userId == u.user_id);
                      if (i >= 0) {
                        users[i].count = u.item_count;
                      }
                    }

                    //console.log('-- load count --');
                    //console.log(itemCountPerUser);

                    //console.log('-- users with count --');
                    //console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });

                    //console.log('-- after reduce --');
                    //console.log(reduce);

                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);
                  } else if (pickSubAssignType == SUB_ASSIGN_TYPE.EQUAL_LOAD) {
                    let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));
                    users.sort((a, b) => a.userId.localeCompare(b.userId));

                    for (let u of userAssgins) {
                      let i = users.findIndex((x: any) => x.userId == u.userId);
                      //console.log(`found assign user at index: ${i}`);
                      if (i >= 0) {
                        users[i].count = u.count;
                      }
                    }

                    //console.log(`user with item count:-`);
                    //console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });

                    sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                    let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                    let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                    if (i < 0) {
                      userAssgins.push({
                        userId: reduce.userId,
                        // count: 1
                        count: cnt[0].cnt
                      });
                    } else {
                      // userAssgins[i].count = userAssgins[i].count + 1;
                      userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                    }

                    //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                    //console.log(userAssgins);
                    //console.log('\n');
                  } else if (pickSubAssignType == SUB_ASSIGN_TYPE.ROUND_ROBIN) {
                    let users = shuffleActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                    for (let u of userAssgins) {
                      let i = users.findIndex((x: any) => x.userId == u.userId);
                      //console.log(`found assign user at index: ${i}`);
                      if (i >= 0) {
                        users[i].count = u.count;
                      }
                    }

                    //console.log(`user with item count:-`);
                    //console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });

                    sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                    let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                    let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                    if (i < 0) {
                      userAssgins.push({
                        userId: reduce.userId,
                        // count: 1
                        count: cnt[0].cnt
                      });
                    } else {
                      // userAssgins[i].count = userAssgins[i].count + 1;
                      userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                    }

                    //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                    //console.log(userAssgins);
                    //console.log('\n');
                  }
                }
              } else {
                console.log('ELSE -------------- ');
                let [countPickTask, pickTaskId] = await this.insertPickTask(conn, '', '', currentBu, waveId, multiorderId).then((res: any) => {
                  return [res.countPickTask, res.pick_task_id];
                });
                await UtilModel.getInstance().sqlConnInsert(conn, "error_log", ['method', 'api_path', 'req_body', 'message'], [['post', '/setPickTaskAllocation', `pickTaskId: ${pickTaskId}`, '']]);

                countPickTasks += countPickTask;
                filterActiveUser = activeUsers.data.userList;

                let queryIn: any = [];
                pickRequestIds.forEach(i => queryIn.push('?'));
                let insertDetail = `
                  insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
                  select ?,
                    id,
                    pickRequestId,
                    itemKey,
                    current_timestamp
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (${queryIn.join(',')})
                `;

                // let insertDetailParam = [pickTaskId, slicePickRequestIds];
                let insertDetailParam = [pickTaskId];
                for (let p of pickRequestIds) {
                  insertDetailParam.push(p);
                }

                // append where condition when group items by ???
                if (groupBy.length > 0) {
                  for await (let col of groupBy) {
                    insertDetail += `
                      and ${col} = ?
                    `;

                    // insert value of group by column to filter data each task
                    insertDetailParam.push(group[col]);

                    // filter user to match responsibility (allocate column)
                    filterActiveUser = filterActiveUser.filter((x: any) => "A" == x[FM_UM_MAPPING[col]] || group[col] == x[FM_UM_MAPPING[col]]);
                  }
                  //console.log(`found active user match with allocation: ${filterActiveUser.map((x: any) => x.userId)}`);
                }

                // reduce duplicate userId after match
                filterActiveUser = [...filterActiveUser.reduce((x: any, y: any) => {
                  const key = y.userId;
                  const item = x.get(key) || Object.assign({}, y);
                  return x.set(key, item);
                }, new Map).values()];
                //console.log(`-- reduce userId --`);
                //console.log(filterActiveUser.map((x: any) => x.userId));


                //console.log('-- insert pick_task_detail --');
                //console.log(insertDetail);

                await UtilModel.getInstance().sqlConnQuery(conn, insertDetail, insertDetailParam);

                sql = `
                  select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
                  inner join ${this.SCHEMA + '.'}pr_items i
                  on pt.pickRequestId = i.pickRequestId
                  and pt.itemKey = i.itemKey
                  and pt.pickItemId = i.ID
                  where pt.pickTaskId = ?
                `;

                // expand qty of items into pick_task_detail_item
                const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                console.log('results : ', results)
                let arr_sqldata = new Array();
                for (let r of results) {
                  for (let i = 0; i < r.qty; i++) {
                    arr_sqldata.push([r.id, r.status, '', new Date()]);
                  }
                }

                let pickTaskDetailItemCols = [
                  'pickTaskDetailId',
                  'itemStatus',
                  'statusReason',
                  'created_time'
                ];

                console.log('pickTaskDetailItemCols : ', pickTaskDetailItemCols)
                console.log('arr_sqldata : ', arr_sqldata)
                await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', pickTaskDetailItemCols, arr_sqldata);

                /********** push assignment block **********/
                if (pickAssignType == ASSIGN_TYPE.PULL) {
                  // multi orde is push only but need to check for skip if found pull mode
                  continue;
                }

                if (filterActiveUser.length == 0) {
                  // not assign when all active user not reponsibility with task
                  continue;
                }

                let shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);

                if (pickSubAssignType == SUB_ASSIGN_TYPE.LEAST_LOAD) {
                  sql = `
                    select pt.user_id, count(0) as item_count from ${this.SCHEMA + '.'}pick_task pt
                    join ${this.SCHEMA + '.'}pick_task_detail pd
                    on pt.id = pd.pickTaskId
                    join ${this.SCHEMA + '.'}pick_task_detail_item pi
                    on pd.id = pi.pickTaskDetailId
                    where pt.user_id in (?)
                    and pi.itemStatus = ?
                    group by pt.user_id
                  `;

                  let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                  let itemCountPerUser = await UtilModel.getInstance().sqlConnQuery(conn, sql, [users.map((x: any) => x.userId), ITEM_STATUS.NEW]);
                  for await (let u of itemCountPerUser) {
                    let i = users.findIndex((x: any) => x.userId == u.user_id);
                    if (i >= 0) {
                      users[i].count = u.item_count;
                    }
                  }

                  //console.log('-- load count --');
                  //console.log(itemCountPerUser);

                  //console.log('-- users with count --');
                  //console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  //console.log('-- after reduce --');
                  //console.log(reduce);

                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);
                } else if (pickSubAssignType == SUB_ASSIGN_TYPE.EQUAL_LOAD) {
                  let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));
                  users.sort((a, b) => a.userId.localeCompare(b.userId));

                  for (let u of userAssgins) {
                    let i = users.findIndex((x: any) => x.userId == u.userId);
                    //console.log(`found assign user at index: ${i}`);
                    if (i >= 0) {
                      users[i].count = u.count;
                    }
                  }

                  //console.log(`user with item count:-`);
                  //console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                  let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                  let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                  if (i < 0) {
                    userAssgins.push({
                      userId: reduce.userId,
                      // count: 1
                      count: cnt[0].cnt
                    });
                  } else {
                    // userAssgins[i].count = userAssgins[i].count + 1;
                    userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                  }

                  //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                  //console.log(userAssgins);
                  //console.log('\n');
                } else if (pickSubAssignType == SUB_ASSIGN_TYPE.ROUND_ROBIN) {
                  let users = shuffleActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                  for (let u of userAssgins) {
                    let i = users.findIndex((x: any) => x.userId == u.userId);
                    //console.log(`found assign user at index: ${i}`);
                    if (i >= 0) {
                      users[i].count = u.count;
                    }
                  }

                  //console.log(`user with item count:-`);
                  //console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                  let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                  let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                  if (i < 0) {
                    userAssgins.push({
                      userId: reduce.userId,
                      // count: 1
                      count: cnt[0].cnt
                    });
                  } else {
                    // userAssgins[i].count = userAssgins[i].count + 1;
                    userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                  }

                  //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                  //console.log(userAssgins);
                  //console.log('\n');
                }

              }
            }

            queryIn = [];
            pickRequestIds.forEach(i => queryIn.push('?'));
            sql = `
              update ${this.SCHEMA + '.'}pick_request
              set status = ?
              where status = ?
              and pickRequestId in (${queryIn.join(',')})
            `;
            params = [PICK_REQUEST_STATUS.ASSIGN, PICK_REQUEST_STATUS.PICK_RESERVE];
            for (let p of pickRequestIds) {
              params.push(p);
            }
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then(res => console.log('update pick_request result : ', res));

            wave_id = waveId;
          }

          if (pickMode == PICK_MODE.WAVE) {
            waveId += 1;
            sql = `update ${this.SCHEMA + '.'}wave_seq set id = ?`;
            await UtilModel.getInstance().sqlConnQuery(conn, sql, [waveId]);
          } else if (pickMode == PICK_MODE.MULTI_ORDER) {
            multiorderId += 1;
            sql = `update ${this.SCHEMA + '.'}multiorder_seq set id = ?`;
            await UtilModel.getInstance().sqlConnQuery(conn, sql, [multiorderId]);
          }

          /********** commit block **********/

          await UtilModel.getInstance().sqlConnCommit(conn);

          resolve({
            countRequest: countRequest,
            countPickTask: countPickTasks,
            wave_id: wave_id
          });
        } catch (err) {
          //console.log('rollback');
          conn.query('ROLLBACK');
          reject(err);
        } finally {
          if (pickMode == PICK_MODE.WAVE) {
            console.log(' --------- update wave --------- ');
            await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set wave = ? where sourceBU = ? and sourceLoc = ?`, ['N', this.bu, this.loc]);
          } else if (pickMode == PICK_MODE.MULTI_ORDER) {
            console.log(' --------- update multi order --------- ');
            await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set multiOrder = ? where sourceBU = ? and sourceLoc = ?`, ['N', this.bu, this.loc]);
          }
          await UtilModel.getInstance().sqlConnCommit(conn);
          // conn.release();
          // conn.end();
        }
      });
    });
  }

  async allocatePickByOrder(conn: any) {
    return new Promise((resolve: any, reject: any) => {
      conn.query('BEGIN', async (errTrans: any) => {
        if (errTrans) {
          reject(errTrans);
        }

        const sleep = (milliseconds: number) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
        try {
          let sql = `select count(0) as cnt from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
          console.log('sql : ', sql);

          let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [this.bu, this.loc]);
          console.log('cnt : ', cnt);

          if (cnt != undefined && cnt.length > 0 && cnt[0].cnt == 0) {
            await UtilModel.getInstance().sqlConnInsert(conn, "alloc_process", ['sourceBU', 'sourceLoc', 'byOrder', 'multiOrder', 'wave'], [[this.bu, this.loc, 'N', 'N', 'N']]);
          }

          let state = 'Y';
          while (state == 'Y') {
            let jobchk = `select byOrder from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
            let rest = await UtilModel.getInstance().sqlConnQuery(conn, jobchk, [this.bu, this.loc]);
            state = rest[0].wave;
            if (state == 'Y') {
              await sleep(2000);
            }
          }

          // await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set byOrder = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);
          // await UtilModel.getInstance().sqlConnCommit(conn);
          await UtilModel.getInstance().sqlQuery(`update ${this.SCHEMA + '.'}alloc_process set byOrder = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);


          let pickAssignType: string;
          let pickSubAssignType: string;
          let filterActiveUser: any;
          let shuffleActiveUser: any;

          sql = `
            select pr.id, pr.pickRequestId, pr.sourceBU, pr.sourceLoc, pr.status, pa.pickAllocationType, pa.pickCatLvl, pp.pickAssignType, pp.pickSubAssignType
            from ${this.SCHEMA + '.'}pick_request pr
            join ${this.SCHEMA + '.'}pr_pick_params pp
            on pr.pickRequestId = pp.pickRequestId
            join ${this.SCHEMA + '.'}pr_pickparams_allocation pa
            on pr.pickRequestId = pa.pickRequestId
            where pr.status = ?
            and pp.pickMode = ?
          `;

          let params = [PICK_REQUEST_STATUS.NEW, PICK_MODE.BY_ORDER];
          if (this.pickRequestId != null && this.pickRequestId != undefined && this.pickRequestId.length > 0) {
            sql += ' and pr.pickRequestId = ? '
            params.push(this.pickRequestId);
          }

          if (this.bu != null && this.bu != undefined && this.bu.length > 0) {
            sql += ' and pr.sourceBU = ? '
            params.push(this.bu);
          }

          if (this.loc != null && this.loc != undefined && this.loc.length > 0) {
            sql += ' and pr.sourceLoc = ? '
            params.push(this.loc);
          }

          sql += 'order by pr.pickRequestId, pr.status, pa.pickAllocationTypeSeq';

          let countRequest = 0;
          let countPickTask = 0;
          let currentRequest = '';
          let currentBu = '';
          let currentLoc = '';

          let items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

          // console.log('before sleep 5 sec');
          // await sleep(5000);
          // console.log('after sleep 5 sec');

          pickSubAssignType = '';
          pickAssignType = '';

          let groupBy = new Array<string>();
          for await (let item of items) {
            //console.log(`-- currentRequest: ${currentRequest}, pickRequest: ${item.pickrequestid}`);
            if (currentRequest == '') {
              currentRequest = item.pickrequestid;
              currentBu = item.sourcebu;
              currentLoc = item.sourceloc;
              countRequest += 1;
            }

            /********** get active user block **********/
            let activeUsers: any;
            try {
              // activeUsers = await this.getActiveUser(currentBu, currentLoc);
              // console.log('activeUsers ======>>>>>> ', activeUsers);
              let user = new User();
              let user_list = await user.getUser(currentBu, currentLoc);
              activeUsers = {
                data: user_list
              }
              console.log('activeUsers ======>>>>>> ', activeUsers);

            } catch (err) {
              console.log('ERROR 4 : ', err);

              throw new Error(ERR_forbiddenUM);
            }

            // initialize filter active user as all active user
            filterActiveUser = activeUsers.data.userList;
            shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);

            // new pick request
            if (currentRequest != item.pickrequestid) {
              //console.log('-- request changed');
              if (groupBy.length == 0) {
                //console.log(`-- insert task detail no group with assign type: ${pickAssignType}`);
                await this.insertTaskDetailNoGroup(conn, currentRequest, currentBu, pickAssignType, pickSubAssignType, filterActiveUser, shuffleActiveUser).then((res: any) => countPickTask += res);
              } else {
                //console.log(`-- insert task detail combine group with assign type: ${pickAssignType}`);
                await this.insertTaskDetailWithCombineGrouping(conn, groupBy, currentRequest, currentBu, pickAssignType, pickSubAssignType, groupBy, filterActiveUser, shuffleActiveUser).then((res: any) => countPickTask += res);
              }

              sql = `
                update ${this.SCHEMA + '.'}pick_request
                set status = ?
                where status = ?
                and pickRequestId = ?
              `;
              await UtilModel.getInstance().sqlConnQuery(conn, sql, [PICK_REQUEST_STATUS.ASSIGN, PICK_REQUEST_STATUS.NEW, currentRequest]);

              currentRequest = item.pickrequestid;
              countRequest += 1;
              groupBy = new Array();
            }

            // delay set pick assign type for run in next loop
            pickAssignType = item.pickassigntype;
            pickSubAssignType = item.picksubassigntype;

            if (item.pickallocationtype == PICK_ALLOCATION_TYPE.NONE) {
              // handle with do nothing
            } else if (item.pickallocationtype == PICK_ALLOCATION_TYPE.BY_CATEGORY) {
              let catCol = `cat${item.pickcatlvl}`;
              groupBy.push(catCol);
            } else if (item.pickallocationtype == PICK_ALLOCATION_TYPE.BY_ZONE) {
              groupBy.push("zone");
            } else if (item.pickallocationtype == PICK_ALLOCATION_TYPE.BY_BRAND) {
              groupBy.push("brand");
            } else {
              // handle with do nothing
            }
          }

          //console.log('-- check request after loop');
          if (currentRequest) {
            if (groupBy.length == 0) {
              //console.log(`-- insert task detail no group with assign type: ${pickAssignType}`);
              await this.insertTaskDetailNoGroup(conn, currentRequest, currentBu, pickAssignType, pickSubAssignType, filterActiveUser, shuffleActiveUser).then((res: any) => countPickTask += res);
            } else {
              //console.log(`-- insert task detail combine group with assign type: ${pickAssignType}`);
              await this.insertTaskDetailWithCombineGrouping(conn, groupBy, currentRequest, currentBu, pickAssignType, pickSubAssignType, groupBy, filterActiveUser, shuffleActiveUser).then((res: any) => countPickTask += res);
            }

            sql = `
              update ${this.SCHEMA + '.'}pick_request
              set status = ?
              where status = ?
              and pickRequestId = ?
            `;
            await UtilModel.getInstance().sqlConnQuery(conn, sql, [PICK_REQUEST_STATUS.ASSIGN, PICK_REQUEST_STATUS.NEW, currentRequest]);

          }

          resolve({
            countRequest: countRequest,
            countPickTask: countPickTask
          });
        } catch (err) {
          console.log('catch error : ', err);
          reject(err);
        } finally {
          await UtilModel.getInstance().sqlQuery(`update ${this.SCHEMA + '.'}alloc_process set byOrder = ? where sourceBU = ? and sourceLoc = ?`, ['N', this.bu, this.loc]);
        }
      });
    });
  }

  async allocateTask() {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect(async (errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }
        try {
          let countRequest = 0;
          let countPickTask = 0;

          let wave_id = 0;
          await this.allocatePickByOrder(conn).then((x: any) => {
            countRequest += x.countRequest;
            countPickTask += x.countPickTask;
          }).catch(err => { console.log('ERROR 5 : ', err) });
          await this.allocatePickByMode(conn, PICK_MODE.MULTI_ORDER).then((x: any) => {
            countRequest += x.countRequest;
            countPickTask += x.countPickTask;
            wave_id = x.wave_id;
          }).catch((err: any) => {
            console.log('ERROR 6 : ', err);
            throw err;
          });
          await this.allocatePickByMode(conn, PICK_MODE.WAVE).then((x: any) => {
            countRequest += x.countRequest;
            countPickTask += x.countPickTask;
            wave_id = x.wave_id;
          }).catch((err: any) => {
            console.log('ERROR 7 : ', err);
            throw err;
          });

          resolve({
            countRequest: countRequest,
            countPickTask: countPickTask,
            wave_id: wave_id
          });
        } catch (err) {
          //console.log(err);
          console.log('ERROR 3 : ', err);
          reject(err);
        } finally {
          done();
        }
      });
    });
  }

  /**
   * Assign task to user for pick mode
   * 1. By Order
   * 2. Multi Order (implement later)
   * 3. Wave (implement later)
   */
  async assignTask() {
    return new Promise((resolve: any, reject: any) => {
      let sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, role = ?, updated_time = ? where id = ? and status = ? `;
      let setStatus = (this.user_id == '' && this.role == '') ? TASK_STATUS.NEW : TASK_STATUS.PENDING;
      let arr_data = [
        setStatus,
        this.user_id,
        this.role,
        new Date(),
        this.id,
        setStatus == TASK_STATUS.NEW ? TASK_STATUS.PENDING : TASK_STATUS.NEW
      ];

      resolve(UtilModel.getInstance().sqlQuery(sql, arr_data));
    });
  }

  /**
   * Insert Piack task and return pick task id
   * @param conn - connection
   * @param pickRequestId
   * @param pickAllocationType
   */
  async insertPickTask(conn: any, pickRequestId: string, pickAllocationType: string, bu: string, waveId: any, multiorderId: any) {
    let cols = ['pickRequestId', 'pickAllocationType', 'wave_id', 'multiorder_id', 'user_id', 'bu', 'role', 'status', 'created_time'];
    let arr_data = [
      [
        pickRequestId,
        pickAllocationType,
        waveId,
        multiorderId,
        '',
        bu,
        this.role,
        TASK_STATUS.NEW,
        new Date()
      ]
    ];

    let affectedRows = 0;
    let currId = 0;
    await UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_data).then((res: any) => {
      console.log('insert res : ', res)
      currId = res.rows[0].id;
    });
    console.log('currId : ', currId);

    return await UtilModel.getInstance().sqlConnQuery(conn, `select max(id) id from ${this.SCHEMA + '.'}${this.USE_TABLE} `, [])
      // return await UtilModel.getInstance().sqlConnQuery(conn, ` SELECT currval(pg_get_serial_sequence('pick_task','id')); `, [])
      .then((res: any) => {
        console.log('select pick_task max id res : ', res)
        if (!res[0].id) {
          return Promise.reject('500,Cannot get data');
        }

        return Promise.resolve({
          countPickTask: affectedRows,
          // pick_task_id: res[0].id
          pick_task_id: currId
        });
      })
  }

  /**
   * Insert Task Detail with combine grouping
   * @param conn
   * @param item
   */
  async insertTaskDetailWithCombineGrouping(
    conn: any,
    cols: Array<string>,
    pickRequestId: string,
    bu: string,
    pickAssignType: string,
    pickSubAssignType: string,
    groupBy: any,
    filterActiveUser: any,
    shuffleActiveUser: any
  ) {
    let countPickTasks = 0;
    let sql = `
      select ${cols.join(', ')}
      from ${this.SCHEMA + '.'}pr_items
      where pickRequestId = ?
      group by ${cols.join(', ')}
    `;

    let activeUsers = filterActiveUser;
    let groups = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickRequestId]);
    let userAssigns = Array();
    for await (let group of groups) {
      filterActiveUser = activeUsers;
      //console.log(`-- insert pick task for ${pickRequestId}`);
      let [countPickTask, pickTaskId] = await this.insertPickTask(conn, pickRequestId, '', bu, 0, 0)
        .then((res: any) => {
          return [res.countPickTask, res.pick_task_id];
        });
      countPickTasks += countPickTask;

      let insertDetail = `
        insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
        select ?,
          id,
          pickRequestId,
          itemKey,
          current_timestamp
        from ${this.SCHEMA + '.'}pr_items
        where pickRequestId = ?
      `;

      let insertDetailParam = [pickTaskId, pickRequestId];

      for await (let col of cols) {
        insertDetail += `
          and ${col} = ?
        `;

        insertDetailParam.push(group[col]);
      }

      //console.log(`group data by: ${groupBy}`);

      // append where condition when group items by ???
      for await (let col of groupBy) {
        // filter user to match responsibility (allocate column)
        filterActiveUser = filterActiveUser.filter((x: any) => "A" == x[FM_UM_MAPPING[col]] || group[col] == x[FM_UM_MAPPING[col]]);
      }
      //console.log(`found active user match with allocation: ${filterActiveUser.map((x: any) => x.userId)}`);

      // reduce duplicate userId after match
      filterActiveUser = [...filterActiveUser.reduce((x: any, y: any) => {
        const key = y.userid;
        const item = x.get(key) || Object.assign({}, y);
        return x.set(key, item);
      }, new Map).values()];
      //console.log(`-- reduce userId --`);
      //console.log(filterActiveUser.map((x: any) => x.userId));

      await UtilModel.getInstance().sqlConnQuery(conn, insertDetail, insertDetailParam).catch(err => reject(err));

      sql = `
        select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
        inner join ${this.SCHEMA + '.'}pr_items i
        on pt.pickRequestId = i.pickRequestId
        and pt.itemKey = i.itemKey
        and pt.pickitemid = i.id
        where pt.pickTaskId = ?
      `;

      const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);

      let arr_sqldata = new Array();
      for (let r of results) {
        for (let i = 0; i < r.qty; i++) {
          // arr_sqldata.push([r.ID, ITEM_STATUS.NEW, '', new Date()]);
          let pickTaskDetailItemCols = [
            'pickTaskDetailId',
            'itemStatus',
            'statusReason',
            'created_time'
          ];

          await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', pickTaskDetailItemCols, [[r.id, ITEM_STATUS.NEW, '', new Date()]]).catch(err => reject(err));
        }
      }


      if (pickAssignType == ASSIGN_TYPE.PUSH) {
        shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);
        userAssigns = await this.pushAssign(conn, pickSubAssignType, pickTaskId, userAssigns, filterActiveUser, shuffleActiveUser);
      }

      LogUtil.logSnowFlake(conn, pickTaskId, TASK_STATUS.NEW, 'function insertTaskDetailWithCombineGrouping');
    }

    return Promise.resolve(countPickTasks);
  }

  /**
   * Insert Task Detail by allocation status = '0001'
   * Without Grouping
   * @param conn
   * @param item
   */
  async insertTaskDetailNoGroup(
    conn: any,
    pickRequestId: string,
    bu: string,
    pickAssignType: string,
    pickSubAssignType: string,
    filterActiveUser: any,
    shuffleActiveUser: any
  ) {
    //console.log('insertTaskDetailNoGroup');
    let [countPickTask, pickTaskId] = await this.insertPickTask(conn, pickRequestId, '', bu, 0, 0)
      .then((res: any) => {
        return [res.countPickTask, res.pick_task_id];
      });

    let sql = `
      insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
      select ?,
        ID,
        pickRequestId,
        itemKey,
        current_timestamp
      from ${this.SCHEMA + '.'}pr_items
      where pickRequestId = ?
    `;

    await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId, pickRequestId]).catch(err => reject(err));

    sql = `
      select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
      inner join ${this.SCHEMA + '.'}pr_items i
      on pt.pickRequestId = i.pickRequestId
      and pt.itemKey = i.itemKey
      and pt.pickItemId = i.ID
      where pt.pickTaskId = ?
    `;

    const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
    console.log('pick_task_detail results : ', results);
    let arr_sqldata = new Array();
    for (let r of results) {
      for (let i = 0; i < r.qty; i++) {
        // arr_sqldata.push([r.id, ITEM_STATUS.NEW, '', new Date()]);

        let cols = [
          'pickTaskDetailId',
          'itemStatus',
          'statusReason',
          'created_time'
        ];

        // await UtilModel.getInstance().sq lConnInsert(conn, 'pick_task_detail_item', cols, arr_sqldata).catch(err => reject(err));
        await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', cols, [[r.id, ITEM_STATUS.NEW, '', new Date()]]).catch(err => reject(err));
      }
    }


    if (pickAssignType == ASSIGN_TYPE.PUSH) {
      await this.pushAssign(conn, pickSubAssignType, pickTaskId, [], filterActiveUser, shuffleActiveUser);
    }

    LogUtil.logSnowFlake(conn, pickTaskId, TASK_STATUS.NEW, 'function insertTaskDetailNoGroup');
    return Promise.resolve(countPickTask);
  }

  async findTaskById(taskId: number) {
    let sql = `select * from ${this.SCHEMA + '.'}${this.USE_TABLE} where id = ?`;
    let results = await UtilModel.getInstance().sqlQuerySync(sql, [taskId]);
    if (!results || results.length == 0) {
      throw new Error("500,No data found");
    }

    this.id = results[0].id;
    this.pickRequestId = results[0].pickrequestid;
    this.pickAllocationType = results[0].pickallocationtype;
    this.user_id = results[0].user_id;
    this.bu = results[0].bu;
    this.role = results[0].role;
    this.status = results[0].status;
  }

  async getTaskData(taskId: number, findRelated: boolean) {
    return new Promise(async (resolve: any, reject: any) => {
      try {
        let taskSql = `
          select
            pt.status, pt.ID, pt.wave_id, pt.multiorder_id, pt.bu, pt.role, pt.user_id, pt.pickAllocationType,
            pd.pickRequestId, pr.sourceSubOrderId, pr.orderKey,
            pi.packReceiveDate packReceiveTime, locBarcode locBarcode,
            pr.orderKey, pr.sourceOrderId, pr.sourceBU, pr.sourceLoc, pm.pickMode,
            pm.receivingScan, pm.locHandling, prm.message , prm.messagetype
          from ${this.SCHEMA + '.'}pick_task pt
          inner join ${this.SCHEMA + '.'}pick_task_detail pd
          on pt.id = pd.pickTaskId
          inner join (
            select pickTaskDetailId, locBarcode, packReceiveDate
            from ${this.SCHEMA + '.'}pick_task_detail_item p1
          ) pi
          on pd.id = pi.pickTaskDetailId
          left join ${this.SCHEMA + '.'}pick_request pr
          on pd.pickRequestId = pr.pickRequestId
          left join ${this.SCHEMA + '.'}pr_pick_params pm
          on pd.pickRequestId = pm.pickRequestId
          left join ${this.SCHEMA + '.'}pr_remark prm
          on prm.pickrequestid = pr.pickrequestid
          and prm.messagetype = '0007'
          where pt.id = ?
        `;
        // let tasks = await UtilModel.getInstance().sqlQuerySync(taskSql, [taskId, taskId]);
        console.log(`[getTaskData] (taskId=${taskId}) taskSql : `, taskSql);
        let tasksList = await UtilModel.getInstance().sqlQuerySync(taskSql, [taskId]);
        if (tasksList.length == 0) {
          throw new Error('500,No data found');
        }
        //console.log('taskdata check out -----');
        //console.log('status of task: ', tasks[0], 'status of compare: ', TASK_STATUS.PENDING);

        let tasks: any = [];
        for (let t of tasksList) {
          let indx = tasks.findIndex(i => i.id === t.id && i.pickrequestid === t.pickrequestid);
          if (indx === -1) tasks.push(t);
        }

        if (findRelated) {
          //console.log('taskdata check out -----');
          //console.log('is relate task? : ', findRelated);
          //console.log('status of task: ', tasks[0], 'status of compare: ', TASK_STATUS.PENDING);

          if (tasks[0].status == TASK_STATUS.NEW) {
            throw new Error('500,Error new task');
          } else if (tasks[0].status == TASK_STATUS.CANCELED) {
            throw new Error('500,Error canceled task');
          } else if (tasks[0].status == TASK_STATUS.PENDING) {
            throw new Error('500,Error pending task');
          }
        }



        let relateTasks = new Array();
        let orders = new Array();
        let taskIds = new Array();
        for (let t of tasks) {

          let locbarcode = '';
          let sql = `
            select pdi.locbarcode from ${this.SCHEMA}.pick_task_detail pd
            join ${this.SCHEMA}.pick_task_detail_item pdi on pdi.picktaskdetailid = pd.id
            where pd.pickrequestid = ?
            order by pdi.updated_time desc, pdi.itemstatus desc limit 1
          `;
          let locData = await UtilModel.getInstance().sqlQuery(sql, [t.pickrequestid]);
          if (locData.length > 0) {
            locbarcode = locData[0].locbarcode === null ? '' : locData[0].locbarcode;
          }

          let order: any = {
            pickRequestId: t.pickrequestid,
            orderKey: t.orderkey,
            sourceOrderId: t.sourceorderid,
            sourceBU: t.sourcebu,
            sourceLoc: t.sourceloc,
            pickMode: t.pickmode,
            pickAllocationType: t.pickallocationtype,
            user_id: t.user_id,
            bu: t.bu,
            role: t.role,
            status: t.status,
            receivingScan: t.receivingscan,
            locHandling: t.lochandling,
            sourceSubOrderId: t.sourcesuborderid,
            locBarcode: locbarcode,
            remark_message: {
              messageType: t.message,
              message: t.messagetype
            }
          }

          if (!findRelated) {
            order.packReceiveTime = t.packreceivetime ? t.packreceivetime : "";
            order.locBarcode = t.locbarcode ? t.locbarcode : "";
          }
          orders.push(order);

          if (findRelated) {
            let sql = `
              select distinct pt.id from ${this.SCHEMA + '.'}pick_task pt
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join ${this.SCHEMA + '.'}pick_request pr
              on pd.pickRequestId = pr.pickRequestId
              where pr.pickRequestId = ?
              and pt.id <> ?
            `;

            let params = [t.pickrequestid, taskId];
            if (taskIds.length > 0) {
              let paramsArr: any = [];
              taskIds.forEach(t => {
                paramsArr.push('?');
                params.push(t);
              });
              sql += `and pt.id not in (${paramsArr.join(',')})`;
              // params.push(taskIds);
            }

            let relates = await UtilModel.getInstance().sqlQuerySync(sql, params);
            console.log('relates : ', relates);
            for (let r of relates) {
              if (!taskIds.includes(r.id)) {
                taskIds.push(r.id);
              }
              console.log('relateTasks taskId : ', r.id);
              relateTasks.push(await this.getTaskData(r.id, false));
              console.log('relateTasks : ', relateTasks);
            }
          }
        }

        let itemSql = `
          select
            i.id, i.pickRequestId, pr.orderKey, pr.sourceOrderId, ppr.pickMode, i.itemKey, i.sourceItemId, i.sourceItemNumber, i.barcode,
            i.sku, i.sourcePrice, i.standardPrice, i.currency, i.color, i.size, count(0) as qty, i.qty as prQty, i.qtyUnit,
            i.weight, i.weightUnit, i.productNameTH, i.productNameEN, i.productNameIT, i.productNameDE, i.imageUrl,
            i.locBarcode, i.cat1, i.cat2, i.cat3, i.cat4, i.cat5, i.cat6, i.brand, i.zone,
            i.status, i.statusReason, i.statusDate, i.created_time, i.updated_time,
            ppr.locHandling, ppr.receivingScan, pr.sourceSubOrderId,
            string_agg(pi.pickBarcode, ',') as pickBarcodes,
            i.statusReason, i.statusDate itemStatusDate, i.subbarcode, i.brandCode,
            pri.message itemMessage, pri.messagetype itemMessageType
          from ${this.SCHEMA + '.'}pick_task_detail pd
          inner join ${this.SCHEMA + '.'}pr_items i
          on pd.pickRequestId = i.pickRequestId
          and pd.pickItemId = i.id
          inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
          on pd.id = pi.pickTaskDetailId
          inner join ${this.SCHEMA + '.'}pick_request pr
          on pd.pickRequestId = pr.pickRequestId
          inner join ${this.SCHEMA + '.'}pr_pick_params ppr
          on pr.pickRequestId = ppr.pickRequestId
          and pd.itemKey = i.itemKey
          left join ${this.SCHEMA + '.'}pr_remark_item pri on pri.pickrequestid = i.pickrequestid
          and pri.itemkey = i.itemkey and pri.messagetype = '0006'
          where pd.pickTaskId = ?
          and (pi.itemStatus not in (?, ?, ?, ?)
          or pi.statusReason = 'WAITING_DECLINE')
          group by
            i.id, i.pickRequestId, pr.orderKey, pr.sourceOrderId, ppr.pickMode, i.itemKey, i.sourceItemId, i.sourceItemNumber, i.barcode,
            i.sku, i.sourcePrice, i.standardPrice, i.currency, i.color, i.size, i.qty, i.qtyUnit,
            i.weight, i.weightUnit, i.productNameTH, i.productNameEN, i.productNameIT, i.productNameDE, i.imageUrl,
            i.locBarcode, i.cat1, i.cat2, i.cat3, i.cat4, i.cat5, i.cat6, i.brand, i.zone,
            i.status, i.statusReason, i.statusDate, i.created_time, i.updated_time,
            ppr.locHandling, ppr.receivingScan, pr.sourceSubOrderId, pri.message, pri.messagetype
        `;

        let items = await UtilModel.getInstance().sqlQuerySync(itemSql, [taskId, ITEM_STATUS.NEW, ITEM_STATUS.DECLINED, ITEM_STATUS_FM_MAPPING[4000], ITEM_STATUS_FM_MAPPING[4100]]);

        if (items == undefined || items.length == 0) {
          if (findRelated) {
            // throw new Error('500,No picked items found.');
          }
        }

        this.pickingItems = new Array();
        let pickingItems = new Array();
        for (let item of items) {
          let sql = `
            select pi.id, pi.itemStatus as status, pi.statusReason, pi.pickBarcode
            from ${this.SCHEMA + '.'}pick_task_detail pd
            inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
            on pd.id = pi.pickTaskDetailId
            where pd.pickTaskId = ?
            and pd.pickRequestId = ?
            and pd.itemKey = ?
            and pd.pickItemId = ?
          `;
          let itemsStatus: any = [];

          console.log('sql : ', sql);
          console.log('params : ', [taskId, item.pickrequestid, item.itemkey, item.id]);
          await UtilModel.getInstance().sqlQuerySync(sql, [taskId, item.pickrequestid, item.itemkey, item.id]).then(res => {
            console.log('res length : ', res.length);
            for (let i of res) {
              itemsStatus.push({
                id: i.id,
                status: i.status,
                statusReason: i.statusreason,
                pickBarcode: i.pickbarcode
              });
            }
          });

          let pickingItem = {
            pickRequestId: item.pickrequestid,
            orderKey: item.orderkey,
            sourceOrderId: item.sourceorderid,
            pickMode: item.pickmode,
            receivingScan: item.receivingscan,
            locHandling: item.lochandling,
            sourceSubOrderId: item.sourcesuborderid,
            itemKey: item.itemkey,
            sourceItemId: item.sourceitemid,
            sourceItemNumber: item.sourceitemnumber,
            barcode: item.barcode,
            pickBarcodes: (item.pickbarcodes) ? item.pickbarcodes.length == 0 ? [] : item.pickbarcodes.split(',').filter((x: any) => !!x) : [],
            sku: item.sku,
            sourcePrice: item.sourceprice,
            standardPrice: item.standardprice,
            currency: item.currency,
            color: item.color,
            size: item.size,
            qty: parseInt(item.qty),
            prQty: item.prqty,
            qtyUnit: item.qtyunit,
            weight: item.weight,
            weightUnit: item.weightunit,
            productNameTH: item.productnameth,
            productNameEN: item.productnameen,
            productNameIT: item.productnameit,
            productNameDE: item.productnamede,
            imageUrl: item.imageurl,
            locBarcode: item.locbarcode,
            cat1: item.cat1,
            cat2: item.cat2,
            cat3: item.cat3,
            cat4: item.cat4,
            cat5: item.cat5,
            cat6: item.cat6,
            brand: item.brand,
            zone: item.zone,
            status: item.status,
            statusReason: item.statusreason,
            statusDate: item.statusdate,
            itemStatus: itemsStatus,
            remark_message: {
              message: item.itemmessage,
              messageType: item.itemmessagetype,
            }
          }
          pickingItems.push(pickingItem);
        }

        let response: any = {
          taskId: tasks[0].id,
          wave_id: tasks[0].wave_id,
          multiorder_id: tasks[0].multiorder_id,
          orders: orders,
          pickingItems: pickingItems
        }

        if (findRelated) {
          response.relateTasks = relateTasks;
        }

        resolve(response);
      } catch (err) {
        reject(err);
      }
    });
  }

  async getTaskDataToReceive(taskId: number) {
    return await this.getTaskData(taskId, true);
  }

  async updateItemStatus(data: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let searchBy = 'id';

            if (!data.hasOwnProperty('itemStatusID') || data.itemStatusID == undefined || data.itemStatusID.length == 0) {
              searchBy = 'sku';
              if (!data.hasOwnProperty('sku') || data.sku == undefined || data.sku.length == 0) {
                searchBy = 'key';
                if (!data.hasOwnProperty('itemKey') || data.itemKey == undefined || data.itemKey.length == 0) {
                  throw new Error(ERR_IdSkuAndItemKey);
                }
              }
            }

            let affectedRows = 0;
            if (data.set_status == ITEM_STATUS.DECLINED && !data.reason) {
              throw new Error(ERR_cancelRequire);
            }

            if (!data.hasOwnProperty('qty')) data.qty = 1;

            if (searchBy == 'id' && data.qty != 1) {
              throw new Error(ERR_wrongUpdateCondition);
            }

            if (data.qty <= 0) {
              throw new Error(ERR_qtyLE0);
            }

            /************ update status to pick task detail item ************/
            let sql = `
              select pd.pickRequestId, pd.itemKey, i.weightedItem, pi.*
              from ${this.SCHEMA + '.'}pick_task pt
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
              on pd.id = pi.pickTaskDetailId
              inner join ${this.SCHEMA + '.'}pick_request pr
              on pd.pickRequestId = pr.pickRequestId
              inner join ${this.SCHEMA + '.'}pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              and pd.pickItemId = i.id
              where pr.orderKey = ?
              and pt.id = ?
            `;

            let params = [
              data.orderKey,
              data.task_id
            ];

            if (searchBy == 'id') {
              sql += ` and pi.id = ? `
              params.push(data.itemStatusID);
            } else if (searchBy == 'sku') {
              sql += ` and i.sku = ? `;
              params.push(data.sku);
            } else {
              sql += ` and pd.itemKey = ? `;
              params.push(data.itemKey);
            }

            // sql += ` order by i.itemkey `;
            let res = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            res.sort((x: any, y: any) => {
              return x.itemkey < y.itemkey ? -1 : 1;
            });

            console.log(`[update pr_items] param select: `, params);
            console.log(`[update pr_items] res select: `, res);

            if (res.length == 0) {
              reject(new Error(ERR_itemNotFound));
            }

            if (res[0].weighteditem.toUpperCase() === 'Y' && data.set_status != ITEM_STATUS.DECLINED) {
              if (res[0].weighteditem.toUpperCase() == 'Y' && (parseInt(data.barcode.slice(data.barcode.length - 6, data.barcode.length - 1)) / 1000) == 0) {
                throw new Error(ERR_actualWeightIs0);
              }
            }
            let newItems = res.filter((x: any) => x.itemstatus != ITEM_STATUS.DECLINED && x.itemstatus != ITEM_STATUS.PICKED);
            let procItems = res.filter((x: any) => x.itemstatus != ITEM_STATUS.DECLINED);
            if ((newItems.length == 0 && data.set_status != ITEM_STATUS.DECLINED) || procItems.length == 0) {
              throw new Error(ERR_wrongUpdateCondition);
            }

            if (newItems.length < data.qty && data.set_status != ITEM_STATUS.DECLINED) {
              throw new Error(ERR_wrongUpdateCondition)
            }

            let qty = procItems.length < data.qty ? procItems.length : data.qty;
            let j = 0;
            let k = 0;

            for (let i = 0; i < qty; i++) {

              sql = `
                update ${this.SCHEMA + '.'}pick_task_detail_item
                set itemStatus = ?, pickBarcode = ?, statusReason = ?
                where id = ?
              `;

              if (newItems.length > i) {
                /* update new item to set_status */
                params = [
                  data.set_status,
                  data.barcode,
                  data.set_status == ITEM_STATUS.DECLINED ? data.reason : "",
                  searchBy == 'id' ? data.itemStatusID : newItems[j++].id
                ];
              } else {
                /* update any item to cancel */
                params = [
                  data.set_status,
                  '',
                  '',
                  searchBy == 'id' ? data.itemStatusID : procItems[k++].id
                ];
              }

              console.log('[pr_items] params : ', params);
              await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then((res: any) => affectedRows += res.rowCount);

            }

            /************ update item status when all qty picked or canceled ************/

            sql = `
              select distinct itemStatus
              from ${this.SCHEMA + '.'}pick_task_detail_item pi
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pi.pickTaskDetailId = pd.id
              inner join ${this.SCHEMA + '.'}pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              and pd.pickItemId = i.id
              where pd.pickRequestId = ?
              and itemStatus not in (?, ?)
            `;

            params = [];
            params.push(res[0].pickrequestid, ITEM_STATUS_FM_MAPPING[4000], ITEM_STATUS_FM_MAPPING[4100]);

            // params = [res[0].pickrequestid];
            let keys = res.filter((x: any) => x.itemstatus != ITEM_STATUS.DECLINED);
            // console.log(`[update pr_items] filter keys: `, keys);

            keys.sort((x: any, y: any) => {
              if (x.itemstatus != y.itemstatus) {
                return (x.itemstatus < y.itemstatus) ? -1 : 1;
              } else {
                return (x.itemkey < y.itemkey) ? -1 : 1;
              }
            });
            // console.log(`[update pr_items] sorts keys: `, keys);
            keys = keys.map((x: any) => x.itemkey).slice(0, qty);
            // console.log(`[update pr_items] map keys: `, keys);

            if (searchBy == 'sku') {
              console.log(' ----------------------- searchBy sku ----------------------- ');
              let sql_in: any = [];
              keys.forEach((i: any) => {
                sql_in.push('?');
                params.push(i)
              });
              sql += ` and i.itemKey in (${sql_in.join(',')}) `;
              // params.push(keys);
            } else { // search by id use same condition of itemKey to update pr_items
              console.log(' ----------------------- searchBy itemKey ----------------------- ');
              sql += ` and i.itemKey = ? `;
              params.push(res[0].itemkey);
            }

            console.log('[pr_items] sql : ', sql);
            console.log('[pr_items] params : ', params);
            let chk = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            //console.log('-- check item status --');
            console.log('pick_task_detail_item chk : ', chk);

            let status = ITEM_STATUS.PARTIAL;
            if (chk.length == 1 && chk[0].itemstatus == ITEM_STATUS.DECLINED) {
              status = ITEM_STATUS.DECLINED;
            }

            if (status != ITEM_STATUS.DECLINED) {
              chk = chk.filter((x: any) => x.itemstatus != ITEM_STATUS.DECLINED);
              if (chk.length == 1 && chk[0].itemstatus == ITEM_STATUS.PICKED) {
                status = ITEM_STATUS.PICKED;
              }
            }

            console.log('pr_items status : ', status);
            sql = `
              update ${this.SCHEMA + '.'}pr_items
              set status = ?
              ${(res[0].weighteditem.toUpperCase() === 'Y' && data.set_status != ITEM_STATUS.DECLINED) ? `, actualWeight = ? ` : ``}
              where pickRequestId = ?
            `;

            if (res[0].weighteditem.toUpperCase() === 'Y' && data.set_status != ITEM_STATUS.DECLINED) {
              let actualWeight: number = (parseInt(data.barcode.slice(data.barcode.length - 6, data.barcode.length - 1)) / 1000);

              params = [
                status,
                actualWeight,
                res[0].pickrequestid
              ];
            } else {
              params = [
                status,
                res[0].pickrequestid
              ];
            }

            console.log('keys : ', keys);
            if (searchBy == 'sku') {
              // sql += ` and itemKey in (?) `;
              // params.push(keys);
              let sql_in: any = [];
              keys.forEach((i: any) => {
                sql_in.push('?');
                params.push(i)
              });
              sql += ` and itemKey in (${sql_in.join(',')}) `;
            } else { // use same business for searchBy == 'id' and 'key'
              sql += ` and itemKey = ? `;
              params.push(res[0].itemkey);
            }

            // console.log(`[update pr_items] searchBy: `, searchBy);
            // console.log(`[update pr_items] keys: `, keys);
            // console.log(`[update pr_items] res[0].itemKey: `, res[0].itemkey);
            // console.log(`[update pr_items] sql: `, sql);
            // console.log(`[update pr_items] params: `, params);
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            /**************************************** check last items status ****************************************/

            if (data.set_status == ITEM_STATUS.DECLINED) {
              sql = ` select distinct itemkey from ${this.SCHEMA + '.'}pick_task_detail ptd where ptd.pickrequestid = ? `;
              let itemKeyList = await UtilModel.getInstance().sqlConnQuery(conn, sql, [res[0].pickrequestid]);

              for (let item of itemKeyList) {
                sql = `
                  select distinct pi.status , pdi.itemstatus from ${this.SCHEMA + '.'}pr_items pi
                  join ${this.SCHEMA + '.'}pick_task_detail pd on pd.pickrequestid = pi.pickrequestid and pd.itemkey = pi.itemkey and pd.pickItemId = pi.id
                  join ${this.SCHEMA + '.'}pick_task_detail_item pdi on pdi.picktaskdetailid = pd.id
                  where pd.pickrequestid = ? and pd.itemkey = ?
                `;
                let lastItemStatus = await UtilModel.getInstance().sqlConnQuery(conn, sql, [res[0].pickrequestid, item.itemkey]);

                if (lastItemStatus.length == 1) {
                  sql = `update ${this.SCHEMA + '.'}pr_items set status = ? where pickrequestid = ? and itemkey = ? `;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [lastItemStatus[0].itemstatus, res[0].pickrequestid, item.itemkey]);
                }

              }
            }

            /************ update task status when all item picked or canceled ************/

            sql = `
              select distinct i.status from ${this.SCHEMA + '.'}pick_task pt
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join ${this.SCHEMA + '.'}pick_request pr
              on pd.pickRequestId = pr.pickRequestId
              inner join ${this.SCHEMA + '.'}pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              and pd.pickItemId = i.id
              where pt.id = ?
              and i.status <> ?
            `;

            params = [data.task_id, ITEM_STATUS.DECLINED];
            let results = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            //console.log('log-row1198====>update');
            //console.log(results);
            let taskStatus = TASK_STATUS.PENDING;
            /**
             * @KLA
             * ADD item status canceled for check task too.
             *
             * @Tar 2019-08-21
             * Remove check status on result, check sql instead.
             */
            if (results.length == 0) {
              taskStatus = TASK_STATUS.CANCELED;
            } else if (results.length == 1 && results[0].status == ITEM_STATUS.PICKED) {
              taskStatus = TASK_STATUS.PICKED;
            } else {
              taskStatus = TASK_STATUS.PARTIAL;
            }

            sql = `
              select distinct pdi.itemStatus , pdi.statusReason
              from ${this.SCHEMA + '.'}pick_task_detail pd
              join ${this.SCHEMA + '.'}pick_task_detail_item pdi on pdi.pickTaskDetailId = pd.ID
              where pd.pickTaskId = ?
            `;
            let checkItemStatus = await UtilModel.getInstance().sqlConnQuery(conn, sql, [data.task_id]);

            let picked = checkItemStatus.filter(i => i.itemstatus == ITEM_STATUS.PICKED);
            let new_items = checkItemStatus.filter(i => i.itemstatus == ITEM_STATUS.NEW);
            let waiting = checkItemStatus.filter(i => i.itemstatus == ITEM_STATUS.DECLINED && i.statusreason == 'WAITING_DECLINE');
            // let declined = checkItemStatus.filter(i => i.itemstatus == ITEM_STATUS.DECLINED && i.statusreason != 'WAITING_DECLINE');

            if ((checkItemStatus.length == 1 && checkItemStatus[0].itemstatus == ITEM_STATUS.DECLINED && checkItemStatus[0].statusreason == 'WAITING_DECLINE') ||
              (new_items.length == 0 && picked.length == 0 && waiting.length > 0)) {
              taskStatus = TASK_STATUS.WAITING_DECLINE;
            }

            params = [taskStatus, data.task_id];
            sql = ` update ${this.SCHEMA + '.'}pick_task set status = ? where id = ? `;

            await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            if (waiting.length == 0 && new_items.length == 0) {
              await this.addResponseQueue(conn, res[0].pickrequestid, 0);
            }

            LogUtil.logSnowFlake(conn, data.task_id, taskStatus, 'function updateItemStatus');
            await UtilModel.getInstance().sqlConnCommit(conn);

            let updatedTask = await this.getUpdateItemsStatus(data.task_id, taskStatus);
            resolve({
              affectedRows: affectedRows,
              set_task_status: taskStatus,
              updatedTask: updatedTask
            });

          } catch (err) {
            conn.query('ROLLBACK');
            reject(err);
          } finally {
            done();
          }
        });
      });
    });
  }

  async getUpdateItemsStatus(taskId: number, taskStatus: string) {
    let result = {
      taskId: taskId,
      status: taskStatus,
      items: []
    };
    let sql: string = ``;

    sql = `
      select
        pd.id as taskDetailId, pd.pickrequestid,
        pi.pickBarcode as taskItemPickBarcode,
        pi.id as taskItemId, pi.itemStatus as taskItemStatus, pi.statusReason as taskStatusReason,
        i.id itemId, i.itemKey, i.sourceItemId, i.sourceItemNumber, i.barcode, i.sku, i.sourcePrice, i.standardPrice, i.currency, i.color, i.gender, i.itemLocCode, i.itemLocDesc, i.itemAreaCode, i.itemAreaDesc,
        i.size, i.qty, i.qtyUnit, i.weight, i.weightUnit, i.weightedItem, i.productNameTH, i.productNameEN, i.productNameIT, i.productNameDE, i.imageUrl, i.locBarcode,
        i.cat1, i.cat2, i.cat3, i.cat4, i.cat5, i.cat6, i.brand, i.zone, i.status itemStatus,
        i.statusReason, i.statusDate itemStatusDate, i.subbarcode, i.brandCode,
        pri.message itemMessage, pri.messagetype itemMessageType
      from ${this.SCHEMA + '.'}pick_task_detail pd
      join ${this.SCHEMA + '.'}pr_items i on i.pickrequestid = pd.pickrequestid and i.itemkey = pd.itemkey
      join ${this.SCHEMA + '.'}pick_task_detail_item pi on pi.picktaskdetailid = pd.id
      left join ${this.SCHEMA + '.'}pr_remark_item pri on pri.pickrequestid = i.pickrequestid
      and pri.itemkey = i.itemkey and pri.messagetype = '0006'
      where pd.picktaskid = ?
    `;
    let itemsArray = await UtilModel.getInstance().sqlQuery(sql, [taskId]);

    let items: any = [];
    for (let r of itemsArray) {
      let indx = items.findIndex(i => i.pickRequestId === r.pickrequestid && i.itemKey === r.itemkey);
      if (indx === -1) {
        let details = itemsArray
          .filter(x => x.pickrequestid == r.pickrequestid && x.itemkey == r.itemkey)
          .map(x => ({ id: x.taskitemid, status: x.taskitemstatus, reason: x.taskstatusreason, pickBarcode: x.taskitempickbarcode }));
        items.push({
          pickRequestId: r.pickrequestid,
          // pickMode: r.pickmode,
          itemId: r.itemid,
          itemKey: r.itemkey,
          sourceItemId: r.sourceitemid,
          sourceItemNumber: r.sourceitemnumber,
          barcode: r.barcode,
          subbarcode: r.subbarcode,
          sku: r.sku,
          sourcePrice: r.sourceprice == null ? 0 : r.sourceprice,
          standardPrice: r.standardprice == null ? 0 : r.standardprice,
          currency: r.currency == null ? '' : r.currency,
          color: r.color == null ? '' : r.color,
          gender: r.gender == null ? '' : r.gender,
          brandCode: r.brandcode == null ? '' : r.brandcode,
          itemLocCode: r.itemloccode == null ? '' : r.itemloccode,
          itemLocDesc: r.itemlocdesc == null ? '' : r.itemlocdesc,
          itemAreaCode: r.itemareacode == null ? '' : r.itemareacode,
          itemAreaDesc: r.itemareadesc == null ? '' : r.itemareadesc,
          size: r.size == null ? '' : r.size,
          qty: parseInt(r.qty),
          qtyUnit: r.qtyunit,
          weight: r.weight == null ? 0 : r.weight,
          weightUnit: r.weightunit == null ? '' : r.weightunit,
          weightedItem: r.weighteditem == null ? '' : r.weighteditem,
          productNameTH: r.productnameth,
          productNameEN: r.productnameen,
          productNameIT: r.productnameit == null ? '' : r.productnameit,
          productNameDE: r.productnamede == null ? '' : r.productnamede,
          imageUrl: r.imageurl,
          locBarcode: r.locbarcode || ' ',
          cat1: r.cat1,
          cat2: r.cat2,
          cat3: r.cat3,
          cat4: r.cat4,
          cat5: r.cat5,
          cat6: r.cat6,
          brand: r.brand,
          zone: r.zone,
          status: r.itemstatus,
          statusReason: r.statusreason,
          statusDate: dateToStrDate(r.itemstatusdate),
          itemStatus: details,
          remark_message: {
            message: r.itemmessage,
            messageType: r.itemmessagetype,
          }

        })
      }
    }

    result.items = items;
    return result;
  }

  async updateTaskStatus(data: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let params = [data.task_status, data.task_id];

            let sql = `
              update pick_task set status = ? where id = ?
            `;

            await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            await UtilModel.getInstance().sqlConnCommit(conn);
            resolve({});
          } catch (err) {
            conn.rollback();
            reject(err);
          } finally {
            conn.release();
          }
        });
      });
    });
  }

  async setPackReceiveItemStatus(datas: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let affectedRows = 0;
            /**
             * loop item assume data submit each task
             */
            console.log('----------------- BEGIN setPackReceiveItemStatus -----------------');

            let i = 0;
            for await (let data of datas) {
              console.log(new Date().toISOString() + ',' + '/setPackReceiveItemStatus,loop[' + i + '], data :' + JSON.stringify(data));
              let sql = `
                select distinct pi.itemStatus
                from ${this.SCHEMA + '.'}pick_task_detail pd
                inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
                on pd.id = pi.pickTaskDetailId
                inner join ${this.SCHEMA + '.'}pr_pick_params pp
                on pp.pickRequestId = pd.pickRequestId
                inner join ${this.SCHEMA + '.'}pick_request pr
                on pd.pickRequestId = pr.pickRequestId
                inner join ${this.SCHEMA + '.'}pr_items i
                on pd.pickRequestId = i.pickRequestId
                and pd.itemKey = i.itemKey
                where pd.pickTaskId = ?
                and (pi.itemStatus = ? and pi.statusReason = 'WAITING_DECLINE')
                and pr.orderKey = ?
              `;
              let itemsStatusArray = await UtilModel.getInstance().sqlConnQuery(conn, sql, [data.task_id, ITEM_STATUS.DECLINED, data.orderKey]);
              console.log('[setPackReceiveItemStatus] itemsStatusArray decline status: ', itemsStatusArray);
              let isEndPackReceived = itemsStatusArray.length == 0 ? true : false;
              console.log('[setPackReceiveItemStatus] isEndPackReceived boolean: ', isEndPackReceived);

              sql = `
                select pi.id, pd.pickRequestId, pd.itemKey, pi.itemStatus
                from ${this.SCHEMA + '.'}pick_task_detail pd
                inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
                on pd.id = pi.pickTaskDetailId
                inner join ${this.SCHEMA + '.'}pr_pick_params pp
                on pp.pickRequestId = pd.pickRequestId
                inner join ${this.SCHEMA + '.'}pick_request pr
                on pd.pickRequestId = pr.pickRequestId
                inner join ${this.SCHEMA + '.'}pr_items i
                on pd.pickRequestId = i.pickRequestId
                and pd.itemKey = i.itemKey
                where pd.pickTaskId = ?
                and case when pp.receivingScan = 'Y' then (i.barcode = ? or pi.pickBarcode = ?) else true end
                and pi.itemStatus = ?
                and pr.orderKey = ?
              `;

              let params = [
                data.task_id,
                data.item_barcode,
                data.item_barcode,
                ITEM_STATUS.PICKED,
                data.orderKey
              ];
              let items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              // if (items == undefined || items.length == 0) {
              console.log(new Date().toISOString() + ',' + '/setPackReceiveItemStatus loop[' + i + '], check item :' + JSON.stringify(items));
              if (items !== undefined && items.length > 0) {
                // let res = await this.checkCancelledStatus(data);
                // let returnErr = "";
                // if (typeof res != "string") {
                //   returnErr = "," + JSON.stringify(res);
                // }

                // throw new Error("500,No data found" + returnErr);

                console.log('[setPackReceiveItemStatus] STEP update pick_task_detail_item');
                // update status to READY_TO_PACK each qty
                sql = `
                  update ${this.SCHEMA + '.'}pick_task_detail_item
                  set itemStatus = ?, locBarcode = ?, packReceiveDate = ?
                  where id = ?
                `;

                params = [
                  ITEM_STATUS.READY_TO_PACK,
                  data.location_barcode,
                  new Date(),
                  items[0].id
                ];

                let res_pick_task_detail_item = await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then((res: any) => affectedRows += res.rowCount);
                console.log(new Date().toISOString, ', setPackReceiveItemStatus round : ' + i + ', SQL : update pick_task_detail_item, data=>', JSON.stringify(res_pick_task_detail_item));

                if (items.filter((x: any) => x.itemkey == items[0].itemkey).length == 1) {
                  sql = `
                    update ${this.SCHEMA + '.'}pr_items
                    set status = ?, locBarcode = ?
                    where pickRequestId = ?
                    and itemKey = ?
                  `;

                  params = [
                    ITEM_STATUS.READY_TO_PACK,
                    data.location_barcode,
                    items[0].pickrequestid,
                    items[0].itemkey
                  ];

                  let res_pr_items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                  console.log(new Date().toISOString, ', setPackReceiveItemStatus round : ' + i + ', SQL : update pr_items, data=>', JSON.stringify(res_pr_items));
                }

                // check all item in task has READY_TO_PACK status
                console.log('[setPackReceiveItemStatus] STEP select pr_items');

                sql = `
                  select distinct i.status
                  from ${this.SCHEMA + '.'}pr_items i
                  inner join ${this.SCHEMA + '.'}pick_task_detail pd
                  on i.pickRequestId = pd.pickRequestId
                  and i.itemKey = pd.itemKey
                  where pd.pickTaskId = ?
                  and i.status = ?
                `;

                params = [
                  data.task_id,
                  ITEM_STATUS.READY_TO_PACK
                ];

                let stats = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

                // update task status when all item in task has READY_TO_PACK status (concern for request done but task not done in multi order)
                console.log(new Date().toISOString, ', setPackReceiveItemStatus round : ' + i + ', SQL : select distinct pr_items, data=>', JSON.stringify(stats));
                // console.log(new Date().toISOString, ', setPackReceiveItemStatus round : ' + i + ', Check Condition update pick_task, stats[0].status =>', stats[0].status);
                if (stats != undefined && stats.length == 1 && stats[0].status == ITEM_STATUS.READY_TO_PACK) {

                  sql = `
                    update ${this.SCHEMA + '.'}pick_task set status = ?, user_pack = ? where id = ?
                  `;

                  params = [
                    isEndPackReceived ? TASK_STATUS.READY_TO_PACK : TASK_STATUS.WAITING_DECLINE,
                    data.user_id,
                    data.task_id
                  ];
                  let res_picktask = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                  console.log(new Date().toISOString, ', setPackReceiveItemStatus round : ' + i + ', Condition Pass update pick_task, ', JSON.stringify(res_picktask));
                }

                LogUtil.logSnowFlake(conn, data.task_id, (isEndPackReceived ? TASK_STATUS.READY_TO_PACK : TASK_STATUS.WAITING_DECLINE), 'function setPackReceiveItemStatus');
                if (isEndPackReceived) {
                  await this.addResponseQueue(conn, items[0].pickrequestid, 0);
                }
                await UtilModel.getInstance().sqlConnCommit(conn);
                //this.batchPickResponse();

                // let res = await this.checkCancelledStatus(data);
                // (typeof res != "string") ? res : [];
                let returnErr = "";

                resolve(returnErr);
              }
              i++;
            }
          } catch (err) {
            console.log('[setPackReceiveItemStatus] STEP select pr_items');
            console.error('[setPackReceiveItemStatus] ERROR : ', err);
            LogUtil.logError('post', '/setPackReceiveItemStatus', 'in catch', err.toString());
            console.log('error : ', err);
            conn.query('ROLLBACK');
            reject(err);
          } finally {
            conn.release();
          }
        });
      });
    });
  }

  async setSuppervisorApprove(data: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let affectedRows = 0;
            let sql = `
              select
                pd.pickRequestId, p.id itemId, pdi.id itemDetailId
              from ${this.SCHEMA + '.'}pick_task_detail pd
              join ${this.SCHEMA + '.'}pick_task_detail_item pdi on pdi.pickTaskDetailId = pd.ID
              join ${this.SCHEMA + '.'}pick_request pr on pr.pickRequestId = pd.pickRequestId
              join ${this.SCHEMA + '.'}pr_items p on p.pickRequestId = pd.pickRequestId and p.itemKey = pd.itemKey and p.id = pd.pickItemId
              where pdi.itemStatus = ? and pdi.statusReason = ?
              and pr.orderKey = ?
              and pd.pickTaskId = ?
              and pd.itemKey = ?
            `;
            let params = [
              ITEM_STATUS.DECLINED,
              'WAITING_DECLINE',
              data.orderKey,
              data.task_id,
              data.itemKey
            ];
            let itemIdArray = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log('[setSuppervisorApprove] itemIdArray: ', itemIdArray);
            if (itemIdArray.length == 0) {
              reject(new Error(ERR_itemNotFound));
            }

            sql = `update ${this.SCHEMA + '.'}pick_task_detail_item set itemStatus = ?, statusReason = ?, pickBarcode = ? where id = ? `;
            params = [
              data.set_status,
              (data.set_status == ITEM_STATUS.DECLINED) ? data.reason : "",
              (data.set_status == ITEM_STATUS.PICKED) ? data.barcode : "",
              itemIdArray[0].itemdetailid
            ];
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then(res => {
              console.log('response : ', res);
              affectedRows += res.rowCount
            });
            console.log('[setSuppervisorApprove] update pick_task_detail_item affectedRows: ', affectedRows);

            sql = `
              select distinct itemStatus , pi.statusReason
              from ${this.SCHEMA + '.'}pick_task_detail_item pi
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pi.pickTaskDetailId = pd.id
              inner join ${this.SCHEMA + '.'}pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              and pd.pickItemId = i.id
              where pd.pickRequestId = ?
              and pd.itemKey = ?
            `;
            params = [
              itemIdArray[0].pickrequestid,
              data.itemKey
            ];
            let checkStatus = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
            console.log('[setSuppervisorApprove] checkStatus: ', checkStatus);

            let itemStatus = ITEM_STATUS.PARTIAL;
            if (checkStatus.length == 1 && checkStatus[0].itemstatus == ITEM_STATUS.PICKED) {
              itemStatus = ITEM_STATUS.PICKED;
            } else if (checkStatus.filter(s => s.statusreason == 'WAITING_DECLINE').length == 0) {
              itemStatus = ITEM_STATUS.DECLINED;
            }

            sql = `update ${this.SCHEMA + '.'}pr_items set status = ? where pickRequestId = ? and itemKey = ?`;
            params = [
              itemStatus,
              itemIdArray[0].pickrequestid,
              data.itemKey
            ];
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then(res => affectedRows += res.rowCount);
            console.log('[setSuppervisorApprove] update pr_items affectedRows: ', affectedRows);

            sql = `
              select distinct i.status, pdi.statusReason from ${this.SCHEMA + '.'}pick_task pt
              inner join ${this.SCHEMA + '.'}pick_task_detail pd
              on pt.id = pd.pickTaskId
              inner join ${this.SCHEMA + '.'}pick_task_detail_item pdi
              on pdi.pickTaskDetailId = pd.ID
              inner join ${this.SCHEMA + '.'}pick_request pr
              on pd.pickRequestId = pr.pickRequestId
              inner join ${this.SCHEMA + '.'}pr_items i
              on pd.pickRequestId = i.pickRequestId
              and pd.itemKey = i.itemKey
              where pt.id = ?
            `;
            params = [
              data.task_id
            ];
            let checkTaskStatus = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

            let waitingDeclineArray = checkTaskStatus.filter(i => i.status == ITEM_STATUS.DECLINED && i.statusreason == 'WAITING_DECLINE');
            let pickArray = checkTaskStatus.filter(i => i.status == ITEM_STATUS.PICKED);
            let receiveArray = checkTaskStatus.filter(i => i.status == ITEM_STATUS.READY_TO_PACK)
            console.log('[setSuppervisorApprove] checkTaskStatus: ', checkTaskStatus);
            console.log('[setSuppervisorApprove] readyToPackArray: ', waitingDeclineArray);
            console.log('[setSuppervisorApprove] pickArray: ', pickArray);

            let taskStatus = TASK_STATUS.WAITING_DECLINE;
            if (waitingDeclineArray.length == 0) {
              taskStatus = pickArray.length > 0 || receiveArray.length > 0 ? pickArray.length > 0 ? TASK_STATUS.PICKED : TASK_STATUS.READY_TO_PACK : TASK_STATUS.CANCELED;
            }

            LogUtil.logSnowFlake(conn, data.task_id, taskStatus, 'function setSuppervisorApprove');
            if (taskStatus != TASK_STATUS.WAITING_DECLINE) {
              sql = `update ${this.SCHEMA + '.'}pick_task set status = ? where id = ? `;
              params = [
                taskStatus,
                data.task_id
              ];

              await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then(res => affectedRows += res.rowCount);
              console.log('[setSuppervisorApprove] update pick_task affectedRows: ', affectedRows);

              await this.addResponseQueue(conn, itemIdArray[0].pickrequestid, data.task_id);
            }
            await UtilModel.getInstance().sqlConnCommit(conn);

            resolve({
              affectedRows: affectedRows,
              set_task_status: taskStatus
            });

          } catch (err) {
            LogUtil.logError('post', '/setSuppervisorApprove', 'in catch', err.toString());
            conn.rollback();
            conn.query('ROLLBACK');
            reject(err);
          } finally {
            // conn.release();
            done();
          }
        });
      });
    });
  }

  async getPickTaskListByScope(userid: any, scope: any, role: any, bu: string, loc: string, task_id: number) {
    console.log('TIME 1 : ', new Date().toUTCString());

    let activeUsers: any;
    let PickRole = 'picker';

    console.log('role: ', role);
    let position = 0;
    if (role.indexOf(':') > -1) {
      position = role.split(':')[1];
      role = role.split(':')[0];
    }
    console.log('position: ', position);
    try {
      // activeUsers = await this.getActiveUserIgnoreStatus(bu, loc);
      let userModel = new User();
      let user_list = await userModel.getUserByStatus(bu, loc, 'Y');
      activeUsers = {
        data: user_list
      }
    } catch (err) {
      console.log('error : ', err);
      throw new Error(ERR_forbiddenUM);
    }


    let myProfile = activeUsers.data.userList.filter((x: any) => x.userId.toLowerCase() == userid.toLowerCase());
    console.log('myProfile: ', myProfile);
    if (myProfile == null || myProfile == undefined || myProfile.length == 0) {
      if (PickRole == role.toLowerCase()) {
        return new Array();
      }
    }

    let sql = this.getPickTaskListSQL(task_id);

    let param = new Array<Object>();
    if (PickRole == role.toLowerCase()) {
      sql += `and pr.sourceBU = ? `
      sql += `and pr.sourceLoc = ? `
      param.push(bu);
      param.push(loc);
    }

    if (task_id != undefined) {
      sql += `and pt.id = ? `;
      param.push(task_id);
    }

    /**
     * scope case :
     * - A = NEW only
     * - M = My Task (pending to user)
     * - H = Picked Only *-> change to Picked & Cancel
     * - R = Ready To Pack (pending in pack receive)
     * - F = Pack Recevied (Finish Task)
     * - P = Partial or Picked
     */
    if ('M' == scope) {
      sql += `and lower(pt.user_id) = ? and pt.status in (?, ?) `;
      sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PENDING}', '${TASK_STATUS.PARTIAL}'`);
      param.push(userid.toLowerCase());
      param.push(TASK_STATUS.PENDING, TASK_STATUS.PARTIAL);
    } else if ('H' == scope) {
      sql += `and lower(pt.user_id) = ? `;
      param.push(userid.toLowerCase());

      if (position == 60) {
        sql += `and pt.status in (?, ?)`;
        sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PICKED}', '${TASK_STATUS.WAITING_DECLINE}'`);
        param.push(TASK_STATUS.PICKED, TASK_STATUS.WAITING_DECLINE);
      } else {
        sql += `and pt.status in (?)`;
        sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PICKED}'`);
        param.push(TASK_STATUS.PICKED);
      }
    } else if ('R' == scope) {

      if (PickRole !== role.toLowerCase()) {
        sql += `and pr.sourceBU = ? `
        sql += `and pr.sourceLoc = ? `
        param.push(bu);
        param.push(loc);
      }

      if (position == 60) {
        sql += `and pt.status in (?, ?)`;
        sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PICKED}', '${TASK_STATUS.WAITING_DECLINE}'`);
        param.push(TASK_STATUS.PICKED, TASK_STATUS.WAITING_DECLINE);
      } else {
        sql += `and pt.status in (?)`;
        sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PICKED}'`);
        param.push(TASK_STATUS.PICKED);
      }
    } else if ('F' == scope) {
      sql += `and (lower(pt.user_id) = ? or lower(pt.user_pack) = ?) and pt.status in (?) `;
      sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.FINISHED}'`);
      param.push(userid.toLowerCase());
      param.push(userid.toLowerCase());
      param.push(TASK_STATUS.FINISHED);
    } else if ('P' == scope) {
      sql += `and lower(pt.user_id) = ? and pt.status in (?, ?) `;
      sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.PARTIAL}', '${TASK_STATUS.PICKED}'`);
      param.push(userid.toLowerCase());
      param.push(TASK_STATUS.PARTIAL, TASK_STATUS.PICKED);
    } else if ('A' == scope) {
      sql += `and pt.status = ? `;
      sql = sql.split('STATUSTXT').join(`'${TASK_STATUS.NEW}'`);
      param.push(TASK_STATUS.NEW);
    }

    // sql += `order by i.cat3 desc `;

    console.log('tasklist sql : ', sql);
    console.log('tasklist param : ', param);
    let tasks = new Array();
    let results: any = await UtilModel.getInstance().sqlQuerySync(sql, param);
    //console.log('-- raw output --', results.sql);
    console.log('results : ', results.length);

    // reduce qty + priority level to unique item level
    let itemReduce = [...results.reduce((x: any, y: any) => {
      const key = y.pickrequestid + '-' + y.taskdetailid;
      // const key = y.pickrequestid + '-' + y.rownum;
      const item = x.get(key) || Object.assign({}, y);
      return x.set(key, item);
    }, new Map).values()];
    console.log('itemReduce : ', itemReduce.length);

    // reduce qty + priority level to qty level
    let qtyReduce = [...results.reduce((x: any, y: any) => {
      const key = y.pickrequestid + '-' + y.taskitemid;
      // const key = y.pickrequestid + '-' + y.rownum;
      const item = x.get(key) || Object.assign({}, y);
      return x.set(key, item);
    }, new Map).values()];
    console.log('qtyReduce : ', qtyReduce.length);

    // reduce qty + priority level to requesty + priority time level
    let pickPriorityTimeReduce = [...results.reduce((x: any, y: any) => {
      const key = y.pickrequestid + '-' + y.pickprioritytimelevel + '-' + dateToStrDate(y.pickprioritytime);
      const item = x.get(key) || Object.assign({}, y);
      return x.set(key, item);
    }, new Map).values()];
    console.log('pickPriorityTimeReduce : ', pickPriorityTimeReduce.length);

    let allocationReduce = [...results.reduce((x: any, y: any) => {
      const key = y.taskid + '-' + y.pickallocationtypeseq + '-' + y.pickallocationtype;
      const item = x.get(key) || Object.assign({}, y);
      return x.set(key, item);
    }, new Map).values()];
    console.log('allocationReduce : ', allocationReduce.length);

    console.log('-- reduce pick priority by time --');
    // console.log(pickPriorityTimeReduce);

    let taskReduceGroupByTaskId = ArrayUtil.groupBy(itemReduce, 'taskid');
    let taskGroupByTaskId = ArrayUtil.groupBy(qtyReduce, 'taskid');
    // console.log('taskReduceGroupByTaskId : ', taskReduceGroupByTaskId);
    // console.log('taskGroupByTaskId : ', taskGroupByTaskId);

    for (let k in taskReduceGroupByTaskId) {
      let orders: any = new Array();
      let items = new Array();
      let pickPriorityType = '0000';
      let pickPriorityLevel = '0000';
      let pickPriorityTime = new Date(8640000000000000);
      let priority = 0;
      let priorityTimeLevel = '';
      const now = new Date();

      let orderGroupByPickRequestId = ArrayUtil.groupBy(taskReduceGroupByTaskId[k], 'pickrequestid');

      //console.log('-- order group by request id --');
      ////console.log(orderGroupByPickRequestId);

      for (let o in orderGroupByPickRequestId) {
        if (orderGroupByPickRequestId[o][0].pickprioritytype > pickPriorityType) {
          pickPriorityType = orderGroupByPickRequestId[o][0].pickprioritytype;
        }

        if (orderGroupByPickRequestId[o][0].pickprioritylevel > pickPriorityLevel) {
          pickPriorityLevel = orderGroupByPickRequestId[o][0].pickprioritylevel;
        }

        // find pick priority time and level when pick by time to delivery otherwise do nothing
        let pickPriorityByTimes = new Array<any>();
        if (pickPriorityType == PICK_PRIORITY_TYPE.TIME_TO_DELIVERY) {
          let filter = pickPriorityTimeReduce.filter((x: any) => x.pickrequestid == orderGroupByPickRequestId[o][0].pickrequestid);

          //console.log('-- priority before sort --');
          //console.log(pickPriorityTimeReduce);

          filter.sort((x, y) => { return x.pickprioritytime.getTime() - y.pickprioritytime.getTime() });
          for (let f of filter) {
            let pickPriorityByTime = {
              pickPriorityLevel: f.pickprioritytimelevel,
              pickPriorityTime: f.pickprioritytime
            }
            pickPriorityByTimes.push(pickPriorityByTime);

            if (f.pickprioritytime.getTime() < f.currtime.getTime()) {
              pickPriorityTime = f.pickprioritytime;
              priorityTimeLevel = f.pickprioritytimelevel;
            } else if (f.currtime.getTime() < f.pickprioritytime.getTime() && f.pickprioritytime.getTime() < pickPriorityTime.getTime()) {
              pickPriorityTime = f.pickprioritytime;
            }
          }
        }

        // sql = `select messageType, message from ${this.SCHEMA + '.'}pr_remark where pickRequestId = ? and messageType = ? `;
        // let prm_message = await UtilModel.getInstance().sqlQuerySync(sql, [orderGroupByPickRequestId[o][0].pickrequestid, '0004']);

        let prm_message = results.filter(i => i.pickrequestid === orderGroupByPickRequestId[o][0].pickrequestid);

        if (prm_message.length == 0) {
          prm_message = [{
            messageType: "",
            message: ""
          }]
        } else {
          let messageArray: any = [];
          prm_message.forEach(prm => { messageArray.push({ messageType: prm.messagetype, message: prm.message }) });
          prm_message = messageArray;
        }

        // let locbarcode = '';
        // if (position == 60) {
        //   let sql = `
        //     select pdi.locbarcode from ${this.SCHEMA}.pick_task_detail pd
        //     join ${this.SCHEMA}.pick_task_detail_item pdi on pdi.picktaskdetailid = pd.id
        //     where pd.pickrequestid = ?
        //     order by pdi.updated_time desc, pdi.itemstatus desc limit 1
        //   `;
        //   let locData = await UtilModel.getInstance().sqlQuery(sql, [orderGroupByPickRequestId[o][0].pickrequestid]);
        //   if (locData.length > 0) {
        //     locbarcode = locData[0].locbarcode === null ? '' : locData[0].locbarcode;
        //   }
        // }

        let order = {
          pickRequestId: orderGroupByPickRequestId[o][0].pickrequestid,
          pickRequestDate: orderGroupByPickRequestId[o][0].pickrequestdate,
          subOrderKey: orderGroupByPickRequestId[o][0].suborderkey,
          sourceSubOrderId: orderGroupByPickRequestId[o][0].sourcesuborderid,
          orderKey: orderGroupByPickRequestId[o][0].orderkey,
          sourceOrderId: orderGroupByPickRequestId[o][0].sourceorderid,
          bu: orderGroupByPickRequestId[o][0].bu,
          pickAllFlag: orderGroupByPickRequestId[o][0].pickallflag,
          status: orderGroupByPickRequestId[o][0].reqstatus,
          pickPriorityType: orderGroupByPickRequestId[o][0].pickprioritytype,
          pickPriorityLevel: orderGroupByPickRequestId[o][0].pickprioritylevel,
          deliveryType: orderGroupByPickRequestId[o][0].deliverytype === null ? "" : this.getDeliveryType(orderGroupByPickRequestId[o][0].deliverytype),
          deliverySubType: orderGroupByPickRequestId[o][0].deliverysubtype === null ? "" : this.getDeliverySubType(orderGroupByPickRequestId[o][0].deliverysubtype),
          remark_message: prm_message[0],
          // locbarcode_latest: locbarcode,
          pickPriorityByTime: pickPriorityByTimes
        }

        orders.push(order);
      }
      // console.log('orders : ', orders.length);

      orders.sort(function (a, b) {
        return a.sourceOrderId.localeCompare(b.sourceOrderId);
      });

      if (task_id) {
        console.log(`>>> (${'k : ' + k}) taskReduceGroupByTaskId[k] : `, taskReduceGroupByTaskId[k]);
        for (let r of taskReduceGroupByTaskId[k]) {
          let details = taskGroupByTaskId[k]
            .filter(x => x.pickrequestid == r.pickrequestid && x.itemkey == r.itemkey)
            .map(x => ({ id: x.taskitemid, status: x.taskitemstatus, reason: x.taskstatusreason, pickBarcode: x.taskitempickbarcode }));
          // console.log('details : ', details);

          let item = {
            pickRequestId: r.pickrequestid,
            pickMode: r.pickmode,
            itemId: r.itemid,
            itemKey: r.itemkey,
            sourceItemId: r.sourceitemid,
            sourceItemNumber: r.sourceitemnumber,
            barcode: r.barcode,
            subbarcode: r.subbarcode,
            sku: r.sku,
            sourcePrice: r.sourceprice == null ? 0 : r.sourceprice,
            standardPrice: r.standardprice == null ? 0 : r.standardprice,
            currency: r.currency == null ? '' : r.currency,
            color: r.color == null ? '' : r.color,
            gender: r.gender == null ? '' : r.gender,
            brandCode: r.brandcode == null ? '' : r.brandcode,
            itemLocCode: r.itemloccode == null ? '' : r.itemloccode,
            itemLocDesc: r.itemlocdesc == null ? '' : r.itemlocdesc,
            itemAreaCode: r.itemareacode == null ? '' : r.itemareacode,
            itemAreaDesc: r.itemareadesc == null ? '' : r.itemareadesc,
            size: r.size == null ? '' : r.size,
            qty: parseInt(r.qty),
            qtyUnit: r.qtyunit,
            weight: r.weight == null ? 0 : r.weight,
            weightUnit: r.weightunit == null ? '' : r.weightunit,
            weightedItem: r.weighteditem == null ? '' : r.weighteditem,
            productNameTH: r.productnameth,
            productNameEN: r.productnameen,
            productNameIT: r.productnameit == null ? '' : r.productnameit,
            productNameDE: r.productnamede == null ? '' : r.productnamede,
            imageUrl: r.imageurl,
            locBarcode: r.locbarcode || ' ',
            cat1: r.cat1,
            cat2: r.cat2,
            cat3: r.cat3,
            cat4: r.cat4,
            cat5: r.cat5,
            cat6: r.cat6,
            brand: r.brand,
            zone: r.zone,
            status: r.itemstatus,
            statusReason: r.statusreason,
            statusDate: dateToStrDate(r.itemstatusdate),
            itemStatus: details,
            remark_message: {
              message: r.itemmessage,
              messageType: r.itemmessagetype,
            }
          }
          items.push(item);
        }
      }
      // console.log('items : ', items[0]);

      if (task_id && 'A' == scope) {
        let allocationFilter = allocationReduce.filter((x: any) => x.taskid == taskReduceGroupByTaskId[k][0].taskid)
          .map(x => ({ taskId: x.taskid, pickAllocationTypeSeq: x.pickallocationtypeseq, pickAllocationType: x.pickallocationtype, pickCatLvl: x.pickcatlvl }))
          .sort((x, y) => { return x.pickAllocationTypeSeq - y.pickAllocationTypeSeq });

        let show = true;
        for (let a of allocationFilter) {
          // console.log('a.pickAllocationType : ', a.pickAllocationType);
          if (a.pickAllocationType == PICK_ALLOCATION_TYPE.BY_BRAND) {
            for (let i of items) {
              // not found match brand or A on each item.
              if (myProfile.filter((x: any) => x.brand == i.brand || x.brand == 'A').length == 0) {
                show = false;
                break;
              }
            }

            // if (items.filter(x => x.brand != myProfile[0].brand).length > 0 && myProfile[0].brand != 'A') {
            //   show = false;
            //   break;
            // }
          } else if (a.pickAllocationType == PICK_ALLOCATION_TYPE.BY_ZONE) {
            for (let i of items) {
              // not found match zone or A on each item.
              if (myProfile.filter((x: any) => x.zone == i.zone || x.zone == 'A').length == 0) {
                show = false;
                break;
              }
            }

            // if (items.filter(x => x.zone != myProfile[0].zone).length > 0 && myProfile[0].zone != 'A') {
            //   show = false;
            //   break;
            // }
          } else if (a.pickAllocationType == PICK_ALLOCATION_TYPE.BY_CATEGORY) {
            for (let i of items) {
              // not found match cat or A on each item.
              if (myProfile.filter((x: any) => i[`cat${a.pickCatLvl}`] == x[`cat_0${a.pickCatLvl}`] || x[`cat_0${a.pickCatLvl}`] == 'A').length == 0) {
                show = false;
                break;
              }
            }

            // if (items.filter(x => x[`cat${a.pickCatLvl}`] != myProfile[0][`cat_0${a.pickCatLvl}`]).length > 0 && myProfile[0][`cat_0${a.pickCatLvl}`] != 'A') {
            //   show = false;
            //   break;
            // }
          } else if (a.pickAllocationType == PICK_ALLOCATION_TYPE.NONE) {
            if (myProfile.filter((x: any) =>
              'A' == x.cat_01 &&
              'A' == x.cat_02 &&
              'A' == x.cat_03 &&
              'A' == x.cat_04 &&
              'A' == x.cat_05 &&
              'A' == x.zone &&
              'A' == x.brand
            ).length == 0) {
              show = false;
              break;
            }

            // if (
            //   'A' != myProfile[0].cat_01 ||
            //   'A' != myProfile[0].cat_02 ||
            //   'A' != myProfile[0].cat_03 ||
            //   'A' != myProfile[0].cat_04 ||
            //   'A' != myProfile[0].cat_05 ||
            //   'A' != myProfile[0].zone ||
            //   'A' != myProfile[0].brand
            // ) {
            //   show = false;
            //   break;
            // }
          }
        }

        if (!show) {
          continue;
        }
      }


      // requirement fix priority to medium when task is multiorder/wave
      if (orders.length > 1) {
        let priorityLevelHigh = orders.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.HIGH);
        let priorityLevelMedium = orders.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.MEDIUM);
        let priorityLevelNormal = orders.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.NORMAL);

        if (priorityLevelHigh.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.HIGH;
        } else if (priorityLevelMedium.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.MEDIUM;
        } else if (priorityLevelNormal.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.NORMAL;
        }
      }

      if (pickPriorityType == PICK_PRIORITY_TYPE.FIXED) {
        priority += 100000;

        if (pickPriorityLevel == PICK_PRIORITY_LEVEL.HIGH) {
          priority += 30000;
        } else if (pickPriorityLevel == PICK_PRIORITY_LEVEL.MEDIUM) {
          priority += 20000;
        } else if (pickPriorityLevel == PICK_PRIORITY_LEVEL.NORMAL) {
          priority += 10000;
        }
      } else if (pickPriorityType == PICK_PRIORITY_TYPE.TIME_TO_DELIVERY) {
        priority += 100000;

        if (priorityTimeLevel == PICK_PRIORITY_LEVEL.HIGH) {
          priority += 30000;
        } else if (priorityTimeLevel == PICK_PRIORITY_LEVEL.MEDIUM) {
          priority += 20000;
        } else if (priorityTimeLevel == PICK_PRIORITY_LEVEL.NORMAL) {
          priority += 10000;
        }

      }

      let timelife = Math.floor((now.getTime() - taskReduceGroupByTaskId[k][0].taskcreateddate.getTime()) / 3600000);
      priority += timelife;

      if (!task_id) {
        sql = `select count(pd.id) cnt from ${this.SCHEMA + '.'}pick_task_detail pd where pd.picktaskid = ?`;
        let countResp = await UtilModel.getInstance().sqlQuery(sql, [taskReduceGroupByTaskId[k][0].taskid]);
        let itemCnt = parseInt(countResp[0].cnt);
        do {
          items.push({});
          itemCnt -= 1;
        } while (itemCnt > 0);
      }

      let task = {
        taskId: taskReduceGroupByTaskId[k][0].taskid,
        wave_id: taskReduceGroupByTaskId[k][0].wave_id,
        multiorder_id: taskReduceGroupByTaskId[k][0].multiorder_id,
        windowStartTime: taskReduceGroupByTaskId[k][0].windowstarttime,
        startTime: taskReduceGroupByTaskId[k][0].starttime,
        priority: priority,
        priorityTimeLevel: priorityTimeLevel,
        createdDate: dateToStrDate3(taskReduceGroupByTaskId[k][0].taskcreateddate),
        taskCreatedDate: taskReduceGroupByTaskId[k][0].taskcreateddate,
        channel: this.getChannels(taskReduceGroupByTaskId[k][0].channel),
        orders: orders,
        status: taskReduceGroupByTaskId[k][0].taskstatus,
        statusReason: '',
        statusDate: dateToStrDate3(taskReduceGroupByTaskId[k][0].taskstatusdate),
        // itemsCount: parseInt(countResp[0].cnt),
        items: items
      };

      tasks.push(task);
    }

    tasks.sort(function (a, b) {
      let aPriority = a.priority;
      let bPriority = b.priority;
      let aStartTime = new Date(a.windowStartTime).getTime();
      let bStartTime = new Date(b.windowStartTime).getTime();
      let aCreatedDate = new Date(a.taskCreatedDate).getTime();
      let bCreatedDate = new Date(b.taskCreatedDate).getTime();

      let aStatus = a.status;
      let bStatus = b.status;

      let aScopePriority = aPriority >= 130000 ? 1 : aPriority >= 120000 && aPriority < 130000 ? 2 : aPriority >= 110000 && aPriority < 120000 ? 3 : 4;
      let bScopePriority = bPriority >= 130000 ? 1 : bPriority >= 120000 && bPriority < 130000 ? 2 : bPriority >= 110000 && bPriority < 120000 ? 3 : 4;
      let sameScopePriority: boolean = false;
      if (aScopePriority == bScopePriority) {
        sameScopePriority = true;
      }

      if (scope = 'R' && position == 60) {
        return (aStatus > bStatus) ? -1 : 1;
      } else if (sameScopePriority && aStartTime != bStartTime) {
        return (aStartTime < bStartTime) ? -1 : 1;
      } else if (!sameScopePriority) {
        return (aPriority > bPriority) ? -1 : 1;
      } else {
        return (aCreatedDate < bCreatedDate) ? -1 : 1;
      }

    });

    console.log('TIME 2 : ', new Date().toUTCString());
    return tasks;
  }

  getPickTaskListSQL(taskId: number) {
    let sql = ``;
    if (taskId) {
      sql = `
        select
          pr.pickRequestId, to_char(pr.pickRequestDate, 'YYYY-MM-DD HH24:MI') as pickRequestDate, pr.subOrderKey, pr.sourceSubOrderId,
          pr.orderKey, pr.sourceOrderId, pr.bu, pr.status reqStatus, pr.deliveryType, pr.deliverySubType,
          ppr.pickMode, ppr.pickAllFlag,
          ppa.pickAllocationTypeSeq, ppa.pickAllocationType, ppa.pickCatLvl,
          pp.pickPriorityType, pp.pickPriorityLevel,
          pt.id taskId, pt.status taskStatus, pt.wave_id, pt.multiorder_id, pt.created_time taskCreatedDate, pt.updated_time taskStatusDate,
          pd.id as taskDetailId,
          pi.pickBarcode as taskItemPickBarcode,
          pi.id as taskItemId, pi.itemStatus as taskItemStatus, pi.statusReason as taskStatusReason,
          i.id itemId, i.itemKey, i.sourceItemId, i.sourceItemNumber, i.barcode, i.sku, i.sourcePrice, i.standardPrice, i.currency, i.color, i.gender, i.itemLocCode, i.itemLocDesc, i.itemAreaCode, i.itemAreaDesc,
          i.size, i.qty, i.qtyUnit, i.weight, i.weightUnit, i.weightedItem, i.productNameTH, i.productNameEN, i.productNameIT, i.productNameDE, i.imageUrl, i.locBarcode,
          i.cat1, i.cat2, i.cat3, i.cat4, i.cat5, i.cat6, i.brand, i.zone, i.status itemStatus,
          i.statusReason, i.statusDate itemStatusDate, i.subbarcode, i.brandCode,
          to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI') windowStartTime, concat(to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI'), ' - ', to_char(pppw.endTime, 'YYYY-MM-DD HH24:MI')) startTime,
          prm.message , prm.messagetype, pri.message itemMessage, pri.messagetype itemMessageType,
          ppt.pickPriorityLevel as pickPriorityTimeLevel,
          ppt.pickPriorityTime at time zone 'Asia/Bangkok' pickPriorityTime,
          current_timestamp at time zone 'Asia/Bangkok' currTime
        from ${this.SCHEMA + '.'}pick_request pr
        inner join ${this.SCHEMA + '.'}pr_pick_params ppr
        on pr.pickRequestId = ppr.pickRequestId
        inner join ${this.SCHEMA + '.'}pr_pickparams_allocation ppa
        on pr.pickRequestId = ppa.pickRequestId
        inner join ${this.SCHEMA + '.'}pr_pickparams_priority pp
        on pr.pickRequestId = pp.pickRequestId
        left join ${this.SCHEMA + '.'}pr_pickparams_priority_time ppt
        on pr.pickRequestId = ppt.pickRequestId
        inner join ${this.SCHEMA + '.'}pick_task_detail pd
        on pr.pickRequestId = pd.pickRequestId
        inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
        on pd.id = pi.pickTaskDetailId
        inner join ${this.SCHEMA + '.'}pick_task pt
        on pt.id = pd.pickTaskId
        inner join ${this.SCHEMA + '.'}pr_items i
        on pd.pickRequestId = i.pickRequestId
        and pd.itemKey = i.itemKey
        left join ${this.SCHEMA + '.'}pr_pickparams_priority_window pppw
        on pppw.pickRequestId = pd.pickRequestId
        left join ${this.SCHEMA + '.'}pr_remark prm
        on prm.pickrequestid = pr.pickrequestid
        and prm.messagetype = '0007'
        left join ${this.SCHEMA + '.'}pr_remark_item pri on pri.pickrequestid = pr.pickrequestid
        and pri.itemkey = i.itemkey
        and pri.messagetype = '0006'
        where 1 = 1
      `;
    } else {
      sql = `
        select
          row_number() OVER (ORDER BY pt.id) AS taskDetailId, row_number() OVER (ORDER BY pt.id) AS taskItemId,
          pr.pickRequestId, to_char(pr.pickRequestDate, 'YYYY-MM-DD HH24:MI') as pickRequestDate, pr.subOrderKey, pr.sourceSubOrderId, pr.orderKey, pr.sourceOrderId,
          pr.bu, pr.status reqStatus, pr.deliveryType, pr.deliverySubType,
          ppr.pickMode, ppr.pickAllFlag,
          ppa.pickAllocationTypeSeq, ppa.pickAllocationType, ppa.pickCatLvl,
          pp.pickPriorityType, pp.pickPriorityLevel,
          ppt.pickPriorityLevel as pickPriorityTimeLevel,
          pt.id taskId, pt.status taskStatus, pt.wave_id, pt.multiorder_id, pt.created_time taskCreatedDate, pt.updated_time taskStatusDate,
          to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI') windowStartTime, concat(to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI'), ' - ', to_char(pppw.endTime, 'YYYY-MM-DD HH24:MI')) startTime,
          prm.message , prm.messagetype , pr.channel,
          ppt.pickPriorityTime at time zone 'Asia/Bangkok' pickPriorityTime,
          current_timestamp at time zone 'Asia/Bangkok' currTime
        from ${this.SCHEMA + '.'}pick_request pr
        inner join ${this.SCHEMA + '.'}pr_pick_params ppr on pr.pickRequestId = ppr.pickRequestId
        inner join ${this.SCHEMA + '.'}pr_pickparams_allocation ppa on pr.pickRequestId = ppa.pickRequestId
        inner join ${this.SCHEMA + '.'}pr_pickparams_priority pp on pr.pickRequestId = pp.pickRequestId
        left join ${this.SCHEMA + '.'}pr_pickparams_priority_time ppt on pr.pickRequestId = ppt.pickRequestId
        inner join (
          select ipdt.pickTaskId, ipdt.pickRequestId
          from ${this.SCHEMA + '.'}pick_task ipt, ${this.SCHEMA + '.'}pick_task_detail ipdt
          where ipt.id = ipdt.picktaskid and ipt.status in (STATUSTXT) group by ipdt.pickTaskId, ipdt.pickRequestId
        ) pd on pd.pickRequestId = pr.pickRequestId
        inner join ${this.SCHEMA + '.'}pick_task pt on pt.id = pd.pickTaskId
        left join ${this.SCHEMA + '.'}pr_pickparams_priority_window pppw on pppw.pickRequestId = pd.pickRequestId
        left join ${this.SCHEMA + '.'}pr_remark prm on prm.pickrequestid = pr.pickrequestid and prm.messagetype = '0004'
        where 1 = 1
      `;
    }

    return sql;
  }

  async setCancelPickItem(reqBody: any) {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }

          try {
            let isCanCancel = await this.checkItemAvaliableToCancel(conn, reqBody);
            console.log("[setCancelPickItem] isCanCancel ========>>>>>> ", isCanCancel);
            if (!isCanCancel) {
              LogUtil.logError('post', '/setCancelItemStatus', reqBody, 'into CASE : All cancel Ignore');
              resolve({
                affectedRows: 0
              });
              return;
            }


            let affectedRows = 0;
            let sourceSubOrderId = reqBody.lineStatusUpdate.sourceSubOrderId;
            for (let data of reqBody.lineStatusUpdate.items) {

              /************ update status to pick task detail item ************/

              let sql = `
                select pr.pickRequestId, pd.pickTaskId, i.itemKey, pi.*
                from ${this.SCHEMA + '.'}pick_request pr
                join ${this.SCHEMA + '.'}pr_items i on pr.pickRequestId = i.pickRequestId
                join ${this.SCHEMA + '.'}pick_task_detail pd on i.pickRequestId = pd.pickRequestId and i.itemKey = pd.itemKey
                join ${this.SCHEMA + '.'}pick_task_detail_item pi on pd.id = pi.pickTaskDetailId
                where 1=1
                  and pr.sourceSubOrderId = ?
                  and i.itemKey = ?
              `;

              let params = [
                sourceSubOrderId,
                data.itemKey
              ];

              let res = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              if (res.length == 0) {
                LogUtil.logError('post', '/setCancelItemStatus', sql, sourceSubOrderId + ':' + data.itemKey);
                throw new Error(ERR_canItemNotFound);
              }

              sql = `
                select *
                from ${this.SCHEMA + '.'}pr_items
                where pickRequestId = ?
                and itemKey = ?
              `;

              params = [
                res[0].pickrequestid,
                res[0].itemkey
              ];

              let chk = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              if (chk[0].qty < data.qty) {
                throw new Error(ERR_qtyLE0);
              }

              // sort DECLINED < NEW < ... < CANCEL_BY_FM
              let priorityItems = res.sort((x: any, y: any) => {
                let p1 = x.itemstatus;
                let p2 = y.itemstatus;
                if (x.itemstatus == ITEM_STATUS.DECLINED) {
                  p1 = '0000';
                  if (x.statusreason == 'CANCEL_BY_FM') {
                    p1 = '0010';
                  }
                }

                if (y.itemstatus == ITEM_STATUS.DECLINED) {
                  p2 = '0000';
                  if (y.statusreason == 'CANCEL_BY_FM') {
                    p2 = '0010'
                  }
                }
                return p1 - p2;
              });

              //console.log(`--> cancel by priority start here <--`);
              //console.log(JSON.stringify(priorityItems));
              //console.log(`--> cancel by priority end here <--`);

              sql = `
                update ${this.SCHEMA + '.'}pick_task_detail_item
                set itemStatus = ?, statusReason = ?
                where id in
              `;

              let cancelItemIds: any = priorityItems.map((x: any) => x.id).slice(0, data.qty);
              //console.log(`--> cancel ids: ${cancelItemIds} <--`);
              params = [
                ITEM_STATUS.DECLINED,
                'CANCEL_BY_FM'
              ];

              let sql_in: any = [];
              cancelItemIds.forEach(i => {
                params.push(i);
                sql_in.push('?');
              });
              sql += `(${sql_in.join(', ')})`;

              await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then((res: any) => affectedRows += res.rowCount);
              sql = `
                update ${this.SCHEMA + '.'}pr_items
                set status = ?
                where pickRequestId = ?
                and itemKey = ?
              `;

              // default as declined
              let status = ITEM_STATUS.DECLINED;
              let procItems = priorityItems.slice(data.qty, priorityItems.length).filter((x: any) => x.itemstatus != ITEM_STATUS.DECLINED);
              //console.log(`--> remaining items without declined start here <--`);
              //console.log(JSON.stringify(procItems));
              //console.log(`--> remaining items without declined end here <--`);
              // found one or more item that not declined -> set as partial
              if (procItems.length > 0) {
                //console.log(`--> found remaining item, set status to partial`);
                status = ITEM_STATUS.PARTIAL;

                procItems = procItems.filter((x: any) => x.itemStatus != ITEM_STATUS.PICKED);
                // not found unpicked item -> set as picked
                if (procItems.length == 0) {
                  //console.log(`--> not found remaining item after filter picked item, set status to picked`);
                  status = ITEM_STATUS.PICKED;
                }
              }

              //console.log(`set pr_items status: ${status}`);
              params = [
                status,
                res[0].pickrequestid,
                res[0].itemkey
              ];

              await UtilModel.getInstance().sqlConnQuery(conn, sql, params);

              sql = `select pt.ID, pt.pickRequestId from ${this.SCHEMA + '.'}pick_task pt where pt.pickRequestId = ?`;
              params = [res[0].pickrequestid];

              let tasks = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
              for (let t of tasks) {
                sql = `
                  select distinct pi.itemStatus, pt.status from ${this.SCHEMA + '.'}pick_task pt
                  inner join ${this.SCHEMA + '.'}pick_task_detail pd
                  on pt.id = pd.pickTaskId
                  inner join ${this.SCHEMA + '.'}pick_task_detail_item pi
                  on pd.id = pi.pickTaskDetailId
                  inner join ${this.SCHEMA + '.'}pick_request pr
                  on pd.pickRequestId = pr.pickRequestId
                  inner join ${this.SCHEMA + '.'}pr_items i
                  on pd.pickRequestId = i.pickRequestId
                  and pd.itemKey = i.itemKey
                  where pt.id = ?
                  and pi.itemStatus <> ?
                `;

                params = [t.id, ITEM_STATUS.DECLINED];
                let results = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                //console.log('--> remain item status after cancel <--');
                //console.log(results);
                let taskStatus = TASK_STATUS.PENDING;
                /**
                 * @KLA
                 * ADD item status canceled for check task too.
                 *
                 * @Tar 2019-08-21
                 * Remove check status on result, check sql instead.
                 */
                if (results.length == 0) {
                  taskStatus = TASK_STATUS.CANCELED;
                } else if (results.length == 1 && results[0].itemstatus == ITEM_STATUS.NEW && results[0].status == TASK_STATUS.PENDING) {
                  taskStatus = TASK_STATUS.PENDING;
                } else if (results.length == 1 && results[0].itemstatus == ITEM_STATUS.NEW && results[0].status == TASK_STATUS.NEW) {
                  taskStatus = TASK_STATUS.NEW;
                } else if (results.length == 1 && results[0].itemstatus == ITEM_STATUS.PICKED) {
                  taskStatus = TASK_STATUS.PICKED;
                } else {
                  taskStatus = TASK_STATUS.PARTIAL;
                }

                params = [taskStatus, t.id];
                sql = `update ${this.SCHEMA + '.'}pick_task set status = ? where id = ?`;
                await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
                //console.log('log-row1222====>');
                //console.log(taskStatus);
                LogUtil.logSnowFlake(conn, data.task_id, taskStatus, 'function setCancelPickItem');
              }

              await this.addResponseQueue(conn, res[0].pickrequestid, 0);

              await UtilModel.getInstance().sqlConnCommit(conn);
              //this.batchPickResponse();
            }

            resolve({
              affectedRows: affectedRows
            });
          } catch (err) {
            //console.log(err);
            conn.query('ROLLBACK');
            reject(err);
          } finally {
            done();
          }


        });
      });
    });
  }

  async getActiveUser(bu: string, loc: string) {
    console.log(`[getActiveUser] url : ${FM_WS.GET_ACTIVE_USER}/${bu}/${loc}`)
    return await axios.get(
      `${FM_WS.GET_ACTIVE_USER}/${bu}/${loc}`,
      {
        timeout: 1000,
        headers: {
          client_id: FM_AUTHORITIES.CLIENT_ID,
          client_secret: FM_AUTHORITIES.CLIENT_SECRET
        }
      }
    );
  }

  async getActiveUserIgnoreStatus(bu: string, loc: string) {
    return await axios.get(
      `${FM_WS.GET_ACTIVE_USER}/${bu}/${loc}?ignoreStatus=Y`,
      {
        timeout: 3000,
        headers: {
          client_id: FM_AUTHORITIES.CLIENT_ID,
          client_secret: FM_AUTHORITIES.CLIENT_SECRET
        }
      }
    );
  }

  async pushAssign(conn: any, pickSubAssignType: any, pickTaskId: any, userAssgins: any, filterActiveUser: any, shuffleActiveUser: any) {
    //console.log('-- call push assign for by order --');
    let sql: string;

    if (filterActiveUser.length == 0) {
      return userAssgins;
    }

    if (pickSubAssignType == SUB_ASSIGN_TYPE.LEAST_LOAD) {
      sql = `
        select pt.user_id, count(0) as item_count from ${this.SCHEMA + '.'}pick_task pt
        join ${this.SCHEMA + '.'}pick_task_detail pd
        on pt.id = pd.pickTaskId
        join ${this.SCHEMA + '.'}pick_task_detail_item pi
        on pd.id = pi.pickTaskDetailId
        where pt.user_id in (?)
        and pi.itemStatus = ?
        group by pt.user_id
      `;

      let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

      let itemCountPerUser = await UtilModel.getInstance().sqlConnQuery(conn, sql, [users.map((x: any) => x.userId), ITEM_STATUS.NEW]);
      for await (let u of itemCountPerUser) {
        let i = users.findIndex((x: any) => x.userid == u.user_id);
        if (i >= 0) {
          users[i].count = u.item_count;
        }
      }

      //console.log('-- load count --');
      //console.log(itemCountPerUser);

      //console.log('-- users with count --');
      //console.log(users);

      let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
        return (obj.count < res.count) ? obj : res;
      });

      //console.log('-- after reduce --');
      //console.log(reduce);

      sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
      await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);
    } else if (pickSubAssignType == SUB_ASSIGN_TYPE.EQUAL_LOAD) {
      let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));
      users.sort((a, b) => a.userId.localeCompare(b.userId));

      for (let u of userAssgins) {
        let i = users.findIndex((x: any) => x.userId == u.userId);
        //console.log(`found assign user at index: ${i}`);
        if (i >= 0) {
          users[i].count = u.count;
        }
      }

      //console.log(`user with item count:-`);
      //console.log(users);

      let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
        return (obj.count < res.count) ? obj : res;
      });

      sql = `
        select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
        inner join ${this.SCHEMA + '.'}pr_items i
        on pi.pickRequestId = i.pickRequestId
        and pi.itemKey = i.itemKey
        and pi.pickItemId = i.id
        where pi.pickTaskId = ?
      `;

      let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
      sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
      await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

      let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
      if (i < 0) {
        userAssgins.push({
          userId: reduce.userId,
          // count: 1
          count: cnt[0].cnt
        });
      } else {
        // userAssgins[i].count = userAssgins[i].count + 1;
        userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
      }

      //console.log(`assign userId: ${reduce.userId} to userAssigns:-`);
      //console.log(userAssgins);
      //console.log('\n');
    } else if (pickSubAssignType == SUB_ASSIGN_TYPE.ROUND_ROBIN) {
      let users = shuffleActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

      for (let u of userAssgins) {
        let i = users.findIndex((x: any) => x.userId == u.userId);
        //console.log(`found assign user at index: ${i}`);
        if (i >= 0) {
          users[i].count = u.count;
        }
      }

      //console.log(`user with item count:-`);
      //console.log(users);

      let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
        return (obj.count < res.count) ? obj : res;
      });

      sql = `
        select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
        inner join ${this.SCHEMA + '.'}pr_items i
        on pi.pickRequestId = i.pickRequestId
        and pi.itemKey = i.itemKey
        and pi.pickItemId = i.id
        where pi.pickTaskId = ?
      `;

      let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
      sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
      await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

      let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
      if (i < 0) {
        userAssgins.push({
          userId: reduce.userId,
          // count: 1
          count: cnt[0].cnt
        });
      } else {
        // userAssgins[i].count = userAssgins[i].count + 1;
        userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
      }

      //console.log(`assign userId: ${reduce.userId} to userAssigns:-`);
      //console.log(userAssgins);
      //console.log('\n');
    }

    return userAssgins;
  }

  async checkItemAvaliableToCancel(conn: any, reqBody: any) {
    //console.log("[checkItemAvaliableToCancel] reqBody ===>>> ", reqBody);
    let r = reqBody.lineStatusUpdate;

    let qty_req: number = 0;
    r.items.forEach(i => qty_req += i.qty);

    let sql = `
        select
          max(pt.user_id) user_id,
          pt.status taskstatus,
          pdi.itemStatus,
          pdi.statusReason,
          count(0) as cnt
        from ${this.SCHEMA + '.'}pick_request pr
        join ${this.SCHEMA + '.'}pick_task_detail pd on pd.pickRequestId = pr.pickRequestId
        join ${this.SCHEMA + '.'}pick_task pt on pt.ID = pd.pickTaskId
        join ${this.SCHEMA + '.'}pick_task_detail_item pdi on pdi.pickTaskDetailId = pd.ID
        where
          pr.sourceSubOrderId = ?
        group by
          pt.status,
          pdi.itemStatus,
          pdi.statusReason,
          pt.user_id
      `;

    let arr_res = await UtilModel.getInstance().sqlConnQuery(conn, sql, [r.sourceSubOrderId]);
    console.log('[checkItemAvaliableToCancel] arr_res : ', arr_res);

    if (arr_res[0].taskstatus != TASK_STATUS.NEW) return false;

    let all_qty: number = 0;
    let new_item_qty: number = 0;
    let cancel_qty: number = 0;
    let another_item_qty: number = 0;

    arr_res.forEach(i => {
      all_qty += i.cnt
      cancel_qty += (i.itemstatus === ITEM_STATUS.DECLINED && i.statusreason === 'CANCEL_BY_FM') ? i.cnt : 0;
      new_item_qty += (i.itemstatus === ITEM_STATUS.NEW) ? i.cnt : 0;
      another_item_qty += (i.itemstatus !== ITEM_STATUS.NEW && !(i.itemstatus === ITEM_STATUS.DECLINED && i.statusreason === 'CANCEL_BY_FM')) ? i.cnt : 0;
    });

    //hotfix if assigned
    if (arr_res.length > 0 && arr_res[0].user_id != "" && arr_res[0].user_id != null) {

      //if cancel all
      if (qty_req >= all_qty) {
        return false;
      }

      //if last one cxl
      if ((all_qty - cancel_qty) <= 1) {
        return false;
      }

    } else {

      //no cancel item available
      //1 < 4 - 3 : false
      //2 < 4 - 1 : true
      /*if (qty_req < (all_qty - cancel_qty)) {
        return false;
      }*/
    }

    // /**
    //  * @Poom
    //  * hotfix if assigned then ignore
    //  * for V5
    //  */
    // if (arr_res[0].user_id != "") return false;

    return true;
  }

  async getDeclineReason(bu: string) {
    try {
      let sql = `select id, reason_key, reason_valueTH, reason_valueEN, reason_valuevn from ${this.SCHEMA + '.'}ms_decline_reason where sourceBu = ? order by priority asc`;
      let res = await UtilModel.getInstance().sqlQuery(sql, [bu]);

      return res;
    } catch (error) {
      LogUtil.logError('get', '/getDeclineReason', `Error`, error.message);
      return [];
    }
  }

  async checkAppVersion(appVersion: string) {
    let checkIsPickingModuleVersion = await this.pickingAppVersion(appVersion);
    if (checkIsPickingModuleVersion != '') {
      return '401,' + checkIsPickingModuleVersion;
    }

    let checkIsPackingModuleVersion = await this.packingAppVersion(appVersion);
    if (checkIsPackingModuleVersion != '') {
      return '401,' + checkIsPackingModuleVersion;
    }

    let checkIsCncModuleVersion = await this.cncAppVersion(appVersion);
    if (checkIsCncModuleVersion != '') {
      return '401,' + checkIsCncModuleVersion;
    }

    return '';
  }

  async pickingAppVersion(appVersion: string) {
    let sql = `select api_version from db_version where app_version = ? order by updated_time desc`;

    let res = await UtilModel.getInstance().sqlQuery(sql, [appVersion]);
    console.log(`[${new Date()}](pickAppVersion) result api version from appVersion=${appVersion} : `, res);

    let api_version = "";
    if (res.length > 0 && res[0].api_version == PROJECT_VERSION.API_VERSION) {
      api_version = res[0].api_version;

      sql = `select db_version from db_version where api_version = ? order by updated_time desc`;
      res = await UtilModel.getInstance().sqlQuery(sql, [api_version]);
      console.log(`[${new Date()}](pickAppVersion) result db version from apiVersion=${api_version} : `, res);

      if (res.length == 0 || res[0].db_version != PROJECT_VERSION.DB_VERSION) {
        return 'Database is not current version';
      }

    } else {

      sql = `select app_version from db_version order by updated_time desc`;
      res = await UtilModel.getInstance().sqlQuery(sql, []);

      return 'Please download current app version is ' + res[0].app_version;

    }

    return '';
  }

  async packingAppVersion(appVersion: string) {
    let packingAppVersionResponse = await axios.get('http://localhost:8001/getApiVersion?app_version=' + appVersion);
    console.log(`[${new Date()}](packingAppVersion) result db version from apiVersion=${appVersion} : `, packingAppVersionResponse);

    if (!packingAppVersionResponse.data || packingAppVersionResponse.data.result == 'E') {
      return packingAppVersionResponse.data.statusMessage;
    }

    return '';
  }

  async cncAppVersion(appVersion: string) {
    let cncAppVersionResponse = await axios.get('http://localhost:8003/getApiVersion?app_version=' + appVersion);
    console.log(`[${new Date()}](cncAppVersion) result db version from apiVersion=${appVersion} : `, cncAppVersionResponse);

    if (!cncAppVersionResponse.data || cncAppVersionResponse.data.result == 'E') {
      return cncAppVersionResponse.data.statusMessage;
    }

    return '';
  }

  async getDynamicFieldList(bu: string, loc: string, role: string) {
    try {
      let userType = 'picker';

      if (role.indexOf(':') > -1) userType = role.split(':')[1] == '60' ? 'suppicker' : userType;

      const result = {
        task_list: [],
        order_page: {
          task_detail: {
            show: [],
            expand: []
          },
          item_detail: {
            show: {
              left: [],
              right: []
            },
            expand: {
              left: [],
              right: []
            }
          }
        },
        item_page: {
          show: [],
          expand: []
        },
        search_list: [],
        filter_list: [],
        sort_item_list: [],
      }

      let sql = `
      select priority, type, part, col_header, expand, col_left, col_right
      from ${this.SCHEMA + '.'}m_display_col where bu = ? and role = ?
    `;
      let params = [bu, userType];
      let dynamicColList: any = await UtilModel.getInstance().sqlQuery(sql, params);
      console.log('[getDynamicFieldList] dynamicColList length : ', dynamicColList.length);

      if (dynamicColList.length == 0) {
        return {};
      }

      sql = `
      select
        lm_task, lm_order_h_co, lm_order_h_ex, lm_order_co_left, lm_order_co_right,
        lm_order_ex_left, lm_order_ex_right, lm_items, lm_filter, lm_search
      from ${this.SCHEMA + '.'}m_display_limit where bu = ?
    `;
      params = [bu];
      let dynamicLimit: any = await UtilModel.getInstance().sqlQuery(sql, params);

      if (dynamicLimit.length == 0) {
        dynamicLimit = {
          lm_task: 5,
          lm_order_h_co: 3,
          lm_order_h_ex: 5,
          lm_order_co_left: 3,
          lm_order_co_right: 3,
          lm_order_ex_left: 5,
          lm_order_ex_right: 5,
          lm_items: 20,
          lm_filter: 5,
          lm_search: 5
        }
      }
      dynamicLimit = dynamicLimit[0];

      let taskListArray = dynamicColList.filter(i => i.type == 'tasklist');
      let orderDetailHeaderCollapseArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '1' && i.expand == 'N');
      let orderDetailHeaderExpandArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '1' && i.expand == 'Y');
      let orderDetailCollapseLeftArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '2' && i.expand == 'N' && i.col_left != null && i.col_left != "");
      let orderDetailCollapseRightArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '2' && i.expand == 'N' && i.col_right != null && i.col_right != "");
      let orderDetailExpandLeftArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '2' && i.expand == 'Y' && i.col_left != null && i.col_left != "");
      let orderDetailExpandRightArray = dynamicColList.filter(i => i.type == 'orderdetail' && i.part == '2' && i.expand == 'Y' && i.col_right != null && i.col_right != "");
      let itemDetailArray = dynamicColList.filter(i => i.type == 'itemdetail');

      result.task_list = this.mappingValue(taskListArray, 'col_header', dynamicLimit.lm_task);
      result.order_page.task_detail.show = this.mappingValue(orderDetailHeaderCollapseArray, 'col_header', dynamicLimit.lm_order_h_co);
      result.order_page.task_detail.expand = this.mappingValue(orderDetailHeaderExpandArray, 'col_header', dynamicLimit.lm_order_h_ex);
      result.order_page.item_detail.show.left = this.mappingValue(orderDetailCollapseLeftArray, 'col_left', dynamicLimit.lm_order_co_left);
      result.order_page.item_detail.show.right = this.mappingValue(orderDetailCollapseRightArray, 'col_right', dynamicLimit.lm_order_co_right);
      result.order_page.item_detail.expand.left = this.mappingValue(orderDetailExpandLeftArray, 'col_left', dynamicLimit.lm_order_ex_left);
      result.order_page.item_detail.expand.right = this.mappingValue(orderDetailExpandRightArray, 'col_right', dynamicLimit.lm_order_ex_right);

      let itemDetailMappingArray = this.mappingValue(itemDetailArray, 'col_header', dynamicLimit.lm_items);
      if (itemDetailMappingArray.length > 0) {
        result.item_page.show = itemDetailMappingArray.slice(0, 3);
        result.item_page.expand = itemDetailMappingArray.slice(3, itemDetailMappingArray.length);
      }

      sql = `
      select priority, col_name, type
      from ${this.SCHEMA + '.'}m_display_sf
      where bu = ? and role = ?
    `;
      params = [bu, userType];
      let dynamicSearchFilter = await UtilModel.getInstance().sqlQuery(sql, params);
      console.log('dynamicSearchFilter length : ', dynamicSearchFilter.length);

      let dynamicFilterArray = dynamicSearchFilter.filter(i => i.type == 'sort');
      let dynamicSearchArray = dynamicSearchFilter.filter(i => i.type == 'filter');
      let dynamicSortItemsArray = dynamicSearchFilter.filter(i => i.type == 'sortitem');

      result.filter_list = this.mappingValue(dynamicFilterArray, 'col_name', dynamicLimit.lm_filter);
      result.search_list = this.mappingValue(dynamicSearchArray, 'col_name', dynamicLimit.lm_search);
      result.sort_item_list = this.mappingValue(dynamicSortItemsArray, 'col_name', dynamicLimit.lm_search);

      return result;
    } catch (error) {
      LogUtil.logError('get', '/getDynamicFieldList', `Error`, error.message);
      return [];
    }
  }

  async getMergeOrder(bu) {

    let orders = new Array();
    let remarks = new Array();
    let newdatas = new Array();

    let sql = `
    select distinct
    pr.id as "taskId"
    , pr.id
    , pr.status as  "taskStatus"
    , '' as "wave_id"
    , ''  as "multiorder_id"
    , pr.created_time as "createdDate"
    , pr.updated_time as "taskStatusDate"
    , to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI') windowStartTime
    , concat(to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI')
    , ' - '
    , to_char(pppw.endTime, 'YYYY-MM-DD HH24:MI')) startTime
    , pr.channel

    , pr.pickrequestid as "pickRequestId"
    , to_char(pr.pickRequestDate
    , 'YYYY-MM-DD HH24:MI') as "pickRequestDate"
    , pr.orderkey as "orderKey"
    , pr.sourceorderid as "sourceOrderId"
    , pr.subOrderKey as "subOrderKey"
    , pr.bu
    , ppr.pickAllFlag as "pickAllFlag"
    , pr.status

    , prm.message
    , prm.messagetype

    , pp.pickPriorityType as "pickPriorityType"
    , pp.pickPriorityLevel as "pickPriorityLevel"
    , pp.pickprioritytime as "pickprioritytime"

    from ${this.SCHEMA + '.'}pick_request pr
    inner join ${this.SCHEMA + '.'}pr_pick_params ppr on pr.pickRequestId = ppr.pickRequestId
    inner join ${this.SCHEMA + '.'}pr_pickparams_priority_window pppw on pppw.pickRequestId = pr.pickRequestId
    inner join ${this.SCHEMA + '.'}pr_pickparams_priority pp on pp.pickrequestid = pr.pickrequestid
    left join ${this.SCHEMA + '.'}pr_remark prm on prm.pickrequestid = pr.pickrequestid

    where pr.status = ?
    and ppr.pickmode = ?
    and pr.sourcebu = ?
    `;
    let results: any = await UtilModel.getInstance().sqlQuery(sql, [PICK_REQUEST_STATUS.NEW, PICK_MODE.MERGE_ORDER, bu]);
    console.log('[results]', results)

    let pickPriorityTimeReduce = [...results.reduce((x: any, y: any) => {
      const key = y.pickrequestid + '-' + y.pickPriorityLevel + '-' + dateToStrDate(y.pickprioritytime);
      const item = x.get(key) || Object.assign({}, y);
      return x.set(key, item);
    }, new Map).values()];
    console.log('pickPriorityTimeReduce : ', pickPriorityTimeReduce);

    let sqlSumQty = `
    select
       SUM(i.qty ) AS "countQty"
      ,COUNT (DISTINCT i.sku ) AS "countSku"
    from ${this.SCHEMA + '.'}pick_request pr
    inner join ${this.SCHEMA + '.'}pr_pick_params ppr on pr.pickRequestId = ppr.pickRequestId
    inner join ${this.SCHEMA + '.'}pr_items i on i.pickrequestid = pr.pickrequestid
    where pr.status = ?
    and ppr.pickmode = ?
    and pr.sourcebu = ?
    `
    let resultsSumQty: any = await UtilModel.getInstance().sqlQuery(sqlSumQty, [TASK_STATUS.NEW, PICK_MODE.MERGE_ORDER, bu]);

    let pickPriorityType = '0000';
    let pickPriorityLevel = '0000';
    let priority = 0;
    let priorityTimeLevel = '';

    for (var i = 0; i < results.length; i++) {

      if (results.length > 1) {
        let priorityLevelHigh = results.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.HIGH);
        let priorityLevelMedium = results.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.MEDIUM);
        let priorityLevelNormal = results.filter(i => i.pickPriorityLevel == PICK_PRIORITY_LEVEL.NORMAL);

        if (priorityLevelHigh.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.HIGH;
        } else if (priorityLevelMedium.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.MEDIUM;
        } else if (priorityLevelNormal.length > 0) {
          pickPriorityLevel = PICK_PRIORITY_LEVEL.NORMAL;
        }
      }

      if (results[i].pickPriorityType == PICK_PRIORITY_TYPE.FIXED) {
        priority += 100000;

        if (pickPriorityLevel == PICK_PRIORITY_LEVEL.HIGH) {
          priority += 30000;
        } else if (pickPriorityLevel == PICK_PRIORITY_LEVEL.MEDIUM) {
          priority += 20000;
        } else if (pickPriorityLevel == PICK_PRIORITY_LEVEL.NORMAL) {
          priority += 10000;
        }
      } else if (results[i].pickPriorityType == PICK_PRIORITY_TYPE.TIME_TO_DELIVERY) {
        priority += 100000;

        if (priorityTimeLevel == PICK_PRIORITY_LEVEL.HIGH) {
          priority += 30000;
        } else if (priorityTimeLevel == PICK_PRIORITY_LEVEL.MEDIUM) {
          priority += 20000;
        } else if (priorityTimeLevel == PICK_PRIORITY_LEVEL.NORMAL) {
          priority += 10000;
        }

      }

      console.log("======pickPriorityLevel======", i, pickPriorityLevel)
      console.log("======pickPriorityType======", i, pickPriorityType)



      const now = new Date();
      let timelife = Math.floor((now.getTime() - results[i].createdDate.getTime()) / 3600000);
      priority += timelife;

      console.log("======priority======", i, priority, timelife)


      let remark = {
        message: results[i].message,
        messagetype: results[i].messagetype
      }
      remarks.push(remark)

      let order = {
        pickRequestId: results[i].pickRequestId,
        pickRequestDate: results[i].pickRequestDate,
        orderKey: results[i].orderKey,
        sourceOrderId: results[i].sourceOrderId,
        subOrderKey: results[i].subOrderKey,
        bu: results[i].bu,
        pickAllFlag: results[i].pickAllFlag,
        status: results[i].status,
        pickPriorityType: results[i].pickPriorityType,
        pickPriorityLevel: results[i].pickPriorityLevel,
        pickprioritytime: results[i].pickprioritytime,
        remark_message: remarks[i]
      }
      orders.push(order);

      let newdata = {
        taskId: results[i].taskId,
        orderId: results[i].id,
        taskStatus: results[i].taskStatus,
        wave_id: results[i].wave_id,
        multiorder_id: results[i].multiorder_id,
        createdDate: results[i].createdDate,
        taskStatusDate: results[i].taskStatusDate,
        windowStartTime: results[i].windowstarttime,
        startTime: results[i].starttime,
        channel: results[i].channel,
        priority: priority,
        countQty: (resultsSumQty[0].countQty !== undefined ? parseInt(resultsSumQty[0].countQty) : ''),
        countSku: (resultsSumQty[0].countSku !== undefined ? resultsSumQty[0].countSku : ''),
        orders: [orders[i]]
      }

      newdatas.push(newdata)

      priority = 0
    }
    return newdatas;
  }

  async getMergeOrderDetail(task_id) {

    let orders = new Array()
    let items = new Array()
    let itemStatus = new Array()
    let newResults = {}
    let newRes = new Array()

    let sql = `
    select distinct
      pr.sourceOrderId as "sourceOrderId"
    , pr.sourceSubOrderId as "sourceSubOrderId"
    , ppr.pickAllFlag as "pickAllFlag"
    , pr.pickRequestId as "pickRequestId"
    , to_char(pr.pickRequestDate, 'YYYY-MM-DD HH24:MI') as "pickRequestDate"
    , pr.subOrderKey as "subOrderKey"
    , pr.orderKey as "orderKey"
    , pr.status as requestStatus
    , pr.deliveryType as "deliveryType"
    , pr.deliverySubType as "deliverySubType"
    , pr.bu as "bu"
    , '' as "wave_id"
    , pr.id as "taskId"
    , '' as "multiorder_id"
    , to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI') as "windowStartTime"
    , to_char(pppw.startTime, 'YYYY-MM-DD HH24:MI') as "windowStartTime"
    , pr.created_time as "taskCreatedDate"
    , pr.channel as "channel"

    , i.id as "itemId"
    , i.itemKey as "itemKey"
    , i.sourceItemId as "sourceItemId"
    , i.sourceItemNumber as "sourceItemNumber"
    , i.barcode as "barcode"
    , i.sku as "sku"
    , i.sourcePrice as "sourcePrice"
    , i.standardPrice as "standardPrice"
    , i.currency as "currency"
    , i.color as "color"
    , i.gender as "gender"
    , i.itemLocCode as "itemLocCode"
    , i.itemLocDesc as "itemLocDesc"
    , i.itemAreaCode as "itemAreaCode"
    , i.itemAreaDesc as "itemAreaDesc"
    , i.size as "size"
    , i.qty as "qty"
    , i.qtyUnit as "qtyUnit"
    , i.weight as "weight"
    , i.weightUnit as "weightUnit"
    , i.weightedItem as "weightedItem"
    , i.productNameTH as "productNameTH"
    , i.productNameEN as "productNameEN"
    , i.productNameIT as "productNameIT"
    , i.productNameDE as productNameDE
    , i.imageUrl as "imageUrl"
    , i.locBarcode as "locBarcode"
    , i.cat1 as "cat1"
    , i.cat2 as "cat2"
    , i.cat3 as "cat3"
    , i.cat4 as "cat4"
    , i.cat5 as "cat5"
    , i.cat6 as "cat6"
    , i.brand as "brand"
    , i.zone as "zone"
    , i.status as "itemStatus"
    , i.statusReason as "statusReason"
    , i.statusDate as "itemStatusDate"
    , i.subbarcode as "subbarcode"
    , i.brandCode as "brandCode"
    , '' as "pickBarcode"
    , '' as "id"
    , '' as "statusreason"
    , '' as "itemstatus"
    , pr.pickRequestId as "pickRequestId"

    from ${this.SCHEMA + '.'}pick_request pr
    inner join ${this.SCHEMA + '.'}pr_pick_params ppr on pr.pickRequestId = ppr.pickRequestId
    inner join ${this.SCHEMA + '.'}pr_items i on pr.pickRequestId = i.pickRequestId
    left join ${this.SCHEMA + '.'}pr_pickparams_priority_window pppw on pppw.pickRequestId = pr.pickRequestId
    where 1 = 1
  and pr.id = ?
    `

    let results: any = await UtilModel.getInstance().sqlQuery(sql, [task_id]);
    console.log('=================results====================', results)
    console.log('results[i].pickRequestId', results[0].pickRequestId)

    let sql2 = `
    select
      SUM(i.qty ) AS "countQty"
    , COUNT (DISTINCT i.sku ) AS "countSku"
    from ${this.SCHEMA + '.'}pr_items i
    where pickrequestid = ?
    `

    let results2: any = await UtilModel.getInstance().sqlQuery(sql2, [results[0].pickRequestId]);
    console.log('results2results2results2', results2)

    for (var i = 0; i < results.length; i++) {

      let order = {
        sourceOrderId: (results[i].sourceOrderId !== undefined ? results[i].sourceOrderId : ''),
        sourceSubOrderId: (results[i].sourceSubOrderId !== undefined ? results[i].sourceSubOrderId : ''),
        pickAllFlag: (results[i].pickAllFlag !== undefined ? results[i].pickAllFlag : ''),
        pickRequestId: (results[i].pickRequestId !== undefined ? results[i].pickRequestId : ''),
        pickRequestDate: (results[i].pickRequestDate !== undefined ? results[i].pickRequestDate : ''),
        subOrderKey: (results[i].subOrderKey !== undefined ? results[i].subOrderKey : ''),
        orderKey: (results[i].orderKey !== undefined ? results[i].orderKey : ''),
        requestStatus: (results[i].requestStatus !== undefined ? results[i].requestStatus : ''),
        deliveryType: (results[i].deliveryType !== undefined ? results[i].deliveryType : ''),
        deliverySubType: (results[i].deliverySubType !== undefined ? results[i].deliverySubType : ''),
        bu: (results[i].bu !== undefined ? results[i].bu : '')
      }
      orders.push(order);

      let iStatus = {
        id: (results[i].id === undefined ? '' : results[i].id),
        status: (results[i].itemstatus !== undefined ? results[i].itemstatus : ''),
        reason: (results[i].statusReason !== undefined ? results[i].statusReason : ''),
        pickBarcode: (results[i].pickBarcode !== undefined ? results[i].pickBarcode : ''),
        pickRequestId: (results[i].pickRequestId !== undefined ? results[i].pickRequestId : '')
      }
      itemStatus.push(iStatus)

      let item = {
        itemId: (results[i].itemId !== undefined ? results[i].itemId : ''),
        itemKey: (results[i].itemKey !== undefined ? results[i].itemKey : ''),
        sourceItemId: (results[i].sourceItemId !== undefined ? results[i].sourceItemId : ''),
        sourceItemNumber: (results[i].sourceItemNumber !== undefined ? results[i].sourceItemNumber : ''),
        barcode: (results[i].barcode !== undefined ? results[i].barcode : ''),
        sku: (results[i].sku !== undefined ? results[i].sku : ''),
        sourcePrice: (results[i].sourcePrice !== undefined ? results[i].sourcePrice : ''),
        standardPrice: (results[i].standardPrice !== undefined ? results[i].standardPrice : ''),
        currency: (results[i].currency !== undefined ? results[i].currency : ''),
        color: (results[i].color !== undefined ? results[i].color : ''),
        gender: (results[i].gender !== undefined ? results[i].gender : ''),
        itemLocCode: (results[i].itemLocCode !== undefined ? results[i].itemLocCode : ''),
        itemLocDesc: (results[i].itemLocDesc !== undefined ? results[i].itemLocDesc : ''),
        itemAreaCode: (results[i].itemAreaCode !== undefined ? results[i].itemAreaCode : ''),
        itemAreaDesc: (results[i].itemAreaDesc !== undefined ? results[i].itemAreaDesc : ''),
        size: (results[i].size !== undefined ? results[i].size : ''),
        qty: (results[i].qty !== undefined ? parseInt(results[i].qty) : ''),
        qtyUnit: (results[i].qtyUnit !== undefined ? results[i].qtyUnit : ''),
        weight: (results[i].weight !== undefined ? results[i].weight : ''),
        weightUnit: (results[i].weightUnit !== undefined ? results[i].weightUnit : ''),
        weightedItem: (results[i].weightedItem !== undefined ? results[i].weightedItem : ''),
        productNameTH: (results[i].productNameTH !== undefined ? results[i].productNameTH : ''),
        productNameEN: (results[i].productNameEN !== undefined ? results[i].productNameEN : ''),
        productNameIT: (results[i].productNameIT !== undefined ? results[i].productNameIT : ''),
        productNameDE: (results[i].productNameDE !== undefined ? results[i].productNameDE : ''),
        imageUrl: (results[i].imageUrl !== undefined ? results[i].imageUrl : ''),
        locBarcode: (results[i].locBarcode !== undefined ? results[i].locBarcode : ''),
        cat1: (results[i].cat1 !== undefined ? results[i].cat1 : ''),
        cat2: (results[i].cat2 !== undefined ? results[i].cat2 : ''),
        cat3: (results[i].cat3 !== undefined ? results[i].cat3 : ''),
        cat4: (results[i].cat4 !== undefined ? results[i].cat4 : ''),
        cat5: (results[i].cat5 !== undefined ? results[i].cat5 : ''),
        cat6: (results[i].cat6 !== undefined ? results[i].cat6 : ''),
        brand: (results[i].brand !== undefined ? results[i].brand : ''),
        zone: (results[i].zone !== undefined ? results[i].zone : ''),
        statusReason: (results[i].statusReason !== undefined ? results[i].statusReason : ''),
        itemStatusDate: (results[i].itemStatusDate !== undefined ? results[i].itemStatusDate : ''),
        subbarcode: (results[i].subbarcode !== undefined ? results[i].subbarcode : ''),
        brandCode: (results[i].brandCode !== undefined ? results[i].brandCode : ''),
        itemStatus: [itemStatus[i]]
      }
      items.push(item)
    }

    newResults = {
      taskId: (results[0].taskId !== undefined ? results[0].taskId : ''),
      wave_id: (results[0].wave_id !== undefined ? results[0].wave_id : ''),
      multiorder_id: (results[0].multiorder_id !== undefined ? results[0].multiorder_id : ''),
      windowStartTime: (results[0].windowStartTime !== undefined ? results[0].windowStartTime : ''),
      taskCreatedDate: (results[0].taskCreatedDate !== undefined ? results[0].taskCreatedDate : ''),
      countQty: (results2[0].countQty !== undefined ? results2[0].countQty : ''),
      countSku: (results2[0].countSku !== undefined ? results2[0].countSku : ''),
      channel: (results[0].channel !== undefined ? results[0].channel : ''),
      orderKey: (results[0].orderKey !== undefined ? results[0].orderKey : ''),

      orders: [orders[0]],
      items: items
    }
    newRes.push(newResults)
    return newRes
  }
  async getTaskIdAfterMerge(reqBody) {

    let queryIn = new Array()
    for (var i = 0; i < reqBody.mergeOrder.length; i++) {
      queryIn.push((reqBody.mergeOrder[i].sourceorderid !== undefined ? reqBody.mergeOrder[i].sourceorderid : ''))
    }
    let sql = `

    select distinct pt.id from ${this.SCHEMA + '.'}pick_request pr
    inner join ${this.SCHEMA + '.'}pick_task_detail ptd on pr.pickrequestid = ptd.pickrequestid
    inner join ${this.SCHEMA + '.'}pick_task pt on ptd.picktaskid = pt.id
    where pr.sourceorderid in ('${queryIn.join("','")}')
    `
    let results: any = await UtilModel.getInstance().sqlQuery(sql, []);
    console.log('results', results)
    return results
  }

  async setMergeOrder(reqBody) {

    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect(async (errConn: any, conn: any, done: any) => {
        if (errConn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errConn);
            }
            done();
          })
        }
        try {

          let queryIn = new Array()
          for (var i = 0; i < reqBody.mergeOrder.length; i++) {
            queryIn.push((reqBody.mergeOrder[i].sourceorderid !== undefined ? reqBody.mergeOrder[i].sourceorderid : ''))
          }
          // let sql = `
          // select *
          // from ${this.SCHEMA + '.'}pick_request pr
          // where pr.id in ('${queryIn.join("','")}')
          // `
          // let results: any = await UtilModel.getInstance().sqlQuery(sql, []);

          let countRequest = 0;
          let countPickTask = 0;
          let wave_id = 0;

          await this.allocateMergeOrder(conn, PICK_MODE.MERGE_ORDER, queryIn, reqBody).then((x: any) => {
            countRequest += x.countRequest;
            countPickTask += x.countPickTask;
            wave_id = x.wave_id;
          }).catch((err: any) => {
            console.log('ERROR 6 : ', err);
            throw err;
          });
          //
          resolve(0)
        } catch (err) {
          //console.log(err);
          console.log('ERROR 3 : ', err);
          conn.query('ROLLBACK');
          reject(err);
        } finally {
          await UtilModel.getInstance().sqlConnCommit(conn);
          done();
        }
      });
    });
  }

  async allocateMergeOrder(conn: any, pickMode: string, queryInMerge, reqBody) {
    return new Promise((resolve: any, reject: any) => {
      conn.query('BEGIN', async (errTrans: any) => {
        if (errTrans) {
          reject(errTrans);
        }

        this.user_id = '';
        const sleep = (milliseconds: number) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
        //console.log(`allocation task by pick mode: ${pickMode}`);

        try {
          /********** allocate multi order task block **********/

          let sql = `select count(0) as cnt from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
          let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [this.bu, this.loc]);

          console.log('sqlsql', sql)
          console.log('cntcnt', cnt)
          if (cnt != undefined && cnt.length > 0 && cnt[0].cnt == 0) {
            await UtilModel.getInstance().sqlConnInsert(conn, "alloc_process", ['sourceBU', 'sourceLoc', 'byOrder', 'multiOrder', 'wave'], [[this.bu, this.loc, 'N', 'N', 'N']]);
          }

          // if (pickMode == PICK_MODE.WAVE) {
          //   let state = 'Y';
          //   while (state == 'Y') {
          //     let jobchk = `select wave from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
          //     let rest = await UtilModel.getInstance().sqlConnQuery(conn, jobchk, [this.bu, this.loc]);
          //     console.log('rest : ', rest);
          //     state = rest[0].wave;
          //     if (state == 'Y') {
          //       await sleep(2000);
          //     }
          //   }

          //   await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set wave = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);
          //   await UtilModel.getInstance().sqlConnCommit(conn);
          // } else if (pickMode == PICK_MODE.MULTI_ORDER) {
          // let state = 'Y';
          // while (state == 'Y') {
          //   let jobchk = `select multiOrder from ${this.SCHEMA + '.'}alloc_process where sourceBU = ? and sourceLoc = ?`;
          //   let rest = await UtilModel.getInstance().sqlConnQuery(conn, jobchk, [this.bu, this.loc]);
          //   console.log("====jobchk=====", jobchk)
          //   console.log("====rest=====", [this.bu, this.loc])
          //   console.log("====rest=====", [this.bu, this.loc])
          //   state = rest[0].wave;
          //   if (state == 'Y') {
          //     await sleep(2000);
          //   }
          // }

          // const aaa = await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set multiOrder = ? where sourceBU = ? and sourceLoc = ?`, ['Y', this.bu, this.loc]);
          // await UtilModel.getInstance().sqlConnCommit(conn);

          // console.log('aaaaaa', aaa)
          //}


          let countRequest = 0;
          let countPickTasks = 0;
          let pickAssignType: string;
          let pickSubAssignType: string;
          let waveId = 0;
          let multiorderId = 0;

          let wave_id = 0;

          sql = `
            select p.*, concat(p.sourceBU, '-', p.sourceLoc, '-', p.pickAllocationType, '-' , p.pickCatLvl) allocationKey from (
              select
                  pr.pickRequestId, pr.sourceBU, pr.sourceLoc,
                  pp.pickMode, pp.pickAssignType, pp.pickSubAssignType,
                  string_agg(pa.pickAllocationType::char, '|' order by pa.pickAllocationType) pickAllocationType,
                  string_agg(pa.pickAllocationTypeSeq::char, '|' order by pa.pickAllocationTypeSeq) pickAllocationTypeSeq,
                  string_agg(pa.pickCatLvl::char, '|' order by pa.pickCatLvl) pickCatLvl,
                  count(0) countAllocation
              from ${this.SCHEMA + '.'}pick_request pr
              join ${this.SCHEMA + '.'}pr_pick_params pp
              on pr.pickRequestId = pp.pickRequestId
              join ${this.SCHEMA + '.'}pr_pickparams_allocation pa
              on pr.pickRequestId = pa.pickRequestId
              where pr.status = ?
              and pp.pickMode = ?
          `;

          let params = [PICK_REQUEST_STATUS.NEW, pickMode];

          if (this.bu != null && this.bu != undefined && this.bu.length > 0) {
            sql += ' and pr.sourceBU = ? '
            params.push(this.bu);

            // update_sql += `and sourceBU = ? `;
            // update_param.push(this.bu);
          }

          if (this.loc != null && this.loc != undefined && this.loc.length > 0) {
            sql += ' and pr.sourceLoc = ? '
            params.push(this.loc);

            // update_sql += ' and sourceLoc = ? '
            // update_param.push(this.loc);
          }

          if (this.mergeOrder != null && this.mergeOrder != undefined && this.mergeOrder.length > 0) {
            sql += ` and pr.sourceorderid in ('${queryInMerge.join("','")}') `
            // params.push(this.loc);

          }

          sql += `
              group by pr.pickRequestId, pr.sourceBU, pr.sourceLoc, pp.pickMode, pp.pickAssignType, pp.pickSubAssignType
            ) p
          `;
          console.log('sql1 : ', sql);
          console.log('params1 : ', params);
          let items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);
          console.log('items : ', items);

          let allocateItem = ArrayUtil.groupBy(items, 'allocationkey');

          sql = `select id from ${this.SCHEMA + '.'}multiorder_seq limit 1`;
          items = await UtilModel.getInstance().sqlConnQuery(conn, sql, []);
          multiorderId = items[0].id;

          console.log('allocateItem : ', allocateItem);

          // console.log('before sleep 5 sec');
          // await sleep(5000);
          // console.log('after sleep 5 sec');

          // await UtilModel.getInstance().sqlConnQuery(conn, update_sql, update_param);

          // loop each allocation grouping
          for (let k in allocateItem) {
            // define load check array (equal load, round robin) for allocate level
            let userAssgins = new Array();
            let currentBu = allocateItem[k][0].sourcebu;
            let currentLoc = allocateItem[k][0].sourceloc;
            pickAssignType = allocateItem[k][0].pickassigntype;
            pickSubAssignType = allocateItem[k][0].picksubassigntype;

            let pickRequestIds = new Array<string>();
            for (let r of allocateItem[k]) {
              pickRequestIds.push(r.pickrequestid);
              countRequest += 1;
            }

            let queryIn: any = [];
            pickRequestIds.forEach(i => queryIn.push('?'));
            let update_sql = `
              update ${this.SCHEMA + '.'}pick_request
              set status = ?
              where status = ?
              and pickRequestId in (${queryIn.join(',')})
            `;
            params = [PICK_REQUEST_STATUS.PICK_RESERVE, PICK_REQUEST_STATUS.NEW];
            for (let p of pickRequestIds) {
              params.push(p);
            }
            await UtilModel.getInstance().sqlConnQuery(conn, update_sql, params).then(res => console.log('update pick_request result : ', res));


            //console.log('-- pickRequestIds --');
            //console.log(pickRequestIds);

            sql = `
              select pa.pickAllocationType, pa.pickCatLvl
              from ${this.SCHEMA + '.'}pr_pickparams_allocation pa
              where pa.pickRequestId = ?
              order by pa.pickAllocationTypeSeq
            `;

            params = [allocateItem[k][0].pickrequestid];
            items = await UtilModel.getInstance().sqlConnQuery(conn, sql, params);


            // allocation column name list
            let groupBy = new Array<string>();
            for (let i of items) {

              // if (i.pickallocationtype == PICK_ALLOCATION_TYPE.NONE) {
              //   // handle with do nothing
              // } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_CATEGORY) {
              //   let catCol = `cat${i.pickcatlvl}`;
              //   groupBy.push(catCol);
              // } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_ZONE) {
              //   groupBy.push("zone");
              // } else if (i.pickallocationtype == PICK_ALLOCATION_TYPE.BY_BRAND) {
              //   groupBy.push("brand");
              // } else {
              //   throw new Error('500,Error data not support');
              // }
            }

            //console.log('-- groupBy --');
            //console.log(groupBy);

            // create only 1 task when pick allocation type == '0001'

            queryIn = [];
            pickRequestIds.forEach(i => queryIn.push('?'));

            let groupSql;
            if (groupBy.length == 0) {
              groupSql = `
                select 'g' as g
                from ${this.SCHEMA + '.'}pr_items
                where pickRequestId in (${queryIn.join(',')})
                group by g
              `
            } else {
              groupSql = `
                select ${groupBy.join(', ')}, count(distinct pickRequestId) as cntReq
                from ${this.SCHEMA + '.'}pr_items
                where pickRequestId in (${queryIn.join(',')})
                group by ${groupBy.join(', ')}
              `;
            }

            console.log('groupSql : ', groupSql);

            //return ''

            /********** get active user block **********/
            let activeUsers: any;
            try {
              // activeUsers = await this.getActiveUser(currentBu, currentLoc);
              let user = new User();
              // let user_list = await user.getUserByStatus(currentBu, currentLoc, 'Y');

              // let user_list = await user.getUserByStatus(currentBu, currentLoc, 'N');
              // activeUsers = {
              //   data: user_list
              // }


              activeUsers = {
                data: {
                  id: 1,
                  bu: this.bu,
                  loc: this.loc,
                  status: 'A',
                  userList: [{
                    userId: reqBody.user_id,
                    cat_01: "A",
                    cat_02: "A",
                    cat_03: "A",
                    cat_04: "A",
                    cat_05: "A",
                    zone: "A",
                    brand: "A"
                  }]
                }
              }
              console.log('activeUsers ======>>>>>> ', activeUsers.data.userList, activeUsers);
            } catch (err) {
              throw new Error(ERR_forbiddenUM);
            }

            // max request per task = 15
            // for (let i = 0; i < pickRequestIds.length; i += 15) {


            // let slicePickRequestIds = pickRequestIds.slice(i, i + 15);

            //console.log('-- slice pickRequestId --');
            //console.log(slicePickRequestIds);

            // get items group by column name of allocation
            // let groups = await UtilModel.getInstance().sqlConnQuery(conn, groupSql, [slicePickRequestIds]);
            let groups = await UtilModel.getInstance().sqlConnQuery(conn, groupSql, pickRequestIds);
            console.log('groupsactiveUsers : ', groups);
            // initialize filter active user as all active user
            let filterActiveUser = activeUsers.data.userList;
            //console.log('-- active user --');
            //console.log(filterActiveUser);

            // insert task group by pick allocation (eg. cat1)

            // groups = [
            //   { brand: 'XXX', cntreq: '1' }
            // ]

            for await (let group of groups) {

              // if req of each group more than max req per task
              // then split items of that group to multi task
              // by select distinct req of that group with value of group to get exact pickRequestId and split them
              // eg where cat1, cat2 dynamic by pick allocation
              // need to refactor if/else to call single method later to reduct duplicate of code
              console.log('group[cntReq] > MAX_REQ_PER_TASK');
              if (group['cntReq'] > MAX_REQ_PER_TASK) {
                let splitParams = pickRequestIds;
                let splitSql = `
                  select distinct pickRequestId
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (?)
                `;

                for await (let col of groupBy) {
                  splitSql += `
                    and ${col} = ?
                  `;
                  splitParams.push(group[col]);
                }

                let groupRequest = await UtilModel.getInstance().sqlConnQuery(conn, splitSql, splitParams);
                console.log('groupRequest : ', groupRequest);
                let groupPickRequestIds = new Array<string>();
                for (let r of groupRequest) {
                  groupPickRequestIds.push(r.pickrequestid);
                }

                for (let i = 0; i < groupPickRequestIds.length; i += MAX_REQ_PER_TASK) {
                  let slicePickRequestIds = groupPickRequestIds.slice(i, i + MAX_REQ_PER_TASK);

                  let [countPickTask, pickTaskId] = await this.insertPickTask(conn, '', '', currentBu, waveId, multiorderId).then((res: any) => {
                    return [res.countPickTask, res.pick_task_id];
                  });
                  await UtilModel.getInstance().sqlConnInsert(conn, "error_log", ['method', 'api_path', 'req_body', 'message'], [['post', '/setPickTaskAllocation', `pickTaskId: ${pickTaskId}`, '']]);

                  countPickTasks += countPickTask;
                  filterActiveUser = activeUsers.data.userList;

                  let insertDetail = `
                  insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
                  select ?,
                    id,
                    pickRequestId,
                    itemKey,
                    current_timestamp
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (?)
                `;

                  // use slice pick request
                  let insertDetailParam = [pickTaskId];
                  for (let p of slicePickRequestIds) {
                    insertDetailParam.push(p);
                  }

                  // append where condition when group items by ???
                  if (groupBy.length > 0) {
                    for await (let col of groupBy) {
                      insertDetail += `
                      and ${col} = ?
                    `;

                      // insert value of group by column to filter data each task
                      insertDetailParam.push(group[col]);

                      // filter user to match responsibility (allocate column)
                      filterActiveUser = filterActiveUser.filter((x: any) => "A" == x[FM_UM_MAPPING[col]] || group[col] == x[FM_UM_MAPPING[col]]);
                    }
                    //console.log(`found active user match with allocation: ${filterActiveUser.map((x: any) => x.userId)}`);
                  }

                  // reduce duplicate userId after match
                  filterActiveUser = [...filterActiveUser.reduce((x: any, y: any) => {
                    const key = y.userId;
                    const item = x.get(key) || Object.assign({}, y);
                    return x.set(key, item);
                  }, new Map).values()];
                  //console.log(`-- reduce userId --`);
                  //console.log(filterActiveUser.map((x: any) => x.userId));


                  //console.log('-- insert pick_task_detail --');
                  //console.log(insertDetail);
                  console.log("HERE insertDetail ---------- ");
                  await UtilModel.getInstance().sqlConnQuery(conn, insertDetail, insertDetailParam);

                  sql = `
                  select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
                  inner join ${this.SCHEMA + '.'}pr_items i
                  on pt.pickRequestId = i.pickRequestId
                  and pt.itemKey = i.itemKey
                  and pt.pickItemId = i.id
                  where pt.pickTaskId = ?
                `;

                  // expand qty of items into pick_task_detail_item
                  const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);

                  let arr_sqldata = new Array();
                  for (let r of results) {
                    for (let i = 0; i < r.qty; i++) {
                      arr_sqldata.push([r.id, r.status, '', new Date()]);
                    }
                  }

                  let pickTaskDetailItemCols = [
                    'pickTaskDetailId',
                    'itemStatus',
                    'statusReason',
                    'created_time'
                  ];

                  await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', pickTaskDetailItemCols, arr_sqldata);

                  /********** push assignment block **********/
                  if (pickAssignType == ASSIGN_TYPE.PULL) {
                    // multi orde is push only but need to check for skip if found pull mode
                    continue;
                  }


                  if (filterActiveUser.length == 0) {
                    // not assign when all active user not reponsibility with task
                    continue;
                  }

                  let shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);

                  if (pickSubAssignType == SUB_ASSIGN_TYPE.LEAST_LOAD) {
                    sql = `
                    select pt.user_id, count(0) as item_count from ${this.SCHEMA + '.'}pick_task pt
                    join ${this.SCHEMA + '.'}pick_task_detail pd
                    on pt.id = pd.pickTaskId
                    join ${this.SCHEMA + '.'}pick_task_detail_item pi
                    on pd.id = pi.pickTaskDetailId
                    where pt.user_id in (?)
                    and pi.itemStatus = ?
                    group by pt.user_id
                  `;

                    let users = filterActiveUser.map((x: any) => ({ userId: x.userid, count: 0 }));

                    let itemCountPerUser = await UtilModel.getInstance().sqlConnQuery(conn, sql, [users.map((x: any) => x.userId), ITEM_STATUS.NEW]);
                    for await (let u of itemCountPerUser) {
                      let i = users.findIndex((x: any) => x.userId == u.user_id);
                      if (i >= 0) {
                        users[i].count = u.item_count;
                      }
                    }

                    //console.log('-- load count --');
                    //console.log(itemCountPerUser);

                    console.log('-- users with count --');
                    console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });

                    console.log('-- after reduce --');
                    console.log(reduce);

                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);
                  } else if (pickSubAssignType == SUB_ASSIGN_TYPE.EQUAL_LOAD) {
                    let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));
                    users.sort((a, b) => a.userId.localeCompare(b.userId));

                    for (let u of userAssgins) {
                      let i = users.findIndex((x: any) => x.userId == u.userId);
                      //console.log(`found assign user at index: ${i}`);
                      if (i >= 0) {
                        users[i].count = u.count;
                      }
                    }

                    //console.log(`user with item count:-`);
                    //console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });

                    sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                    let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                    let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                    if (i < 0) {
                      userAssgins.push({
                        userId: reduce.userId,
                        // count: 1
                        count: cnt[0].cnt
                      });
                    } else {
                      // userAssgins[i].count = userAssgins[i].count + 1;
                      userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                    }

                    //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                    //console.log(userAssgins);
                    //console.log('\n');
                  } else if (pickSubAssignType == SUB_ASSIGN_TYPE.ROUND_ROBIN) {
                    let users = shuffleActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                    for (let u of userAssgins) {
                      let i = users.findIndex((x: any) => x.userId == u.userId);
                      //console.log(`found assign user at index: ${i}`);
                      if (i >= 0) {
                        users[i].count = u.count;
                      }
                    }

                    console.log(`user with item count:-`);
                    console.log(users);

                    let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                      return (obj.count < res.count) ? obj : res;
                    });
                    console.log(`reduce with item count:-`);
                    console.log(reduce);

                    sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                    let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                    sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                    await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                    let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                    if (i < 0) {
                      userAssgins.push({
                        userId: reduce.userId,
                        // count: 1
                        count: cnt[0].cnt
                      });
                    } else {
                      // userAssgins[i].count = userAssgins[i].count + 1;
                      userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                    }

                    //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                    //console.log(userAssgins);
                    //console.log('\n');
                  }
                }
              } else {
                console.log('ELSE -------------- ');
                console.log('multiorderId', multiorderId)
                let [countPickTask, pickTaskId] = await this.insertPickTask(conn, '', '', currentBu, waveId, multiorderId).then((res: any) => {
                  return [res.countPickTask, res.pick_task_id];
                });
                await UtilModel.getInstance().sqlConnInsert(conn, "error_log", ['method', 'api_path', 'req_body', 'message'], [['post', '/setPickTaskAllocation', `pickTaskId: ${pickTaskId}`, '']]);

                countPickTasks += countPickTask;
                filterActiveUser = activeUsers.data.userList;

                let queryIn: any = [];
                pickRequestIds.forEach(i => queryIn.push('?'));
                let insertDetail = `
                  insert into ${this.SCHEMA + '.'}pick_task_detail (pickTaskId, pickItemId, pickRequestId, itemKey, created_time)
                  select ?,
                    id,
                    pickRequestId,
                    itemKey,
                    current_timestamp
                  from ${this.SCHEMA + '.'}pr_items
                  where pickRequestId in (${queryIn.join(',')})
                `;

                // let insertDetailParam = [pickTaskId, slicePickRequestIds];
                let insertDetailParam = [pickTaskId];
                for (let p of pickRequestIds) {
                  insertDetailParam.push(p);
                }

                // append where condition when group items by ???
                if (groupBy.length > 0) {
                  for await (let col of groupBy) {
                    insertDetail += `
                      and ${col} = ?
                    `;

                    // insert value of group by column to filter data each task
                    insertDetailParam.push(group[col]);

                    // filter user to match responsibility (allocate column)
                    filterActiveUser = filterActiveUser.filter((x: any) => "A" == x[FM_UM_MAPPING[col]] || group[col] == x[FM_UM_MAPPING[col]]);
                  }
                  //console.log(`found active user match with allocation: ${filterActiveUser.map((x: any) => x.userId)}`);
                }

                // reduce duplicate userId after match
                filterActiveUser = [...filterActiveUser.reduce((x: any, y: any) => {
                  const key = y.userId;
                  const item = x.get(key) || Object.assign({}, y);
                  return x.set(key, item);
                }, new Map).values()];
                //console.log(`-- reduce userId --`);
                //console.log(filterActiveUser.map((x: any) => x.userId));


                //console.log('-- insert pick_task_detail --');
                //console.log(insertDetail);

                await UtilModel.getInstance().sqlConnQuery(conn, insertDetail, insertDetailParam);

                sql = `
                  select pt.*, i.qty, i.status from ${this.SCHEMA + '.'}pick_task_detail pt
                  inner join ${this.SCHEMA + '.'}pr_items i
                  on pt.pickRequestId = i.pickRequestId
                  and pt.itemKey = i.itemKey
                  and pt.pickItemId = i.ID
                  where pt.pickTaskId = ?
                `;

                // expand qty of items into pick_task_detail_item
                const results = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                console.log('results : ', results)
                let arr_sqldata = new Array();
                for (let r of results) {
                  for (let i = 0; i < r.qty; i++) {
                    arr_sqldata.push([r.id, r.status, '', new Date()]);
                  }
                }

                let pickTaskDetailItemCols = [
                  'pickTaskDetailId',
                  'itemStatus',
                  'statusReason',
                  'created_time'
                ];

                console.log('pickTaskDetailItemCols : ', pickTaskDetailItemCols)
                console.log('arr_sqldata : ', arr_sqldata)
                await UtilModel.getInstance().sqlConnInsert(conn, 'pick_task_detail_item', pickTaskDetailItemCols, arr_sqldata);

                /********** push assignment block **********/
                // if (pickAssignType == ASSIGN_TYPE.PULL) {
                //   // multi orde is push only but need to check for skip if found pull mode
                //   continue;
                // }

                // if (filterActiveUser.length == 0) {
                //   // not assign when all active user not reponsibility with task
                //   continue;
                // }

                let shuffleActiveUser = ArrayUtil.shuffle(filterActiveUser);

                if (pickSubAssignType == SUB_ASSIGN_TYPE.LEAST_LOAD) {
                  sql = `
                    select pt.user_id, count(0) as item_count from ${this.SCHEMA + '.'}pick_task pt
                    join ${this.SCHEMA + '.'}pick_task_detail pd
                    on pt.id = pd.pickTaskId
                    join ${this.SCHEMA + '.'}pick_task_detail_item pi
                    on pd.id = pi.pickTaskDetailId
                    where pt.user_id in (?)
                    and pi.itemStatus = ?
                    group by pt.user_id
                  `;

                  let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                  let itemCountPerUser = await UtilModel.getInstance().sqlConnQuery(conn, sql, [users.map((x: any) => x.userId), ITEM_STATUS.NEW]);
                  for await (let u of itemCountPerUser) {
                    let i = users.findIndex((x: any) => x.userId == u.user_id);
                    if (i >= 0) {
                      users[i].count = u.item_count;
                    }
                  }

                  //console.log('-- load count --');
                  //console.log(itemCountPerUser);

                  console.log('-- users with count --');
                  console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  console.log('-- after reduce --');
                  console.log(reduce);

                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);
                } else if (pickSubAssignType == SUB_ASSIGN_TYPE.EQUAL_LOAD) {
                  let users = filterActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));
                  users.sort((a, b) => a.userId.localeCompare(b.userId));

                  for (let u of userAssgins) {
                    let i = users.findIndex((x: any) => x.userId == u.userId);
                    //console.log(`found assign user at index: ${i}`);
                    if (i >= 0) {
                      users[i].count = u.count;
                    }
                  }

                  //console.log(`user with item count:-`);
                  //console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                  let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                  let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                  if (i < 0) {
                    userAssgins.push({
                      userId: reduce.userId,
                      // count: 1
                      count: cnt[0].cnt
                    });
                  } else {
                    // userAssgins[i].count = userAssgins[i].count + 1;
                    userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                  }

                  //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                  //console.log(userAssgins);
                  //console.log('\n');
                } else if (pickSubAssignType == SUB_ASSIGN_TYPE.ROUND_ROBIN) {
                  let users = shuffleActiveUser.map((x: any) => ({ userId: x.userId, count: 0 }));

                  for (let u of userAssgins) {
                    let i = users.findIndex((x: any) => x.userId == u.userId);
                    //console.log(`found assign user at index: ${i}`);
                    if (i >= 0) {
                      users[i].count = u.count;
                    }
                  }

                  //console.log(`user with item count:-`);
                  //console.log(users);

                  let reduce = users.reduce((res: { userId: string, count: number }, obj: { userId: string, count: number }) => {
                    return (obj.count < res.count) ? obj : res;
                  });

                  sql = `
                    select sum(i.qty) cnt from ${this.SCHEMA + '.'}pick_task_detail pi
                    inner join ${this.SCHEMA + '.'}pr_items i
                    on pi.pickRequestId = i.pickRequestId
                    and pi.itemKey = i.itemKey
                    and pi.pickItemId = i.id
                    where pi.pickTaskId = ?
                  `;

                  let cnt = await UtilModel.getInstance().sqlConnQuery(conn, sql, [pickTaskId]);
                  sql = `update ${this.SCHEMA + '.'}pick_task set status = ?, user_id = ?, updated_time = ? where id = ?`;
                  await UtilModel.getInstance().sqlConnQuery(conn, sql, [TASK_STATUS.PENDING, reduce.userId, new Date(), pickTaskId]);

                  let i = userAssgins.findIndex((x: any) => x.userId == reduce.userId);
                  if (i < 0) {
                    userAssgins.push({
                      userId: reduce.userId,
                      // count: 1
                      count: cnt[0].cnt
                    });
                  } else {
                    // userAssgins[i].count = userAssgins[i].count + 1;
                    userAssgins[i].count = userAssgins[i].count + cnt[0].cnt;
                  }

                  //console.log(`${k} | assign userId: ${reduce.userId} to userAssigns:-`);
                  //console.log(userAssgins);
                  //console.log('\n');
                }

              }
            }

            queryIn = [];
            pickRequestIds.forEach(i => queryIn.push('?'));
            sql = `
              update ${this.SCHEMA + '.'}pick_request
              set status = ?
              where status = ?
              and pickRequestId in (${queryIn.join(',')})
            `;
            params = [PICK_REQUEST_STATUS.ASSIGN, PICK_REQUEST_STATUS.PICK_RESERVE];
            for (let p of pickRequestIds) {
              params.push(p);
            }
            await UtilModel.getInstance().sqlConnQuery(conn, sql, params).then(res => console.log('update pick_request result : ', res));

            wave_id = waveId;
          }
          multiorderId += 1;
          sql = `update ${this.SCHEMA + '.'}multiorder_seq set id = ?`;
          await UtilModel.getInstance().sqlConnQuery(conn, sql, [multiorderId]);

          sql = `update ${this.SCHEMA + '.'}pr_pick_params
                set pickmode = ?
                where pickrequestid in (
                  select pr.pickrequestid
                from ${this.SCHEMA + '.'}pick_request pr
                inner join ${this.SCHEMA + '.'}pr_pick_params pp on pp.pickrequestid = pr.pickrequestid
                where pr.sourceorderid in ('${queryInMerge.join("','")}')
                )
          `;
          await UtilModel.getInstance().sqlConnQuery(conn, sql, [PICK_MODE.MULTI_ORDER]);



          /********** commit block **********/

          await UtilModel.getInstance().sqlConnCommit(conn);

          resolve({
            countRequest: countRequest,
            countPickTask: countPickTasks,
            wave_id: wave_id
          });
        } catch (err) {
          //console.log('rollback');
          conn.query('ROLLBACK');
          reject(err);
        } finally {

          console.log(' --------- update multi order --------- ');
          await UtilModel.getInstance().sqlConnQuery(conn, `update ${this.SCHEMA + '.'}alloc_process set multiOrder = ? where sourceBU = ? and sourceLoc = ?`, ['N', this.bu, this.loc]);

          await UtilModel.getInstance().sqlConnCommit(conn);
          // conn.release();
          // conn.end();
        }
      });
    });
  }


  mappingValue(data: any, key: string, limit: number) {
    let mappedValue: any = [];
    if (data.length == 0) {
      return mappedValue;
    }

    data.sort(function (a, b) { return (a.priority < b.priority) ? -1 : 1 });

    let indx = 0;
    data.forEach(i => {
      if (limit > indx) {
        mappedValue.push({
          priority: i.priority,
          value: i[key],
          data: DY_SEARCH.hasOwnProperty(i[key].toUpperCase()) ? mockDataSearchFilterChanel : null,
        });
      }
      indx++;
    });

    return mappedValue;
  }

  getDeliveryType(task: any) {
    let deliverytype = "";
    switch (task) {
      case "0001":
        deliverytype = "home delivery"
        break;
      case "0002":
        deliverytype = "click and collect"
        break;
      case "0003":
        deliverytype = "click and reserve"
        break;
      case "0004":
        deliverytype = "click and collect (AP)"
        break;
      default:
        deliverytype = ""
        break;
    }
    return deliverytype;
  }

  getDeliverySubType(task: any) {
    let deliverysubtype = "";
    switch (task) {
      case "0001":
        deliverysubtype = "Same day"
        break;
      case "0002":
        deliverysubtype = "Next day"
        break;
      case "0003":
        deliverysubtype = "Standard"
        break;
      case "0004":
        deliverysubtype = "Express"
        break;
      case "0005":
        deliverysubtype = "1hr."
        break;
      case "0006":
        deliverysubtype = "2hrs."
        break;
      case "0007":
        deliverysubtype = "3hrs."
        break;
      default:
        deliverysubtype = ""
        break;
    }
    return deliverysubtype;
  }

  getChannels(task: any) {
    let channel = "";
    switch (task) {
      case "0100":
        channel = "offline"
        break;
      case "0001":
        channel = "online"
        break;
      case "0002":
        channel = "market place"
        break;
      case "0003":
        channel = "chat&shop"
        break;
      case "0004":
        channel = "Shopee"
        break;
      case "0005":
        channel = "Lazada"
        break;
      case "0006":
        channel = "Mirakl (COL)"
        break;
      case "0007":
        channel = "Mirakl (ROL)"
        break;
      case "0008":
        channel = "Mirakl (OfficeMate)"
        break;
      case "0009":
        channel = "JD"
        break;
      case "0010":
        channel = "ChefYim"
        break;
      case "0011":
        channel = "e-ordering Online"
        break;
      case "0012":
        channel = "e-ordering Offline"
        break;
      case "0013":
        channel = "Mirakl (Powerbuy)"
        break;
      case "0015":
        channel = "Zalo Shop"
        break;
      case "0016":
        channel = "Zalo Chat"
        break;
      case "0017":
        channel = "NOW"
        break;
      case "0018":
        channel = "Baemin"
        break;
      case "0019":
        channel = "Grab Mart"
        break;
      case "0020":
        channel = "Telesales"
        break;
      case "0021":
        channel = "Mobile App"
        break;
      case "0022":
        channel = "Website"
        break;
      default:
        channel = ""
        break;
    }
    return channel;
  }

}

const DY_SEARCH = {
  CHANNEL,
}

const mockDataSearchFilterChanel = [
  {
    "prority": 1,
    "values": CHANNEL['0100'],
  },
  {
    "prority": 2,
    "values": CHANNEL['0001'],
  },
  {
    "prority": 3,
    "values": CHANNEL['0002'],
  },
  {
    "prority": 4,
    "values": CHANNEL['0003'],
  },
  {
    "prority": 5,
    "values": CHANNEL['0004'],
  },
  {
    "prority": 6,
    "values": CHANNEL['0005'],
  },
  {
    "prority": 7,
    "values": CHANNEL['0006'],
  },
  {
    "prority": 8,
    "values": CHANNEL['0007'],
  },
  {
    "prority": 9,
    "values": CHANNEL['0008'],
  },
  {
    "prority": 10,
    "values": CHANNEL['0009'],
  },
  {
    "prority": 11,
    "values": CHANNEL['0010'],
  },
  {
    "prority": 12,
    "values": CHANNEL['0011'],
  },
  {
    "prority": 13,
    "values": CHANNEL['0012'],
  },
  {
    "prority": 14,
    "values": CHANNEL['0013'],
  },
  {
    "prority": 15,
    "values": CHANNEL['0015'],
  },
  {
    "prority": 16,
    "values": CHANNEL['0016'],
  },
  {
    "prority": 17,
    "values": CHANNEL['0017'],
  },
  {
    "prority": 18,
    "values": CHANNEL['0018'],
  },
  {
    "prority": 19,
    "values": CHANNEL['0019'],
  },
  {
    "prority": 20,
    "values": CHANNEL['0020'],
  },
  {
    "prority": 21,
    "values": CHANNEL['0021'],
  },
  {
    "prority": 22,
    "values": CHANNEL['0022'],
  },
]
