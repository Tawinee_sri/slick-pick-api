import { UtilModel } from '../../UtilModel';
import { any } from 'bluebird';

const ERR_updateId = '30215, Error Required Field updateId';
const ERR_updateIdDate = '30216, Error Required Field updateIdDate';
const ERR_subOrderKey = '30217, Error Required Field subOrderKey';
const ERR_sourceSubOrderId = '30218, Error Required Field sourceSubOrderId';
const ERR_orderKey = '30219, Error Required Field orderKey';
const ERR_sourceOrderId = '30220, Error Required Field sourceOrderId';
const ERR_endTime = '30221, Error Required Field endTime';

export class PickShipmentRequest {

  SCHEMA: string = 'slick_picking';

  updateId: string;
  updateIdDate: string;
  subOrderKey: string;
  sourceSubOrderId: string;
  orderKey: string;
  sourceOrderId: string;
  pickingParam: {
    pickPriority: Object
  }
  pickPriority: {
    DeliveryWindow: [],
    pickPriorityByTime: [],
  }

  constructor() { }
  setData(reqBody) {

    if (!(reqBody instanceof Object)) {
      throw new Error('500,ERROR,Current Data is not an object');
    }
    let data = reqBody.pickShipmentUpdateRequest;

    if (!data.hasOwnProperty('updateId') || data.updateId == null || data.updateId.trim().length == 0) {
      throw new Error(ERR_updateId);
    }
    this.updateId = data.updateId;

    if (!data.hasOwnProperty('updateIdDate') || data.updateIdDate == null || data.updateIdDate.trim().length == 0) {
      throw new Error(ERR_updateIdDate);
    }
    this.updateIdDate = data.updateIdDate;

    if (!data.hasOwnProperty('subOrderKey') || data.subOrderKey == null || data.subOrderKey.trim().length == 0) {
      throw new Error(ERR_subOrderKey);
    }
    this.subOrderKey = data.subOrderKey;

    if (!data.hasOwnProperty('sourceSubOrderId') || data.sourceSubOrderId == null || data.sourceSubOrderId.trim().length == 0) {
      throw new Error(ERR_sourceSubOrderId);
    }
    this.sourceSubOrderId = data.sourceSubOrderId;

    if (!data.hasOwnProperty('orderKey') || data.orderKey == null || data.orderKey.trim().length == 0) {
      throw new Error(ERR_orderKey);
    }
    this.orderKey = data.orderKey;

    if (!data.hasOwnProperty('sourceOrderId') || data.sourceOrderId == null || data.sourceOrderId.trim().length == 0) {
      throw new Error(ERR_sourceOrderId);
    }
    this.sourceOrderId = data.sourceOrderId;

    let res = this.set_picking_param(data.pickingParam);
    if (res != "") {
      throw new Error(res);
    }

  }

  set_picking_param(data: any) {
    this.pickingParam = { pickPriority: {} };

    if (!(data instanceof Object)) {
      return '500,ERROR pickingParam is not an object';
    }

    let res: any = this.set_pick_priority(data.pickPriority);
    if (res != '') {
      return res;
    }
    this.pickingParam.pickPriority = this.pickPriority;

    return '';
  }

  set_pick_priority(data: any) {
    this.pickPriority = { DeliveryWindow: [], pickPriorityByTime: [] }

    if (!(data instanceof Object)) {
      return '500,ERROR pickPriority is not an object';
    }

    let deliveryWindow: any = [];
    if (data.DeliveryWindow instanceof Array && data.DeliveryWindow.length > 0) {

      for (let d of data.DeliveryWindow) {

        if (!(d.hasOwnProperty('startTime')) || d.startTime == null || d.startTime.trim().length == 0) {
          d.startTime = '';
        }

        if (!(d.hasOwnProperty('endTime')) || d.endTime == null || d.endTime.trim().length == 0) {
          return ERR_endTime;
        }

        deliveryWindow.push({
          startTime: d.startTime,
          endTime: d.endTime
        })

      }

    }
    this.pickPriority.DeliveryWindow = deliveryWindow;

    let pickPriorityByTime: any = [];
    if (data.pickPriorityByTime instanceof Array && data.pickPriorityByTime.length > 0) {

      for (let p of data.pickPriorityByTime) {

        if (!(p.hasOwnProperty('pickPriorityLevel')) || p.pickPriorityLevel == null || p.pickPriorityLevel.trim().length == 0) {
          p.pickPriorityLevel = '';
        }

        if (!(p.hasOwnProperty('pickPriorityTime')) || p.pickPriorityTime == null || p.pickPriorityTime.trim().length == 0) {
          p.pickPriorityTime = '';
        }

        pickPriorityByTime.push({
          pickPriorityLevel: p.pickPriorityLevel,
          pickPriorityTime: p.pickPriorityTime
        });

      }

    }
    this.pickPriority.pickPriorityByTime = pickPriorityByTime;

    return '';
  }

  getCurrentData() {
    return {
      pickShipmentUpdateRequest: {
        updateId: this.updateId,
        updateIdDate: this.updateIdDate,
        subOrderKey: this.subOrderKey,
        sourceSubOrderId: this.sourceSubOrderId,
        orderKey: this.orderKey,
        sourceOrderId: this.sourceSubOrderId,
        pickingParam: this.pickingParam
      }
    }

  }

  async save() {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errconn: any, conn: any, done: any) => {
        if (errconn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errconn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }
          try {
            let affectedRows = 0;

            // where pr.pickRequestId = ?
            let sql = `
              select
                count(0) as cnt, pr.pickRequestId
              from ${this.SCHEMA + '.'}pick_request pr
              where pr.subOrderKey = ?
              and pr.sourceSubOrderId  = ?
              and pr.orderKey = ?
              and pr.sourceOrderId = ?
              group by pr.pickRequestId
            `;
            let w = [
              // this.pickRequestId,
              this.subOrderKey,
              this.sourceSubOrderId,
              this.orderKey,
              this.sourceOrderId
            ];
            let res = await UtilModel.getInstance().sqlConnQuery(conn, sql, w);
            if (res[0].cnt === 0) throw new Error('999, No task in database.');

            // sql = `update pack_request set `;

            let isUpdate: boolean = true;
            for (let p of this.pickPriority.pickPriorityByTime) {
              sql = `select count(*) as cnt from ${this.SCHEMA + '.'}pr_pickparams_priority_time where pickRequestId = ? and pickPriorityLevel = ? `;
              await UtilModel.getInstance().sqlConnQuery(conn, sql, [res[0].pickrequestid, p['pickPriorityLevel']]).then((res: any) => isUpdate = (res[0].cnt === 0) ? false : true);
              console.log(`${new Date()} pickPriority Mode is: ${(isUpdate) ? 'UPDATE' : 'INSERT'}`);

              if (isUpdate) {

                sql = `update ${this.SCHEMA + '.'}pr_pickparams_priority_time set pickPriorityTime = ? where pickRequestId = ? and pickPriorityLevel = ? `;
                await UtilModel.getInstance().sqlConnQuery(conn, sql, [p['pickPriorityTime'], res[0].pickrequestid, p['pickPriorityLevel']]).then((res: any) => affectedRows += res.rowCount);

              } else {
                let arr_cols = ['pickRequestId', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];

                console.log(`${new Date()} this.pickPriority.pickPriorityByTime ====>>> `, this.pickPriority.pickPriorityByTime);
                let arr_data = [res[0].pickrequestid, p['pickPriorityLevel'], p['pickPriorityTime'], new Date()];
                await UtilModel.getInstance().sqlConnInsert(conn, 'pr_pickparams_priority_time', arr_cols, [arr_data]).then((res: any) => affectedRows += res.rowCount);

              }

            }

            sql = `select count(*) as cnt from ${this.SCHEMA + '.'}pr_pickparams_priority_window where pickRequestId = ? `;
            isUpdate = true;
            await UtilModel.getInstance().sqlConnQuery(conn, sql, [res[0].pickrequestid]).then((res: any) => isUpdate = (res[0].cnt === 0) ? false : true);
            console.log(`${new Date()} pickPriorityWindow Mode is: ${(isUpdate) ? 'UPDATE' : 'INSERT'}`);

            if (isUpdate) {

              sql = `update ${this.SCHEMA + '.'}pr_pickparams_priority_window set startTime = ?, endTime = ? where pickRequestId = ? `;
              for await (let p of this.pickPriority.DeliveryWindow) {
                await UtilModel.getInstance().sqlConnQuery(conn, sql, [p['startTime'], p['endTime'], res[0].pickrequestid]).then((res: any) => affectedRows += res.rowCount);
              }

            } else {
              let arr_cols = ['pickRequestId', 'startTime', 'endTime', 'created_time'];

              console.log(`${new Date()} this.pickPriority.pickPriorityByTime ====>>> `, this.pickPriority.pickPriorityByTime);
              for await (let p of this.pickPriority.DeliveryWindow) {
                let arr_data = [res[0].pickrequestid, p['startTime'], p['endTime'], new Date()];
                await UtilModel.getInstance().sqlConnInsert(conn, 'pr_pickparams_priority_window', arr_cols, [arr_data]).then((res: any) => affectedRows += res.rowCount);
              }

            }
            await UtilModel.getInstance().sqlConnCommit(conn);

            resolve(affectedRows);
          } catch (err) {
            // conn.rollback();
            conn.query('ROLLBACK');
            console.error(err);
            reject(err);
          } finally {
            // conn.release();
            conn.end();
          }
        });
      });
    });
  }

}
