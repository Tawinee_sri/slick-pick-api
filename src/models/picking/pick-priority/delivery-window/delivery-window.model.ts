import { UtilModel } from '../../../UtilModel';
import { PICK_PRIORITY_TYPE } from '../../constant';

const ERR_DeliveryWindow = '31101,Error Wrong Type';
const ERR_startTime = '31102,Error Require Field';
const ERR_endTime = '31103,Error Require Field';

export class DeliveryWindow {
  // config
  USE_TABLE: string = 'pr_pickparams_priority_window';

  // pk
  id: number;

  // link
  pickRequestId: string;

  // field
  slotId: string;
  startTime: string;
  endTime: string;

  constructor() { }

  setData(pickRequestId: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error(ERR_DeliveryWindow);
    }

    this.pickRequestId = pickRequestId;

    if (!data.hasOwnProperty('slotId')) {
      data.slotId = '';
    }
    this.slotId = data.slotId;

    if (!data.hasOwnProperty('startTime')) {
      throw new Error(ERR_startTime);
    }
    this.startTime = data.startTime;

    if (!data.hasOwnProperty('endTime')) {
      throw new Error(ERR_endTime);
    }
    this.endTime = data.endTime;
  }

  getData(): Object {
    return {
      startTime: this.startTime,
      endTime: this.endTime
    };
  }

  async save(conn: any) {
    return await this.insertData(conn);
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'startTime',
      'endTime',
      'slotId',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.startTime,
        this.endTime,
        this.slotId,
        new Date()
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }
}
