import { UtilModel } from '../../../UtilModel';
import { PICK_PRIORITY_TYPE } from '../../constant';

const ERR_pickPriorityByTime = '31001,Error Wrong Type';
const ERR_pickPriorityLevel = '31002,Error Require Field';
const ERR_pickPriorityTime = '31003,Error Require Field';

export class PickPriorityByTime {
  // config
  USE_TABLE: string = 'pr_pickparams_priority_time';

  // pk
  id: number;

  // link
  pickRequestId: string;

  // field
  pickPriorityLevel: string;
  pickPriorityTime: string;

  constructor() { }

  setData(pickRequestId: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error(ERR_pickPriorityByTime);
    }

    this.pickRequestId = pickRequestId;

    if (!data.hasOwnProperty('pickPriorityLevel')) {
      throw new Error(ERR_pickPriorityLevel);
    }
    this.pickPriorityLevel = data.pickPriorityLevel;

    if (!data.hasOwnProperty('pickPriorityTime')) {
      throw new Error(ERR_pickPriorityTime);
    }
    this.pickPriorityTime = data.pickPriorityTime;
  }

  getData(): Object {
    return {
      pickPriorityLevel: this.pickPriorityLevel,
      pickPriorityTime: this.pickPriorityTime
    };
  }

  async save(conn: any) {
    return await this.insertData(conn);
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'pickPriorityLevel',
      'pickPriorityTime',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.pickPriorityLevel,
        this.pickPriorityTime,
        new Date()
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }
}
