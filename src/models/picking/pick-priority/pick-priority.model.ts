import { dateToStrDate, strToDdate } from '../../DateUtils';
import { UtilModel } from '../../UtilModel';
import { PICK_PRIORITY_TYPE } from '../constant';
import { PickPriorityByTime } from './pick-priority-by-time/pick-priority-by-time.model';
import { DeliveryWindow } from './delivery-window/delivery-window.model'

/**
 * Declear Error Message
 */
const ERR_pickPriorityType = '30501,Error Required Field';
const ERR_pickPriorityLevel = '30502,Error Required Field';
const ERR_pickPriorityTime = '30503,Error Required Field';
const ERR_pickPriority = '30504,Error Wrong Type';
const ERR_pickPriorityByTime = '30505,Error Wrong Type';
const ERR_DeliveryWindow = '30506,Error Wrong Type';
const ERR_missingPickPriorityByTime = '30507,Error Required Field';

export class PickPriority {

  // config
  USE_TABLE: string = 'pr_pickparams_priority';

  // pk
  id: number;

  // link
  pickRequestId: string;
  pickMode: string;

  // field
  pickPriorityType: string;
  pickPriorityLevel: string;
  DeliveryWindows: Array<DeliveryWindow>;
  pickPriorityByTimes: Array<PickPriorityByTime>;

  constructor() {

  }

  setData(pickRequestId: string, pickMode: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error(ERR_pickPriority);
    }

    this.pickRequestId = pickRequestId;
    this.pickMode = pickMode;

    // pickPriorityType
    if (!data.hasOwnProperty('pickPriorityType') || data.pickPriorityType == null || data.pickPriorityType.trim().length == 0) {
      throw new Error(ERR_pickPriorityType);
    }
    this.pickPriorityType = data.pickPriorityType;

    // pickPriorityLevel (not required)
    if (!data.hasOwnProperty('pickPriorityLevel') || data.pickPriorityLevel == null || data.pickPriorityLevel.trim().length == 0) {
      if (this.pickPriorityType == PICK_PRIORITY_TYPE.FIXED) {
        throw new Error(ERR_pickPriorityLevel);
      }
    }
    this.pickPriorityLevel = data.pickPriorityLevel;

    this.DeliveryWindows = new Array();
    if ((data.hasOwnProperty('DeliveryWindow') && data.DeliveryWindow != null) || (data.hasOwnProperty('deliveryWindow') && data.deliveryWindow != null)) {
      if (!(data.DeliveryWindow instanceof Array) && !(data.deliveryWindow instanceof Array)) {
        throw new Error(ERR_DeliveryWindow);
      }

      let deliveryWindowArray = data.DeliveryWindow && data.DeliveryWindow != null ? data.DeliveryWindow : data.deliveryWindow;
      for (let item of deliveryWindowArray) {
        let deliveryWindow = new DeliveryWindow();
        deliveryWindow.setData(this.pickRequestId, item);
        this.DeliveryWindows.push(deliveryWindow);
      }
    }

    // pickPriorityTime (not required)
    // if (!data.hasOwnProperty('pickPriorityTime')) {
    //   throw new Error(ERR_pickPriorityTime);
    // }

    if (this.pickPriorityType == PICK_PRIORITY_TYPE.TIME_TO_DELIVERY) {
      if (!data.hasOwnProperty('pickPriorityByTime')) {
        throw new Error(ERR_missingPickPriorityByTime)
      }

      if (data.pickPriorityByTime.length == 0) {
        throw new Error(ERR_missingPickPriorityByTime);
      }
    }

    this.pickPriorityByTimes = new Array();
    if (data.hasOwnProperty('pickPriorityByTime') && data.pickPriorityByTime != null) {
      if (!(data.pickPriorityByTime instanceof Array)) {
        throw new Error(ERR_pickPriorityByTime);
      }

      for (let item of data.pickPriorityByTime) {
        let pickPriorityByTime = new PickPriorityByTime();
        pickPriorityByTime.setData(this.pickRequestId, item);
        this.pickPriorityByTimes.push(pickPriorityByTime);
      }
    }

  }

  getData(): Object {
    return {
      pickPriorityType: this.pickPriorityType,
      pickPriorityLevel: this.pickPriorityLevel,
      DeliveryWindow: this.DeliveryWindows,
      pickPriorityByTime: this.pickPriorityByTimes
    };
  }

  async save(conn: any) {
    let affectedRows = 0;
    await this.insertData(conn).then((res: any) => affectedRows += res.rowCount);
    for await (let item of this.DeliveryWindows) {
      await item.save(conn).then((res: any) => affectedRows += res.rowCount);
    }
    for await (let item of this.pickPriorityByTimes) {
      await item.save(conn).then((res: any) => affectedRows += res.rowCount);
    }
    return Promise.resolve(affectedRows);
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'pickMode',
      'pickPriorityType',
      'pickPriorityLevel',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.pickMode,
        this.pickPriorityType,
        this.pickPriorityLevel,
        new Date()
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }

}
