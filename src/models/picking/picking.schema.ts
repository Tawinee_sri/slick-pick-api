/**
 * Const for fulfilment interface data
 */
module.exports.PICKING_SCHEMA = {
  type: "object",
  properties: {
    pickRequest: {
      type: "object",
      properties: {
        pickRequestId: { type: "string", example: "XXX-123-XXX" },
        pickRequestDate: { type: "string", example: "2019-01-28 15:47:07" },
        subOrderKey: { type: "string", example: "XXX-123-S01" },
        sourceSubOrderId: { type: "string", example: "S01-123-S01" },
        orderKey: { type: "string", example: "ORD201901011230001" },
        sourceOrderId: { type: "string", example: "123-20190101001" },
        bu: { type: "string", example: "CDS" },
        sourceBU: { type: "string", example: "00000" },
        sourceLoc: { type: "string", example: "000000001" },
        status: { type: "string", example: "0001" },
        statusReason: { type: "string", nullable: true, example: "" },
        channel: { type: "string", example: "" },
        deliveryType: { type: "string", example: "", nullable: true },
        deliverySubType: { type: "string", example: "", nullable: true },
        fullTaxInvoice: { type: "string", example: "" },
        giftWrapFlag: { type: "string", example: "" },
        remarks: {
          type: "array",
          items: {
            type: "object",
            properties: {
              messageType: { type: 'string', example: 'Y' },
              message: { type: 'string', nullable: true, example: '' }
            }
          }
        },
        pickingParam: {
          type: "object",
          properties: {
            pickMode: { type: "string", example: "0001" },
            pickAll: { type: "string", example: "Y" },
            receivingScan: { type: "string", example: "Y" },
            locHandling: { type: "string", example: "Y" },
            receiptPhotoTake: { type: "string", example: "Y" },
            pickAllocation: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  pickAllocationTypeSeq: { type: "number", example: 1 },
                  pickAllocationType: { type: "string", example: "0001" },
                  pickCatLvl: { type: "number", nullable: true }
                }
              }
            },
            pickAssignType: { type: "string", example: "0001" },
            pickSubAssignType: {
              type: "string",
              example: "0001",
              nullable: true
            },
            pickPriority: {
              type: "object",
              properties: {
                pickPriorityType: { type: "string", example: "0002" },
                pickPriorityLevel: {
                  type: "string",
                  example: "0002",
                  nullable: true
                },
                DeliveryWindow: {
                  type: "array",
                  nullable: true,
                  items: {
                    type: "object",
                    properties: {
                      slotId: { type: "string", example: "XXXXXX" },
                      startTime: { type: "string", example: "2019-11-23  15:47:07" },
                      endTime: { type: "string", example: "2019-11-23  15:47:07" }
                    }
                  }
                },
                pickPriorityByTime: {
                  type: "array",
                  nullable: true,
                  items: {
                    type: "object",
                    properties: {
                      pickPriorityLevel: { type: "string", example: "0001" },
                      pickPriorityTime: {
                        type: "string",
                        example: "2019-11-23  15:47:07"
                      }
                    }
                  }
                }
              }
            }
          }
        },
        items: {
          type: "array",
          items: {
            type: "object",
            properties: {
              itemKey: { type: "string", example: "ITM10503C" },
              sourceItemId: { type: "string", example: "ITM10500C" },
              sourceItemNumber: { type: "number", example: 0 },
              barcode: { type: "string", example: "8850379708036" },
              maxBarcode: { type: "string", example: "8850379708036", nullable: true },
              sku: { type: "string", example: "Fd1oXKlsVp0Lz8Op9NFxJGxCbIeyK3" },
              catalogInfo: { type: "string", example: "", nullable: true },
              sourcePrice: { type: "number", example: 0, nullable: true },
              standardPrice: { type: "number", example: 0, nullable: true },
              currency: { type: "string", example: "THB" },
              color: { type: "string", example: "Red", nullable: true },
              size: { type: "string", example: "AA", nullable: true },
              gender: { type: "string", example: "gender" },
              realBrandCode: { type: "string", example: "realBrandCode", nullable: true },
              brandCode: { type: "string", example: "brandCode", nullable: true },
              itemLocCode: { type: "string", example: "itemLocCode" },
              itemLocDesc: { type: "string", example: "itemLocDesc" },
              itemAreaCode: { type: "string", example: "itemAreaCode" },
              itemAreaDesc: { type: "string", example: "itemAreaDesc" },
              qty: { type: "number", example: 10 },
              qtyUnit: { type: "string", example: "pcs" },
              weightedItem: { type: "string", example: "Y" },
              weight: { type: "number", example: 10, nullable: true },
              weightUnit: { type: "string", example: "KG", nullable: true },
              productNameTH: { type: "string" },
              productNameEN: { type: "string" },
              productNameIT: { type: "string", nullable: true },
              productNameDE: { type: "string", nullable: true },
              imageUrl: { type: "string", example: "" },
              cat1: { type: "string", example: "" },
              cat2: { type: "string", example: "" },
              cat3: { type: "string", example: "" },
              cat4: { type: "string", example: "" },
              cat5: { type: "string", example: "" },
              cat6: { type: "string", example: "" },
              brand: { type: "string" },
              zone: { type: "string" },
              status: { type: "string", example: "Y" },
              statusReason: { type: "string", nullable: true, example: "" },
              statusDate: { type: "string", example: "2019-01-28 15:47:07" },
              itemRemarks: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    messageType: { type: 'string', example: '' },
                    message: { type: 'string', nullable: true, example: '' }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};

module.exports.CNS_UPLOAD_SCHEMA = {};

module.exports.PICK_TASK_ALLOCATION = {
  type: "object",
  properties: {
    user_id: { type: "string", example: "xxxx" },
    role: { type: "string", example: "xxxx" },
    bu: { type: "string", example: "CDS" },
    loc: { type: "string", example: "10116" }
  }
};

module.exports.PICK_TASK_MERGEORDER = {
  type: "object",
  properties: {
    bu: { type: "string", example: "xxxx" },
    loc: { type: "string", example: "10116" },
    user_id: { type: "string", example: "xxxx" },
    role: { type: "string", example: "xxxx" },
    mergeOrder: {
      type: "array",
      items: {
        type: "object",
        properties: {
          orderId: { type: 'string', example: 'xxxx' },
        }
      }
    },
  }
}

module.exports.PICK_ASSIGN_PICK_TASK = {
  type: "object",
  properties: {
    user_id: { type: "string", example: "xxxx" },
    role: { type: "string", example: "xxxx" },
    task_id: { type: "number", example: 1 },
    checkAppVersion: { type: "string", example: "Y", nullable: true },
    appVersion: { type: "string", example: "0.0.0.0", nullable: true },
  }
};

module.exports.SCAN_BARCODE = {
  type: "object",
  properties: {
    barcode: { type: "string", example: "6925409712801" },
    task_id: { type: "number", example: 1 },
    user_id: { type: "string", example: "0001" },
    bu: { type: "string", example: "CSV" },
    orderKey: { type: "string", example: "ORD201901011230001" }
  }
};

module.exports.UPDATE_ITEM_STATUS = {
  type: "object",
  properties: {
    barcode: { type: "string", example: "1391541" },
    sku: { type: "string", example: "Fd1oXKlsVp0Lz8Op9NFxJGxCbIeyK3" },
    itemKey: { type: "string", example: "ITM10503C" },
    orderKey: { type: "string", example: "ORD201901011230001" },
    task_id: { type: "number", example: 1 },
    itemStatusID: { type: "number", example: 501 },
    set_status: { type: "string", example: "0002" },
    reason: { type: "string", example: "test" },
    qty: { type: "number", example: 2 },
    isSuppApprove: { type: "string", example: "Y", nullable: true }
  }
};

module.exports.PACK_RECEIVE_ITEM_STATUS = {
  type: "array",
  items: {
    type: "object",
    properties: {
      task_id: { type: "number", example: 1 },
      user_id: { type: "string", example: "Fulfilment003" },
      item_barcode: { type: "string", example: "8850379708036" },
      location_barcode: { type: "string", example: "" },
      orderKey: { type: "string", example: "ORD201901011230001" },
      isApproveDecline: { type: "string", example: "Y", nullable: true },
    }
  }
};

module.exports.PICKTASK_SCHEMA = {
  type: "object",
  properties: {
    result: { type: "string", example: "S" },
    statusCode: { type: "number", example: 200 },
    statusMessage: { type: "string", example: "Success" },
    data: {
      type: "array",
      items: {
        type: "object",
        properties: {
          id: { type: "number", example: 312 },
          wave_id: { type: "number", example: 0 },
          multiorder_id: { type: "number", example: 0 },
          priority: { type: "number", example: 29856 },
          pickPriorityTimeLevel: { type: "string", example: "" },
          createdDate: { type: "string", example: "2019-08-27 16:32:50" },
          orders: {
            type: "array",
            items: {
              type: "object",
              properties: {
                pickRequestId: {
                  type: "string",
                  example: "SLICKPICKING00000009998"
                },
                pickRequestDate: {
                  type: "string",
                  example: "2019-08-23 14:46:02"
                },
                subOrderKey: { type: "string", example: "102" },
                sourceSubOrderId: {
                  type: "string",
                  example: "CO20190820039902-1"
                },
                orderKey: { type: "string", example: "102" },
                sourceOrderId: { type: "string", example: "CO20190820039902" },
                bu: { type: "string", example: "CDS" },
                status: { type: "string", example: "0002" },
                pickPriorityType: { type: "string", example: "0001" },
                pickPriorityLevel: { type: "string", example: "0001" },
                pickPriorityTimeLevel: { type: "string", example: "" },
                pickPriorityTime: {
                  type: "string",
                  example: "2019-08-07 15:47:07"
                }
              }
            }
          },
          status: { type: "string", example: "0001" },
          statusReason: { type: "string", example: "defect" },
          statusDate: { type: "string", example: "2019-08-27 16:32:50" },
          items: {
            type: "array",
            items: {
              type: "object",
              properties: {
                pickRequestId: {
                  type: "string",
                  example: "SLICKPICKING00000009998"
                },
                pickAllFlag: { type: "string", example: "N" },
                itemId: { type: "number", example: "726" },
                itemKey: { type: "string", example: "127" },
                sourceItemId: { type: "string", example: "11293-1205" },
                sourceItemNumber: { type: "number", example: 5 },
                barcode: { type: "string", example: "17070638" },
                sku: { type: "string", example: "4830945" },
                qty: { type: "number", example: 1 },
                qtyUnit: { type: "string", example: "pcs" },
                productNameTH: {
                  type: "string",
                  example: '"GoPro สายคล้องคอ สีขาว"'
                },
                productNameEN: {
                  type: "string",
                  example: '"GoPro SLEEVE LANYARD WHITE"'
                },
                imageUrl: {
                  type: "string",
                  example:
                    '"https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg"'
                },
                locBarcode: { type: "string", example: "" },
                cat1: { type: "string", example: "400" },
                cat2: { type: "string", example: "412" },
                cat3: { type: "string", example: "402" },
                cat4: { type: "string", example: "2" },
                cat5: { type: "string", example: "0" },
                cat6: { type: "string", example: "null" },
                brand: { type: "string", example: "GOPRO" },
                zone: { type: "string", example: "null" },
                status: { type: "string", example: "New" },
                statusReason: { type: "string", example: "" },
                statusDate: { type: "string", example: "2019-08-20 15:22:07" }
              }
            }
          }
        }
      }
    }
  }
};

module.exports.GET_TASK_DATA = {
  type: "object",
  properties: {
    task_id: { type: "number", example: 1 }
  }
};

module.exports.CANCEL_ORDER_ITEM = {
  type: "object",
  properties: {
    lineStatusUpdate: {
      type: "object",
      properties: {
        lineStatusUpdateId: {
          type: "string",
          example: "LineUpdate000000000221"
        },
        pickRequestDate: { type: "string", example: "2019-10-24 16:47:02" },
        subOrderKey: { type: "string", example: "57" },
        sourceSubOrderId: { type: "string", example: "CO20190820000801-1" },
        orderKey: { type: "string", example: "94" },
        sourceOrderId: { type: "string", example: "CO20190820000801" },
        bu: { type: "string", example: "CDS" },
        items: {
          type: "array",
          items: {
            type: "object",
            properties: {
              itemKey: { type: "string", example: "107" },
              sourceItemId: { type: "string", example: "1-2101" },
              sourceItemNumber: { type: "number", example: 1 },
              qty: { type: "number", example: 2 },
              barcode: { type: "string", example: "10095737" },
              status: { type: "string", example: "0005" },
              statusReason: { type: "string", example: "" },
              statusBy: { type: "string", example: "Online" },
              statusDate: { type: "string", example: "2019-10-24 15:22:07" }
            }
          }
        }
      }
    }
  }
};

module.exports.POST_USER_SCHEMA = {
  type: "object",
  required: [
    "userId",
    "cat_01",
    "cat_02",
    "cat_03",
    "cat_04",
    "cat_05",
    "zone",
    "brand",
    "bu",
    "loc",
    "status"
  ],
  properties: {
    userId: { type: "string", example: "00001" },
    cat_01: { type: "string", example: "A" },
    cat_02: { type: "string", example: "A" },
    cat_03: { type: "string", example: "A" },
    cat_04: { type: "string", example: "A" },
    cat_05: { type: "string", example: "A" },
    cat_06: { type: "string", example: "A" },
    zone: { type: "string", example: "A" },
    brand: { type: "string", example: "A" },
    remark: { type: "string", example: "" },
    bu: { type: "string", example: "CDS" },
    loc: { type: "string", example: "10116" },
    status: { type: "string", example: "A" }
  }
};

module.exports.PUT_USER_SCHEMA = {
  type: "object",
  required: ["userId"],
  properties: {
    userId: { type: "string", example: "00001" },
    cat_01: { type: "string", example: "A" },
    cat_02: { type: "string", example: "A" },
    cat_03: { type: "string", example: "A" },
    cat_04: { type: "string", example: "A" },
    cat_05: { type: "string", example: "A" },
    cat_06: { type: "string", example: "A" },
    zone: { type: "string", example: "A" },
    brand: { type: "string", example: "A" },
    remark: { type: "string", example: "" },
    bu: { type: "string", example: "CDS" },
    loc: { type: "string", example: "10116" },
    status: { type: "string", example: "A" }
  }
};

module.exports.DEL_USER_SCHEMA = {
  type: "object",
  required: ["userId"],
  properties: {
    userId: { type: "string", example: "00001" }
  }
};

module.exports.SHIPMENT_UPDATE_REQUEST = {
  type: 'object',
  properties: {
    pickShipmentUpdateRequest: {
      type: 'object',
      properties: {
        updateId: { type: 'string', example: '' },
        updateIdDate: { type: 'string', example: '' },
        subOrderKey: { type: 'string', example: '' },
        sourceSubOrderId: { type: 'string', example: '' },
        orderKey: { type: 'string', example: '' },
        sourceOrderId: { type: 'string', example: '' },
        pickingParam: {
          type: 'object',
          properties: {
            pickPriority: {
              type: 'object',
              properties: {
                DeliveryWindow: {
                  type: 'array',
                  nullable: true,
                  items: {
                    type: 'object',
                    properties: {
                      startTime: { type: 'string', nullable: true, example: '' },
                      endTime: { type: 'string', example: '' },
                    }
                  }
                },
                pickPriorityByTime: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: {
                      pickPriorityLevel: { type: 'string', nullable: true, example: '' },
                      pickPriorityTime: { type: 'string', nullable: true, example: '' },
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

module.exports.UPDATE_Q_SCHEMA = {
  type: 'object',
  properties: {
    limit: { type: 'number', nullable: true, example: 10 }
  }
}

module.exports.LOGINAUTH = {
  type: 'object',
  properties: {
    username: { type: 'string', example: 'X10user' },
    password: { type: 'string', example: '1234' }
  }
}

module.exports.PICK_LIST = {
  type: "object",
  properties: {
    fulfillmentType: { type: "string", example: "xxxx" },
    wave_id: { type: "string", example: "xxxx" },
    sourceBU: { type: "string", example: "CDS" },
    sourceLoc: { type: "string", example: "10116" },
    subDeliveryType: { type: "string", example: "10116" },
    requestDateStart: { type: "string", example: "2021/05/20" },
    requestDateTo: { type: "string", example: "2021/05/20" },
  },
};
