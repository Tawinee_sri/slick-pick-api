import { UtilModel } from "../../UtilModel";
import { PICK_ALLOCATION_TYPE } from "../constant";


/**
 * Declear Error Message
 */
const ERR_pickAllocationTypeSeq = '30401,Error Required Field';
const ERR_pickAllocationType = '30402,Error Required Field';
const ERR_pickCatLvl = '30403,Error Required Field';

export class PickAllocation {

  // config
  USE_TABLE: string = 'pr_pickparams_allocation';

  // pk
  id: number;

  // link
  pickRequestId: string;
  pickMode: string;

  // field
  pickAllocationTypeSeq: number;
  pickAllocationType: string;
  pickCatLvl: number;

  constructor() { }

  setData(pickRequestId: string, pickMode: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Cuurent Data is not an object');
    }

    this.pickRequestId = pickRequestId;
    this.pickMode = pickMode;

    // pickAllocationTypeSeq
    if (!data.hasOwnProperty('pickAllocationTypeSeq') || data.pickAllocationTypeSeq == null) {
      throw new Error(ERR_pickAllocationTypeSeq);
    }
    this.pickAllocationTypeSeq = data.pickAllocationTypeSeq;

    // pickAllocationType
    if (!data.hasOwnProperty('pickAllocationType') || data.pickAllocationType == null || data.pickAllocationType.trim().length == 0) {
      throw new Error(ERR_pickAllocationType);
    }
    this.pickAllocationType = data.pickAllocationType;

    // pickCatLvl (not required)
    if (!data.hasOwnProperty('pickCatLvl') || data.pickCatLvl == null) {
      if (this.pickAllocationType == PICK_ALLOCATION_TYPE.BY_CATEGORY) {
        throw new Error(ERR_pickCatLvl);
      }
    }
    this.pickCatLvl = data.pickCatLvl;

  }

  getData(): Object {
    return {
      pickAllocationTypeSeq: this.pickAllocationTypeSeq,
      pickAllocationType: this.pickAllocationType,
      pickCatLvl: this.pickCatLvl
    };
  }

  async save(conn: any) {
    return await this.insertData(conn);
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'pickMode',
      'pickAllocationTypeSeq',
      'pickAllocationType',
      'pickCatLvl',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.pickMode,
        this.pickAllocationTypeSeq,
        this.pickAllocationType,
        this.pickCatLvl,
        new Date()
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }

}
