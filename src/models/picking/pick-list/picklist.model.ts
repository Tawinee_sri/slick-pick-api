import { UtilModel } from "../../UtilModel";
import { ITEM_STATUS } from "../constant";
import { IfileExcel, PICK_STATUS, THEN_PICK_STATUS } from "./constant";
const xl = require("excel4node");
const fs = require("fs");

export class PickListModel {

  async picklist(reqBody: any) {
    return new Promise(async (resolve, reject) => {
      try {
        let sql = `
          select
            p.id itemid , pdi.id itemdetailid ,
            pr.pickrequestid , pr.sourceorderid , pr.sourcesuborderid ,
            pr.sourcebu fullfillmentbu , pr.sourceloc fullfillmentloc , pr.giftwrapflag ,
            p.productnameth , 1 qty, p.barcode , p.maxbarcode , p.cataloginfo ,
            p.brand , p.cat2 ,
            pt.id::varchar pickingtaskno , concat('*',pt.id,'*') as pickingtasknobarcode,
            (
              CASE
                WHEN pdi.itemstatus = '${ITEM_STATUS.NEW}' THEN '${THEN_PICK_STATUS.THEN_New}'
                WHEN pdi.itemstatus = '${ITEM_STATUS.PENDING}' THEN '${THEN_PICK_STATUS.THEN_Picking}'
                WHEN pdi.itemstatus = '${ITEM_STATUS.PICKED}' THEN '${THEN_PICK_STATUS.THEN_Picked}'
                WHEN pdi.itemstatus = '${ITEM_STATUS.READY_TO_PACK}' THEN '${THEN_PICK_STATUS.THEN_Packing}'
                WHEN pdi.itemstatus = '${ITEM_STATUS.DECLINED}' THEN '${THEN_PICK_STATUS.THEN_Declined}'
                ELSE concat('others:', pdi.itemstatus)
              END
            ) AS linestatus
          from ${UtilModel.SCHEMA}.pick_request pr
          join ${UtilModel.SCHEMA}.pick_task_detail pd on pd.pickrequestid = pr.pickrequestid
          join ${UtilModel.SCHEMA}.pick_task pt on pt.id = pd.picktaskid
          join ${UtilModel.SCHEMA}.pick_task_detail_item pdi on pdi.picktaskdetailid = pd.id
          join ${UtilModel.SCHEMA}.pr_items p on p.pickrequestid = pd.pickrequestid and p.itemkey = pd.itemkey and p.id = pd.pickitemid
          where 1=1
          and p.brand is not null
          and p.cat2 is not null
          and pr.sourcebu = ?
          and pr.sourceloc = ?
        `;
        if (reqBody.subDeliveryType == '' && reqBody.fulfillmentType.toUpperCase() === 'PICKLIST') {
          sql += `
            and (pt.user_id is null or pt.user_id = '')
            and pt.status = '0001'
            and (pt.wave_id is null or pt.wave_id = 0)
            and pr.deliverysubtype in ('0001', '0002', '0003')
          `;
        } else if (reqBody.subDeliveryType == '' && reqBody.fulfillmentType.toUpperCase() === 'PICKLIST_WAVE') {
          let w_id = "";
          if (reqBody.wave_id[reqBody.wave_id.length - 1] === ",") {
            w_id = reqBody.wave_id.slice(0, reqBody.wave_id.length - 1);
          } else {
            w_id = reqBody.wave_id;
          }
          sql += `
            and (pt.user_id  is not null or pt.user_id <> '')
            and pt.status = '0003'
            and pt.wave_id in (${w_id})
          `;
        } else {
          sql += `
            and pr.deliverysubtype = '${reqBody.subDeliveryType}'
            and pt.status in ('0001')
          `;
        }
        console.log('sql : ', sql)

        let data: any = await UtilModel.getInstance().sqlQuery(sql, [reqBody.sourceBU, reqBody.sourceLoc]);

        let dataGroupArray: any = [];
        for (let i of data) {
          let indx = dataGroupArray.findIndex(d => d.itemid === i.itemid);
          if (indx > -1) {
            dataGroupArray[indx].Qty += 1;
          } else {
            dataGroupArray.push({
              itemid: i.itemid,
              OrderId: i.sourceorderid,
              BarcodeIBC: i.barcode,
              BarcodeSBC: i.maxbarcode,
              Description: i.productnameth,
              Catalogue: i.cataloginfo,
              LineStatus: i.linestatus,
              PickingTaskNo: i.pickingtaskno,
              PickingTaskNoBarcode: i.pickingtasknobarcode,
              Qty: parseInt(i.qty),
              FullTaxRequired: "N",
              GiftWrapMessage: i.giftwrapflag,
              cat2: i.cat2,
              Brand: i.brand,
            })
          }
        }

        let dataPrepareXls: any = [];
        let i = 0;
        for (let d of dataGroupArray) {
          let indx = dataPrepareXls.findIndex(i => i.cat2 === d.cat2 && i.brand === d.Brand);
          if (indx === -1) {
            dataPrepareXls.push({
              sheet_name: 'PICKLIST ' + (i + 1),
              brand: d.Brand,
              cat2: d.cat2,
              fontBarcode: 'Free 3 of 9 Extended',
              fontName: 'Calibri',
              fulfilmentBU: reqBody.sourceBU,
              fulfilmentLoc: reqBody.sourceLoc,
              data: [d]
            })
          } else {
            dataPrepareXls[indx].data.push(d);
          }
          i++;
        }

        if (dataPrepareXls.length == 0) {
          reject('No data found.');
          return;
        }

        let excel: any = await this.exportExcelPicklist(reqBody, dataPrepareXls);
        console.log('excel : ', excel);

        setTimeout(async () => {
          let file = await fs.readFileSync(`${excel.filepath}${excel.fileName}`);
          console.log("file : ", file);

          resolve(file);
        }, 2000);

      } catch (error) {
        reject(error);
      }
    })
  }

  async exportExcelPicklist(reqBody: any, data: any) {
    return new Promise(async (resolve, reject) => {
      const wb = new xl.Workbook({
        defaultFont: {
          size: 11,
          name: "Calibri",
        },
      });

      for (let e = 0; e < data.length; e++) {
        await this.setSheet(wb, data[e]).then((res) => { }).catch((err) => console.log(err));
      }

      console.log('wb : ', wb)
      const filepath = "./excel_export";
      if (!fs.existsSync(filepath)) {
        fs.mkdirSync(filepath);
      }

      console.log("exportExcelPicklist", data);

      const fileName = `/${reqBody.fulfillmentType}${reqBody.wave_id}_export_${new Date().toISOString().slice(0, 10)}_${new Date().getTime()}.xlsx`;

      await wb.write(filepath + fileName);

      const file: IfileExcel = {
        filepath: filepath,
        fileName: fileName,
      };
      resolve(file);
    })
  }

  private async setSheet(wb, sectionTitle) {
    console.log('sectionTitle : ', sectionTitle);
    let jsonData = sectionTitle.data;
    var sheet = wb.addWorksheet(sectionTitle.sheet_name);
    var col = 8;
    var pickList = wb.createStyle({
      font: {
        size: 35,
        bold: true,
        name: sectionTitle.fontName,
      },
      alignment: {
        vertical: "center",
        horizontal: "center",
      },
    });
    var profile = wb.createStyle({
      font: {
        size: 11,
        name: sectionTitle.fontName,
      },
      alignment: {
        vertical: "center",
        horizontal: "left",
      },
    });
    var profileValue = wb.createStyle({
      font: {
        size: 11,
        name: sectionTitle.fontName,
      },
      alignment: {
        vertical: "center",
        horizontal: "left",
      },
      border: {
        bottom: {
          style: "thin",
          color: "black",
        },
        outline: false,
      },
    });
    var botoomBorder = wb.createStyle({
      border: {
        bottom: {
          style: "thick",
          color: "black",
        },
        outline: false,
      },
    });
    var barcode = wb.createStyle({
      font: {
        size: 48,
        name: sectionTitle.fontBarcode,
      },
      border: {
        left: {
          style: "thin",
          color: "black",
        },
        right: {
          style: "thin",
          color: "black",
        },
        top: {
          style: "thin",
          color: "black",
        },
        bottom: {
          style: "thin",
          color: "black",
        },
        outline: false,
      },
      alignment: {
        vertical: "top",
        horizontal: "center",
      },
    });
    var fontText = wb.createStyle({
      font: {
        size: 11,
        name: sectionTitle.fontName,
      },
      alignment: {
        vertical: "top",
        horizontal: "left",
      },
      border: {
        left: {
          style: "thin",
          color: "black",
        },
        right: {
          style: "thin",
          color: "black",
        },
        top: {
          style: "thin",
          color: "black",
        },
        bottom: {
          style: "thin",
          color: "black",
        },
        outline: false,
      },
    });
    var headerText = wb.createStyle({
      font: {
        size: 11,
        name: sectionTitle.fontName,
      },
      fill: {
        type: "pattern",
        patternType: "solid",
        bgColor: "#cacaca",
        fgColor: "#cacaca",
      },
      border: {
        left: {
          style: "thin",
          color: "black",
        },
        right: {
          style: "thin",
          color: "black",
        },
        top: {
          style: "thin",
          color: "black",
        },
        bottom: {
          style: "thin",
          color: "black",
        },
        outline: false,
      },
    });

    sheet.column(1).setWidth(20);
    sheet.column(2).setWidth(15);
    sheet.column(3).setWidth(15);
    sheet.column(4).setWidth(20);
    sheet.column(5).setWidth(10);
    sheet.column(6).setWidth(10);
    sheet.column(7).setWidth(20);
    sheet.column(8).setWidth(35);
    sheet.column(9).setWidth(10);
    sheet.column(10).setWidth(15);
    sheet.column(11).setWidth(15);

    sheet
      .cell(1, 2, 1, 11, true)
      .string("PICK LIST")
      .style(pickList);

    /* Profile start */
    sheet
      .cell(3, 1)
      .string("Fulfilment BU:")
      .style(profile);
    sheet
      .cell(4, 1)
      .string("Fulfilment Location :")
      .style(profile);
    sheet
      .cell(5, 1)
      .string("Brand :")
      .style(profile);
    sheet
      .cell(6, 1)
      .string("Cat 2 :")
      .style(profile);

    sheet
      .cell(3, 2)
      .string(sectionTitle.fulfilmentBU)
      .style(profileValue);
    sheet
      .cell(4, 2)
      .string(sectionTitle.fulfilmentLoc)
      .style(profileValue);
    sheet
      .cell(5, 2)
      .string(sectionTitle.brand)
      .style(profileValue);
    sheet
      .cell(6, 2)
      .string(sectionTitle.cat2)
      .style(profileValue);
    /* Profile end */

    /* hader start */
    sheet
      .cell(col, 1)
      .string("Order Id")
      .style(headerText);
    sheet
      .cell(col, 2)
      .string("Barcode/IBC")
      .style(headerText);
    sheet
      .cell(col, 3)
      .string("Barcode/SBC")
      .style(headerText);
    sheet
      .cell(col, 4)
      .string("Description")
      .style(headerText);
    sheet
      .cell(col, 5)
      .string("Catalogue")
      .style(headerText);
    sheet
      .cell(col, 6)
      .string("Line Status")
      .style(headerText);
    sheet
      .cell(col, 7)
      .string("Picking Task No.")
      .style(headerText);
    sheet
      .cell(col, 8)
      .string("Picking TaskNo Barcode")
      .style(headerText);
    sheet
      .cell(col, 9)
      .string("Qty")
      .style(headerText);
    sheet
      .cell(col, 10)
      .string("FullTax Required")
      .style(headerText);
    sheet
      .cell(col, 11)
      .string("GiftWrap Message")
      .style(headerText);

    for (let i = 0; i < jsonData.length; i++) {
      if (i + 1 === jsonData.length) {
        const t = true;
        await this.setCell(sheet, i, col, jsonData[i], fontText, barcode, t, profile, botoomBorder);
      } else {
        const t = false;
        await this.setCell(sheet, i, col, jsonData[i], fontText, barcode, t, profile, botoomBorder);
      }
    }
    return new Promise((resolve, reject) => { resolve(sheet); });
  }

  private async setCell(wbdata, o, l, data, fontText, barcode, t, profile, botoomBorder) {
    wbdata.row(o + (l + 1)).setHeight(65);
    wbdata
      .cell(o + (l + 1), 1)
      .string(data.OrderId)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 2)
      .string(data.BarcodeIBC)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 3)
      .string(data.BarcodeSBC)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 4)
      .string(data.Description)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 5)
      .string(data.Catalogue)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 6)
      .string(data.LineStatus)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 7)
      .string(data.PickingTaskNo)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 8)
      .string(data.PickingTaskNoBarcode)
      .style(barcode);
    wbdata
      .cell(o + (l + 1), 9)
      .number(data.Qty)
      .style(fontText)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 10)
      .string(data.FullTaxRequired)
      .style(fontText);
    wbdata
      .cell(o + (l + 1), 11)
      .string(data.GiftWrapMessage)
      .style(fontText);

    if (t === true) {
      const fo = o + (l + 3);
      wbdata
        .cell(fo, 3)
        .string("พนักงานจัดสินค้า")
        .style(profile);
      wbdata
        .cell(fo + 2, 3)
        .string("อนุมัติโดย")
        .style(profile);
      wbdata
        .cell(fo, 4, fo, 5, true)
        .string("")
        .style(botoomBorder);
      wbdata
        .cell(fo + 2, 4, fo + 2, 5, true)
        .string("")
        .style(botoomBorder);

      wbdata
        .cell(fo, 8)
        .string("รับสินค้าโดย")
        .style(profile);
      wbdata
        .cell(fo + 2, 8)
        .string("พนักงาน GR")
        .style(profile);
      wbdata
        .cell(fo, 9, fo, 11, true)
        .string("")
        .style(botoomBorder);
      wbdata
        .cell(fo + 2, 9, fo + 2, 11, true)
        .string("")
        .style(botoomBorder);
    }
    return new Promise((resolve, reject) => {
      resolve(wbdata);
    });
  }

}
