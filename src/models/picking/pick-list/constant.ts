export interface IfileExcel {
  filepath: string;
  fileName: string;
}

export const PICK_STATUS = {
  New: "1000",
  Picking: "0002",
  Picked: "2001",
  Packing: "2002",
  Shipped: "2004",
  CC_Requested: "2005",
  CC_Received: "2006",
  Store_Deliverd: "2007",
  Ready_To_Collect: "2008",
  Payment_Capture: "2009",
  Menifest_Pickup: "2010",
  In_Transit: "2011",
  Exception: "2012",
  Completed: "3000",
  Cancelled: "4000",
  Payment_Capture_Failed: "4001",
  Declined: "4100",
  Ship_Failed: "4101",
  Declined_End: "4100",
};

export const THEN_PICK_STATUS = {
  THEN_New: "1000:New",
  THEN_Picking: "0002:Picking",
  THEN_Picked: "2001:Picked",
  THEN_Packing: "2002:Packing",
  THEN_Shipped: "2004:Shipped",
  THEN_CC_Requested: "2005:CC Requested",
  THEN_CC_Received: "2006:CC Received",
  THEN_Store_Deliverd: "2007:Store Deliverd",
  THEN_Ready_To_Collect: "2008:Ready To Collect",
  THEN_Payment_Capture: "2009:Payment Capture",
  THEN_Menifest_Pickup: "2010:Menifest Pickup",
  THEN_In_Transit: "2011:In Transit",
  THEN_Exception: "2012:Exception",
  THEN_Completed: "3000:Completed",
  THEN_Cancelled: "4000:Cancelled",
  THEN_Payment_Capture_Failed: "4001:Payment Capture Failed",
  THEN_Declined: "4100:Declined",
  THEN_Ship_Failed: "4101:Ship Failed",
  THEN_Declined_End: "4100:Declined",
};
