import { LogUtil } from 'src/models/LogUtil';
import { dateToStrDate, strToDdate } from '../../DateUtils';
import { UtilModel } from '../../UtilModel';
import { ITEM_STATUS } from '../constant';
import { PickItemRemark } from '../pick-remark/pick-item-remark.model';
/**
 * Declear Error Message
 */
const ERR_itemKey = '30601,Error Required Field';
const ERR_sourceItemId = '30602,Error Required Field';
const ERR_sourceItemNumber = '30603,Error Required Field';
const ERR_barcode = '30604,Error Required Field';
const ERR_qty = '30605,Error Required Field';
const ERR_qtyUnit = '30606,Error Required Field';
const ERR_productNameTH = '30607,Error Required Field';
const ERR_productNameEN = '30608,Error Required Field';
const ERR_imageUrl = '30609,Error Required Field';
const ERR_cat1 = '30610,Error Required Field';
const ERR_cat2 = '30611,Error Required Field';
const ERR_cat3 = '30612,Error Required Field';
const ERR_cat4 = '30613,Error Required Field';
const ERR_cat5 = '30614,Error Required Field';
const ERR_cat6 = '30615,Error Required Field';
const ERR_brand = '30616,Error Required Field';
const ERR_zone = '30617,Error Required Field';
const ERR_status = '30618,Error Required Field';
const ERR_statusReason = '30619,Error Required Field';
const ERR_statusDate = '30620,Error Required Field';
const ERR_sku = '30621,Error Required Field';
const ERR_currency = '30622,Error Required Field';
const ERR_weightedItem = '30623,Error Required Field';

export class PickingItem {

  // config
  USE_TABLE: string = 'pr_items';

  // pk
  id: number;

  // link
  pickRequestId: string;

  // field
  itemKey: string;
  sourceItemId: string;
  sourceItemNumber: number;
  barcode: string;
  maxBarcode: string;
  sku: string;
  catalogInfo: string;
  sourcePrice: number;
  standardPrice: number;
  currency: string;
  color: string;
  size: string;
  gender: string;
  realBrandCode: string;
  brandCode: string;
  itemLocCode: string;
  itemLocDesc: string;
  itemAreaCode: string;
  itemAreaDesc: string;
  qty: number;
  qtyUnit: string;
  weightedItem: string;
  weight: number;
  weightUnit: string;
  productNameTH: string;
  productNameEN: string;
  productNameIT: string;
  productNameDE: string;
  imageUrl: string;
  cat1: string;
  cat2: string;
  cat3: string;
  cat4: string;
  cat5: string;
  cat6: string;
  brand: string;
  zone: string;
  status: string;
  statusReason: string;
  statusDate: string;

  subBarcode: string;

  // extra field for pack receive
  pickBarcodes: Array<string>;
  itemRemarks: Array<PickItemRemark>;

  setData(pickRequestId: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,item Data is not an object');
    }

    this.pickRequestId = pickRequestId;

    // itemKey
    if (!data.hasOwnProperty('itemKey')) {
      throw new Error(ERR_itemKey);
    }
    this.itemKey = data.itemKey;

    // sourceItemId
    if (!data.hasOwnProperty('sourceItemId')) {
      throw new Error(ERR_sourceItemId);
    }
    this.sourceItemId = data.sourceItemId;

    // sourceItemNumber (not required)
    // if (!data.hasOwnProperty('sourceItemNumber')) {
    //   throw new Error(ERR_sourceItemNumber);
    // }
    this.sourceItemNumber = data.sourceItemNumber;

    // barcode
    if (!data.hasOwnProperty('barcode')) {
      throw new Error(ERR_barcode);
    }
    this.barcode = data.barcode;

    // maxBarcode
    if (!data.hasOwnProperty('maxBarcode')) {
      data.maxBarcode = '';
    }
    this.maxBarcode = data.maxBarcode;

    // catalogInfo
    if (!data.hasOwnProperty('catalogInfo')) {
      data.catalogInfo = '';
    }
    this.catalogInfo = data.catalogInfo;

    // sku
    if (!data.hasOwnProperty('sku')) {
      throw new Error(ERR_sku);
    } else if (data.sku.trim().length == 0) {
      throw new Error(ERR_sku);
    }
    this.sku = data.sku;

    // sourcePrice
    this.sourcePrice = data.sourcePrice;

    // standardPrice
    this.standardPrice = data.standardPrice;

    // currenty
    if (!data.hasOwnProperty('currency')) {
      throw new Error(ERR_currency);
    }
    this.currency = data.currency;

    // color
    this.color = data.color;

    // size
    this.size = data.size;

    //gender
    if (!data.hasOwnProperty('gender') || data.gender == null || data.gender.trim().length == 0) {
      this.gender = "";
    } else {
      this.gender = data.gender;
    }

    // realBrandCode
    if (!data.hasOwnProperty('realBrandCode') || data.realBrandCode == null || data.realBrandCode.trim().length == 0) {
      data.realBrandCode = "";
    }
    this.realBrandCode = data.realBrandCode;

    // brandCode
    if (!data.hasOwnProperty('brandCode') || data.brandCode == null || data.brandCode.trim().length == 0) {
      data.brandCode = "";
    }
    this.brandCode = data.brandCode;

    //itemLocCode
    if (!data.hasOwnProperty('itemLocCode') || data.itemLocCode == null || data.itemLocCode.trim().length == 0) {
      this.itemLocCode = "";
    } else {
      this.itemLocCode = data.itemLocCode;
    }
    //itemLocDesc
    if (!data.hasOwnProperty('itemLocDesc') || data.itemLocDesc == null || data.itemLocDesc.trim().length == 0) {
      this.itemLocDesc = "";
    } else {
      this.itemLocDesc = data.itemLocDesc;
    }
    //itemAreaCode
    if (!data.hasOwnProperty('itemAreaCode') || data.itemAreaCode == null || data.itemAreaCode.trim().length == 0) {
      this.itemAreaCode = "";
    } else {
      this.itemAreaCode = data.itemAreaCode;
    }
    //itemAreaDesc
    if (!data.hasOwnProperty('itemAreaDesc') || data.itemAreaDesc == null || data.itemAreaDesc.trim().length == 0) {
      this.itemAreaDesc = "";
    } else {
      this.itemAreaDesc = data.itemAreaDesc;
    }
    // qty
    if (!data.hasOwnProperty('qty')) {
      throw new Error(ERR_qty);
    }
    this.qty = data.qty;

    // qtyUnit
    if (!data.hasOwnProperty('qtyUnit')) {
      throw new Error(ERR_qtyUnit);
    }
    this.qtyUnit = data.qtyUnit;

    // qtyUnit
    if (!data.hasOwnProperty('weightedItem')) {
      throw new Error(ERR_weightedItem);
    }
    this.weightedItem = data.weightedItem;

    // weight
    this.weight = data.weight;

    // weightUnit
    this.weightUnit = data.weightUnit;

    // productNameTH
    if (!data.hasOwnProperty('productNameTH')) {
      throw new Error(ERR_productNameTH);
    }
    this.productNameTH = data.productNameTH;

    // productNameEN
    if (!data.hasOwnProperty('productNameEN')) {
      throw new Error(ERR_productNameEN);
    }
    this.productNameEN = data.productNameEN;

    // productNameIT
    this.productNameIT = data.productNameIT;

    // productNameDE
    this.productNameDE = data.productNameDE;

    // imageUrl (not required)
    // if (!data.hasOwnProperty('imageUrl')) {
    //   throw new Error(ERR_imageUrl);
    // }
    this.imageUrl = data.imageUrl;

    // cat1 (not required)
    // if (!data.hasOwnProperty('cat1')) {
    //   throw new Error(ERR_cat1);
    // }
    this.cat1 = data.cat1;

    // cat2 (not required)
    // if (!data.hasOwnProperty('cat2')) {
    //   throw new Error(ERR_cat2);
    // }
    this.cat2 = data.cat2;

    // cat3 (not required)
    // if (!data.hasOwnProperty('cat3')) {
    //   throw new Error(ERR_cat3);
    // }
    this.cat3 = data.cat3;

    // cat4 (not required)
    // if (!data.hasOwnProperty('cat4')) {
    //   throw new Error(ERR_cat4);
    // }
    this.cat4 = data.cat4;

    // cat5 (not required)
    // if (!data.hasOwnProperty('cat5')) {
    //   throw new Error(ERR_cat5);
    // }
    this.cat5 = data.cat5;

    // cat6 (not required)
    // if (!data.hasOwnProperty('cat6')) {
    //   throw new Error(ERR_cat6);
    // }
    this.cat6 = data.cat6;

    // zone (not required)
    // if (!data.hasOwnProperty('zone')) {
    //   throw new Error(ERR_zone);
    // }
    this.zone = data.zone;

    // brand (not required)
    // if (!data.hasOwnProperty('brand')) {
    //   throw new Error(ERR_brand);
    // }
    this.brand = data.brand;

    // status
    if (!data.hasOwnProperty('status')) {
      throw new Error(ERR_status);
    }
    this.status = data.status;

    // statusReason (not required)
    // if (!data.hasOwnProperty('statusReason')) {
    //   throw new Error(ERR_statusReason);
    // }
    this.statusReason = data.statusReason;

    // statusDate
    if (!data.hasOwnProperty('statusDate')) {
      throw new Error(ERR_statusDate);
    }
    this.statusDate = data.statusDate;

    this.itemRemarks = new Array();
    if (data.hasOwnProperty('itemRemarks')) {
      if (!data.itemRemarks) {
        data.itemRemarks = [];
      }

      if (data.itemRemarks.length > 0) {
        for (let itemRemarks of data.itemRemarks) {
          let prItemRemark = new PickItemRemark();
          prItemRemark.setData(this.pickRequestId, this.itemKey, itemRemarks);
          this.itemRemarks.push(prItemRemark);
        }
      } else {
        let itemRemarks = {
          messageType: ".",
          messge: ""
        }
        let prItemRemark = new PickItemRemark();
        prItemRemark.setData(this.pickRequestId, this.itemKey, itemRemarks);
        this.itemRemarks.push(prItemRemark);
      }

    } else {
      let itemRemarks = {
        messageType: ".",
        messge: ""
      }
      let prItemRemark = new PickItemRemark();
      prItemRemark.setData(this.pickRequestId, this.itemKey, itemRemarks);
      this.itemRemarks.push(prItemRemark);
    }

    if (data.hasOwnProperty('subBarcodeList') && data.subBarcodeList.length > 0) {
      this.setSubBarcodeList(data.subBarcodeList);
    }

  }

  getData(): Object {
    return {
      itemKey: this.itemKey,
      sourceItemId: this.sourceItemId,
      sourceItemNumber: this.sourceItemNumber,
      barcode: this.barcode,
      subBarcodeList: this.subBarcode,
      sku: this.sku,
      sourcePrice: this.sourcePrice,
      standardPrice: this.standardPrice,
      currency: this.currency,
      color: this.color,
      size: this.size,
      gender: this.gender,
      realBrandCode: this.realBrandCode,
      brandCode: this.brandCode,
      itemLocCode: this.itemLocCode,
      itemLocDesc: this.itemLocDesc,
      itemAreaCode: this.itemAreaCode,
      itemAreaDesc: this.itemAreaDesc,
      qty: this.qty,
      qtyUnit: this.qtyUnit,
      weightedItem: this.weightedItem,
      weight: this.weight,
      weightUnit: this.weightUnit,
      productNameTH: this.productNameTH,
      productNameEN: this.productNameEN,
      productNameIT: this.productNameIT,
      productNameDE: this.productNameDE,
      imageUrl: this.imageUrl,
      status: this.status,
      statusReason: this.statusReason,
      statusDate: this.statusDate,
      itemRemarks: this.getItemRemarks()
    };
  }

  getItemRemarks(): Array<Object> {
    let itemRemarks = new Array<Object>();
    for (let i of this.itemRemarks) {
      itemRemarks.push(i.getData());
    }

    return itemRemarks;
  }

  setSubBarcodeList(barcodeList: any) {

    let subBarcode: any = [];
    for (let b of barcodeList) {
      subBarcode.push(b.subBarcode);
    }
    console.log('[setSubBarcodeList] =====>>> ', subBarcode);
    this.subBarcode = subBarcode.join(',');

  }

  async save(conn: any) {
    // return await this.insertData(conn);
    let affectedRows = 0;

    for await (let i_rem of this.itemRemarks) {
      await i_rem.save(conn).then((res: any) => affectedRows += res.rowCount);
    }
    await this.insertData(conn).then((res: any) => affectedRows += res.rowCount);

    return affectedRows;
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'itemKey',
      'sourceItemId',
      'sourceItemNumber',
      'barcode',
      'subbarcode',
      'maxBarcode',
      'sku',
      'catalogInfo',
      'sourcePrice',
      'standardPrice',
      'currency',
      'color',
      'size',
      'gender',
      'realBrandCode',
      'brandCode',
      'itemLocCode',
      'itemLocDesc',
      'itemAreaCode',
      'itemAreaDesc',
      'qty',
      'qtyUnit',
      'weightedItem',
      'weight',
      'weightUnit',
      'productNameTH',
      'productNameEN',
      'productNameIT',
      'productNameDE',
      'imageUrl',
      'cat1',
      'cat2',
      'cat3',
      'cat4',
      'cat5',
      'cat6',
      'zone',
      'brand',
      'status',
      'statusReason',
      'statusDate',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.itemKey,
        this.sourceItemId,
        this.sourceItemNumber,
        this.barcode,
        (this.subBarcode && this.subBarcode != null && this.subBarcode != '') ? this.subBarcode : null,
        this.maxBarcode,
        this.sku,
        this.catalogInfo,
        this.sourcePrice,
        this.standardPrice,
        this.currency,
        this.color,
        this.size,
        this.gender,
        this.realBrandCode,
        this.brandCode,
        this.itemLocCode,
        this.itemLocDesc,
        this.itemAreaCode,
        this.itemAreaDesc,
        this.qty,
        this.qtyUnit,
        this.weightedItem,
        this.weight,
        this.weightUnit,
        this.productNameTH,
        this.productNameEN,
        this.productNameIT,
        this.productNameDE,
        this.imageUrl,
        this.cat1,
        this.cat2,
        this.cat3,
        this.cat4,
        this.cat5,
        this.cat6,
        this.zone,
        this.brand,
        this.status === ITEM_STATUS.REQ_NEW ? ITEM_STATUS.NEW : this.status,
        this.statusReason,
        this.statusDate,
        new Date()
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }

}
