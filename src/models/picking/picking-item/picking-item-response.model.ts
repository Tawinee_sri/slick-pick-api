export class PickingItemResponse {
  // field
  itemKey: string;
  sourceItemId: string;
  sourceItemNumber: number;
  barcode: string;
  sku: string;
  qty: number;
  qtyUnit: string;
  weight: number;
  weightUnit: string;
  status: string;
  statusReason: any;
  statusDate: string;
  pickRequestDate: string;
  pickAllocationDate: string;
  pickAssignDate: string;
  pickedBy: string;
  pickBarcode: string;
  pickedDate: string;
  packReceivedBy: string;
  packedReceivedDate: string;
  locId: string;
}
