import { dateToStrDate, strToDdate } from '../../DateUtils';
import { UtilModel } from '../../UtilModel';
import { ITEM_STATUS } from '../constant';
/**
 * Declear Error Message
 */
const ERR_messageType = '30700,Error Required Field';

export class PickItemRemark {

  // config
  USE_TABLE: string = 'pr_remark_item';

  // pk
  id: number;

  // link
  pickRequestId: string;
  itemKey: string;

  // field
  messageType: string;
  message: string;

  setData(pickRequestId: string, itemKey: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,item Data is not an object');
    }
    this.pickRequestId = pickRequestId;
    this.itemKey = itemKey;

    // itemKey
    if (!data.hasOwnProperty('messageType') || data.messageType == null || data.messageType.trim().length == 0) {
      throw new Error(ERR_messageType);
    }
    this.messageType = data.messageType;

    this.itemKey = itemKey;

    // sourceItemId
    if (!data.hasOwnProperty('message')) {
      data.message = "";
    }
    this.message = data.message;

  }

  getData(): Object {
    return {
      messageType: this.messageType,
      message: this.message,
    };
  }

  async save(conn: any) {
    return await this.insertData(conn);
  }

  insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'messageType',
      'message',
      'created_time',
      'itemkey'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.messageType,
        this.message,
        new Date(),
        this.itemKey
      ],
    ];
    return UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }

}
