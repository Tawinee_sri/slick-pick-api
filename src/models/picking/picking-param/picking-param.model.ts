import { PickAllocation } from '../pick-allocation/pick-allocation.model';
import { PickPriority } from '../pick-priority/pick-priority.model';
import { UtilModel } from '../../UtilModel';

/**
 * Declear Error Message
 */
const ERR_pickMode = '30301,Error Required Field';
const ERR_pickAssignType = '30302,Error Required Field';
const ERR_pickSubAssignType = '30303,Error Required Field';
const ERR_pickAllocation = '30304,Error Required Field';
const ERR_pickPriority = '30305,Error Required Field';
const ERR_receivingScan = '30306,Error Required Field';
const ERR_locHandling = '30307,Error Required Field';
const ERR_receiptPhotoTake = '30308,Error Required Field';
const ERR_pickAll = '30309,Error Required Field';

export class PickingParam {

  // config
  USE_TABLE: string = 'pr_pick_params';

  // pk
  id: number;

  // link
  pickRequestId: string;

  // field
  pickMode: string;
  pickAll: string;
  pickAssignType: string;
  pickSubAssignType: string;

  pickAllocations: Array<PickAllocation>;
  pickPriority: PickPriority;

  receivingScan: string;
  locHandling: string;
  receiptPhotoTake: string;

  constructor() { }

  setData(pickRequestId: string, data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,Current Data is not an object');
    }

    this.pickRequestId = pickRequestId;

    // pickMode
    if (!data.hasOwnProperty('pickMode') || data.pickMode == null || data.pickMode.trim().length == 0) {
      throw new Error(ERR_pickMode);
    }
    this.pickMode = data.pickMode;

    // pickAll
    if (!data.hasOwnProperty('pickAll') || data.pickAll == null || data.pickAll.trim().length == 0) {
      // throw new Error(ERR_pickAll);
      data.pickAll = 'Y';
    }
    this.pickAll = data.pickAll;

    // pickAssignType
    if (!data.hasOwnProperty('pickAssignType') || data.pickAssignType == null || data.pickAssignType.trim().length == 0) {
      throw new Error(ERR_pickAssignType);
    }
    this.pickAssignType = data.pickAssignType;

    // pickSubAssignType (not required)
    if (!data.hasOwnProperty('pickSubAssignType') || data.pickSubAssignType == null || data.pickSubAssignType.trim().length == 0) {
      if (this.pickAssignType == '0002') {
        throw new Error(ERR_pickSubAssignType);
      }
    }
    this.pickSubAssignType = data.pickSubAssignType;

    // pickAllocation
    if (data.hasOwnProperty('pickAllocation')) {
      if (!(data.pickAllocation instanceof Array)) {
        throw new Error('500,ERROR,Current Data is not an array');
      }

      if (data.pickAllocation.length == 0) {
        throw new Error(ERR_pickAllocation);
      }

      this.pickAllocations = new Array();
      for (let allocation of data.pickAllocation) {
        let item = new PickAllocation();
        item.setData(this.pickRequestId, this.pickMode, allocation);
        this.pickAllocations.push(item);
      }
    } else {
      throw new Error(ERR_pickAllocation);
    }

    // pickPriority
    if (data.hasOwnProperty('pickPriority')) {
      this.pickPriority = new PickPriority();
      this.pickPriority.setData(this.pickRequestId, this.pickMode, data.pickPriority);
    } else {
      throw new Error(ERR_pickPriority);
    }

    // receivingScan
    if (!data.hasOwnProperty('receivingScan')) {
      throw new Error(ERR_receivingScan);
    }
    this.receivingScan = data.receivingScan;

    // locHandling
    if (!data.hasOwnProperty('locHandling')) {
      throw new Error(ERR_locHandling);
    }
    this.locHandling = data.locHandling;

    // receiptPhotoTake
    if (!data.hasOwnProperty('receiptPhotoTake')) {
      throw new Error(ERR_receiptPhotoTake);
    }
    this.receiptPhotoTake = data.receiptPhotoTake;

  }

  getData(): Object {
    return {
      pickMode: this.pickMode,
      pickAll: this.pickAll,
      pickAssignType: this.pickAssignType,
      pickSubAssignType: this.pickSubAssignType,
      pickAllocations: this.getPickAllocation(),
      pickPriority: this.pickPriority.getData()
    };
  }

  getPickAllocation(): Array<Object> {
    let allocations = new Array<Object>();

    for (let item of this.pickAllocations) {
      allocations.push(item.getData());
    }

    return allocations;
  }

  async save(conn: any) {
    try {
      let affectedRows = 0;
      await this.pickPriority.save(conn).then((res: any) => affectedRows += res);
      for await (let allocation of this.pickAllocations) {
        await allocation.save(conn).then((res: any) => affectedRows += res.rowCount);
      }
      await this.insertData(conn).then((res: any) => affectedRows += res.rowCount);

      return Promise.resolve(affectedRows);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'pickMode',
      'receivingScan',
      'locHandling',
      'pickAllFlag',
      'receiptPhotoTake',
      'pickAssignType',
      'pickSubAssignType',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.pickMode,
        this.receivingScan,
        this.locHandling,
        this.pickAll,
        this.receiptPhotoTake,
        this.pickAssignType,
        this.pickSubAssignType,
        new Date()
      ],
    ];
    return await UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }


}
