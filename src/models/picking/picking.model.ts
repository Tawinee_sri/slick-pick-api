import { PickingItemResponse } from './picking-item/picking-item-response.model';
import { PickRequest } from './pick-request.model';

const ERR_pickRequest = '30200,Error Required Field';

export class PickResponse {
  pickResponse: PickingResponse;
}

export class PickingResponse {

  // field
  pickRequestId: string;
  pickResponseDate: string;
  subOrderKey: string;
  orderKey: string;
  bu: string;
  status: string;
  statusReason: any;
  channel: any;
  items: Array<PickingItemResponse>;

}

export class PickingRequest {
  pickRequest: PickRequest;

  constructor() { }

  setData(data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Current Data is not an object');
    }

    this.pickRequest = new PickRequest();
    if (data.hasOwnProperty('pickRequest')) {
      this.pickRequest.setData(data.pickRequest)
    } else {
      throw new Error(ERR_pickRequest);
    }
  }

  getPickingItem(): Object {
    return {
      pickRequest: this.pickRequest.getPickingItem()
    };
  }

  async save() {
    return await this.pickRequest.save();
  }
}
