import { PickingParam } from './picking-param/picking-param.model';
import { PickingItem } from './picking-item/picking-item.model';
import { UtilModel } from '../UtilModel';
import { PICK_ALLOCATION_TYPE, ITEM_STATUS, PICK_REQUEST_STATUS, EORDERING } from './constant';
import { PickTask } from './pick-task/pick-task.model';
import { PickRemark } from '../picking/pick-remark/pick-remark.model';
import { LogUtil } from '../LogUtil';

/**
 * Declear Error Message
 */
const ERR_pickRequestId = '30201,Error Required Field';
const ERR_pickRequestDate = '30202,Error Required Field';
const ERR_subOrderKey = '30203,Error Required Field';
const ERR_sourceSubOrderId = '30204,Error Required Field';
const ERR_orderKey = '30205,Error Required Field';
const ERR_sourceOrderId = '30206,Error Required Field';
const ERR_bu = '30207,Error Required Field';
const ERR_status = '30208,Error Required Field';
const ERR_statusReason = '30209,Error Required Field';
const ERR_pickingParam = '30210,Error Required Field';
const ERR_pickingItem = '30211,Error Required Field';
const ERR_sourceBU = '30212,Error Required Field';
const ERR_sourceLoc = '30213,Error Required Field';
const ERR_duplicate = '30214,Error Duplicate';

const ERR_channel = '30222,Error Required Field';
const ERR_fullTaxInvoice = '30223,Error Required Field';
const ERR_giftWrapFlag = '30224,Error Required Field';

export class PickRequest {

  //config
  USE_TABLE = 'pick_request';

  // field
  pickRequestId: string;
  pickRequestDate: string;
  subOrderKey: string;
  sourceSubOrderId: string;
  orderKey: string;
  sourceOrderId: string;
  bu: string;
  sourceBU: string;
  sourceLoc: string;
  status: string;
  statusReason: string;
  channel: string;
  deliveryType: string;
  deliverySubType: string;
  fullTaxInvoice: string;
  giftWrapFlag: string;
  remark: Array<PickRemark>

  pickingParam: PickingParam;
  pickingItems: Array<PickingItem>;

  curr_timestamp: any;

  constructor() { }

  setData(data: any): void {
    if (!(data instanceof Object)) {
      throw new Error('500,ERROR,Current Data is not an object');
    }

    // pickRequestId
    if (!data.hasOwnProperty('pickRequestId') || data.pickRequestId == null || data.pickRequestId.trim().length == 0) {
      throw new Error(ERR_pickRequestId);
    }
    this.pickRequestId = data.pickRequestId;

    // pickRequestDate
    if (!data.hasOwnProperty('pickRequestDate') || data.pickRequestDate == null || data.pickRequestDate.trim().length == 0) {
      throw new Error(ERR_pickRequestDate);
    }
    this.pickRequestDate = data.pickRequestDate;

    // subOrderKey
    if (!data.hasOwnProperty('subOrderKey') || data.subOrderKey == null || data.subOrderKey.trim().length == 0) {
      throw new Error(ERR_subOrderKey);
    }
    this.subOrderKey = data.subOrderKey;

    // sourceSubOrderId
    if (!data.hasOwnProperty('sourceSubOrderId') || data.sourceSubOrderId == null || data.sourceSubOrderId.trim().length == 0) {
      throw new Error(ERR_sourceSubOrderId);
    }
    this.sourceSubOrderId = data.sourceSubOrderId;

    // orderKey
    if (!data.hasOwnProperty('orderKey') || data.orderKey == null || data.orderKey.trim().length == 0) {
      throw new Error(ERR_orderKey);
    }
    this.orderKey = data.orderKey;

    // sourceOrderId
    if (!data.hasOwnProperty('sourceOrderId') || data.sourceOrderId == null || data.sourceOrderId.trim().length == 0) {
      throw new Error(ERR_sourceOrderId);
    }
    this.sourceOrderId = data.sourceOrderId;

    // bu
    if (!data.hasOwnProperty('bu') || data.bu == null || data.bu.trim().length == 0) {
      throw new Error(ERR_bu);
    }
    this.bu = data.bu;

    // sourceBU
    if (!data.hasOwnProperty('sourceBU') || data.sourceBU == null || data.sourceBU.trim().length == 0) {
      throw new Error(ERR_sourceBU);
    }
    this.sourceBU = data.sourceBU;

    // sourceLoc
    if (!data.hasOwnProperty('sourceLoc') || data.sourceLoc == null || data.sourceLoc.trim().length == 0) {
      throw new Error(ERR_sourceLoc);
    }
    this.sourceLoc = data.sourceLoc;

    // status
    if (!data.hasOwnProperty('status')) {
      throw new Error(ERR_status);
    }
    this.status = data.status;

    // statusReason (not required)
    // if (!data.hasOwnProperty('statusReason')) {
    //   throw new Error(ERR_statusReason);
    // }
    this.statusReason = data.statusReason;

    // channel
    if (!data.hasOwnProperty('channel')) {
      throw new Error(ERR_channel);
    }
    this.channel = data.channel;

    if (this.channel == EORDERING.EORDERING_ONLINE || this.channel == EORDERING.EORDERING_OFFLINE) {
      this.sourceBU = this.sourceBU + '-E';
    }

    // deliveryType
    if (!data.hasOwnProperty('deliveryType')) {
      data.deliveryType = '';
    }
    this.deliveryType = data.deliveryType;

    // deliverySubType
    if (!data.hasOwnProperty('deliverySubType')) {
      data.deliverySubType = '';
    }
    this.deliverySubType = data.deliverySubType;

    // status
    if (!data.hasOwnProperty('fullTaxInvoice')) {
      throw new Error(ERR_fullTaxInvoice);
    }
    this.fullTaxInvoice = data.fullTaxInvoice;

    // status
    if (!data.hasOwnProperty('giftWrapFlag')) {
      throw new Error(ERR_giftWrapFlag);
    }
    this.giftWrapFlag = data.giftWrapFlag;

    this.remark = new Array();
    if (data.hasOwnProperty('remarks')) {
      if (!data.remarks) {
        data.remark = [];
      }

      if (data.remarks.length > 0) {
        for (let remark of data.remarks) {
          let prRemark = new PickRemark();
          prRemark.setData(this.pickRequestId, remark);
          this.remark.push(prRemark);
        }
      } else {
        let remark = {
          messageType: ".",
          messge: ""
        }
        let prRemark = new PickRemark();
        prRemark.setData(this.pickRequestId, remark);
        this.remark.push(prRemark);
      }

    } else {
      let remark = {
        messageType: ".",
        messge: ""
      }
      let prRemark = new PickRemark();
      prRemark.setData(this.pickRequestId, remark);
      this.remark.push(prRemark);
    }

    // pickingParam
    this.pickingParam = new PickingParam();
    if (data.hasOwnProperty('pickingParam')) {
      this.pickingParam.setData(this.pickRequestId, data.pickingParam)
    } else {
      throw new Error(ERR_pickingParam);
    }

    // pickingItem
    this.pickingItems = new Array();
    if (data.hasOwnProperty('items')) {
      if (!(data.items instanceof Array)) {
        throw new Error('500,Error Items data is not an array');
      }

      if (data.items.length == 0) {
        throw new Error('500,Error array length');
      }

      for (let item of data.items) {
        let pickingItem = new PickingItem();
        pickingItem.setData(this.pickRequestId, item);
        this.pickingItems.push(pickingItem);
      }
    } else {
      throw new Error(ERR_pickingItem);
    }
  }

  getPickingItem(): Object {
    return {
      pickRequestId: this.pickRequestId,
      pickRequestDate: this.pickRequestDate,
      subOrderKey: this.subOrderKey,
      sourceSubOrderId: this.sourceSubOrderId,
      orderKey: this.orderKey,
      sourceOrderId: this.sourceOrderId,
      bu: this.bu,
      sourceBU: this.sourceBU,
      sourceLoc: this.sourceLoc,
      status: this.status,
      statusReason: this.statusReason,
      channel: this.channel,
      deliveryType: this.deliveryType,
      deliverySubType: this.deliverySubType,
      fullTaxInvoice: this.fullTaxInvoice,
      giftWrapFlag: this.giftWrapFlag,
      remark: this.getRemark(),
      pickingParam: this.pickingParam.getData(),
      pickingItems: this.getPickingItems()
    };
  }

  getPickingItems(): Array<Object> {
    let items = new Array<Object>();
    for (let item of this.pickingItems) {
      items.push(item.getData());
    }

    return items;
  }

  getRemark(): Array<Object> {
    let remark = new Array<Object>();
    for (let r of this.remark) {
      remark.push(r.getData());
    }

    return remark;
  }

  async save() {
    return new Promise((resolve: any, reject: any) => {
      UtilModel.getPool().connect((errconn: any, conn: any, done: any) => {
        if (errconn) {
          conn.query('ROLLBACK', err => {
            if (err) {
              reject(errconn);
            }
            done();
          })
        }

        conn.query('BEGIN', async (errTrans: any) => {
          if (errTrans) {
            reject(errTrans);
          }
          try {
            let affectedRows = 0;

            let sql = `select count(0) as cnt from slick_picking.pick_request where pickRequestId = ?`;
            let res = await UtilModel.getInstance().sqlConnQuery(conn, sql, [this.pickRequestId]);
            console.log('count pick_request res : ', res);
            if (res != undefined && res.length > 0 && res[0].cnt > 0) {
              throw new Error(ERR_duplicate);
            }

            // sql = `select convert_tz(NOW(), @@session.time_zone, '+07:00') as curr_time `;
            sql = `select NOW() as curr_time `;
            this.curr_timestamp = await UtilModel.getInstance().sqlConnQuery(conn, sql, []);
            console.log('curr_timestamp res : ', this.curr_timestamp);
            this.curr_timestamp = this.curr_timestamp.length > 0 ? this.curr_timestamp[0].curr_time : new Date();
            console.log('curr_timestamp : ', this.curr_timestamp);
            console.log('new Date curr_timestamp : ', new Date(this.curr_timestamp));

            await this.pickingParam.save(conn).then((res: any) => affectedRows += res);
            console.log(`save picking param complete affectedRows ${affectedRows} rows`);

            for await (let rem of this.remark) {
              await rem.save(conn).then((res: any) => affectedRows += res.rowCount);
            }

            for await (let item of this.pickingItems) {
              await item.save(conn).then((res: any) => affectedRows += res);
              // LogUtil.logSnowFlake(this.sourceOrderId, '', item.itemKey, );
            }

            await this.insertData(conn).then((res: any) => affectedRows += res.rowCount);
            // if (this.pickingParam.pickAllocations.filter(x => x.pickAllocationType != PICK_ALLOCATION_TYPE.NONE).length == 0) {
            console.log('----> auto allocate task');
            let pickTask = new PickTask();
            pickTask.pickRequestId = this.pickRequestId;
            pickTask.setAllocateData({
              user_id: '',
              role: '',
              bu: this.sourceBU,
              loc: this.sourceLoc
            });
            await pickTask.allocatePickByOrder(conn).catch(err => {
              LogUtil.logError('post error', '/setPickingRequest', err, 'Error allocatePickByOrder');
              throw new Error(err);
            });
            // }
            await UtilModel.getInstance().sqlConnCommit(conn);

            resolve(affectedRows);
          } catch (err) {
            console.error('ERROR setPickingRequest : ', err);
            conn.query('ROLLBACK');
            reject(err);
          } finally {
            done();
          }
        });
      });
    });
  }

  async insertData(conn: any) {
    let cols = [
      'pickRequestId',
      'pickRequestDate',
      'subOrderKey',
      'sourceSubOrderId',
      'orderKey',
      'sourceOrderId',
      'bu',
      'sourceBU',
      'sourceLoc',
      'status',
      'statusReason',
      'channel',
      'deliveryType',
      'deliverySubType',
      'fullTaxInvoice',
      'giftWrapFlag',
      'created_time'
    ];
    let arr_sqldata = [
      [
        this.pickRequestId,
        this.pickRequestDate,
        this.subOrderKey,
        this.sourceSubOrderId,
        this.orderKey,
        this.sourceOrderId,
        this.bu,
        this.sourceBU,
        this.sourceLoc,
        PICK_REQUEST_STATUS.NEW,
        this.statusReason,
        this.channel,
        this.deliveryType,
        this.deliverySubType,
        this.fullTaxInvoice,
        this.giftWrapFlag,
        new Date(this.curr_timestamp)
      ],
    ];

    return await UtilModel.getInstance().sqlConnInsert(conn, this.USE_TABLE, cols, arr_sqldata);
  }

}
