
/**
 * Declare message error
 */
const ERR_pickTaskId = '30801,Error Required Field';
const ERR_packRequestId = '30802,Error Required Field';
const ERR_itemKey = '30803,Error Required Field';

export class PickTaskDetail {

  // config
  USE_TABLE: string = 'pick_task_detail';

  // pk
  id: number;

  // link
  pickTaskId: number;
  pickRequestId: string;

  // field
  itemKey: string;

}
