import { UtilModel } from './UtilModel';
import { LogUtil } from './LogUtil';
import { PROJECT_VERSION } from './../models/picking/constant';
export class DBVersion {

  constructor() { }

  async getVersion() {
    try {
      let sql = "select db_version from db_version where api_version = ? order by id desc limit 1 ";
      return UtilModel.getInstance().sqlQuerySingleRow(sql, [PROJECT_VERSION.API_VERSION]);
    } catch (error) {
      return [];
    }
  }

  async healthCheck() {
    try {
      let result: any = await UtilModel.getInstance().dbHealthCheck();

      if (!result.status) {
        throw new Error(result.condition.health);
      }

      return result;

    } catch (error) {
      throw new Error(error.message);

    }

  }

}
