import { UtilModel } from './UtilModel';
const { Kafka, logLevel } = require('kafkajs');

const kafka = new Kafka({
  clientId: 'hello-world-kafka',
  brokers: [
    'confluent-cp-kafka-0-dev.central.co.th:9093',
    'confluent-cp-kafka-1-dev.central.co.th:9093',
    'confluent-cp-kafka-2-dev.central.co.th:9093',
    'confluent-cp-kafka-3-dev.central.co.th:9093',
    'confluent-cp-kafka-4-dev.central.co.th:9093'
  ],
  ssl: false,
  sasl: {
    mechanism: 'scram-sha-512',
    username: 'pkt-snow-nonprod',
    password: 'sSd8g6dJeGRQxSLm',
  },
  connectionTimeout: 1000,
  requestTimeout: 30000,
  retry: {
    maxRetryTime: 30000,
    initialRetryTime: 300,
    factor: 0.2,
    multiplier: 2,
    retries: 5,
  },
  logLevel: logLevel.INFO,
});

export interface SNF_DATA {
  fulfilmentLocationBU: string;
  fulfilmentLocationBranchID: string;
  orderId: string;
  taskId: number;
  itemKey: string;
  cat1: string;
  cat2: string;
  catalog: string;
  quantity: number;
  itemStatus: string;
  brand: string;
}

export class LogUtil {

  static async logError(method: string, api: string, reqBody: any, messege: string) {
    let cols = ['method', 'api_path', 'req_body', 'message'];
    let arr_data = [
      [
        method,
        api,
        (reqBody instanceof Object) ? JSON.stringify(reqBody) : reqBody,
        messege
      ]
    ];

    await UtilModel.getInstance().sqlInsert('error_log', cols, arr_data);
  }

  static async logSnowFlake(conn: any, taskId: number, taskStatus: string, description: string) {
    try {
      let schema = 'slick_picking.';
      let result;
      let sql = `
        select
          p.id "prItemId" , pr.pickrequestid "pickRequestId" ,
          to_char(pr.updated_time, 'YYYY-MM-DD HH12:MI:SS') "pickResponseDate" ,
          to_char(pdi.updated_time, 'YYYY-MM-DD HH12:MI:SS') "itemStatusDate" ,
          to_char(pt.updated_time, 'YYYY-MM-DD HH12:MI:SS') "pickTaskStatusDate" ,
          to_char(pt.created_time, 'YYYY-MM-DD HH12:MI:SS') "pickAllocationDate" ,
          to_char(pt.created_time, 'YYYY-MM-DD HH12:MI:SS') "pickAssignDate" ,
          to_char(pdi.updated_time, 'YYYY-MM-DD HH12:MI:SS') "pickedDate" ,
          to_char(pdi.packreceivedate, 'YYYY-MM-DD HH12:MI:SS') "packedReceivedDate" ,
          pr.sourcesuborderid "subOrderId" , pr.sourceorderid "orderId",
          pr.suborderkey "subOrderKey", pr.orderkey "orderKey" , pr.sourcebu bu,
          p.itemkey "itemKey", p.sourceitemid "itemId", p.sourceitemnumber "itemNumber" , p.barcode , 1 qty , p.qtyunit "qtyUnit" , p.weight ,
          p.weightunit "weightUnit" , p.actualweight "actualWeight" , pdi.itemstatus "itemStatus" , pdi.statusreason "itemStatusReason",
          pd.picktaskid "pickTaskId", pt.status "pickTaskStatus" , '' "pickTaskStatusReason" ,
          pt.user_id "pickedBy" , pdi.pickbarcode "pickBarcode" , pdi.locbarcode "locId"
        from ${schema}pick_request pr
        join ${schema}pick_task_detail pd on pd.pickrequestid = pr.pickrequestid
        join ${schema}pick_task_detail_item pdi on pdi.picktaskdetailid = pd.id
        join ${schema}pr_items p on p.id = pd.pickitemid
        join ${schema}pick_task pt on pt.id = pd.picktaskid
        where pd.picktaskid = ?
      `;

      // let resp = await UtilModel.getInstance().sqlConnQuery(conn, sql, [taskId]);
      let resp = await UtilModel.getInstance().sqlQuery(sql, [taskId]);
      console.log('resp : ', resp);

      let dataArray: any = [];
      for (let d of resp) {
        let indx = dataArray.findIndex(i => i.prItemId === d.prItemId);
        if (indx === -1) {
          dataArray.push(d);
        } else {
          dataArray[indx].qty += d.qty;
        }
      }

      for (let i of dataArray) {
        await this.sendingSnowFlake(i);
      }

    } catch (error) {
      console.error('[snowflakeTest] error : ', error);
    }

  }

  static async sendingSnowFlake(data: SNF_DATA) {
    try {
      const producer = kafka.producer();
      await producer.connect();
      await producer.send({
        topic: 'bi.slickpkt.pickresp',
        messages: [
          {
            value: JSON.stringify(data)
          },
        ],
      });
      console.log('Message sent successfully!');
      await producer.disconnect();
    } catch (error) {
      console.error('[snowflakeTest] error : ', error);
    }
  }

}
