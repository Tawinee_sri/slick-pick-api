import { SlickChatAndShopAPIApplication } from './application';
import { ApplicationConfig } from '@loopback/core';
export { SlickChatAndShopAPIApplication };

const ELASTIC_OPT = {
  serviceName: '',
  secretToken: 'yKF0PTfX8345y7DxLY',
  serverUrl: 'https://x10-test-1.apm.ap-southeast-1.aws.found.io',
  environment: 'nonprod'
}
const apm = require('elastic-apm-node').start(ELASTIC_OPT);

export async function main(options: ApplicationConfig = {}) {
  const app = new SlickChatAndShopAPIApplication(options);
  await app.boot();
  await app.start();
  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);
  return app;
}
