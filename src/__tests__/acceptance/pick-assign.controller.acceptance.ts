import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import { TASK_STATUS } from '../../models/picking/constant';
import fs = require('fs');

describe('PickAssignController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST001');

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuerySync(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
  }

  async function prepareTestData() {
    let pickTaskCols = ['ID', 'pickRequestId', 'pickAllocationType', 'wave_id', 'multiorder_id', 'user_id', 'user_pack', 'bu', 'role', 'status', 'created_time'];
    let pickTaskDatas = [
      ['1', 'TEST_PICK_REQUEST001', '', '0', '0', '', '', 'CDS', '', '0001', '2019-09-17 14:08:56']
    ];

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_task', pickTaskCols, pickTaskDatas)
    ]);
  }

  it('PickAssignRequest | Positive Case:001 | Assign task to user and update status to PENDING', async () => {
    let testData = JSON.parse(fs.readFileSync('./data/test_setAssignPickTask001.json', 'utf-8'));
    await client.post('/setAssignPicktask').set('Content-Type', 'application/json').send(testData).expect(200);

    let sql1 = 'select * from pick_task where id = 1';
    const res = await UtilModel.getInstance().sqlQuerySync(sql1, []);

    expect(res[0].status).to.equal(TASK_STATUS.PENDING);
    expect(res[0].user_id).to.equal('Fulfilment001');
  });

});
