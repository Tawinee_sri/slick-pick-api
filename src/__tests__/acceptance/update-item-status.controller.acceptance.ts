import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import { ITEM_STATUS } from '../../models/picking/constant';
import fs = require('fs');

describe('UpdateItemStatusController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST012');
  pickRequestIdList.push('TEST_PICK_REQUEST013');
  pickRequestIdList.push('TEST_PICK_REQUEST014');
  pickRequestIdList.push('TEST_PICK_REQUEST015');
  pickRequestIdList.push('TEST_PICK_REQUEST016');
  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuery(`delete from pick_request where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_items where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pick_params where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_allocation where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_priority where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail_item pi where exists (select * from pick_task_detail pd where pi.pickTaskDetailId = pd.id and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_users where id >= 100`, []);
  }

  async function prepareTestData() {
    let nullValue = null;

    let pickRequestCols = ['id', 'pickRequestId', 'pickRequestDate', 'subOrderKey', 'sourceSubOrderId', 'orderKey', 'sourceOrderId', 'bu', 'sourceBU', 'sourceLoc', 'status', 'statusReason', 'created_time'];
    let pickRequestDatas = [
      ['100', 'TEST_PICK_REQUEST012', '2019-09-17 12:24:20', 'SUB_ORDER_ID_012', 'SUB_ORDER_ID_012', 'ORDER_KEY_012', 'SUB_ORDER_ID_012', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:14:57'],
      ['101', 'TEST_PICK_REQUEST013', '2019-09-17 12:24:20', 'SUB_ORDER_ID_013', 'SUB_ORDER_ID_013', 'ORDER_KEY_013', 'SUB_ORDER_ID_013', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['102', 'TEST_PICK_REQUEST014', '2019-09-17 12:24:20', 'SUB_ORDER_ID_014', 'SUB_ORDER_ID_014', 'ORDER_KEY_014', 'SUB_ORDER_ID_014', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['103', 'TEST_PICK_REQUEST015', '2019-09-17 12:24:20', 'SUB_ORDER_ID_015', 'SUB_ORDER_ID_015', 'ORDER_KEY_015', 'SUB_ORDER_ID_015', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['104', 'TEST_PICK_REQUEST016', '2019-09-17 12:24:20', 'SUB_ORDER_ID_016', 'SUB_ORDER_ID_016', 'ORDER_KEY_016', 'SUB_ORDER_ID_016', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54']
    ];

    let prItemsCols = ['id', 'pickRequestId', 'itemKey', 'sourceItemId', 'sourceItemNumber', 'barcode', 'sku', 'qty', 'qtyUnit', 'productNameTH', 'productNameEN', 'imageUrl', 'locBarcode', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'brand', 'zone', 'status', 'statusReason', 'statusDate', 'created_time'];
    let prItemsDatas = [
      ['100', 'TEST_PICK_REQUEST012', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '20', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['101', 'TEST_PICK_REQUEST012', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['102', 'TEST_PICK_REQUEST012', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['103', 'TEST_PICK_REQUEST012', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['104', 'TEST_PICK_REQUEST012', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['105', 'TEST_PICK_REQUEST013', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['106', 'TEST_PICK_REQUEST013', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['107', 'TEST_PICK_REQUEST013', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['108', 'TEST_PICK_REQUEST013', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['109', 'TEST_PICK_REQUEST013', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['110', 'TEST_PICK_REQUEST014', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['111', 'TEST_PICK_REQUEST014', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['112', 'TEST_PICK_REQUEST014', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['113', 'TEST_PICK_REQUEST014', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['114', 'TEST_PICK_REQUEST014', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['115', 'TEST_PICK_REQUEST015', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['116', 'TEST_PICK_REQUEST015', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['117', 'TEST_PICK_REQUEST015', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['118', 'TEST_PICK_REQUEST015', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['119', 'TEST_PICK_REQUEST015', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['120', 'TEST_PICK_REQUEST016', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['121', 'TEST_PICK_REQUEST016', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095738', '4132533', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['122', 'TEST_PICK_REQUEST016', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['123', 'TEST_PICK_REQUEST016', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['124', 'TEST_PICK_REQUEST016', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54']
    ];

    let pickParamCols = ['id', 'pickRequestId', 'pickMode', 'receivingScan', 'locHandling', 'pickAssignType', 'pickSubAssignType', 'created_time'];
    let pickParamDatas = [
      ['100', 'TEST_PICK_REQUEST013', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:20:54'],
      ['101', 'TEST_PICK_REQUEST012', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['102', 'TEST_PICK_REQUEST014', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['103', 'TEST_PICK_REQUEST015', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['104', 'TEST_PICK_REQUEST016', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57']
    ];

    let pickAllocationCols = ['id', 'pickRequestId', 'pickMode', 'pickAllocationTypeSeq', 'pickAllocationType', 'pickCatLvl', 'created_time'];
    let pickAllocationDatas = [
      ['100', 'TEST_PICK_REQUEST013', '0001', '1', '0001', '1', '2019-09-17 15:20:54'],
      ['101', 'TEST_PICK_REQUEST012', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['102', 'TEST_PICK_REQUEST014', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['103', 'TEST_PICK_REQUEST015', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['104', 'TEST_PICK_REQUEST015', '0001', '2', '0002', '2', '2019-09-17 15:14:57'],
      ['105', 'TEST_PICK_REQUEST016', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['106', 'TEST_PICK_REQUEST016', '0001', '2', '0004', '2', '2019-09-17 15:14:57']
    ];

    let pickPriorityCols = ['id', 'pickRequestId', 'pickMode', 'pickPriorityType', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];
    let pickPriorityDatas = [
      ['100', 'TEST_PICK_REQUEST013', '0001', '0001', '0001', nullValue, '2019-09-17 15:20:54'],
      ['101', 'TEST_PICK_REQUEST012', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['102', 'TEST_PICK_REQUEST014', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['103', 'TEST_PICK_REQUEST015', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['104', 'TEST_PICK_REQUEST016', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57']
    ];

    let pickUsersCols = ['id', 'userId', 'cat_01', 'cat_02', 'cat_03', 'cat_04', 'cat_05', 'cat_06', 'zone', 'brand', 'remark', 'bu', 'loc', 'status'];
    let pickUsersDatas = [
      ['100', 'Fulfilment001', '2000', '2010', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['101', 'Fulfilment001', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['102', 'Fulfilment002', '2000', '2020', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['103', 'Fulfilment002', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['104', 'Fulfilment003', '4000', 'A', '', '', '', '', '5000', 'A', '', 'CDS', '10116', 'A']
    ]

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_request', pickRequestCols, pickRequestDatas),
      UtilModel.getInstance().sqlInsert('pr_items', prItemsCols, prItemsDatas),
      UtilModel.getInstance().sqlInsert('pr_pick_params', pickParamCols, pickParamDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_allocation', pickAllocationCols, pickAllocationDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_priority', pickPriorityCols, pickPriorityDatas),
      UtilModel.getInstance().sqlInsert('pick_users', pickUsersCols, pickUsersDatas)
    ]);

    let assignData = JSON.parse(fs.readFileSync('./data/test_setPickTaskAllocation.json', 'utf-8'));
    await client.post('/setPickTaskAllocation').set('Content-Type', 'application/json').send(assignData).expect(200);
  }

  it('UpdateItemStatusRequest | Case:001 Decline item on task that has 1 item | Task should be 0002', async () => {
    let sql = `
      select min(id) id, pickTaskId from (
        select pi.id, pi.pickTaskDetailId, pd.pickTaskId from pick_task_detail_item pi
        inner join pick_task_detail pd
        on pi.pickTaskDetailId = pd.id
        where pd.pickRequestId = ?
      ) a group by pickTaskId
      having count(0) = 1
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016']);
    let update = {
      barcode: "17070638",
      sku: "",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      itemStatusID: task[0].id,
      set_status: "0002",
      reason: "decline test",
      qty: 1
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);

    expect(task[0].status).to.equal('0002');
  });

  it('UpdateItemStatusRequest | Case:002 Picked 1 item on task that has 2 item | Task should be 0004', async () => {
    let sql = `
      select pi.id, pd.pickTaskId from pick_task_detail_item pi
      inner join pick_task_detail pd
      on pi.pickTaskDetailId = pd.id
      where pd.pickRequestId = ?
      and pd.itemkey = ?
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_005']);
    let update = {
      barcode: "82690632",
      sku: "",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      itemStatusID: task[0].id,
      set_status: "0005",
      reason: "",
      qty: 1
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);

    expect(task[0].status).to.equal('0004');
  });

  it('UpdateItemStatusRequest | Case:003 Picked 1 item left on task that has 2 item | Task should be 0005', async () => {
    let sql = `
      select pi.id, pd.pickTaskId from pick_task_detail_item pi
      inner join pick_task_detail pd
      on pi.pickTaskDetailId = pd.id
      where pd.pickRequestId = ?
      and pd.itemkey = ?
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_004']);
    let update = {
      barcode: "18317152",
      sku: "",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      itemStatusID: task[0].id,
      set_status: "0005",
      reason: "",
      qty: 1
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);

    expect(task[0].status).to.equal('0005');
  });

  it('UpdateItemStatusRequest | Case:004 Picked 1 item of 2 on item that have 2 qty | Task should be 0004, item should be 0004', async () => {
    let sql = `
      select pi.id, pd.pickTaskId from pick_task_detail_item pi
      inner join pick_task_detail pd
      on pi.pickTaskDetailId = pd.id
      where pd.pickRequestId = ?
      and pd.itemkey = ?
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001']);
    let update = {
      barcode: "10095737",
      sku: "",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      itemStatusID: task[0].id,
      set_status: "0005",
      reason: "",
      qty: 1
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);
    expect(task[0].status).to.equal('0004');

    sql = `select * from pr_items where pickRequestId = ? and itemKey = ?`;
    let item = await UtilModel.getInstance().sqlQuery(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001']);
    expect(item[0].status).to.equal('0004');
  });

  it('UpdateItemStatusRequest | Case:005 Picked 1 item left of 2 on item that have 2 qty | Task should be 0004, item should be 0005', async () => {
    let sql = `
      select pi.id, pd.pickTaskId from pick_task_detail_item pi
      inner join pick_task_detail pd
      on pi.pickTaskDetailId = pd.id
      where pd.pickRequestId = ?
      and pd.itemkey = ?
      and pi.itemStatus = ?
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001', ITEM_STATUS.NEW]);
    let update = {
      barcode: "10095737",
      sku: "",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      itemStatusID: task[0].id,
      set_status: "0005",
      reason: "",
      qty: 1
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);
    expect(task[0].status).to.equal('0004');

    sql = `select * from pr_items where pickRequestId = ? and itemKey = ?`;
    let item = await UtilModel.getInstance().sqlQuery(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001']);
    expect(item[0].status).to.equal('0005');
  });

  it('UpdateItemStatusRequest | Case:006 Picked 2 item via sku on item that have 2 qty on incomplete task | Task should be 0005, item should be 0005', async () => {
    let sql = `
      select pi.id, pd.pickTaskId from pick_task_detail_item pi
      inner join pick_task_detail pd
      on pi.pickTaskDetailId = pd.id
      where pd.pickRequestId = ?
      and pd.itemkey = ?
    `;

    let task = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001', ITEM_STATUS.NEW]);
    let update = {
      barcode: "10095737",
      sku: "4132533",
      itemKey: "",
      orderKey: "ORDER_KEY_016",
      task_id: task[0].pickTaskId,
      set_status: "0005",
      reason: "",
      qty: 2
    };

    await client.post('/updateItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    sql = `select * from pick_task where id = ?`;
    task = await UtilModel.getInstance().sqlQuery(sql, [task[0].pickTaskId]);
    expect(task[0].status).to.equal('0005');

    sql = `select * from pr_items where pickRequestId = ? and itemKey = ?`;
    let item = await UtilModel.getInstance().sqlQuery(sql, ['TEST_PICK_REQUEST016', 'ITEM_KEY_001']);
    expect(item[0].status).to.equal('0005');
  });

});
