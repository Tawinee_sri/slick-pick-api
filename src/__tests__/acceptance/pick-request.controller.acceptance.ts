import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import fs = require('fs');

describe('PickRequestController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  after(async () => {
    await app.stop();
  });

  it('invokes GET /ping', async () => {
    const res = await client.get('/ping?msg=world').expect(200);
    expect(res.body).to.containEql({ greeting: 'Hello from LoopBack' });
  });

  //DB Connection
  /*
  it('Connection to DB : check db version @0.0.1', async () => {

    const res = await client.get('/getDBVersion').expect(200);

    expect(res.body.statusCode).to.equal(200);
    expect(res.body.data).to.equal('0.0.1');
  });
  */

  /**
    * Nagative case
    */
  it('ReceivePickRequest | Nagative Case:001 | ไม่ใส่ field ที่มีความสำคัญเช่น pickRequestId', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-001.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30201);

  });

  it('ReceivePickRequest | Nagative Case:002 | ไม่ใส่ field ที่มีความสำคัญเช่น pickRequestDate:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-002.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30202);

  });

  it('ReceivePickRequest | Nagative Case:003 | ไม่ใส่ field ที่มีความสำคัญเช่น subOrderKey:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-003.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30203);

  });


  it('ReceivePickRequest | Nagative Case:004 | ไม่ใส่ field ที่มีความสำคัญเช่น sourceSubOrderId:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-004.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30204);

  });

  it('ReceivePickRequest | Nagative Case:005 | ไม่ใส่ field ที่มีความสำคัญเช่น orderKey:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-005.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30205);

  });

  it('ReceivePickRequest | Nagative Case:006 | ไม่ใส่ field ที่มีความสำคัญเช่น sourceOrderId:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-006.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30206);

  });

  it('ReceivePickRequest | Nagative Case:007 | ไม่ใส่ field ที่มีความสำคัญเช่น bu:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-007.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30207);

  });

  it('ReceivePickRequest | Nagative Case:008 | ไม่ใส่ field ที่มีความสำคัญเช่น status:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-008.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30208);

  });


  it('ReceivePickRequest | Positive Case:009 | ไม่ใส่ field ที่ไม่มีความสำคัญเช่น statusReason:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-009.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(200);

  });

  it('ReceivePickRequest | Nagative Case:010 | ไม่ใส่ field ที่มีความสำคัญเช่น pickingParam:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-010.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30210);

  });


  it('ReceivePickRequest | Nagative Case:011 | ไม่ใส่ field ที่มีความสำคัญเช่น pickingItem:', async () => {

    let TestData = JSON.parse(fs.readFileSync('./data/test_pickrequest_N-011.json', 'utf-8'));

    const res = await client.post('/checkPickRequest')
      .set('Content-Type', 'application/json')
      .send(TestData)
      .expect(200);

    expect(res.body.statusCode).to.equal(30211);

  });

});
