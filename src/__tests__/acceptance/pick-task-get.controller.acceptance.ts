import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import fs = require('fs');

describe('PickTaskGetController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST007');
  pickRequestIdList.push('TEST_PICK_REQUEST008');
  pickRequestIdList.push('TEST_PICK_REQUEST009');
  pickRequestIdList.push('TEST_PICK_REQUEST010');
  pickRequestIdList.push('TEST_PICK_REQUEST011');
  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuery(`delete from pick_request where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_items where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pick_params where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_allocation where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_priority where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail_item pi where exists (select * from pick_task_detail pd where pi.pickTaskDetailId = pd.id and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_users where id >= 100`, []);
  }

  async function prepareTestData() {
    let nullValue = null;

    let pickRequestCols = ['id', 'pickRequestId', 'pickRequestDate', 'subOrderKey', 'sourceSubOrderId', 'orderKey', 'sourceOrderId', 'bu', 'sourceBU', 'sourceLoc', 'status', 'statusReason', 'created_time'];
    let pickRequestDatas = [
      ['50', 'TEST_PICK_REQUEST007', '2019-09-17 12:24:20', 'SUB_ORDER_ID_007', 'SUB_ORDER_ID_007', 'ORDER_KEY_002', 'SUB_ORDER_ID_007', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:14:57'],
      ['51', 'TEST_PICK_REQUEST008', '2019-09-17 12:24:20', 'SUB_ORDER_ID_008', 'SUB_ORDER_ID_008', 'ORDER_KEY_003', 'SUB_ORDER_ID_008', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['52', 'TEST_PICK_REQUEST009', '2019-09-17 12:24:20', 'SUB_ORDER_ID_009', 'SUB_ORDER_ID_009', 'ORDER_KEY_003', 'SUB_ORDER_ID_009', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['53', 'TEST_PICK_REQUEST010', '2019-09-17 12:24:20', 'SUB_ORDER_ID_010', 'SUB_ORDER_ID_010', 'ORDER_KEY_003', 'SUB_ORDER_ID_010', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['54', 'TEST_PICK_REQUEST011', '2019-09-17 12:24:20', 'SUB_ORDER_ID_011', 'SUB_ORDER_ID_011', 'ORDER_KEY_003', 'SUB_ORDER_ID_011', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54']
    ];

    let prItemsCols = ['id', 'pickRequestId', 'itemKey', 'sourceItemId', 'sourceItemNumber', 'barcode', 'sku', 'qty', 'qtyUnit', 'productNameTH', 'productNameEN', 'imageUrl', 'locBarcode', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'brand', 'zone', 'status', 'statusReason', 'statusDate', 'created_time'];
    let prItemsDatas = [
      ['50', 'TEST_PICK_REQUEST007', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '20', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['51', 'TEST_PICK_REQUEST007', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['52', 'TEST_PICK_REQUEST007', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['53', 'TEST_PICK_REQUEST007', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['54', 'TEST_PICK_REQUEST007', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['55', 'TEST_PICK_REQUEST008', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['56', 'TEST_PICK_REQUEST008', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['57', 'TEST_PICK_REQUEST008', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['58', 'TEST_PICK_REQUEST008', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['59', 'TEST_PICK_REQUEST008', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['60', 'TEST_PICK_REQUEST009', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['61', 'TEST_PICK_REQUEST009', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['62', 'TEST_PICK_REQUEST009', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['63', 'TEST_PICK_REQUEST009', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['64', 'TEST_PICK_REQUEST009', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['65', 'TEST_PICK_REQUEST010', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['66', 'TEST_PICK_REQUEST010', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['67', 'TEST_PICK_REQUEST010', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['68', 'TEST_PICK_REQUEST010', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['69', 'TEST_PICK_REQUEST010', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['70', 'TEST_PICK_REQUEST011', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['71', 'TEST_PICK_REQUEST011', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['72', 'TEST_PICK_REQUEST011', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['73', 'TEST_PICK_REQUEST011', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['74', 'TEST_PICK_REQUEST011', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54']
    ];

    let pickParamCols = ['id', 'pickRequestId', 'pickMode', 'receivingScan', 'locHandling', 'pickAssignType', 'pickSubAssignType', 'created_time'];
    let pickParamDatas = [
      ['50', 'TEST_PICK_REQUEST008', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:20:54'],
      ['51', 'TEST_PICK_REQUEST007', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['52', 'TEST_PICK_REQUEST009', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['53', 'TEST_PICK_REQUEST010', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['54', 'TEST_PICK_REQUEST011', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57']
    ];

    let pickAllocationCols = ['id', 'pickRequestId', 'pickMode', 'pickAllocationTypeSeq', 'pickAllocationType', 'pickCatLvl', 'created_time'];
    let pickAllocationDatas = [
      ['50', 'TEST_PICK_REQUEST008', '0001', '1', '0001', '1', '2019-09-17 15:20:54'],
      ['51', 'TEST_PICK_REQUEST007', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['52', 'TEST_PICK_REQUEST009', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['53', 'TEST_PICK_REQUEST010', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['54', 'TEST_PICK_REQUEST010', '0001', '2', '0002', '2', '2019-09-17 15:14:57'],
      ['55', 'TEST_PICK_REQUEST011', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['56', 'TEST_PICK_REQUEST011', '0001', '2', '0004', '2', '2019-09-17 15:14:57']
    ];

    let pickPriorityCols = ['id', 'pickRequestId', 'pickMode', 'pickPriorityType', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];
    let pickPriorityDatas = [
      ['50', 'TEST_PICK_REQUEST008', '0001', '0001', '0001', nullValue, '2019-09-17 15:20:54'],
      ['51', 'TEST_PICK_REQUEST007', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['52', 'TEST_PICK_REQUEST009', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['53', 'TEST_PICK_REQUEST010', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['54', 'TEST_PICK_REQUEST011', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57']
    ];

    let pickUsersCols = ['id', 'userId', 'cat_01', 'cat_02', 'cat_03', 'cat_04', 'cat_05', 'cat_06', 'zone', 'brand', 'remark', 'bu', 'loc', 'status'];
    let pickUsersDatas = [
      ['100', 'Fulfilment001', '2000', '2010', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['101', 'Fulfilment001', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['102', 'Fulfilment002', '2000', '2020', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['103', 'Fulfilment002', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['104', 'Fulfilment003', '4000', 'A', '', '', '', '', '5000', 'A', '', 'CDS', '10116', 'A']
    ]

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_request', pickRequestCols, pickRequestDatas),
      UtilModel.getInstance().sqlInsert('pr_items', prItemsCols, prItemsDatas),
      UtilModel.getInstance().sqlInsert('pr_pick_params', pickParamCols, pickParamDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_allocation', pickAllocationCols, pickAllocationDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_priority', pickPriorityCols, pickPriorityDatas),
      UtilModel.getInstance().sqlInsert('pick_users', pickUsersCols, pickUsersDatas)
    ]);

    let assignData = JSON.parse(fs.readFileSync('./data/test_setPickTaskAllocation.json', 'utf-8'));
    await client.post('/setPickTaskAllocation').set('Content-Type', 'application/json').send(assignData).expect(200);
  }

  it('PickTaskGetRequest | Case:001 | Get all task with correct bu and loc', async () => {
    let res = await client.get('/getPickTaskList/Fulfilment001/A/Picker/CDS/10116').expect(200);
    expect(res.body.data.length).to.equal(3);
  });

  it('PickTaskGetRequest | Case:002 | Get all task with wrong bu and loc', async () => {
    let res = await client.get('/getPickTaskList/Fulfilment001/A/Picker/CDS/10102').expect(200);
    expect(res.body.data.length).to.equal(0);
  });

  it('PickTaskGetRequest | Case:003 | Get Fulfilment001 task', async () => {
    let res = await client.get('/getPickTaskList/Fulfilment001/M/Picker/CDS/10116').expect(200);
    expect(res.body.data.length).to.equalOneOf(2, 3);
  });

  it('PickTaskGetRequest | Case:004 | Get Fulfilment002 task', async () => {
    let res = await client.get('/getPickTaskList/Fulfilment002/M/Picker/CDS/10116').expect(200);
    expect(res.body.data.length).to.equalOneOf(2, 3);
  });

  it('PickTaskGetRequest | Case:005 | Get Fulfilment003 task', async () => {
    let res = await client.get('/getPickTaskList/Fulfilment003/M/Picker/CDS/10116').expect(200);
    expect(res.body.data.length).to.equal(3);
  });

});
