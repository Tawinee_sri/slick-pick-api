import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import { ITEM_STATUS, TASK_STATUS, PICK_REQUEST_STATUS } from '../../models/picking/constant';
import fs = require('fs');

describe('SetPackReceiveItemStatusController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST018');
  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuery(`delete from pick_request where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_items where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pick_params where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_allocation where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_priority where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail_item pi where exists (select * from pick_task_detail pd where pi.pickTaskDetailId = pd.id and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_users where id >= 100`, []);
  }

  async function prepareTestData() {
    let nullValue = null;

    let pickRequestCols = ['id', 'pickRequestId', 'pickRequestDate', 'subOrderKey', 'sourceSubOrderId', 'orderKey', 'sourceOrderId', 'bu', 'sourceBU', 'sourceLoc', 'status', 'statusReason', 'created_time'];
    let pickRequestDatas = [
      ['250', 'TEST_PICK_REQUEST018', '2019-09-17 12:24:20', 'SUB_ORDER_ID_018', 'SUB_ORDER_ID_018', 'ORDER_KEY_018', 'SUB_ORDER_ID_018', 'CDS', 'CDS', '10116', '0002', '', '2019-09-17 15:20:54']
    ];

    let prItemsCols = ['id', 'pickRequestId', 'itemKey', 'sourceItemId', 'sourceItemNumber', 'barcode', 'sku', 'qty', 'qtyUnit', 'productNameTH', 'productNameEN', 'imageUrl', 'locBarcode', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'brand', 'zone', 'status', 'statusReason', 'statusDate', 'created_time'];
    let prItemsDatas = [
      ['251', 'TEST_PICK_REQUEST018', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['252', 'TEST_PICK_REQUEST018', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095738', '4132533', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['253', 'TEST_PICK_REQUEST018', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0002', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['254', 'TEST_PICK_REQUEST018', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['255', 'TEST_PICK_REQUEST018', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54']
    ];

    let pickParamCols = ['id', 'pickRequestId', 'pickMode', 'receivingScan', 'locHandling', 'pickAssignType', 'pickSubAssignType', 'created_time'];
    let pickParamDatas = [
      ['251', 'TEST_PICK_REQUEST018', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57']
    ];

    let pickAllocationCols = ['id', 'pickRequestId', 'pickMode', 'pickAllocationTypeSeq', 'pickAllocationType', 'pickCatLvl', 'created_time'];
    let pickAllocationDatas = [
      ['251', 'TEST_PICK_REQUEST018', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['252', 'TEST_PICK_REQUEST018', '0001', '2', '0004', '2', '2019-09-17 15:14:57']
    ];

    let pickPriorityCols = ['id', 'pickRequestId', 'pickMode', 'pickPriorityType', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];
    let pickPriorityDatas = [
      ['251', 'TEST_PICK_REQUEST018', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57']
    ];

    let pickTaskCols = ['ID', 'pickRequestId', 'pickAllocationType', 'wave_id', 'multiorder_id', 'user_id', 'user_pack', 'bu', 'role', 'status', 'created_time'];
    let pickTaskDatas = [
      ['251', 'TEST_PICK_REQUEST018', '', '0', '0', 'Fulfilment004', nullValue, 'CDS', 'Picker', '0005', '2019-09-18 13:10:15'],
      ['252', 'TEST_PICK_REQUEST018', '', '0', '0', 'Fulfilment005', nullValue, 'CDS', 'Picker', '0002', '2019-09-18 13:10:15'],
      ['253', 'TEST_PICK_REQUEST018', '', '0', '0', 'Fulfilment006', nullValue, 'CDS', 'Picker', '0005', '2019-09-18 13:10:15']
    ];

    let pickTaskDetailCols = ['ID', 'pickTaskId', 'pickRequestId', 'itemKey', 'pickBarcode', 'created_time'];
    let pickTaskDetailDatas = [
      ['251', '251', 'TEST_PICK_REQUEST018', 'ITEM_KEY_001', nullValue, '2019-09-18 06:10:15'],
      ['252', '251', 'TEST_PICK_REQUEST018', 'ITEM_KEY_002', nullValue, '2019-09-18 06:10:15'],
      ['253', '252', 'TEST_PICK_REQUEST018', 'ITEM_KEY_003', nullValue, '2019-09-18 06:10:15'],
      ['254', '253', 'TEST_PICK_REQUEST018', 'ITEM_KEY_004', nullValue, '2019-09-18 06:10:15'],
      ['255', '253', 'TEST_PICK_REQUEST018', 'ITEM_KEY_005', nullValue, '2019-09-18 06:10:15']
    ];

    let pickTaskDetailItemCols = ['id', 'pickTaskDetailId', 'itemStatus', 'statusReason', 'pickBarcode', 'created_time'];
    let pickTaskDetailItemDatas = [
      ['251', '251', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['252', '251', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['253', '252', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['254', '252', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['255', '253', '0002', 'decline test', '82690632', '2019-09-18 13:10:15'],
      ['256', '254', '0005', '', '18317152', '2019-09-18 13:10:15'],
      ['257', '255', '0005', '', '17070638', '2019-09-18 13:10:15']
    ]

    let pickUsersCols = ['id', 'userId', 'cat_01', 'cat_02', 'cat_03', 'cat_04', 'cat_05', 'cat_06', 'zone', 'brand', 'remark', 'bu', 'loc', 'status'];
    let pickUsersDatas = [
      ['100', 'Fulfilment001', '2000', '2010', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['101', 'Fulfilment001', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['102', 'Fulfilment002', '2000', '2020', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['103', 'Fulfilment002', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['104', 'Fulfilment003', '4000', 'A', '', '', '', '', '5000', 'A', '', 'CDS', '10116', 'A']
    ];

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_request', pickRequestCols, pickRequestDatas),
      UtilModel.getInstance().sqlInsert('pr_items', prItemsCols, prItemsDatas),
      UtilModel.getInstance().sqlInsert('pr_pick_params', pickParamCols, pickParamDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_allocation', pickAllocationCols, pickAllocationDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_priority', pickPriorityCols, pickPriorityDatas),
      UtilModel.getInstance().sqlInsert('pick_task', pickTaskCols, pickTaskDatas),
      UtilModel.getInstance().sqlInsert('pick_task_detail', pickTaskDetailCols, pickTaskDetailDatas),
      UtilModel.getInstance().sqlInsert('pick_task_detail_item', pickTaskDetailItemCols, pickTaskDetailItemDatas),
      UtilModel.getInstance().sqlInsert('pick_users', pickUsersCols, pickUsersDatas)
    ]);
  }

  it('SetPackReceiveItemStatus | Case:001 Set pack on canceled item | it should be error', async () => {
    let update = [
      {
        task_id: 252,
        user_id: "Fultilment001",
        item_barcode: "82690632",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];
    let res = await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    expect(res.body.statusCode).to.equal(500);
  });

  it('SetPackReceiveItemStatus | Case:002 Set pack on 1 item of 2 | task should be 0005, item should be 0006', async () => {
    let update = [
      {
        task_id: 253,
        user_id: "Fultilment001",
        item_barcode: "17070638",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];

    await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    let sql = `
      select status from pr_items
      where pickRequestId = ?
      and itemKey = ?
    `;
    let status = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST018', 'ITEM_KEY_005']);
    expect(status[0].status).to.equal(ITEM_STATUS.READY_TO_PACK);

    sql = `select status from pick_task where id = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, [253]);
    expect(status[0].status).to.equal(TASK_STATUS.PICKED);
  });

  it('SetPackReceiveItemStatus | Case:003 Set pack on 2 item of 2 | task should be 0006', async () => {
    let update = [
      {
        task_id: 253,
        user_id: "Fultilment001",
        item_barcode: "18317152",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];

    await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    let sql = `select status from pick_task where id = ?`;
    let status = await UtilModel.getInstance().sqlQuerySync(sql, [253]);
    expect(status[0].status).to.equal(TASK_STATUS.READY_TO_PACK);
  });

  it('SetPackReceiveItemStatus | Case:004 Set pack on 1 item of 2 with qty 1 of 2 | task should be 0005, item should be 0005, detail item should be 0006 and 0005 combine', async () => {
    let update = [
      {
        task_id: 251,
        user_id: "Fultilment001",
        item_barcode: "10095737",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];

    await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    let sql = `
      select status from pr_items
      where pickRequestId = ?
      and itemKey = ?
    `;
    let status = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST018', 'ITEM_KEY_001']);
    expect(status[0].status).to.equal(ITEM_STATUS.PICKED);

    sql = `select status from pick_task where id = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, [251]);
    expect(status[0].status).to.equal(TASK_STATUS.PICKED);

    sql = `select distinct itemStatus from pick_task_detail_item where pickTaskDetailId = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, [251]);
    expect(status.length).to.equal(2);
  });

  it('SetPackReceiveItemStatus | Case:005 Set pack on 1 item of 2 with qty 2 of 2 | task should be 0005, item should be 0006', async () => {
    let update = [
      {
        task_id: 251,
        user_id: "Fultilment001",
        item_barcode: "10095737",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];

    await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    let sql = `
      select status from pr_items
      where pickRequestId = ?
      and itemKey = ?
    `;
    let status = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST018', 'ITEM_KEY_001']);
    expect(status[0].status).to.equal(ITEM_STATUS.READY_TO_PACK);

    sql = `select status from pick_task where id = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, [251]);
    expect(status[0].status).to.equal(TASK_STATUS.PICKED);
  });

  it('SetPackReceiveItemStatus | Case:006 Set pack with array of item 2 of 2 | task should be 0006, item should be 0006', async () => {
    let update = [
      {
        task_id: 251,
        user_id: "Fultilment001",
        item_barcode: "10095738",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      },
      {
        task_id: 251,
        user_id: "Fultilment001",
        item_barcode: "10095738",
        location_barcode: "A1",
        orderKey: "ORDER_KEY_018"
      }
    ];

    await client.post('/setPackReceiveItemStatus').set('Content-Type', 'application/json').send(update).expect(200);
    let sql = `
      select status from pr_items
      where pickRequestId = ?
      and itemKey = ?
    `;
    let status = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST018', 'ITEM_KEY_002']);
    expect(status[0].status).to.equal(ITEM_STATUS.READY_TO_PACK);

    sql = `select status from pick_task where id = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, [251]);
    expect(status[0].status).to.equal(TASK_STATUS.READY_TO_PACK);

    sql = `select status from pick_request where pickRequestId = ?`;
    status = await UtilModel.getInstance().sqlQuerySync(sql, ['TEST_PICK_REQUEST018']);
    expect(status[0].status).to.equal(PICK_REQUEST_STATUS.FINISHED);
  });

});
