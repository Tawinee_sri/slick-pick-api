import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import fs = require('fs');

describe('PickAllocationController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST002');
  pickRequestIdList.push('TEST_PICK_REQUEST003');
  pickRequestIdList.push('TEST_PICK_REQUEST004');
  pickRequestIdList.push('TEST_PICK_REQUEST005');
  pickRequestIdList.push('TEST_PICK_REQUEST006');
  pickRequestIdList.push('TEST_PICK_REQUEST020');
  pickRequestIdList.push('TEST_PICK_REQUEST021');
  pickRequestIdList.push('TEST_PICK_REQUEST022');
  pickRequestIdList.push('TEST_PICK_REQUEST023');
  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuery(`delete from pick_request where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_items where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pick_params where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_allocation where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_priority where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail_item pi where exists (select * from pick_task_detail pd where pi.pickTaskDetailId = pd.id and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task pt where exists (select * from pick_task_detail pd where pt.id = pd.pickTaskId and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_users where id >= 100`, []);
  }

  async function prepareTestData() {
    let nullValue = null;

    let pickRequestCols = ['id', 'pickRequestId', 'pickRequestDate', 'subOrderKey', 'sourceSubOrderId', 'orderKey', 'sourceOrderId', 'bu', 'sourceBU', 'sourceLoc', 'status', 'statusReason', 'created_time'];
    let pickRequestDatas = [
      ['10', 'TEST_PICK_REQUEST002', '2019-09-17 12:24:20', 'SUB_ORDER_ID_002', 'SUB_ORDER_ID_002', 'ORDER_KEY_002', 'SUB_ORDER_ID_002', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:14:57'],
      ['11', 'TEST_PICK_REQUEST003', '2019-09-17 12:24:20', 'SUB_ORDER_ID_003', 'SUB_ORDER_ID_003', 'ORDER_KEY_003', 'SUB_ORDER_ID_003', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['12', 'TEST_PICK_REQUEST004', '2019-09-17 12:24:20', 'SUB_ORDER_ID_004', 'SUB_ORDER_ID_004', 'ORDER_KEY_004', 'SUB_ORDER_ID_004', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['13', 'TEST_PICK_REQUEST005', '2019-09-17 12:24:20', 'SUB_ORDER_ID_005', 'SUB_ORDER_ID_005', 'ORDER_KEY_005', 'SUB_ORDER_ID_005', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['14', 'TEST_PICK_REQUEST006', '2019-09-17 12:24:20', 'SUB_ORDER_ID_006', 'SUB_ORDER_ID_006', 'ORDER_KEY_006', 'SUB_ORDER_ID_006', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['300', 'TEST_PICK_REQUEST020', '2019-09-17 12:24:20', 'SUB_ORDER_ID_020', 'SUB_ORDER_ID_020', 'ORDER_KEY_020', 'SUB_ORDER_ID_020', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['301', 'TEST_PICK_REQUEST021', '2019-09-17 12:24:20', 'SUB_ORDER_ID_021', 'SUB_ORDER_ID_021', 'ORDER_KEY_021', 'SUB_ORDER_ID_021', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['302', 'TEST_PICK_REQUEST022', '2019-09-17 12:24:20', 'SUB_ORDER_ID_022', 'SUB_ORDER_ID_022', 'ORDER_KEY_022', 'SUB_ORDER_ID_022', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54'],
      ['303', 'TEST_PICK_REQUEST023', '2019-09-17 12:24:20', 'SUB_ORDER_ID_023', 'SUB_ORDER_ID_023', 'ORDER_KEY_023', 'SUB_ORDER_ID_023', 'CDS', 'CDS', '10116', '0001', '', '2019-09-17 15:20:54']
    ];

    let prItemsCols = ['id', 'pickRequestId', 'itemKey', 'sourceItemId', 'sourceItemNumber', 'barcode', 'sku', 'qty', 'qtyUnit', 'productNameTH', 'productNameEN', 'imageUrl', 'locBarcode', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'brand', 'zone', 'status', 'statusReason', 'statusDate', 'created_time'];
    let prItemsDatas = [
      ['10', 'TEST_PICK_REQUEST002', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '20', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['11', 'TEST_PICK_REQUEST002', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['12', 'TEST_PICK_REQUEST002', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '20', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['13', 'TEST_PICK_REQUEST002', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['14', 'TEST_PICK_REQUEST002', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:14:57'],
      ['15', 'TEST_PICK_REQUEST003', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['16', 'TEST_PICK_REQUEST003', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['17', 'TEST_PICK_REQUEST003', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['18', 'TEST_PICK_REQUEST003', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['19', 'TEST_PICK_REQUEST003', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['20', 'TEST_PICK_REQUEST004', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['21', 'TEST_PICK_REQUEST004', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['22', 'TEST_PICK_REQUEST004', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['23', 'TEST_PICK_REQUEST004', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['24', 'TEST_PICK_REQUEST004', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['25', 'TEST_PICK_REQUEST005', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['26', 'TEST_PICK_REQUEST005', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['27', 'TEST_PICK_REQUEST005', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['28', 'TEST_PICK_REQUEST005', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['29', 'TEST_PICK_REQUEST005', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['30', 'TEST_PICK_REQUEST006', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['31', 'TEST_PICK_REQUEST006', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['32', 'TEST_PICK_REQUEST006', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['33', 'TEST_PICK_REQUEST006', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['34', 'TEST_PICK_REQUEST006', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['300', 'TEST_PICK_REQUEST020', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['301', 'TEST_PICK_REQUEST020', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['302', 'TEST_PICK_REQUEST020', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['303', 'TEST_PICK_REQUEST020', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['304', 'TEST_PICK_REQUEST020', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['305', 'TEST_PICK_REQUEST021', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['306', 'TEST_PICK_REQUEST021', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['307', 'TEST_PICK_REQUEST021', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['308', 'TEST_PICK_REQUEST021', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['309', 'TEST_PICK_REQUEST021', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['310', 'TEST_PICK_REQUEST022', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['311', 'TEST_PICK_REQUEST022', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['312', 'TEST_PICK_REQUEST022', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['313', 'TEST_PICK_REQUEST022', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['314', 'TEST_PICK_REQUEST022', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['315', 'TEST_PICK_REQUEST023', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['316', 'TEST_PICK_REQUEST023', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095737', '4132532', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['317', 'TEST_PICK_REQUEST023', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['318', 'TEST_PICK_REQUEST023', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['319', 'TEST_PICK_REQUEST023', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0001', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54']
    ];

    let pickParamCols = ['id', 'pickRequestId', 'pickMode', 'receivingScan', 'locHandling', 'pickAssignType', 'pickSubAssignType', 'created_time'];
    let pickParamDatas = [
      ['10', 'TEST_PICK_REQUEST003', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:20:54'],
      ['11', 'TEST_PICK_REQUEST002', '0001', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['12', 'TEST_PICK_REQUEST004', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['13', 'TEST_PICK_REQUEST005', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['14', 'TEST_PICK_REQUEST006', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57'],
      ['300', 'TEST_PICK_REQUEST020', '0002', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['301', 'TEST_PICK_REQUEST021', '0002', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['302', 'TEST_PICK_REQUEST022', '0002', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57'],
      ['303', 'TEST_PICK_REQUEST023', '0002', 'Y', 'Y', '0001', '0001', '2019-09-17 15:14:57']
    ];

    let pickAllocationCols = ['id', 'pickRequestId', 'pickMode', 'pickAllocationTypeSeq', 'pickAllocationType', 'pickCatLvl', 'created_time'];
    let pickAllocationDatas = [
      ['10', 'TEST_PICK_REQUEST003', '0001', '1', '0001', '1', '2019-09-17 15:20:54'],
      ['11', 'TEST_PICK_REQUEST002', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['12', 'TEST_PICK_REQUEST004', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['13', 'TEST_PICK_REQUEST005', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['14', 'TEST_PICK_REQUEST005', '0001', '2', '0002', '2', '2019-09-17 15:14:57'],
      ['15', 'TEST_PICK_REQUEST006', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['16', 'TEST_PICK_REQUEST006', '0001', '2', '0004', '2', '2019-09-17 15:14:57'],
      ['300', 'TEST_PICK_REQUEST020', '0002', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['301', 'TEST_PICK_REQUEST021', '0002', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['302', 'TEST_PICK_REQUEST022', '0002', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['303', 'TEST_PICK_REQUEST022', '0002', '1', '0002', '2', '2019-09-17 15:14:57'],
      ['304', 'TEST_PICK_REQUEST023', '0002', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['305', 'TEST_PICK_REQUEST023', '0002', '1', '0002', '2', '2019-09-17 15:14:57']
    ];

    let pickPriorityCols = ['id', 'pickRequestId', 'pickMode', 'pickPriorityType', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];
    let pickPriorityDatas = [
      ['10', 'TEST_PICK_REQUEST003', '0001', '0001', '0001', nullValue, '2019-09-17 15:20:54'],
      ['11', 'TEST_PICK_REQUEST002', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['12', 'TEST_PICK_REQUEST004', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['13', 'TEST_PICK_REQUEST005', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['14', 'TEST_PICK_REQUEST006', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['300', 'TEST_PICK_REQUEST020', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['301', 'TEST_PICK_REQUEST021', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['302', 'TEST_PICK_REQUEST022', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57'],
      ['303', 'TEST_PICK_REQUEST023', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57']
    ];

    let pickUsersCols = ['id', 'userId', 'cat_01', 'cat_02', 'cat_03', 'cat_04', 'cat_05', 'cat_06', 'zone', 'brand', 'remark', 'bu', 'loc', 'status'];
    let pickUsersDatas = [
      ['100', 'Fulfilment001', '2000', '2010', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['101', 'Fulfilment001', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['102', 'Fulfilment002', '2000', '2020', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['103', 'Fulfilment002', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['104', 'Fulfilment003', '4000', 'A', '', '', '', '', '5000', 'A', '', 'CDS', '10116', 'A']
    ]

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_request', pickRequestCols, pickRequestDatas),
      UtilModel.getInstance().sqlInsert('pr_items', prItemsCols, prItemsDatas),
      UtilModel.getInstance().sqlInsert('pr_pick_params', pickParamCols, pickParamDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_allocation', pickAllocationCols, pickAllocationDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_priority', pickPriorityCols, pickPriorityDatas),
      UtilModel.getInstance().sqlInsert('pick_users', pickUsersCols, pickUsersDatas)
    ]);

    let alloc = JSON.parse(fs.readFileSync('./data/test_setPickTaskAllocation.json', 'utf-8'));
    await client.post('/setPickTaskAllocation').set('Content-Type', 'application/json').send(alloc).expect(200);
  }

  function countTask(pickRequestId: any) {
    return new Promise(async (resolve, reject) => {
      let result = await UtilModel.getInstance().sqlCount(`select * from pick_task where pickRequestId = ?`, [pickRequestId]).then((res: any) => res.cnt);
      resolve(result)
    });
  }

  it('PickAllocationRequest | Case:001 Assign=PULL, Allocate=NONE | Generate one task', async () => {
    let task003 = await countTask(['TEST_PICK_REQUEST003']).then((res: any) => res);
    expect(task003).to.equal(1);
  });

  it('PickAllocationRequest | Case:002 Assign=PULL, Allocate=CatLv1 | Generate 2 task', async () => {
    let task002 = await countTask(['TEST_PICK_REQUEST002']).then((res: any) => res);
    expect(task002).to.equal(2);
  });

  it('PickAllocationRequest | Case:003 Assign=PUSH, Allocate=CatLv1, Load=Least | Generate 2 task for 005 and (001 or 002)', async () => {
    let task = await UtilModel.getInstance().sqlQuerySync('select * from pick_task where pickRequestId = ?', ['TEST_PICK_REQUEST004']);
    let users = task.map((x: any) => x.user_id);
    expect(task.length).to.equal(2);
    expect(users.includes('Fulfilment003')).to.equal(true);
    expect(users.includes('Fulfilment001') || users.includes('Fulfilment002')).to.equal(true);
  });

  it('PickAllocationRequest | Case:004 Assign=PUSH, Allocate=CatLv1+Lv2, Load=Least | Generate 2 task for 001 and 005', async () => {
    let task = await UtilModel.getInstance().sqlQuerySync('select * from pick_task where pickRequestId = ?', ['TEST_PICK_REQUEST005']);
    let users = task.map((x: any) => x.user_id);
    expect(task.length).to.equal(2);
    expect(users.includes('Fulfilment003')).to.equal(true);
    expect(users.includes('Fulfilment001') || users.includes('Fulfilment002')).to.equal(true);
  });

  it('PickAllocationRequest | Case:005 Assign=PUSH, Allocate=CatLv1+Brand, Load=Least | Generate 3 task for 001, 002 and 003', async () => {
    let task = await UtilModel.getInstance().sqlQuerySync('select * from pick_task where pickRequestId = ?', ['TEST_PICK_REQUEST006']);
    let users = task.map((x: any) => x.user_id);
    expect(task.length).to.equal(3);
    expect(users.includes('Fulfilment003')).to.equal(true);
    expect(users.includes('Fulfilment002')).to.equal(true);
    expect(users.includes('Fulfilment001')).to.equal(true);
  });

  it('PickAllocationRequest | Case:006 Multi, Assign=PULL, Allocate=CatLv1 | Generate 2 task with 2 order', async () => {
    let sql = `select distinct pickTaskId from pick_task_detail where pickRequestid in (?)`;
    let task = await UtilModel.getInstance().sqlQuerySync(sql, [['TEST_PICK_REQUEST020', 'TEST_PICK_REQUEST020']]);
    expect(task.length).to.equal(2);
  });

});
