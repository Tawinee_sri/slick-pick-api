import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { PROJECT_VERSION } from './../../models/picking/constant';
import fs = require('fs');

describe('PingController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;
  let currDbVersion: string = PROJECT_VERSION.DB_VERSION;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  after(async () => {
    await app.stop();
  });

  it('invokes GET /ping', async () => {
    const res = await client.get('/ping?msg=world').expect(200);
    expect(res.body).to.containEql({ greeting: 'Hello from LoopBack' });
  });

  //DB Connection
  it('Connection to DB : check db version @' + currDbVersion, async () => {

    const res = await client.get('/getDBVersion').expect(200);

    expect(res.body.statusCode).to.equal(200);
    expect(res.body.data).to.equal(currDbVersion);
  });


});
