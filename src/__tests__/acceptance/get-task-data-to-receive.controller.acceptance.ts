import { Client, expect } from '@loopback/testlab';
import { SlickChatAndShopAPIApplication } from '../..';
import { setupApplication } from './test-helper';
import { UtilModel } from '../../models/UtilModel';
import { ITEM_STATUS } from '../../models/picking/constant';
import fs = require('fs');

describe('GetTaskDataToReceiveController', () => {
  let app: SlickChatAndShopAPIApplication;
  let client: Client;

  let pickRequestIdList: string[] = [];
  pickRequestIdList.push('TEST_PICK_REQUEST017');
  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
    await removeRelateData();
    await prepareTestData();
  });

  after(async () => {
    await removeRelateData();
    await app.stop();
  });

  async function removeRelateData() {
    await UtilModel.getInstance().sqlQuery(`delete from pick_request where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_items where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pick_params where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_allocation where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pr_pickparams_priority where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail_item pi where exists (select * from pick_task_detail pd where pi.pickTaskDetailId = pd.id and pd.pickRequestId in (?))`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_task_detail where pickRequestId in (?)`, [pickRequestIdList]);
    await UtilModel.getInstance().sqlQuery(`delete from pick_users where id >= 100`, []);
  }

  async function prepareTestData() {
    let nullValue = null;

    let pickRequestCols = ['id', 'pickRequestId', 'pickRequestDate', 'subOrderKey', 'sourceSubOrderId', 'orderKey', 'sourceOrderId', 'bu', 'sourceBU', 'sourceLoc', 'status', 'statusReason', 'created_time'];
    let pickRequestDatas = [
      ['200', 'TEST_PICK_REQUEST017', '2019-09-17 12:24:20', 'SUB_ORDER_ID_017', 'SUB_ORDER_ID_017', 'ORDER_KEY_017', 'SUB_ORDER_ID_017', 'CDS', 'CDS', '10116', '0002', '', '2019-09-17 15:20:54']
    ];

    let prItemsCols = ['id', 'pickRequestId', 'itemKey', 'sourceItemId', 'sourceItemNumber', 'barcode', 'sku', 'qty', 'qtyUnit', 'productNameTH', 'productNameEN', 'imageUrl', 'locBarcode', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'brand', 'zone', 'status', 'statusReason', 'statusDate', 'created_time'];
    let prItemsDatas = [
      ['201', 'TEST_PICK_REQUEST017', 'ITEM_KEY_001', 'SOURCE_ID_001', '1', '10095737', '4132532', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['202', 'TEST_PICK_REQUEST017', 'ITEM_KEY_002', 'SOURCE_ID_002', '2', '10095738', '4132533', '2.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า R.N.A. Power Radical New Age ขนาด 80 กรัม', 'SK-II R.N.A. Power Radical New Age 80 g.', 'http://static1.central.co.th/productimages/tpimage/O133390.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'SKII', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['203', 'TEST_PICK_REQUEST017', 'ITEM_KEY_003', 'SOURCE_ID_003', '3', '82690632', '1785452', '1.00', 'pcs', 'ผลิตภัณฑ์บำรุงผิวหน้า Crème de la Mer ขนาด 60 มล.', 'Crème de la Mer 60 ml.', 'http://static1.central.co.th/productimages/tpimage/O189341.jpg', '', '2000', '2010', '2010', '2', '0', '0', 'LA MER', '', '0002', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['204', 'TEST_PICK_REQUEST017', 'ITEM_KEY_004', 'SOURCE_ID_004', '4', '18317152', '4955689', '1.00', 'pcs', 'กล้อง GoPro HERO 7 สีดำ', 'GoPro Hero 7 Black', 'http://static1.central.co.th/productimages/tpimage/2062059.jpg', '', '4000', '4120', '4020', '30', '0', '0', 'GoPro', '', '0002', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54'],
      ['205', 'TEST_PICK_REQUEST017', 'ITEM_KEY_005', 'SOURCE_ID_005', '5', '17070638', '4830945', '1.00', 'pcs', 'GoPro สายคล้องคอ สีขาว', 'GoPro SLEEVE LANYARD WHITE', 'https://backend.central.co.th/media/catalog/product/2/0/2062064.jpg', '', '4000', '4120', '4020', '2', '0', '0', 'GoPro', '', '0005', '', '2019-09-17 12:24:20', '2019-09-17 15:20:54']
    ];

    let pickParamCols = ['id', 'pickRequestId', 'pickMode', 'receivingScan', 'locHandling', 'pickAssignType', 'pickSubAssignType', 'created_time'];
    let pickParamDatas = [
      ['201', 'TEST_PICK_REQUEST017', '0001', 'Y', 'Y', '0002', '0001', '2019-09-17 15:14:57']
    ];

    let pickAllocationCols = ['id', 'pickRequestId', 'pickMode', 'pickAllocationTypeSeq', 'pickAllocationType', 'pickCatLvl', 'created_time'];
    let pickAllocationDatas = [
      ['201', 'TEST_PICK_REQUEST017', '0001', '1', '0002', '1', '2019-09-17 15:14:57'],
      ['202', 'TEST_PICK_REQUEST017', '0001', '2', '0004', '2', '2019-09-17 15:14:57']
    ];

    let pickPriorityCols = ['id', 'pickRequestId', 'pickMode', 'pickPriorityType', 'pickPriorityLevel', 'pickPriorityTime', 'created_time'];
    let pickPriorityDatas = [
      ['201', 'TEST_PICK_REQUEST017', '0001', '0001', '0001', nullValue, '2019-09-17 15:14:57']
    ];

    let pickTaskCols = ['ID', 'pickRequestId', 'pickAllocationType', 'wave_id', 'multiorder_id', 'user_id', 'user_pack', 'bu', 'role', 'status', 'created_time'];
    let pickTaskDatas = [
      ['201', 'TEST_PICK_REQUEST017', '', '0', '0', 'Fulfilment004', nullValue, 'CDS', 'Picker', '0005', '2019-09-18 13:10:15'],
      ['202', 'TEST_PICK_REQUEST017', '', '0', '0', 'Fulfilment005', nullValue, 'CDS', 'Picker', '0002', '2019-09-18 13:10:15'],
      ['203', 'TEST_PICK_REQUEST017', '', '0', '0', 'Fulfilment006', nullValue, 'CDS', 'Picker', '0005', '2019-09-18 13:10:15']
    ];

    let pickTaskDetailCols = ['ID', 'pickTaskId', 'pickRequestId', 'itemKey', 'pickBarcode', 'created_time'];
    let pickTaskDetailDatas = [
      ['201', '201', 'TEST_PICK_REQUEST017', 'ITEM_KEY_001', nullValue, '2019-09-18 06:10:15'],
      ['202', '201', 'TEST_PICK_REQUEST017', 'ITEM_KEY_002', nullValue, '2019-09-18 06:10:15'],
      ['203', '202', 'TEST_PICK_REQUEST017', 'ITEM_KEY_003', nullValue, '2019-09-18 06:10:15'],
      ['204', '203', 'TEST_PICK_REQUEST017', 'ITEM_KEY_004', nullValue, '2019-09-18 06:10:15'],
      ['205', '203', 'TEST_PICK_REQUEST017', 'ITEM_KEY_005', nullValue, '2019-09-18 06:10:15']
    ];

    let pickTaskDetailItemCols = ['id', 'pickTaskDetailId', 'itemStatus', 'statusReason', 'pickBarcode', 'created_time'];
    let pickTaskDetailItemDatas = [
      ['201', '201', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['202', '201', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['203', '202', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['204', '202', '0005', '', '10095737', '2019-09-18 13:10:15'],
      ['205', '203', '0002', 'decline test', '17070638', '2019-09-18 13:10:15'],
      ['206', '204', '0002', 'decline test', '18317152', '2019-09-18 13:10:15'],
      ['207', '205', '0005', '', '82690632', '2019-09-18 13:10:15']
    ]

    let pickUsersCols = ['id', 'userId', 'cat_01', 'cat_02', 'cat_03', 'cat_04', 'cat_05', 'cat_06', 'zone', 'brand', 'remark', 'bu', 'loc', 'status'];
    let pickUsersDatas = [
      ['100', 'Fulfilment001', '2000', '2010', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['101', 'Fulfilment001', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['102', 'Fulfilment002', '2000', '2020', '', '', '', '', '5000', 'LA MER', '', 'CDS', '10116', 'A'],
      ['103', 'Fulfilment002', '2000', 'A', '', '', '', '', '5000', 'SKII', '', 'CDS', '10116', 'A'],
      ['104', 'Fulfilment003', '4000', 'A', '', '', '', '', '5000', 'A', '', 'CDS', '10116', 'A']
    ];

    await Promise.all([
      UtilModel.getInstance().sqlInsert('pick_request', pickRequestCols, pickRequestDatas),
      UtilModel.getInstance().sqlInsert('pr_items', prItemsCols, prItemsDatas),
      UtilModel.getInstance().sqlInsert('pr_pick_params', pickParamCols, pickParamDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_allocation', pickAllocationCols, pickAllocationDatas),
      UtilModel.getInstance().sqlInsert('pr_pickparams_priority', pickPriorityCols, pickPriorityDatas),
      UtilModel.getInstance().sqlInsert('pick_task', pickTaskCols, pickTaskDatas),
      UtilModel.getInstance().sqlInsert('pick_task_detail', pickTaskDetailCols, pickTaskDetailDatas),
      UtilModel.getInstance().sqlInsert('pick_task_detail_item', pickTaskDetailItemCols, pickTaskDetailItemDatas),
      UtilModel.getInstance().sqlInsert('pick_users', pickUsersCols, pickUsersDatas)
    ]);
  }

  it('GetTaskDataToReceive | Case:001 Get success task with item = 2, qty = 2 | item size should be 2 and each item should have 2 pick barcodes', async () => {
    let task = {
      task_id: 201
    }
    let res = await client.post('/getTaskDataToReceive').set('Content-Type', 'application/json').send(task).expect(200);
    expect(res.body.data.pickingItems.length).to.equal(2);
    expect(res.body.data.pickingItems[0].pickBarcodes.length).to.equal(2);
  });

  it('GetTaskDataToReceive | Case:002 Get canceled task with | it should be error ', async () => {
    let task = {
      task_id: 202
    }
    let res = await client.post('/getTaskDataToReceive').set('Content-Type', 'application/json').send(task).expect(200);
    expect(res.body.statusCode).to.equal(500);
  });

  it('GetTaskDataToReceive | Case:003 Get picked task that 1 item decline | item size should be get only picked ', async () => {
    let task = {
      task_id: 203
    }
    let res = await client.post('/getTaskDataToReceive').set('Content-Type', 'application/json').send(task).expect(200);
    expect(res.body.data.pickingItems.length).to.equal(1);
  });

});
