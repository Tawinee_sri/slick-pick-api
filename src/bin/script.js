var fs = require('fs');

function readWriteSync() {
  console.log(' -------------------- [ START readWriteSync ] -------------------- ');
  let env = process.env.ENV;
  console.log('[readWriteSync] ENV : ', env);

  if (!env) {
    env = 'nonprod';
  }

  var data = fs.readFileSync('environment/dbConnect/mysql.connection.' + env + '.json', 'utf-8');
  fs.writeFileSync('src/models/mysql.connection.local.json', data, 'utf-8');

  data = fs.readFileSync('environment/constants/constant.' + env + '.ts', 'utf-8');
  fs.writeFileSync('src/models/picking/constant.ts', data, 'utf-8');

  data = fs.readFileSync('environment/ecosystem/ecosystem.config.' + env + '.js', 'utf-8');
  fs.writeFileSync('ecosystem.config.js', data, 'utf-8');

  console.log(' -------------------- [ END readWriteSync ] -------------------- ');
}

readWriteSync();
