
export const ITEM_STATUS = {
  NEW: '0001',
  DECLINED: '0002',
  PENDING: '0003',
  PARTIAL: '0004',
  PICKED: '0005',
  READY_TO_PACK: '0006',
  CANCELED: '0007',
  FINISHED: '0008',
  REQ_NEW: '1000'
}

export const ITEM_STATUS_FM_MAPPING = {
  '0002': '4100',
  '0006': '2001',
  '0007': '4000',
  '4000': '4000',
  '4100': '4100'
}

export const TASK_STATUS = {
  NEW: '0001',
  CANCELED: '0002',
  PENDING: '0003',
  PARTIAL: '0004',
  PICKED: '0005',
  READY_TO_PACK: '0006',
  FINISHED: '0007',

  WAITING_DECLINE: '9999'
}

export const CHANNEL = {
  "0100": "offline",
  "0001": "online",
  "0002": "market place",
  "0003": "chat&shop",
  "0004": "Shopee",
  "0005": "Lazada",
  "0006": "Mirakl (COL)",
  "0007": "Mirakl (ROL)",
  "0008": "Mirakl (OfficeMate)",
  "0009": "JD",
  "0010": "ChefYim",
  "0011": "e-ordering Online",
  "0012": "e-ordering Offline",
  "0013": "Mirakl (Powerbuy)",
  "0015": "Zalo Shop",
  "0016": "Zalo Chat",
  "0017": "NOW",
  "0018": "Baemin",
  "0019": "Grab Mart",
  "0020": "Telesales",
  "0021": "Mobile App",
  "0022": "Facebook Chat",
  "0023": "Facebook Web",
  "0024": "Central Food Hall (CFH)",
}

export const DY_SEARCH = {
  CHANNEL: {
    "0100": "offline",
    "0001": "online",
    "0002": "market place",
    "0003": "chat&shop",
    "0004": "Shopee",
    "0005": "Lazada",
    "0006": "Mirakl (COL)",
    "0007": "Mirakl (ROL)",
    "0008": "Mirakl (OfficeMate)",
    "0009": "JD",
    "0010": "ChefYim",
    "0011": "e-ordering Online",
    "0012": "e-ordering Offline",
    "0013": "Mirakl (Powerbuy)",
    "0015": "Zalo Shop",
    "0016": "Zalo Chat",
    "0017": "NOW",
    "0018": "Baemin",
    "0019": "Grab Mart",
    "0020": "Telesales",
    "0021": "Mobile App",
    "0022": "Facebook Chat",
    "0023": "Facebook Web",
    "0024": "Central Food Hall (CFH)",
  }
}

export const DELIVERY_TYPE = {
  "0001": "home delivery",
  "0002": "click and collect",
  "0003": "click and reserve",
  "0004": "click and collect (AP)",
}

export const DELIVERY_SUBTYPE = {
  "0001": "Same day",
  "0002": "Next day",
  "0003": "Standard",
  "0004": "Express",
  "0005": "1hr.",
  "0006": "2hrs.",
  "0007": "3hrs.",
}

export const PICK_REQUEST_STATUS = {
  NEW: '0001',
  ASSIGN: '0002',
  FINISHED: '0003',
  CANCELED: '0004',
  DECLINED: '0005',

  PICK_RESERVE: '9000'
}

export const PICK_REQUEST_STATUS_FM_MAPPING = {
  '0003': '3000',
  '0004': '4000',
  '0005': '4100'
}

export const PICK_ALLOCATION_TYPE = {
  NONE: '0001',
  BY_CATEGORY: '0002',
  BY_ZONE: '0003',
  BY_BRAND: '0004'
}

export const PICK_PRIORITY_TYPE = {
  FIXED: '0001',
  TIME_TO_DELIVERY: '0002'
}

export const PICK_PRIORITY_LEVEL = {
  HIGH: '0001',
  MEDIUM: '0002',
  NORMAL: '0003'
}

export const PICK_MODE = {
  BY_ORDER: '0001',
  MULTI_ORDER: '0002',
  WAVE: '0003',
  MERGE_ORDER: '0004'
}

export const ASSIGN_TYPE = {
  PULL: '0001',
  PUSH: '0002'
}

export const SUB_ASSIGN_TYPE = {
  LEAST_LOAD: '0001',
  EQUAL_LOAD: '0002',
  ROUND_ROBIN: '0003'
}

export const FM_UM_MAPPING = {
  cat1: 'cat_01',
  cat2: 'cat_02',
  cat3: 'cat_03',
  cat4: 'cat_04',
  cat5: 'cat_05',
  cat6: 'cat_06',
  zone: 'zone',
  brand: 'brand'
}

export const EORDERING = {
  EORDERING_ONLINE: '0011',
  EORDERING_OFFLINE: '0012',
}

export const FM_WS = {
  GET_SKU: 'https://api.central.co.th/slick/common/product/list',
  GET_ACTIVE_USER: 'http://localhost:8005/users',
  LOGIN_URL: 'https://api.central.co.th/slick/common/auth/member/login',
  // POST_READY_TO_PACK: 'https://services-apim.central.co.th/fms/slickresult-proxyservice/api/v2/PickResp'
  POST_READY_TO_PACK: 'http://slickresult-proxyservice.fms-prd/api/v2/PickResp'
}

export const FM_AUTHORITIES = {
  CLIENT_ID: '743f8aacc60e4c87aa304e46bd004627',
  CLIENT_SECRET: '235847B0051149e2b80C8c54BC07A001',
  LOGIN_CLIENT_ID: '32dd46f67eee425999ae158d2d4f2b14',
  LOGIN_CLIENT_SECRET: '76CD23530Df041Ce882905b35095Bb34',
  AZURE_KEY: 'd0c1cb0100fb40bea95d1d38de22fa6e'
}

export const PROJECT_VERSION = {
  API_VERSION: '1.0.1',
  DB_VERSION: '1.0.1'
}
