var fs = require('fs');
console.log(' ------------------ start sync file ------------------ ');

let env = process.env.ENV;
console.log('[scripts docker] ENV : ', env);

if (!env) {
  env = 'nonprod';
}

var package_json = fs.readFileSync('environment/docker/package_file/package.' + env + '.json', 'utf-8');
fs.writeFileSync('./package.json', package_json, 'utf-8');
console.log('>>> sync package.json complete.');

const jsonPackage = require('../../package.json');
var api_version = jsonPackage.version;
console.log('api_version : ', api_version);

var html_file = fs.readFileSync('environment/docker/html_file/index.' + env + '.html', 'utf-8');
html_file = html_file.replace('API_VERSION', api_version);
fs.writeFileSync('public/index.html', html_file, 'utf-8');
console.log('>>> sync index.html complete.');

var sh_file = fs.readFileSync('environment/docker/sh_file/docker_build.' + env + '.sh', 'utf-8');
sh_file = sh_file.replace('API_VERSION', api_version);
fs.writeFileSync('docker_build.sh', sh_file, 'utf-8');
console.log('>>> sync docker_build.sh complete.');

var yaml_file = fs.readFileSync('environment/docker/yaml_file/docker_deploy.' + env + '.yaml', 'utf-8');
yaml_file = yaml_file.replace('API_VERSION', api_version);
fs.writeFileSync('docker_deploy.yaml', yaml_file, 'utf-8');
console.log('>>> sync docker_deploy.yaml complete.');

var docker_file = fs.readFileSync('environment/docker/docker_file/Dockerfile.' + env, 'utf-8');
docker_file = docker_file.replace('API_VERSION', api_version);
fs.writeFileSync('Dockerfile', docker_file, 'utf-8');
console.log('>>> sync Dockerfile complete.');

console.log(' ------------------ end sync file ------------------ ');



