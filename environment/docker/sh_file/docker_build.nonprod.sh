#!/bin/bash
echo "Enter your build az or docker"
# read name
name=az

#  Container Name and Tag Config
REPO_NAME=slkpkt/th-picking-service-v5
TAG=vAPI_VERSION

#  Azure Configuration
ACR_NAME=cgacraksnonprd
USERNAME=4f2aeb05-706a-4956-84e0-6f65da252728
PASSWORD=fWn5fB_.II01Wis9C~97miGI9M-aOk7g3d
TENANT=central.co.th

if [[ $name == "az" ]]; then
  # AZ_BUILD
  echo "> AZ_BUILD"
  az login --service-principal --username $USERNAME --password $PASSWORD --tenant $TENANT

  az acr build --registry $ACR_NAME --image $REPO_NAME:$TAG --file Dockerfile .

elif [[ $name == "docker" ]]; then
  # DOCKER_BUILD
  echo "> DOCKER_BUILD"
  docker login $ACR_NAME.azurecr.io --username $USERNAME --password $PASSWORD

  docker build -f Dockerfile -t $ACR_NAME.azurecr.io/$REPO_NAME:$TAG .

  docker push $ACR_NAME.azurecr.io/$REPO_NAME:$TAG

  docker image rmi $ACR_NAME.azurecr.io/$REPO_NAME:$TAG
else
  echo "Sorry, try for the next time"
fi
