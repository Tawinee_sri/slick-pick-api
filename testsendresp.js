const axios = require('axios');

var config = {
    headers: {
      client_id: '743f8aacc60e4c87aa304e46bd004627',
      client_secret: '235847B0051149e2b80C8c54BC07A001',
      'content-type': 'application/json'
    }
};

var url = 'http://internal.uat.cgapi.central.co.th:8286/bs/slickfm/pickresp/1_0';

var data = `
{
    "pickResponse": {
        "pickRequestId": "SLICKPICKING000000000001",
        "pickResponseDate": "2019-09-27 13:50:09",
        "subOrderKey": "2",
        "orderKey": "3",
        "bu": "CDS",
        "status": "0002",
        "statusReason": "",
        "items": [
            {
                "itemKey": "2",
                "barcode": "10095737",
                "qty": 1,
                "qtyUnit": "pcs",
                "status": "0006",
                "statusReason": "",
                "statusDate": "2019-09-27 10:10:10",
                "pickRequestDate": "2019-09-20 18:31:34",
                "pickAllocationDate": "2019-09-20 18:31:34",
                "pickAssignDate": "2019-09-27 13:49:57",
                "pickedBy": "Fulfilment002",
                "pickBarcode": "10095737",
                "pickedDate": "2019-09-20 18:31:34",
                "packReceivedBy": "",
                "packedReceivedDate": "2019-09-27 13:50:09",
                "locId": "1234"
            }
        ]
    }
}
`;

axios.post(url, data, config)
  .then(function (response) {
  console.log('================= then response ==================');
    console.log(response);
    console.log('==================== end then response ========================');
  })
  .catch(function (error) {
     console.log('================= then response ==================');
    console.log(error);
    console.log('==================== end then response ========================');
  });