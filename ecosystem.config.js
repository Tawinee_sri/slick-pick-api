module.exports = {
  apps: [
    {
      name: "PickingAPI",
      script: "index.js",
      instances: 1,
      log_date_format: "HH:MM DD-MM-YYYY Z",
      out_file: "../pkt_log/pickingvn-log.log",
      error_file: "../pkt_log/pickingvn-error.log",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
