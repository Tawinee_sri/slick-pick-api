ALTER TABLE pr_items
    ADD gender VARCHAR(30)
    AFTER size;

ALTER TABLE pr_items
    ADD itemLocCode VARCHAR(30)
    AFTER gender;

ALTER TABLE pr_items
    ADD itemLocDesc VARCHAR(100)
    AFTER itemLocCode;

ALTER TABLE pr_items
    ADD itemAreaCode VARCHAR(30)
    AFTER itemLocDesc;

ALTER TABLE pr_items
    ADD itemAreaDesc VARCHAR(100)
    AFTER itemAreaCode;

alter table db_version rename column git_tag_version to api_version;
alter table db_version add column `app_version` varchar(20) null default null after `id`;
alter table db_version add column `deploy_date` timestamp null default null after `db_version`;

insert into db_version(app_version, api_version, db_version, deploy_date, created_time, is_run_q)
values('3.0.5.29', '1.0.1', '1.0.1', current_timestamp, current_timestamp, 'Y');

ALTER TABLE pr_items ADD realBrandCode VARCHAR(50) AFTER gender;
ALTER TABLE pr_items ADD brandCode VARCHAR(50) AFTER gender;


