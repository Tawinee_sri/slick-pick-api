alter table `pick_request` add column `channel` varchar(5) after statusReason;

DROP TABLE IF EXISTS pr_remark;
CREATE TABLE `pr_remark` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(500) NOT NULL,
  `messageType` varchar(4) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `pr_pick_params` add column `receiptPhotoTake` varchar(2) after locHandling;
alter table `pr_items` add column `weightedItem` varchar(2) after qtyUnit;

DROP TABLE IF EXISTS pr_remark_item;
CREATE TABLE `pr_remark_item` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(500) NOT NULL,
  `messageType` varchar(4) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
