set search_path to slick_picking;

DROP TABLE IF EXISTS m_display_col;
CREATE TABLE m_display_col (
  id SERIAL PRIMARY KEY,
  priority int NOT NULL,
  bu varchar(10) NOT NULL,
  loc varchar(10) NOT NULL,
  module varchar(50) NOT NULL,
  type varchar(50) NOT NULL,
  part varchar(1) DEFAULT NULL,
  col_header varchar(255) DEFAULT NULL,
  expand varchar(1) DEFAULT NULL,
  col_left varchar(255) DEFAULT NULL,
  col_right varchar(255) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS m_display_sf;
CREATE TABLE m_display_sf (
  id SERIAL PRIMARY KEY,
  priority int NOT NULL,
  bu varchar(10) NOT NULL,
  loc varchar(10) NOT NULL,
  module varchar(50) NOT NULL,
  col_name varchar(255) DEFAULT NULL,
  type varchar(10) NOT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);


DROP TABLE IF EXISTS m_display_limit;
CREATE TABLE m_display_limit (
  id SERIAL PRIMARY KEY,
  bu varchar(10) NOT NULL,
  loc varchar(10) NOT NULL,
  lm_task int NOT NULL,
  lm_order_h_co int DEFAULT NULL,
  lm_order_h_ex int DEFAULT NULL,
  lm_order_co_left int DEFAULT NULL,
  lm_order_co_right int DEFAULT NULL,
  lm_order_ex_left int DEFAULT NULL,
  lm_order_ex_right int DEFAULT NULL,
  lm_items int DEFAULT NULL,
  lm_filter int DEFAULT NULL,
  lm_search int DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

alter table ms_decline_reason add column reason_valuevn varchar(255) default null;

CREATE TYPE role_enum AS ENUM ('picker', 'suppicker');
alter table m_display_col add column role role_enum default 'picker';
alter table m_display_sf add column role role_enum default 'picker';

alter table fm_response_q add column env varchar(2) default 'vn';

alter table pr_remark_item add column itemkey varchar(255) default null;
