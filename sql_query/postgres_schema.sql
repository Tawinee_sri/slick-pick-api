-- slick_picking.alloc_process definition

-- Drop table

-- DROP TABLE slick_picking.alloc_process;

CREATE TABLE slick_picking.alloc_process (
	sourcebu varchar(5) NOT NULL,
	sourceloc varchar(10) NOT NULL,
	byorder varchar(1) NOT NULL,
	multiorder varchar(1) NOT NULL,
	wave varchar(1) NOT NULL,
	updated_time timestamp NULL DEFAULT now()
);


-- slick_picking.db_version definition

-- Drop table

-- DROP TABLE slick_picking.db_version;

CREATE TABLE slick_picking.db_version (
	id serial NOT NULL,
	app_version varchar(20) NULL DEFAULT NULL::character varying,
	api_version varchar(20) NULL DEFAULT 'NULL'::character varying,
	db_version varchar(20) NULL DEFAULT NULL::character varying,
	deploy_date timestamp NULL,
	created_time timestamp NULL,
	is_run_q varchar(1) NULL DEFAULT 'N'::character varying,
	updated_time timestamp NULL DEFAULT now(),
	CONSTRAINT db_version_pkey PRIMARY KEY (id)
);


-- slick_picking.error_log definition

-- Drop table

-- DROP TABLE slick_picking.error_log;

CREATE TABLE slick_picking.error_log (
	log_time timestamp NOT NULL DEFAULT now(),
	"method" varchar(30) NOT NULL,
	api_path varchar(100) NOT NULL,
	req_body text NULL,
	message text NOT NULL
);


-- slick_picking.fm_response_q definition

-- Drop table

-- DROP TABLE slick_picking.fm_response_q;

CREATE TABLE slick_picking.fm_response_q (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	picktaskid int4 NOT NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	env varchar(2) NULL DEFAULT 'vn'::character varying,
	CONSTRAINT fm_response_q_pkey PRIMARY KEY (id)
);


-- slick_picking.m_display_col definition

-- Drop table

-- DROP TABLE slick_picking.m_display_col;

CREATE TYPE role_enum AS ENUM ('picker', 'suppicker');
CREATE TABLE slick_picking.m_display_col (
	id serial NOT NULL,
	priority int4 NOT NULL,
	bu varchar(10) NOT NULL,
	loc varchar(10) NOT NULL,
	"module" varchar(50) NOT NULL,
	"type" varchar(50) NOT NULL,
	part varchar(1) NULL DEFAULT NULL::character varying,
	col_header varchar(255) NULL DEFAULT NULL::character varying,
	expand varchar(1) NULL DEFAULT NULL::character varying,
	col_left varchar(255) NULL DEFAULT NULL::character varying,
	col_right varchar(255) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	"role" role_enum NULL DEFAULT 'picker',
	CONSTRAINT m_display_col_pkey PRIMARY KEY (id)
);

-- slick_picking.m_display_limit definition

-- Drop table

-- DROP TABLE slick_picking.m_display_limit;

CREATE TABLE slick_picking.m_display_limit (
	id serial NOT NULL,
	bu varchar(10) NOT NULL,
	loc varchar(10) NOT NULL,
	lm_task int4 NOT NULL,
	lm_order_h_co int4 NULL,
	lm_order_h_ex int4 NULL,
	lm_order_co_left int4 NULL,
	lm_order_co_right int4 NULL,
	lm_order_ex_left int4 NULL,
	lm_order_ex_right int4 NULL,
	lm_items int4 NULL,
	lm_filter int4 NULL,
	lm_search int4 NULL,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	CONSTRAINT m_display_limit_pkey PRIMARY KEY (id)
);


-- slick_picking.m_display_sf definition

-- Drop table

-- DROP TABLE slick_picking.m_display_sf;

CREATE TABLE slick_picking.m_display_sf (
	id serial NOT NULL,
	priority int4 NOT NULL,
	bu varchar(10) NOT NULL,
	loc varchar(10) NOT NULL,
	"module" varchar(50) NOT NULL,
	col_name varchar(255) NULL DEFAULT NULL::character varying,
	"type" varchar(10) NOT NULL,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	"role" role_enum NULL DEFAULT 'picker',
	CONSTRAINT m_display_sf_pkey PRIMARY KEY (id)
);


-- slick_picking.ms_decline_reason definition

-- Drop table

-- DROP TABLE slick_picking.ms_decline_reason;

CREATE TABLE slick_picking.ms_decline_reason (
	id serial NOT NULL,
	priority int4 NULL,
	bu varchar(5) NOT NULL,
	sourcebu varchar(10) NOT NULL,
	reason_key varchar(255) NOT NULL,
	reason_valueth varchar(255) NULL,
	reason_valueen varchar(255) NULL,
	reason_valuevn varchar(255) NULL,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	CONSTRAINT ms_decline_reason_pkey PRIMARY KEY (id)
);


-- slick_picking.multiorder_seq definition

-- Drop table

-- DROP TABLE slick_picking.multiorder_seq;

CREATE TABLE slick_picking.multiorder_seq (
	id int4 NOT NULL
);


-- slick_picking.pick_request definition

-- Drop table

-- DROP TABLE slick_picking.pick_request;

CREATE TABLE slick_picking.pick_request (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickrequestdate timestamp NOT NULL,
	suborderkey varchar(100) NOT NULL,
	sourcesuborderid varchar(100) NOT NULL,
	orderkey varchar(100) NOT NULL,
	sourceorderid varchar(100) NOT NULL,
	bu varchar(5) NOT NULL,
	sourcebu varchar(5) NOT NULL,
	sourceloc varchar(10) NOT NULL,
	status varchar(4) NOT NULL,
	statusreason varchar(50) NULL DEFAULT NULL::character varying,
	channel varchar(5) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	fulltaxinvoice varchar(1) NULL DEFAULT NULL::character varying,
	giftwrapflag varchar(1) NULL DEFAULT NULL::character varying,
	CONSTRAINT pick_request_pkey PRIMARY KEY (id)
);


-- slick_picking.pick_task definition

-- Drop table

-- DROP TABLE slick_picking.pick_task;

CREATE TABLE slick_picking.pick_task (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickallocationtype varchar(4) NOT NULL,
	wave_id int4 NOT NULL,
	multiorder_id int4 NOT NULL,
	user_id varchar(30) NOT NULL,
	user_pack varchar(100) NULL DEFAULT NULL::character varying,
	bu varchar(5) NOT NULL,
	"role" varchar(30) NOT NULL,
	status varchar(30) NOT NULL,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	remark varchar(300) NULL DEFAULT NULL::character varying,
	CONSTRAINT pick_task_pkey PRIMARY KEY (id)
);


-- slick_picking.pick_task_detail definition

-- Drop table

-- DROP TABLE slick_picking.pick_task_detail;

CREATE TABLE slick_picking.pick_task_detail (
	id serial NOT NULL,
	picktaskid int4 NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	itemkey varchar(100) NOT NULL,
	pickbarcode varchar(30) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	pickitemid int4 NULL,
	CONSTRAINT pick_task_detail_pkey PRIMARY KEY (id)
);


-- slick_picking.pick_task_detail_item definition

-- Drop table

-- DROP TABLE slick_picking.pick_task_detail_item;

CREATE TABLE slick_picking.pick_task_detail_item (
	id serial NOT NULL,
	picktaskdetailid int4 NOT NULL,
	itemstatus varchar(30) NOT NULL,
	statusreason varchar(100) NULL DEFAULT NULL::character varying,
	pickbarcode varchar(30) NULL DEFAULT NULL::character varying,
	locbarcode varchar(100) NULL DEFAULT NULL::character varying,
	packreceivedate timestamp NULL,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	CONSTRAINT pick_task_detail_item_pkey PRIMARY KEY (id)
);


-- slick_picking.pick_users definition

-- Drop table

-- DROP TABLE slick_picking.pick_users;

CREATE TABLE slick_picking.pick_users (
	id serial NOT NULL,
	userid varchar(100) NULL DEFAULT NULL::character varying,
	cat_01 varchar(100) NULL DEFAULT NULL::character varying,
	cat_02 varchar(100) NOT NULL,
	cat_03 varchar(100) NOT NULL,
	cat_04 varchar(100) NOT NULL,
	cat_05 varchar(100) NOT NULL,
	cat_06 varchar(100) NOT NULL,
	"zone" varchar(100) NOT NULL,
	brand varchar(200) NOT NULL,
	remark text NOT NULL,
	bu varchar(50) NULL DEFAULT NULL::character varying,
	loc varchar(100) NULL DEFAULT NULL::character varying,
	status varchar(30) NULL DEFAULT NULL::character varying,
	CONSTRAINT pick_users_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_items definition

-- Drop table

-- DROP TABLE slick_picking.pr_items;

CREATE TABLE slick_picking.pr_items (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	itemkey varchar(100) NOT NULL,
	sourceitemid varchar(100) NOT NULL,
	sourceitemnumber int4 NULL,
	barcode varchar(30) NOT NULL,
	subbarcode varchar(1000) NULL DEFAULT NULL::character varying,
	sku varchar(350) NOT NULL,
	sourceprice numeric(10,2) NULL DEFAULT NULL::numeric,
	standardprice numeric(10,2) NULL DEFAULT NULL::numeric,
	currency varchar(10) NULL DEFAULT NULL::character varying,
	color varchar(30) NULL DEFAULT NULL::character varying,
	"size" varchar(30) NULL DEFAULT NULL::character varying,
	gender varchar(30) NULL DEFAULT NULL::character varying,
	brandcode varchar(50) NULL DEFAULT NULL::character varying,
	realbrandcode varchar(50) NULL DEFAULT NULL::character varying,
	itemloccode varchar(30) NULL DEFAULT NULL::character varying,
	itemlocdesc varchar(100) NULL DEFAULT NULL::character varying,
	itemareacode varchar(30) NULL DEFAULT NULL::character varying,
	itemareadesc varchar(100) NULL DEFAULT NULL::character varying,
	qty numeric(8,2) NOT NULL,
	qtyunit varchar(30) NOT NULL,
	actualweight numeric(10,4) NULL DEFAULT 0.0000,
	weighteditem varchar(2) NULL DEFAULT NULL::character varying,
	weight numeric(8,2) NULL DEFAULT NULL::numeric,
	weightunit varchar(30) NULL DEFAULT NULL::character varying,
	productnameth varchar(255) NOT NULL,
	productnameen varchar(255) NOT NULL,
	productnameit varchar(50) NULL DEFAULT NULL::character varying,
	productnamede varchar(50) NULL DEFAULT NULL::character varying,
	imageurl varchar(300) NULL DEFAULT NULL::character varying,
	locbarcode varchar(100) NULL DEFAULT NULL::character varying,
	cat1 varchar(15) NULL DEFAULT NULL::character varying,
	cat2 varchar(15) NULL DEFAULT NULL::character varying,
	cat3 varchar(15) NULL DEFAULT NULL::character varying,
	cat4 varchar(15) NULL DEFAULT NULL::character varying,
	cat5 varchar(15) NULL DEFAULT NULL::character varying,
	cat6 varchar(15) NULL DEFAULT NULL::character varying,
	brand varchar(50) NULL DEFAULT NULL::character varying,
	"zone" varchar(30) NULL DEFAULT NULL::character varying,
	status varchar(30) NOT NULL,
	statusreason varchar(100) NULL DEFAULT NULL::character varying,
	statusdate timestamp NOT NULL,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	maxbarcode varchar(30) NULL DEFAULT NULL::character varying,
	cataloginfo varchar(30) NULL DEFAULT NULL::character varying,
	CONSTRAINT pr_items_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_pick_params definition

-- Drop table

-- DROP TABLE slick_picking.pr_pick_params;

CREATE TABLE slick_picking.pr_pick_params (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickmode varchar(4) NOT NULL,
	receivingscan varchar(1) NOT NULL DEFAULT 'Y'::character varying,
	lochandling varchar(1) NOT NULL DEFAULT 'Y'::character varying,
	pickallflag varchar(1) NULL DEFAULT 'Y'::character varying,
	receiptphototake varchar(2) NULL DEFAULT NULL::character varying,
	pickassigntype varchar(4) NOT NULL,
	picksubassigntype varchar(4) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT pr_pick_params_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_pickparams_allocation definition

-- Drop table

-- DROP TABLE slick_picking.pr_pickparams_allocation;

CREATE TABLE slick_picking.pr_pickparams_allocation (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickmode varchar(4) NOT NULL,
	pickallocationtypeseq int4 NOT NULL,
	pickallocationtype varchar(4) NOT NULL,
	pickcatlvl varchar(4) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT pr_pickparams_allocation_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_pickparams_priority definition

-- Drop table

-- DROP TABLE slick_picking.pr_pickparams_priority;

CREATE TABLE slick_picking.pr_pickparams_priority (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickmode varchar(4) NOT NULL,
	pickprioritytype varchar(4) NOT NULL,
	pickprioritylevel varchar(4) NULL DEFAULT NULL::character varying,
	pickprioritytime timestamp NULL,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT pr_pickparams_priority_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_pickparams_priority_time definition

-- Drop table

-- DROP TABLE slick_picking.pr_pickparams_priority_time;

CREATE TABLE slick_picking.pr_pickparams_priority_time (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	pickprioritylevel varchar(4) NULL DEFAULT NULL::character varying,
	pickprioritytime timestamp NULL,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT pr_pickparams_priority_time_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_pickparams_priority_window definition

-- Drop table

-- DROP TABLE slick_picking.pr_pickparams_priority_window;

CREATE TABLE slick_picking.pr_pickparams_priority_window (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	starttime timestamp NULL,
	endtime timestamp NULL,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	slotid varchar(50) NULL DEFAULT NULL::character varying,
	CONSTRAINT pr_pickparams_priority_window_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_remark definition

-- Drop table

-- DROP TABLE slick_picking.pr_remark;

CREATE TABLE slick_picking.pr_remark (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	messagetype varchar(4) NULL DEFAULT NULL::character varying,
	message varchar(500) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	CONSTRAINT pr_remark_pkey PRIMARY KEY (id)
);


-- slick_picking.pr_remark_item definition

-- Drop table

-- DROP TABLE slick_picking.pr_remark_item;

CREATE TABLE slick_picking.pr_remark_item (
	id serial NOT NULL,
	pickrequestid varchar(100) NOT NULL,
	messagetype varchar(4) NULL DEFAULT NULL::character varying,
	message varchar(500) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NULL DEFAULT now(),
	itemkey varchar(255) NULL DEFAULT NULL::character varying,
	CONSTRAINT pr_remark_item_pkey PRIMARY KEY (id)
);


-- slick_picking.users definition

-- Drop table

-- DROP TABLE slick_picking.users;

CREATE TABLE slick_picking.users (
	id serial NOT NULL,
	userid varchar(100) NULL DEFAULT NULL::character varying,
	cat_01 varchar(100) NULL DEFAULT NULL::character varying,
	cat_02 varchar(100) NOT NULL,
	cat_03 varchar(100) NOT NULL,
	cat_04 varchar(100) NOT NULL,
	cat_05 varchar(100) NOT NULL,
	cat_06 varchar(100) NOT NULL,
	"zone" varchar(100) NOT NULL,
	brand varchar(100) NOT NULL,
	remark text NOT NULL,
	bu varchar(50) NULL DEFAULT NULL::character varying,
	loc varchar(100) NULL DEFAULT NULL::character varying,
	status varchar(30) NULL DEFAULT NULL::character varying,
	created_time timestamp NULL,
	updated_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);


-- slick_picking.wave_seq definition

-- Drop table

-- DROP TABLE slick_picking.wave_seq;

CREATE TABLE slick_picking.wave_seq (
	id int4 NOT NULL
);
