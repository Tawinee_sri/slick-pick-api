set search_path to slick_picking;

DROP TABLE IF EXISTS alloc_process;
CREATE TABLE alloc_process (
  sourceBU varchar(5) NOT NULL,
  sourceLoc varchar(10) NOT NULL,
  byOrder varchar(1) NOT NULL,
  multiOrder varchar(1) NOT NULL,
  wave varchar(1) NOT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS db_version;
CREATE TABLE db_version (
  id SERIAL PRIMARY KEY,
  app_version varchar(20) DEFAULT NULL,
  api_version varchar(20) DEFAULT 'NULL',
  db_version varchar(20) DEFAULT NULL,
  deploy_date timestamp NULL DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  is_run_q varchar(1) DEFAULT 'N',
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS error_log;
CREATE TABLE error_log (
  log_time timestamp NOT NULL DEFAULT NOW(),
  method varchar(30) NOT NULL,
  api_path varchar(100) NOT NULL,
  req_body text,
  message text NOT NULL
);

DROP TABLE IF EXISTS fm_response_q;
CREATE TABLE fm_response_q (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickTaskId int NOT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS ms_decline_reason;
CREATE TABLE ms_decline_reason (
  id SERIAL PRIMARY KEY,
  priority int DEFAULT NULL,
  bu varchar(5) NOT NULL,
  sourceBU varchar(10) NOT NULL,
  reason_key varchar(255) NOT NULL,
  reason_valueTH varchar(255) NOT NULL,
  reason_valueEN varchar(255) NOT NULL,
  reason_valueVN varchar(255) NOT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS multiorder_seq;
CREATE TABLE multiorder_seq (
  id int NOT NULL
);

DROP TABLE IF EXISTS pick_request;
CREATE TABLE pick_request (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickRequestDate timestamp NOT NULL,
  subOrderKey varchar(100) NOT NULL,
  sourceSubOrderId varchar(100) NOT NULL,
  orderKey varchar(100) NOT NULL,
  sourceOrderId varchar(100) NOT NULL,
  bu varchar(5) NOT NULL,
  sourceBU varchar(5) NOT NULL,
  sourceLoc varchar(10) NOT NULL,
  status varchar(4) NOT NULL,
  statusReason varchar(50) DEFAULT NULL,
  channel varchar(5) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pick_task;
CREATE TABLE pick_task (
  ID SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickAllocationType varchar(4) NOT NULL,
  wave_id int NOT NULL,
  multiorder_id int NOT NULL,
  user_id varchar(30) NOT NULL,
  user_pack varchar(100) DEFAULT NULL,
  bu varchar(5) NOT NULL,
  role varchar(30) NOT NULL,
  status varchar(30) NOT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW(),
  remark varchar(300) DEFAULT NULL
);

DROP TABLE IF EXISTS pick_task_detail;
CREATE TABLE pick_task_detail (
  ID SERIAL PRIMARY KEY,
  pickTaskId int NOT NULL,
  pickRequestId varchar(100) NOT NULL,
  itemKey varchar(100) NOT NULL,
  pickBarcode varchar(30) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pick_task_detail_item;
CREATE TABLE pick_task_detail_item (
  id SERIAL PRIMARY KEY,
  pickTaskDetailId int NOT NULL,
  itemStatus varchar(30) NOT NULL,
  statusReason varchar(100) DEFAULT NULL,
  pickBarcode varchar(30) DEFAULT NULL,
  locBarcode varchar(100) DEFAULT NULL,
  packReceiveDate timestamp NULL DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pick_users;
CREATE TABLE pick_users (
  id SERIAL PRIMARY KEY,
  userId varchar(100) DEFAULT NULL,
  cat_01 varchar(100) DEFAULT NULL,
  cat_02 varchar(100) NOT NULL,
  cat_03 varchar(100) NOT NULL,
  cat_04 varchar(100) NOT NULL,
  cat_05 varchar(100) NOT NULL,
  cat_06 varchar(100) NOT NULL,
  zone varchar(100) NOT NULL,
  brand varchar(200) NOT NULL,
  remark text NOT NULL,
  bu varchar(50) DEFAULT NULL,
  loc varchar(100) DEFAULT NULL,
  status varchar(30) DEFAULT NULL
);

DROP TABLE IF EXISTS pr_items;
CREATE TABLE pr_items (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  itemKey varchar(100) NOT NULL,
  sourceItemId varchar(100) NOT NULL,
  sourceItemNumber int DEFAULT NULL,
  barcode varchar(30) NOT NULL,
  subbarcode varchar(1000) DEFAULT NULL,
  sku varchar(350) NOT NULL,
  sourcePrice decimal(10,2) DEFAULT NULL,
  standardPrice decimal(10,2) DEFAULT NULL,
  currency varchar(10) DEFAULT NULL,
  color varchar(30) DEFAULT NULL,
  size varchar(30) DEFAULT NULL,
  gender varchar(30) DEFAULT NULL,
  brandCode varchar(50) DEFAULT NULL,
  realBrandCode varchar(50) DEFAULT NULL,
  itemLocCode varchar(30) DEFAULT NULL,
  itemLocDesc varchar(100) DEFAULT NULL,
  itemAreaCode varchar(30) DEFAULT NULL,
  itemAreaDesc varchar(100) DEFAULT NULL,
  qty decimal(8,2) NOT NULL,
  qtyUnit varchar(30) NOT NULL,
  actualWeight decimal(10,4) DEFAULT '0.0000',
  weightedItem varchar(2) DEFAULT NULL,
  weight decimal(8,2) DEFAULT NULL,
  weightUnit varchar(30) DEFAULT NULL,
  productNameTH varchar(255) NOT NULL,
  productNameEN varchar(255) NOT NULL,
  productNameIT varchar(50) DEFAULT NULL,
  productNameDE varchar(50) DEFAULT NULL,
  imageUrl varchar(300) DEFAULT NULL,
  locBarcode varchar(100) DEFAULT NULL,
  cat1 varchar(15) DEFAULT NULL,
  cat2 varchar(15) DEFAULT NULL,
  cat3 varchar(15) DEFAULT NULL,
  cat4 varchar(15) DEFAULT NULL,
  cat5 varchar(15) DEFAULT NULL,
  cat6 varchar(15) DEFAULT NULL,
  brand varchar(50) DEFAULT NULL,
  zone varchar(30) DEFAULT NULL,
  status varchar(30) NOT NULL,
  statusReason varchar(100) DEFAULT NULL,
  statusDate timestamp NOT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_pick_params;
CREATE TABLE pr_pick_params (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickMode varchar(4) NOT NULL,
  receivingScan varchar(1) NOT NULL DEFAULT 'Y',
  locHandling varchar(1) NOT NULL DEFAULT 'Y',
  pickAllFlag varchar(1) DEFAULT 'Y',
  receiptPhotoTake varchar(2) DEFAULT NULL,
  pickAssignType varchar(4) NOT NULL,
  pickSubAssignType varchar(4) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_pickparams_allocation;
CREATE TABLE pr_pickparams_allocation (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickMode varchar(4) NOT NULL,
  pickAllocationTypeSeq int NOT NULL,
  pickAllocationType varchar(4) NOT NULL,
  pickCatLvl varchar(4) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_pickparams_priority;
CREATE TABLE pr_pickparams_priority (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickMode varchar(4) NOT NULL,
  pickPriorityType varchar(4) NOT NULL,
  pickPriorityLevel varchar(4) DEFAULT NULL,
  pickPriorityTime timestamp DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_pickparams_priority_time;
CREATE TABLE pr_pickparams_priority_time (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  pickPriorityLevel varchar(4) DEFAULT NULL,
  pickPriorityTime timestamp DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
 );

DROP TABLE IF EXISTS pr_pickparams_priority_window;
CREATE TABLE pr_pickparams_priority_window (
  id SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  startTime timestamp DEFAULT NULL,
  endTime timestamp DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_remark;
CREATE TABLE pr_remark (
  ID SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  messageType varchar(4) DEFAULT NULL,
  message varchar(500) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS pr_remark_item;
CREATE TABLE pr_remark_item (
  ID SERIAL PRIMARY KEY,
  pickRequestId varchar(100) NOT NULL,
  messageType varchar(4) DEFAULT NULL,
  message varchar(500) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  userId varchar(100) DEFAULT NULL,
  cat_01 varchar(100) DEFAULT NULL,
  cat_02 varchar(100) NOT NULL,
  cat_03 varchar(100) NOT NULL,
  cat_04 varchar(100) NOT NULL,
  cat_05 varchar(100) NOT NULL,
  cat_06 varchar(100) NOT NULL,
  zone varchar(100) NOT NULL,
  brand varchar(100) NOT NULL,
  remark text NOT NULL,
  bu varchar(50) DEFAULT NULL,
  loc varchar(100) DEFAULT NULL,
  status varchar(30) DEFAULT NULL,
  created_time timestamp NULL DEFAULT NULL,
  updated_time timestamp NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS wave_seq;
CREATE TABLE wave_seq (
  id int NOT NULL
);
