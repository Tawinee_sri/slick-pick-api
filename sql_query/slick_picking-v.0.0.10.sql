alter table `pick_task` add column `multiorder_id` int(11) not null after wave_id;

create table `multiorder_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into multiorder_seq values (1);
