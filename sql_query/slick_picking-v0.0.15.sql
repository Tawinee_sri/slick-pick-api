DROP TABLE IF EXISTS alloc_process;
CREATE TABLE `alloc_process` (
  `sourceBU` varchar(5) NOT NULL,
  `sourceLoc` varchar(10) NOT NULL,
  `byOrder` varchar(1) NOT NULL,
  `multiOrder` varchar(1) NOT NULL,
  `wave` varchar(1) NOT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table pr_items add column actualWeight decimal(10,4) default 0 after qtyUnit;

DROP TABLE IF EXISTS ms_decline_reason;
CREATE TABLE `ms_decline_reason` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bu` varchar(5) NOT NULL,
  `sourceBU` varchar(10) NOT NULL,
  `reason_key` varchar(255) NOT NULL,
  `reason_valueTH` varchar(255) NOT NULL,
  `reason_valueEN` varchar(255) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table ms_decline_reason add column `priority` int(10) null default null after `id`;

insert into ms_decline_reason(priority, bu, sourceBU, reason_key, reason_valueTH, reason_valueEN, created_time)
values(1, 'CDS', 'CDS', 'DamagedItem', 'สินค้าชำรุด', 'Damaged Item', current_timestamp()),
(2, 'CDS', 'CDS', 'DemoItem', 'สินค้าตัวโชว์', 'Demo Item', current_timestamp()),
(3, 'CDS', 'CDS', 'Clearance', 'ไม่มีสินค้า - สินค้าล้างสต๊อค', 'Out of Stock - Clearance', current_timestamp()),
(4, 'CDS', 'CDS', 'OutOfStock', 'ไม่มีสินค้า', 'Out of Stock', current_timestamp()),
(5, 'CDS', 'CDS', 'WrongStockAdjust', 'สต๊อคไม่ถูกต้อง - ต้องปรับแก้', 'Wrong Stock - Adjust', current_timestamp()),
(6, 'CDS', 'CDS', 'WrongStockRTV', 'สต๊อคไม่ถูกต้อง - รอ RTV', 'Wrong Stock - RTv', current_timestamp()),
(7, 'CDS', 'CDS', 'cancelUnfoundedItem', 'ยกเลิกการระบุเหตุผล', 'Cancel select reason', current_timestamp());
