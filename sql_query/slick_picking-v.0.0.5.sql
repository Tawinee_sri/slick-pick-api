DROP TABLE IF EXISTS pr_pickparams_priority_time;
CREATE TABLE `slick_picking`.`pr_pickparams_priority_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickPriorityLevel` varchar(4) NULL DEFAULT NULL,
  `pickPriorityTime` datetime NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pr_pickparams_priority_window;
CREATE TABLE `slick_picking`.`pr_pickparams_priority_window` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `startTime` datetime NULL DEFAULT NULL,
  `endTime` datetime NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

alter table pick_task add column user_pack varchar(100) after user_id;
