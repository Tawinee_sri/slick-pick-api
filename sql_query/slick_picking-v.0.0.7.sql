ALTER TABLE `pick_request` MODIFY `subOrderKey` VARCHAR(300) NOT NULL;
ALTER TABLE `pick_request` MODIFY `sourceSubOrderId` VARCHAR(300) NOT NULL;
ALTER TABLE `pick_request` MODIFY `orderKey` VARCHAR(300) NOT NULL;
ALTER TABLE `pick_request` MODIFY `sourceOrderId` VARCHAR(300) NOT NULL;

ALTER TABLE `pr_items` MODIFY `itemKey` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_items` MODIFY `sourceItemId` VARCHAR(300) NOT NULL;

ALTER TABLE `pick_task_detail` MODIFY `itemKey` VARCHAR(300) NOT NULL;
