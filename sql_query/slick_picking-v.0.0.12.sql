CREATE TABLE `fm_response_q` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(300) NOT NULL,
  `pickTaskId` int(10) NOT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
