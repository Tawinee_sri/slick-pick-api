CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(300) DEFAULT NULL,
  `cat_01` varchar(300) DEFAULT NULL,
  `cat_02` varchar(300) NOT NULL,
  `cat_03` varchar(300) NOT NULL,
  `cat_04` varchar(300) NOT NULL,
  `cat_05` varchar(300) NOT NULL,
  `cat_06` varchar(300) NOT NULL,
  `zone` varchar(300) NOT NULL,
  `brand` varchar(1000) NOT NULL,
  `remark` text NOT NULL,
  `bu` varchar(50) DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
