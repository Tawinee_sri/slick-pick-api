set search_path to slick_picking;

alter table pick_task_detail add column pickitemid int default null;

alter table pick_request add column fullTaxInvoice varchar(1) default null;
alter table pick_request add column giftWrapFlag varchar(1) default null;

alter table pr_pickparams_priority_window add column slotId varchar(50) default null;

alter table pr_items add column maxBarcode varchar(30) default null;
alter table pr_items add column catalogInfo varchar(30) default null;
alter table pr_items add column realbrandcode varchar(50) default null;
alter table pr_items add column brandcode varchar(50) default null;

alter table pr_remark_item add column itemkey varchar(255) default null;

alter table pick_request add column deliveryType varchar(5) default null;
alter table pick_request add column deliverySubType varchar(5) default null;

