create table `wave_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into wave_seq values (1);

alter table pick_task add column wave_id int(11) not null after pickAllocationType;
