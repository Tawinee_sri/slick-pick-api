set search_path to slick_picking;

select * from pick_task_detail ptd order by ptd.id asc;

update pick_task_detail as t set
    pickitemid = c.id
from (
	select p.id, p.itemkey, p.pickrequestid from pr_items p
) as c(id, itemkey, pickrequestid)
where c.pickrequestid = t.pickrequestid and c.itemkey = t.itemkey
