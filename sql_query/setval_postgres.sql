SELECT setval('db_version_id_seq', max(id)) FROM db_version;
SELECT setval('fm_response_q_id_seq', max(id)) FROM fm_response_q;
SELECT setval('ms_decline_reason_id_seq', max(id)) FROM ms_decline_reason;
SELECT setval('pick_request_id_seq', max(id)) FROM pick_request;
SELECT setval('pick_task_id_seq', max(id)) FROM pick_task;
SELECT setval('pick_task_detail_id_seq', max(id)) FROM pick_task_detail;
SELECT setval('pick_task_detail_item_id_seq', max(id)) FROM pick_task_detail_item;
SELECT setval('pr_items_id_seq', max(id)) FROM pr_items;
SELECT setval('pr_pick_params_id_seq', max(id)) FROM pr_pick_params;
SELECT setval('pr_pickparams_allocation_id_seq', max(id)) FROM pr_pickparams_allocation;
SELECT setval('pr_pickparams_priority_id_seq', max(id)) FROM pr_pickparams_priority;
SELECT setval('pr_pickparams_priority_time_id_seq', max(id)) FROM pr_pickparams_priority_time;
SELECT setval('pr_pickparams_priority_window_id_seq', max(id)) FROM pr_pickparams_priority_window;
SELECT setval('pr_remark_id_seq', max(id)) FROM pr_remark;
SELECT setval('pr_remark_item_id_seq', max(id)) FROM pr_remark_item;
SELECT setval('users_id_seq', max(id)) FROM users;
