DROP TABLE IF EXISTS db_version;
CREATE TABLE `slick_picking`.`db_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `git_tag_version` varchar(20) DEFAULT 'NULL',
  `db_version` varchar(20) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO db_version (id, git_tag_version, db_version, created_time) VALUES
(1, '0.0.1', '0.0.1', sysdate());

DROP TABLE IF EXISTS pick_request;
CREATE TABLE `slick_picking`.`pick_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickRequestDate` datetime NOT NULL,
  `subOrderKey` varchar(30) NOT NULL,
  `sourceSubOrderId` varchar(30) NOT NULL,
  `orderKey` varchar(30) NOT NULL,
  `sourceOrderId` varchar(30) NOT NULL,
  `bu` varchar(5) NOT NULL,
  `status` varchar(4) NOT NULL,
  `statusReason` varchar(50) NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pr_pick_params;
CREATE TABLE `slick_picking`.`pr_pick_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickMode` varchar(4) NOT NULL,
  `pickAssignType` varchar(4) NOT NULL,
  `pickSubAssignType` varchar(4) NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pr_pickparams_allocation;
CREATE TABLE `slick_picking`.`pr_pickparams_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickMode` varchar(4) NOT NULL,
  `pickAllocationTypeSeq` int(10) NOT NULL,
  `pickAllocationType` varchar(4) NOT NULL,
  `pickCatLvl` varchar(4) NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pr_pickparams_priority;
CREATE TABLE `slick_picking`.`pr_pickparams_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickMode` varchar(4) NOT NULL,
  `pickPriorityType` varchar(4) NOT NULL,
  `pickPriorityLevel` varchar(4) NULL DEFAULT NULL,
  `pickPriorityTime` datetime NULL DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pr_items;
CREATE TABLE `slick_picking`.`pr_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `itemKey` varchar(30) NOT NULL,
  `sourceItemId` varchar(30) NOT NULL,
  `sourceItemNumber` int(3) NULL DEFAULT NULL,
  `barcode` varchar(30) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` double(8,2) NOT NULL,
  `qtyUnit` varchar(30) NOT NULL,
  `productNameTH` varchar(50) NOT NULL,
  `productNameEN` varchar(50) NOT NULL,
  `imageUrl` varchar(100) NULL DEFAULT NULL,
  `locBarcode` varchar(100) NULL DEFAULT NULL,
  `cat1` varchar(15) DEFAULT NULL,
  `cat2` varchar(15) DEFAULT NULL,
  `cat3` varchar(15) DEFAULT NULL,
  `cat4` varchar(15) DEFAULT NULL,
  `cat5` varchar(15) DEFAULT NULL,
  `cat6` varchar(15) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `zone` varchar(30) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `statusReason` varchar(100) NULL DEFAULT NULL,
  `statusDate` datetime NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS pick_task;
CREATE TABLE `pick_task` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pickRequestId` varchar(30) NOT NULL,
  `pickAllocationType` varchar(4) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `bu` varchar(5) NOT NULL,
  `role` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS pick_task_detail;
CREATE TABLE `pick_task_detail` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pickTaskId` int(10) NOT NULL,
  `pickRequestId` varchar(30) NOT NULL,
  `itemKey` varchar(30) NOT NULL,
  `pickBarcode` varchar(30) NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS error_log;
create table `error_log` (
	`log_time` timestamp default current_timestamp,
	`method` varchar(30) not null,
	`api_path` varchar(100) not null,
	`req_body` text default null,
	`message` text not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS pick_task_detail_item;
create table `pick_task_detail_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickTaskDetailId` int(11) NOT NULL,
  `itemStatus` varchar(30) NOT NULL,
  `statusReason` varchar(100) NULL DEFAULT NULL,
  `pickBarcode` varchar(30) NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
