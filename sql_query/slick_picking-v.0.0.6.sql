ALTER TABLE `pick_request` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_pick_params` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_pickparams_allocation` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_pickparams_priority` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_items` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pick_task` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pick_task_detail` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_pickparams_priority_time` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;
ALTER TABLE `pr_pickparams_priority_window` MODIFY `pickRequestId` VARCHAR(300) NOT NULL;

ALTER TABLE `pr_items` MODIFY `productNameTH` VARCHAR(255) NOT NULL;
ALTER TABLE `pr_items` MODIFY `productNameEN` VARCHAR(255) NOT NULL;
