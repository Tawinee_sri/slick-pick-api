DROP TABLE IF EXISTS mock_user;
CREATE TABLE `slick_picking`.`mock_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bu` varchar(30) NOT NULL,
  `loc` varchar(30) NOT NULL,
  `userId` varchar(30) NOT NULL,
  `cat_01` varchar(30) NOT NULL,
  `cat_02` varchar(30) NOT NULL,
  `cat_03` varchar(30) NOT NULL,
  `cat_04` varchar(30) NOT NULL,
  `cat_05` varchar(30) NOT NULL,
  `cat_06` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00001','100','A','A','A','A','A');
INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00002','A','A','A','A','A','A');
INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00003','100','102','A','A','A','A');
INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00004','100','101','A','A','A','A');
INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00005','200','A','A','A','A','A');
INSERT INTO `slick_picking`.`mock_user` (`bu`,`loc`,`userId`,`cat_01`,`cat_02`,`cat_03`,`cat_04`,`cat_05`,`cat_06`) VALUES ('CDS','10116','00006','200','202','A','A','A','A');
