DROP TABLE IF EXISTS pick_task_detail;
CREATE TABLE `pick_task_detail` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pickTaskId` int(10) NOT NULL,
  `pickRequestId` varchar(30) NOT NULL,
  `itemKey` varchar(30) NOT NULL,
  `pickBarcode` varchar(30) NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS pick_task_detail_item;
create table `pick_task_detail_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickTaskDetailId` int(11) NOT NULL,
  `itemStatus` varchar(30) NOT NULL,
  `statusReason` varchar(100) NULL DEFAULT NULL,
  `pickBarcode` varchar(30) NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
