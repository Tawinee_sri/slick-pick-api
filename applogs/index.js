const winston = require('winston');

const mainlog = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: {
    timestamp: new Date()
  },
  transports: [
    new winston.transports.File({ filename: 'logs/error_' + getDateString() + '.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/info_' + getDateString() + '.log' })
  ]
});

mainlog.add(new winston.transports.Console({
  format: winston.format.simple()
}));


module.exports = mainlog;
