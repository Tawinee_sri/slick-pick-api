const application = require("./dist");
module.exports = application;

if (require.main === module) {
  // Run the application

  let env = process.env.ENV;
  console.log('env : ', env);

  if (!env) process.env.PORT = 8105;
  else if (env.toUpperCase().indexOf('VN') > -1) process.env.PORT = 8005;
  else if (env.toUpperCase().indexOf('RINA') > -1) process.env.PORT = 8205;

  const config = {
    rest: {
      port: +(process.env.PORT || 8105),
      host: process.env.HOST,
      openApiSpec: {
        // useful when used with OASGraph to locate your application
        setServersFromRequest: true
      },
      cors: {
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        preflightContinue: false,
        optionsSuccessStatus: 204,
        maxAge: 86400,
        credentials: false
      }
    }
  };

  application.main(config).catch(err => {
    console.error("Cannot start the application.", err);
    process.exit(1);
  });
}
