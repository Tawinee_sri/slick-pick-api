# Use base image node 8 on Alpine LInux
# FROM node:8-alpine
# FROM node:stretch-slim
FROM node:lts-stretch-slim

# Create a directory. -p to make sure parent dir exists
RUN mkdir -p /usr/src/app

# Set working directory in the container
WORKDIR /usr/src/app

# Copy all local files (source) into the container (destination)
COPY . .

# Install dependencies
RUN npm install loopback-cli
RUN npm install
# RUN npm install --save @loopback/rest-explorer

# # Expose port 8005
# EXPOSE 8005

# Set env variable
ENV ENV nonprod

RUN node ./src/bin/script.js && npm run build

# Expose port 8005
EXPOSE 8105

# Run the nodejs app
# ENTRYPOINT [ "npm", "run-script", "runtime" ]
# CMD [ "node", "." ]
CMD [ "node", "index.js" ]
